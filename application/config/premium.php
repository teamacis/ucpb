<?php

$config = [
	'dst'	=> [
		'individual' 	=> [
			'silver'	=> 0.75,
			'gold'		=> 2.50,
			'platinum'	=> 2.50
		],
		'group' 	=> [
			'silver'	=> 0.75,
			'gold'		=> 2.50,
			'platinum'	=> 2.50
		],
		'family' 	=> [
			'silver'	=> 2.50,
			'gold'		=> 2.50,
			'platinum'	=> 2.50
		]
	]
];