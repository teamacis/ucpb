<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

if (in_array(ENVIRONMENT, ['testing', 'testing_ip', 'development', 'development_ip'])) {
    define('TEST', TRUE);
}  else {
    define('TEST', FALSE);
}

if (in_array(ENVIRONMENT, ['development_live'])) {
    define('DP_MERCHANT', 'UCPBGEN');
    define('DP_SECRET_KEY', 'V7E3b@23');
    define('DP_PAYMENT_URL', 'http://test.dragonpay.ph/Pay.aspx');
} else {
    define('DP_MERCHANT', 'UCPBGEN');
    define('DP_SECRET_KEY', 'V7E3b@23');
    define('DP_PAYMENT_URL', 'http://test.dragonpay.ph/Pay.aspx');
}

define('OR_CR_FOLDER', './files/orcr/');
define('POLICIES_FOLDER', './files/policies/');
define('FORMAL_QUOTES_FOLDER', './files/formal_quotes/');

define('MOTOR_QUOTE_FILES_QUOTE_REQUEST', './files/motor_quote/quote_request/');
define('MOTOR_QUOTE_FILES_BANK_CERTIFICATE', './files/motor_quote/bank_certificate/');
define('MOTOR_QUOTE_FILES_INVOICE', './files/motor_quote/invoice/');
define('MOTOR_QUOTE_CONFIRMATION_OF_COVER', './files/motor_quote/confirmation_of_cover/');

define('NOT_REQUESTED', 0);
define('REQUESTED', 1);

/**
 * Motor Insurance Type
 */
define('COMPREHENSIVE', 1);
define('CTPL', 2);
define('COMPREHENSIVE_CTPL', 3);
define('COMPREHENSIVE_AON', 4);
define('COMPREHENSIVE_CTPL_AON', 5);

/**
 * User Type
 */
define('USER_TYPE_INDIVIDUAL', 1);
define('USER_TYPE_COMPANY', 2);

/**
 * Motor PC or CV
 */
define('PC', 'PC');
define('CV', 'CV');

/**
 * Quote Status
 */
define('STATUS_PENDING', 'PENDING');
define('STATUS_SAVED', 'SAVED');
define('STATUS_APPROVED', 'APPROVED');
define('STATUS_DISAPPROVED', 'DISAPPROVED');
define('STATUS_FOR_APPROVAL', 'FOR APPROVAL');
define('STATUS_ISSUED', 'ISSUED');
define('STATUS_ACTIVE', 'ACTIVE');
define('STATUS_EXPIRED', 'EXPIRED');
define('STATUS_COMPLETED', 'COMPLETED');

/**
 * Admin Links
 */
define('L_DASHBOARD', 'access/admin/dashboard');
define('L_TRANSACTIONS_MOTOR', 'access/admin/transactions/motor_insurance');

/**
 * Lines
 */
define('MOTOR', 'motor');
define('HEP', 'hep');
define('ITP', 'itp');

/**
 * User Types
 */
define('USER_CLIENT', 1);
define('USER_ADMIN', 2);
define('USER_AGENT', 3);

/**
 * User Status
 */
define('USER_ACTIVE', 1);
define('USER_INACTIVE', 0);

/**
 * Status Classes on Motor Quote Transaction List
 */
define('STATUS_CLASS', serialize(array(
    'APPROVED'     => 'text-success',
    'ACTIVE'       => 'text-success',
    'FOR APPROVAL' => 'text-info',
    'PENDING'      => 'text-warning',
    'DECLINED'     => 'text-error',
    'EXPIRED'      => 'muted'
)));

/**
 * Insurance Labels
 */
$insuranceLabels[COMPREHENSIVE]          = 'Comprehensive';
$insuranceLabels[CTPL]                   = 'CTPL';
$insuranceLabels[COMPREHENSIVE_CTPL]     = 'Comprehensive and CTPL';
$insuranceLabels[COMPREHENSIVE_AON]      = 'Comprehensive with AON';
$insuranceLabels[COMPREHENSIVE_CTPL_AON] = 'Comprehensive with AON and CTPL';

define('INSURANCE_LABELS', serialize($insuranceLabels));

/**
 * Total Rows for Pagination
 */
define('TOTAL_ROWS', 10);

define('AGE_NEEDS_APPROVAL', 60);

define('AGE_DOUBLE_AMOUNT', 70);

define('AGE_MEMBER_MINIMUM', 18);

/**
 * PURPOSE OF TRAVEL
 */
define('TRAVEL', serialize([
    'business'          => 'Business',
    'leisure'           => 'Leisure',
    'training-seminar'  => 'Training / Seminar',
    'others'            => 'Others',
]));

/**
 * PACKAGE
 */
define('PACKAGE', serialize([
    'platinum' => 'Platinum',
    'gold'     => 'Gold',
    'silver'   => 'Silver'
]));

/**
 * COVERAGE
 */
define('COVERAGE', serialize([
    'family'     => 'Family',
    'group'      => 'Group',
    'individual' => 'Individual',
    'others'     => 'Others'
]));

/**
 * COVERAGE
 */
define('COVERAGE_FAMILY', 'Family');
define('COVERAGE_GROUP', 'Group');
define('COVERAGE_INDIVIDUAL', 'Individual');
define('COVERAGE_OTHERS', 'Others');

/**
 * POLICIES FILTER
 */
define('POLICY_NOT_DELIVERED', 'not-delivered');
define('POLICY_DELIVERED', 'delivered');

define('ITP_TOTAL_MEMBERS_WITH_DISCOUNT', 11);
define('ITP_DISCOUNT', 0.1);

/* RELATIONS */
define('RELATIONS', serialize([
    'Parent'            => 'Parent',
    'Sibling'           => 'Sibling',
    'Spouse'            => 'Spouse',
    'Aunt'              => 'Aunt',
    'Uncle'             => 'Uncle',
    'Niece'             => 'Niece',
    'Nephew'            => 'Nephew',
    'Grand Parent'      => 'Grand Parent',     
    'Grand Child'       => 'Grand Child'
]));

define('IMMEDIATE_FAMILY', serialize([
    'Child'             => 'Child',
    'Spouse'            => 'Spouse'
]));

define('TIN','000-432-798-000');
define('BIR_PERMIT_NO','0612-116-00205-CBA/AR');

define('ITP_QUOTE_FILES', './files/itp_quote/');
define('ITP_QUOTE_FILES_INVOICE', './files/itp_quote/invoice/');
define('ITP_QUOTE_FILES_COVER', './files/itp_quote/confirmation_cover/');
define('ITP_QUOTE_FILES_APPLICATION', './files/itp_quote/application/');

define('CIVIL_STATUSES', serialize([
    ''          => '',
    'Single'    => 'Single',
    'Married'   => 'Married'
]));

define('API_KEY','3fda859ec5b64200c91884002374677f');

define('DATE_ISSUED','June 18, 2012');
define('INVOICE_SERIES','HO-0000000009 To HO-9999999999 ');

define('DOCUMENTS_DIRECTORY', './files/documents/');
define('PHOTOS_DIRECTORY', './files/photos/');

define('ITP_EMAIL', 'itp@ucpbgen.com');
define('ITP_EMAIL_NAME', 'ITP UCPBGEN');