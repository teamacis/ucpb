<?php

$config = [
   "Alien Certificate of Registration (ACR)" => "Alien Certificate of Registration (ACR)",
   "Driver's License"                        => "Driver's License",
   "GSIS"                                    => "GSIS",
   "OFW E-Card"                              => "OFW E-Card",
   "Passport"                                => "Passport",   
   "Philhealth"                              => "Philhealth",
   "Seafarer's Registratin Certificate"      => "Seafarer's Registratin Certificate",
   "Senior Citizen ID"                       => "Senior Citizen ID",
   "SSS"                                     => "SSS",
   "Unified ID"                              => "Unified ID",
   "Voter's ID"                              => "Voter's ID"
];

         
