<?php

$config = [
	STATUS_SAVED 			=> 'Saved',
	STATUS_FOR_APPROVAL 	=> 'For Approval',
	STATUS_APPROVED    		=> 'Approved',
	STATUS_DISAPPROVED  	=> 'Disapproved',
	STATUS_ISSUED			=> 'Issued',
    STATUS_ACTIVE       	=> 'Active',
    STATUS_EXPIRED      	=> 'Expired',
    STATUS_PENDING  		=> 'Abandoned',
];