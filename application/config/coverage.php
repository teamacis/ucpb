<?php

$config = [
	'silver' => [
		'accidental_death' => [
			'label'	=> 'Accidental Death / Disablement',
			'value'	=> 'US $ 10,000.00'
		],
		'accidental_burial_benefit' => [
			'label'	=> 'Accidental Burial Benefit',
			'value'	=> 'US $ 250.00'
		],
		'personal_liability' => [
			'label'	=> 'Personal Liability',
			'value'	=> 'US $ 1,000.00 <br /> (Deductible – US$ 100)'
		],
		'travel_assistant_services' => [
			[
				'label'	=> 'Medical expense (including Sabotage and Terrorism coverage) and hospitalization abroad ***',
				'value'	=> '20,000 US $'
			],
			[
				'label'	=> 'Transport or repatriation in case of illness or accident',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Emergency dental care',
				'value'	=> '200 US $'
			],
			[
				'label'	=> 'Repatriation of a family member traveling with the insured',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Repatriation of mortal remains',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Escort of dependent child',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Travel of one immediate family member',
				'value'	=> 'Travel cost plus up to 100/day maximum 1,000 US $'
			],
			[
				'label'	=> 'Emergency return home following death of a close family member',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Delivery of Medicines',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Relay of urgent messages',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Long distance medical information service',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Medical referral/appointment of local medical specialist',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Connection services',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Advance of bail bond',
				'value'	=> '1,000 US $'
			],
			[
				'label'	=> 'Trip Cancellation',
				'value'	=> '3,000'
			],
			[
				'label'	=> 'Reimbursement of Forfeited Holidays/Trip Curtailment',
				'value'	=> 'Up to 650 US $'
			],
			[
				'label'	=> 'Delayed Departure',
				'value'	=> '200 US $'
			],
			[
				'label'	=> 'Flight Misconnection',
				'value'	=> '100 US $'
			],
			[
				'label'	=> 'Baggage Delay ',
				'value'	=> '40 US $'
			],
			[
				'label'	=> 'Compensation for in-flight loss of checked-in baggage',
				'value'	=> 'Up to 1,200 US $ subject to limit 150 US $ for any item'
			],
			[
				'label'	=> 'Location and forwarding of baggage and personal effects',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Loss of Passport, Driving License, National Identity Card Abroad',
				'value'	=> '250 US $'
			]
		]
	],
	'gold'	=> [
		'accidental_death' => [
			'label'	=> 'Accidental Death / Disablement',
			'value'	=> 'US $ 25,000.00'
		],
		'accidental_burial_benefit' => [
			'label'	=> 'Accidental Burial Benefit',
			'value'	=> 'US $ 500.00'
		],
		'personal_liability' => [
			'label'	=> 'Personal Liability',
			'value'	=> 'US $ 10,000.00 <br /> (Deductible – US$ 1,000)'
		],
		'travel_assistant_services' => [
			[
				'label'	=> 'Medical expense (including Sabotage and Terrorism coverage) and hospitalization abroad ***',
				'value'	=> '45,000 US $'
			],
			[
				'label'	=> 'Transport or repatriation in case of illness or accident',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Emergency dental care',
				'value'	=> '200 US $'
			],
			[
				'label'	=> 'Repatriation of a family member traveling with the insured',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Repatriation of mortal remains',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Escort of dependent child',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Travel of one immediate family member',
				'value'	=> 'Travel cost plus up to 100/day maximum 1,000 US $'
			],
			[
				'label'	=> 'Emergency return home following death of a close family member',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Delivery of Medicines',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Relay of urgent messages',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Long distance medical information service',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Medical referral/appointment of local medical specialist',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Connection services',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Advance of bail bond',
				'value'	=> '1,000 US $'
			],
			[
				'label'	=> 'Trip Cancellation',
				'value'	=> '3,000 US $'
			],
			[
				'label'	=> 'Reimbursement of Forfeited Holidays/Trip Curtailment',
				'value'	=> 'Up to 650 US $'
			],
			[
				'label'	=> 'Delayed Departure',
				'value'	=> '200 US $'
			],
			[
				'label'	=> 'Flight Misconnection',
				'value'	=> '150 US $'
			],
			[
				'label'	=> 'Baggage Delay ',
				'value'	=> '90 US $'
			],
			[
				'label'	=> 'Compensation for in-flight loss of checked-in baggage',
				'value'	=> 'Up to 1,200 US $ subject to limit 150 US $ for any item'
			],
			[
				'label'	=> 'Location and forwarding of baggage and personal effects',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Loss of Passport, Driving License, National Identity Card Abroad',
				'value'	=> '250 US $'
			]
		]
	],
	'platinum'	=> [
		'accidental_death' => [
			'label'	=> 'Accidental Death / Disablement',
			'value'	=> 'US $50,000.00'
		],
		'accidental_burial_benefit' => [
			'label'	=> 'Accidental Burial Benefit',
			'value'	=> 'US $ 1,000.00'
		],
		'personal_liability' => [
			'label'	=> 'Personal Liability',
			'value'	=> 'US $ 20,000.00 <br /> (Deductible – US$ 2,000)'
		],
		'travel_assistant_services' => [
			[
				'label'	=> 'Medical expense (including Sabotage and Terrorism coverage) and hospitalization abroad ***',
				'value'	=> '50,000 US $'
			],
			[
				'label'	=> 'Transport or repatriation in case of illness or accident',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Emergency dental care',
				'value'	=> '200 US $'
			],
			[
				'label'	=> 'Repatriation of a family member traveling with the insured',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Repatriation of mortal remains',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Escort of dependent child',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Travel of one immediate family member',
				'value'	=> 'Travel cost plus up to 100/day maximum 1,000 US $'
			],
			[
				'label'	=> 'Emergency return home following death of a close family member',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Delivery of Medicines',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Relay of urgent messages',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Long distance medical information service',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Medical referral/appointment of local medical specialist',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Connection services',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Advance of bail bond',
				'value'	=> '1,000 US $'
			],
			[
				'label'	=> 'Trip Cancellation',
				'value'	=> '3,000 US $'
			],
			[
				'label'	=> 'Reimbursement of Forfeited Holidays/Trip Curtailment',
				'value'	=> 'Up to 750 US $'
			],
			[
				'label'	=> 'Delayed Departure',
				'value'	=> '200 US $'
			],
			[
				'label'	=> 'Flight Misconnection',
				'value'	=> '150 US $'
			],
			[
				'label'	=> 'Baggage Delay ',
				'value'	=> '100 US $'
			],
			[
				'label'	=> 'Compensation for in-flight loss of checked-in baggage',
				'value'	=> 'Up to 1,200 US $ subject to limit 150 US $ for any item'
			],
			[
				'label'	=> 'Location and forwarding of baggage and personal effects',
				'value'	=> 'Actual Expense'
			],
			[
				'label'	=> 'Loss of Passport, Driving License, National Identity Card Abroad',
				'value'	=> '250 US $'
			]
		]
	]
];