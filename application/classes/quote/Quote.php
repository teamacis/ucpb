<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quote {

    protected $ci;
    protected $header_data;
    protected $rules;

    public function __construct() {
        $this->ci =& get_instance();

        $this->boot();

        // Default Scripts
        $this->header_data = [
            'scripts_ie' => jsScripts([
                jsUrl('core/html5shiv.min.js'),
                jsUrl('core/respond.min.js')
            ]),
            'scripts' => jsScripts([
                jsUrl('core/jquery.min.js'),
                jsUrl('core/jquery-ui-1.11.4/jquery-ui.min.js'),
                jsUrl('core/bootstrap.min.js'),
                //jsUrl('core/bootstrap-datepicker.min.js'),
                //jsUrl('core/jquery.nicefileinput.js'),
                base_url('assets/bootstrap-fileinput/js/fileinput.min.js'),
                jsUrl('core/chosen.jquery.min.js'),
                jsUrl('core/jquery.alphanum.js'),
                jsUrl('core/moment.js'),
                jsUrl('common.js')
            ])
        ];

        $this->ci->load->library('template');

        // Loads the rules for this tab.
        $this->load_rules();

        // Runs the function depending on the method.
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->get();
        }
        else if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->post();
        }  
    }

    /**
     * Loads the view for the current tab.
     *
     * @param $view
     * @param $title
     * @param array $args
     */
    protected function render($view, $title, $args = []) {
        $this->load_tab_scripts();

        $this->header_data['title'] = $title;
        $this->ci->template->preload = TRUE;
        $this->ci->load->view('user/header', $this->header_data);
        $this->ci->template->load('itp_template', $view, $args);
        $this->ci->load->view('user/footer');
    }

    /**
     * Loads the RULES for the current tab.
     */
    protected function load_rules() {
        /**
         * If child's protected $hasRules = true
         */
        if(isset($this->hasRules) && $this->hasRules) {
            // Get the CHILD CLASS NAME that EXTENDS me
            $childClassName = get_called_class();

            // Name of the Rules to be included.
            $rulesClassName = $childClassName.'_rules';

            // Reflect the CHILD CLASS so we can get the path.
            $ruleReflection = new ReflectionClass(get_called_class());

            // Path of the RULES
            $directoryName  = str_replace('.php', '_rules.php', $ruleReflection->getFileName());

            // Check if FILE of the RULE exists.
            if(file_exists($directoryName)) {
                require_once($directoryName);
                $this->rules = new $rulesClassName();
            }
            else {
                dd("Cannot find $rulesClassName");
            }
        }
    }

    /**
     * Load Libraries, Model, etc.
     */
    protected function boot() {

    }

    /**
     * Adds a script for the current tab.
     *
     * @param $scripts
     */
    private function load_tab_scripts() {
        if(isset($this->scripts)) {
            $finalScripts = [];

            foreach($this->scripts as $script) {
                $finalScripts[] = jsUrl($script);
            }

            $this->header_data['scripts'] .= jsScripts($finalScripts);
        }
    }

}