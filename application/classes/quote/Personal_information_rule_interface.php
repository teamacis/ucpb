<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

interface Personal_information_rule_interface {

    public function rules();
    public function check_agent();
}