<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quote_rule {

    protected $ci;
    protected $post;
    protected $get;
    protected $args;

    public function __construct() {
        $this->ci =& get_instance();

        // Set variables from post
        $this->post = $this->ci->input->post();
        $this->get = $this->ci->input->get();

        // Set form validation settings
        $this->ci->form_validation->set_error_delimiters('<div class="error_msg"><p>', '</p></div>');
        $this->ci->form_validation->set_message('required', '{field} is required.');
    }

    public function validate($args = []) {
        $this->args = $args;

        // Set form validation rules
        $this->ci->form_validation->set_rules($this->rules());

        // Run Validation
        return $this->ci->form_validation->run();
    }

}