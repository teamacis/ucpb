<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller 
{

	public function __construct()
	{
		parent::__construct();
	}
}

class Admin_Controller extends MY_Controller
{
	public $header_data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model', 'admin');

		$scriptsIE = jsScripts(array(
            jsUrl('core/html5shiv.min.js'),
            jsUrl('core/respond.min.js')
        ));

        $scripts = jsScripts(array(
            jsUrl('core/jquery.min.js'),
            jsUrl('core/bootstrap.min.js'),
            jsUrl('core/chosen.jquery.min.js'),
            jsUrl('core/moment.js'),
            jsUrl('core/bootstrap-datepicker.min.js'),
            jsUrl('core/bootstrap-switch.min.js'),
            jsUrl('common.js')
        ));

        $this->header_data = array(
            'scripts'   => $scripts,
            'scriptsIE' => $scriptsIE
        );
	}

	public function render($content_view=NULL,$data=NULL,$title=NULL,$page_title=NULL)
	{
		$this->header_data['title'] = $title . " - " . $page_title;
        $this->load->view('admin/header', $this->header_data);
        $content = $this->load->view($content_view, $data, TRUE);
        $this->load->view('admin/template',["content" => $content, "title" => $title, "page_title" => $page_title]);
        $this->load->view('admin/footer');
	}
}

class Client_Controller extends MY_Controller
{
    public $header_data;
    public $user_pk;

    public function __construct()
    {
        parent::__construct();

        $scriptsIE = jsScripts(array(
            jsUrl('core/html5shiv.min.js'),
            jsUrl('core/respond.min.js')
        ));

        $scripts = jsScripts(array(
            jsUrl('core/jquery.min.js'),
            jsUrl('core/bootstrap.min.js'),
            jsUrl('common.js')
        ));

        $this->header_data = array(
            'scripts'   => $scripts,
            'scriptsIE' => $scriptsIE
        );

        $this->user_pk = $this->session->userdata('pk');
    }

    public function render($content_view=NULL,$data=NULL,$title=NULL,$page_title=NULL)
    {
        if ( is_null($page_title) ) {
            "Client";
        }

        if ( is_null($title) ) {
            $title = "Dashboard";
        }
        
        $this->header_data['title'] = $title . " - " . $page_title;
        $this->load->view('client/header', $this->header_data);
        $content = $this->load->view($content_view, $data, TRUE);
        $this->load->view('client/template',["content" => $content, "title" => $title, "page_title" => $page_title]);
        $this->load->view('client/footer');
    }
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */