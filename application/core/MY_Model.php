<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function _data_xss_clean($data=array()) {
		if(count($data) > 0) {
			foreach ($data as $key => $value) {
				$data[$key] = xss_clean($this->db->escape_str($value));
			}
		}
		return $data;
	}

	/**
	 * get function
	 *
	 * Main functionality for getting a data from database
	 *
	 * @param $table (str)
	 * @param $options (array) - fields|conditions|limit|order|join
	 * @return array
	 **/
	public function get ($table=NULL,$options=array())
	{
		if(!is_null($table)) {
			$fields = isset($options['fields']) ? $options['fields'] : "*";
			$where	= (isset($options['where']) AND is_array($options['where']) AND count($options['where']) > 0) ? implode(" AND ", $options['where']) : NULL;
			$limit  = isset($options['limit']) ? (int) $options['limit'] : NULL;
			$offset = isset($options['offset']) ? (int) $options['offset'] : 0;
			$join   = (isset($options['join']) AND is_array($options['join'])) ? implode(" ", $options['join']) : NULL;
			$order  = isset($options['order']) ? $options['order'] : NULL;
			$sort 	= isset($options['sort']) ? $options['sort'] : "ASC";
			$group  = isset($options['group']) ? $options['group'] : NULL;
			$having = isset($options['having']) ? $options['having'] : NULL;
			$return = (isset($options['array']) AND $options['array'] == TRUE) ? "array" : "object";
			$row    = (isset($options['row']) AND $options['row'] == TRUE) ? TRUE : FALSE;

			if(isset($options['row']) AND $options['row'] == TRUE) 
			{
				$sql = "SELECT {$fields}";
			}
			else 
			{
				$sql = "SELECT SQL_CALC_FOUND_ROWS {$fields}";
			}

			$sql .= " FROM {$table}";

			if(!is_null($join))
			{
				$sql .= " " . $join;
			}

			if(!is_null($where))
			{
				$sql .= " WHERE {$where}";
			}

			if(!is_null($order))
			{
				$sql .= " ORDER BY {$order} {$sort}";
			}

			if(!is_null($limit) AND $limit > 0)
			{
				$sql .= " LIMIT {$offset},{$limit}";
			}

			if(!is_null($group))
			{
				$sql .= " GROUP BY {$group}";
			}

			if(!is_null($having))
			{
				$sql .= " HAVING {$having}";
			}
         	
			$query = $this->db->query($sql);
          	
			if ($return == "array") 
			{
				if ($row)
				{
					return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
				}
				else
				{
                    $data = $query->result_array();
					$result = array(
						"count"	=> $this->db->query("SELECT FOUND_ROWS() AS total")->row()->total,
						"data"	=> $data
					);

					return $result;
				}
			}
			else
			{
				if ($row)
				{
					return ($query->num_rows() > 0) ? $query->row() : FALSE;
				}
				else
				{
                    $data = $query->result();
					$result = array(
						"count"	=> $this->db->query("SELECT FOUND_ROWS() AS total")->row()->total,
						"data"	=> $data
					);

					return $result;
				}
			}
		}
		
		return FALSE;
	}

	/*
    insert a data on table

    parameters:
        @table - table name
        @name_value_pairs - array of data that will insert 
                  with key is the field name

    return:
        insert id
    */
    public function insert($table=NULL,$name_value_pairs = array()){
        if(!is_null($table) AND count($name_value_pairs) > 0 AND $this->db->table_exists($table)){
            $this->db->insert($table,$this->_data_xss_clean($name_value_pairs));
    		return $this->db->insert_id();
    	}
    	return FALSE;
    }
	/*
    insert batch data on table

    parameters:
        @table - table name
        @name_value_pairs - array of data that will insert 
                  with key is the field name

    return:
        insert id
    */
    public function insert_batch($table=NULL,$name_value_pairs = array()){
        if(!is_null($table) AND count($name_value_pairs) > 0 AND $this->db->table_exists($table)){
            $this->db->insert_batch($table,$name_value_pairs);
    		return true;
    	}
    	return false;
    }
    /*
	update the data of the table

	parameters:
		@table - table name
		@name_value_pairs - array of updated data 
				  with key is the field name
		@conditions - use to find what data need to update

	Returns:
		affected rows
    */
    public function update($table=NULL, $name_value_pairs = array(),$conditions=array()){
    	if(!is_null($table) AND count($name_value_pairs) > 0 AND $this->db->table_exists($table)){
    		if($this->db->update($table,$this->_data_xss_clean($name_value_pairs),$conditions)) {
    			return TRUE;
    		}	
    	}
        return FALSE;
    }

    /*
	update the multiple row of data of the table

	parameters:
		@table - table name
		@name_value_pairs - array of updated data 
				  with key is the field name
		@conditions - use to find what data need to update

	Returns:
		affected rows
    */
    public function update_batch($table=NULL, $name_value_pairs = array(),$key=NULL){
    	if(!is_null($table) AND count($name_value_pairs) > 0 AND $this->db->table_exists($table) AND !is_null($key)){
    		if($this->db->update_batch($table,$name_value_pairs,$key)) {
    			return TRUE;
    		}	
    	}
        return FALSE;
    }

    /**
     * delete function
     *
     * @return void
     * @author ivan
     **/
    public function delete($table=NULL,$conditions=array(),$hard_delete=false)
    {
        if(!is_null($table) AND $this->db->table_exists($table)){
            if($hard_delete) {
                $this->db->delete($table,$conditions);
            }
            else {
            	if($this->db->field_exists("is_deleted",$table))
            	{
            		$this->db->update($table,array('is_deleted' => 1),$conditions);	
            	}
            }
            return TRUE;
        }
        return FALSE;
    }
}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */