<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('serverUrl()')) {
	function serverUrl() {
		return $_SERVER['SERVER_NAME'];
	}
}

if (!function_exists('cssUrl()')) {
	function cssUrl($css = '') {
		return base_url('assets/css/'.$css);
	}
}

if (!function_exists('cssLinks()')) {
	function cssLinks($href = array()) {
		$css = '';
		foreach ($href as $val) {
			$css .= '<link rel="stylesheet" href="'.$val.'">'."\n";
		}
		return $css;
	}
}

if (!function_exists('jsUrl()')) {
	function jsUrl($js = '') {
		return base_url('assets/js/'.$js);
	}
}

if (!function_exists('jsInline()')) {
    function jsInline($js = '') {
        return '<script type="text/javascript">'.$js.'</script>';
    }
}

if (!function_exists('jsScripts()')) {
	function jsScripts($src = array()) {
		$script = '';
		foreach ($src as $val) {
            if (substr($val, 1, 6) != 'script')
			    $script .= '<script src="'.$val.'"></script>'."\n";
            else
                $script .= $val."\n";
		}
		return $script;
	}
}

if (!function_exists('pluginsUrl()')) {
	function pluginsUrl($plugins = '') {
		return base_url('assets/plugins/'.$plugins);
	}
}

if (!function_exists('imgUrl()')) {
	function imgUrl($img = '') {
		return base_url('assets/images/'.$img);
	}
}

if (!function_exists('fileUrl()')) {
	function fileUrl($folder = '', $file = '') {
		return base_url("files/$folder".$file);
	}
}

if (!function_exists('dd()')) {
	function dd($args = '') {
		echo "<pre style='display:block;padding:9.5px;margin:0 0 10px;font-size:13px;line-height:1.42857143;color:#333;word-break:break-all;word-wrap:break-word;background-color:#f5f5f5;border:1px solid #ccc;border-radius:4px'>";
        var_dump($args);
		echo "</pre>";
		die();
	}
}

if (!function_exists('checkSession()')) {
	function checkSession($img = '') {
		$return = false;

		if (!isset($_SESSION['lastActivity'])) {
			$_SESSION['lastActivity'] = time();
		}

		if (time() - $_SESSION['lastActivity'] > 900) {
			$return = true;
		}

		$_SESSION['lastActivity'] = time();

		if ($return) {
			redirect("access/do_logout");
		}
	}
}

if (!function_exists('errorMsg()')) {
    function errorMsg($msg = "") {
        header("HTTP/1.1 400");
        echo $msg;
    }
}

if (!function_exists('recordLengthAlign()')) {
    function recordLengthAlign($pString = "", $pLenth = 0, $pAlign = "L") {
        $lStrLen = strlen($pString);
        if ($lStrLen > $pLenth) {

            $lNumSpaces = $pLenth - $lStrLen;
            return (substr($pString, 0, $lNumSpaces));
        } else {
            if ($lStrLen == $pLenth)
                return $pString;
            else {
                $lNumSpaces = $pLenth - $lStrLen;
                $lZeros = createZeros($lNumSpaces);
                if ($pAlign == "L") {
                    return ($pString.$lZeros);
                } else {
                    return ($lZeros.$pString);
                }
            }
        }
    }
}
if (!function_exists('createZeros()')) {
    function createZeros($pNumSpace) {
        $lString = "";
        for ($pI = 0; $pI < $pNumSpace; $pI++) {
            $lString .= "0";
        }
        return ($lString);
    }
}

if (!function_exists('generateRandomString()')) {
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

if (!function_exists('ordinal()')) {
    function ordinal($number) {
        $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
        if ((($number % 100) >= 11) && (($number % 100) <= 13))
            return $number . 'th';
        else
            return $number . $ends[$number % 10];
    }
}

if (!function_exists('convert_number_to_words()')) {
    function convert_number_to_words($number) {
        $hyphen = '-';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'fourty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int)($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int)($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string)$fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }
}

if (!function_exists('calculate_age()')) {
    function calculate_age($birthDate,$strDate=NULL) {
        $date = new DateTime($birthDate);
        if ( !is_null($strDate) ) {
            $now  = new DateTime($strDate);
        } else {
            $now  = new DateTime();
        }
        $interval = $now->diff($date);
        return $interval->y;
    }
}

if (!function_exists('subtract_days()')) {
    function subtract_days($start, $end) {
        $start = new DateTime($start);
        $end   = new DateTime($end);
        $interval = $end->diff($start);
        return $interval->d;
    }
}

if ( ! function_exists('convert_value') ) {
    function convert_value($amount=0,$currency_value=0) {
        return $amount * $currency_value;
    }
}

if ( ! function_exists('rprint'))
{
    function rprint($data=NULL)
    {
        if (!is_null($data))
        {
            echo "<pre>";
            print_r($data);
            echo "</pre>";
        }
        
        return FALSE;
    }
}