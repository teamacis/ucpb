<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Template {

    private $CI;

    public $preload = FALSE;
    function __construct()
    {
        $this->CI =& get_instance();
//        parent::__construct();
    }

    public function load($template, $view, $params = array()) {
        $this->$template($view, $params);
    }

    private function motor_template($view, $params = array()) {
        $this->CI->load->view('template/motor_header');
        $this->CI->load->view($view, $params);
        if ($this->preload != FALSE) {
            $this->CI->load->view('preload');
        }
        $this->CI->load->view('template/motor_footer');
    }

    private function itp_template($view, $params = array()) {
        $this->CI->load->view('template/itp/header');
        $this->CI->load->view($view, $params);
        if ($this->preload != FALSE) {
            $this->CI->load->view('preload');
        }
        $this->CI->load->view('template/itp/footer');
    }
}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */
