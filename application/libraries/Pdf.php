<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF {

    private   $ci;
    protected $pdf;
    protected $args = [
        'page_margin' => 10
    ];

    public function __construct($args = []) {
        $this->ci =& get_instance();
        //$this->ci->load->model('motor_quote_file', 'motor_quote_file');

        $this->_load_arguments($args);
        $this->_load_settings();
    }

    public function generate_pages($pages) {
        foreach($pages as $pageContent) {
            $this->pdf->AddPage();
            $this->pdf->writeHTML($pageContent, true, false, true, false, '');
        }
    }

    /**
     * Creates and Saves PDF
     *
     * @param $path
     * @param bool|false $createFile
     * @param bool|false $insertToDB
     */
    public function create($path, $createFile = false, $insertToDB = false) {
        if (!$createFile) {
            $this->pdf->Output($path, 'I');
        }
        else {
            $this->pdf->Output($path, 'F');

            if($insertToDB) {
                $this->ci->motor_quote_file->save($insertToDB);
            }
        }
    }

    /**
     * Update default arguments.
     * @param $args
     */
    private function _load_arguments($args) {
        foreach($args as $key=>$arg) {
            $this->args[$key] = $arg;
        }
    }

    /**
     * Load PDF Settings
     */
    private function _load_settings() {
        $this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // remove default header/footer
        $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(false);

        // set default monospaced font
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $this->pdf->SetMargins($this->args['page_margin'], PDF_MARGIN_RIGHT);

        // set auto page breaks
        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        $this->pdf->SetFont('helvetica', '', 10);
    }

}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */