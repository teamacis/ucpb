<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mortgagee_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_by_field($field, $value=false) {
        if($value) {
            $query = $this->db->where($field, $value)->get('mortgagees');

            if ($query->num_rows()) {
                return $query->row_object();
            }
            else {
                return '';
            }
        }

        return '';
    }

}
