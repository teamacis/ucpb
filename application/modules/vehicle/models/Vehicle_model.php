<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    /**
     * Fetches brands
     * @param $fields
     * @param $value
     * @return bool
     */
    public function get_brands_by_field($fields, $value) {
        $return = false;
        $this->db->order_by("brand");
        $this->db->where($fields, $value);
        $this->db->select('brand');
        $this->db->distinct();
        $this->db->from('motors');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        }
        return $return;
    }

    /**
     * Fetches filtered model
     * @param array $params
     * @return bool
     */
    public function get_filtered_model($params = array()) {
        $query = $this->db->limit(1)
            ->order_by("pk", "DESC")
            ->where("model_name",$params['model_name'])
            ->where("brand",$params['brand'])
            ->where("year",$params['year'])
            ->select('pk, body, pc_cv, model, model_name, year, brand, seating_capacity')
            ->from('motors')
            ->get();
        return $query->num_rows() > 0 ? $query->row_array() : FALSE;
    }







    public function brands($year = 0) {
        $return = false;
        $this->db->order_by("brand");
        $this->db->where("year",$year);
        $this->db->select('brand');
        $this->db->distinct();
        $this->db->from('motors');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        }
        return $return;
    }

    public function models($year = 0, $brand = '') {
        $return = false;
        $this->db->order_by("model_name");
        $this->db->where("brand", $brand);
        $this->db->where("year", $year);
        $this->db->where("sum_insured !=", "X");
        $this->db->where("sum_insured !=", "x");
        $this->db->select('pc_cv');
        $this->db->select('model_name');
        $this->db->select('pk');
        $this->db->from('motors');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        }
        return $return;
    }

    public function get_model($year = 0, $brand = '', $model_name = '') {
        $return = false;
        $this->db->where("model_name",$model_name);
        $this->db->where("brand",$brand);
        $this->db->where("year",$year);
        $this->db->select('seating_capacity');
        $this->db->select('sum_insured');
        $this->db->select('pc_cv');
        $this->db->select('pk');
        $this->db->from('motors');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        }
        return $return;
    }

	/**
	 * Fetches list of motor_insurance accessories
	 * @return bool
	 * @return object
	 */
	public function accessories() {
		$this->db->order_by("accessory");
		$this->db->select('accessory');
		$this->db->select('pk');
		$this->db->from('motor_accessories');
		$query = $this->db->get();
        return $query->result_array();
	}

	/**
	 * Fetches list of motor_insurance colors
	 * @return bool
	 * @return object
	 */
	public function colors() {
		$this->db->order_by("color");
		$this->db->select('color');
		$this->db->select('pk');
		$this->db->from('motor_colors');
		$query = $this->db->get();
        return $query->result_array();
	}

	/**
	 * Fetches data based from pk
	 * @param array $params
	 * @return bool
	 * @return object
	 */
    public function model($motor_pk) {
        $return = false;
        $this->db->order_by("model_name");
        $this->db->where("pk",$motor_pk);
        $this->db->select('seating_capacity');
        $this->db->select('sum_insured');
        $this->db->from('motors');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        }
        return $return;
    }

	/**
	 * Fetches data base from year, brand and model_name
	 * @param array $params
	 * @return bool
	 * @return object
	 */
	public function get_motor_for_computation($pk = null) {
		$return = false;
		$this->db->order_by("model_name");
		$this->db->where("pk", $pk);
		$this->db->select('seating_capacity');
		$this->db->select('sum_insured');
		$this->db->select('pc_cv');
        $this->db->select('year');
        $this->db->select('brand');
        $this->db->select('model_name');
		$this->db->from('motors');
		$query = $this->db->get();
		if ($query->num_rows()) {
			return $query->row_array();
		}
		return $return;
	}
}
