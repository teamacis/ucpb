<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('vehicle_model');
    }

    /**
     * Displays the list of brands from a given year parameter in object format
     */
    public function brands() {
        $year = $this->input->post('year');
        $brands = $this->vehicle_model->brands($year);
        if (!$brands) {
            echo json_encode([]);
        } else {
            echo json_encode($brands);
        }
    }

    /**
     * Displays the list of models from a given year and brand parameter in object format
     */
    public function models() {
        $year = $this->input->post('year');
        $brand = $this->input->post('brand');
        $models = $this->vehicle_model->models($year, $brand);
        if (!$models) {
            echo json_encode([]);
        } else {
            echo json_encode($models);
        }
    }

    /**
     * Displays the list of accessories in object format
     */
//    public function accessories() {
//        $query = $this->vehicle_model->accessories();
//        if (!$query) {
//            echo json_encode([]);
//        } else {
//            echo json_encode($query->result());
//        }
//    }

    /**
     * Displays the list of colors in object format
     */
//    public function colors() {
//        $query = $this->vehicle_model->colors();
//        if (!$query) {
//            echo json_encode([]);
//        } else {
//            echo json_encode($query->result());
//        }
//    }

    /**
     * Displays the information of a specific model from a given pk parameter in object format
     */
    public function model() {
        $motor_pk = $this->input->post('motor_pk');
        $model = $this->vehicle_model->model($motor_pk);
        if (!$model) {
            echo json_encode([]);
        } else {
            echo json_encode($model);
        }
    }
}
