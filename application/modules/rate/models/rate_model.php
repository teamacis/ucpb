<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rate_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_rate_by_rate_location_pk($rateLocationPk) {
        $query = $this->db->where('rate_location_pk', $rateLocationPk)
                          ->order_by('year DESC')
                          ->limit(1)
                          ->get('rates');

        if($query->num_rows() > 0) {
            return $query->row_object();
        }
        else {
            return false;
        }
    }
}