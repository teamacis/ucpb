<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_insurance extends MY_Controller {
    private $scripts_array_ie = array();
    private $scripts_array = array();

    public function __construct() {
        parent::__construct();

        $this->scripts_array_ie = array(
            jsUrl('core/html5shiv.min.js'),
            jsUrl('core/respond.min.js')
        );
        $this->scripts_array = array(
            jsUrl('core/jquery.min.js'),
            jsUrl('core/jquery-ui-1.11.4/jquery-ui.min.js'),
            jsUrl('core/bootstrap.min.js'),
           // jsUrl('core/bootstrap-datepicker.min.js'),
            jsUrl('core/moment.js'),
            jsUrl('common.js')
        );

        $this->load->library('template');
        $this->load->helper('string');
        $this->load->model('motor_model');
        $this->load->model('motor_quote_model');
        $this->load->model('motor_quote_information_model');
        $this->load->model('motor_quote_other_standard_model');
        $this->load->model('motor_quote_non_standard_model');
        $this->load->model('motor_quote_computation_model');
        $this->load->model('motor_action_model');
        $this->load->model('motor_rules_model');
        $this->load->model('motor_quote_agent_model');
        $this->load->model('place/place_model');
        $this->load->model('vehicle/vehicle_model');
        $this->load->model('client/client_model');
        $this->load->model('salutation/salutation_model');
        $this->load->model('suffix/suffix_model');
        $this->load->model('nationality/nationality_model');
        $this->load->model('occupation/occupation_model');
        $this->load->model('personal_information/personal_information_model');
        $this->load->model('personal_information/personal_information_rules_model');
        $this->load->model('coverage_details/coverage_details_model');
        $this->load->model('client/client_rules_model');
        $this->load->model('transaction/transaction_model');
        $this->load->model('motor_quotes_file/motor_quotes_file_model', 'motor_quote_file');
        $this->load->model('coverage_details/coverage_details_rules_model');
    }

    public function index() {
        $this->motor_model->update_allowed_motor_quote_tab(0, true);
        $this->session->set_userdata('motor_step', 0);
        $this->session->set_userdata('motor_quotes_pk', NULL);

        $this->form_validation->set_rules('confirm', 'Confirm', 'required');
        if ($this->form_validation->run() == FALSE) {
            if ($this->input->post('submit') != NULL) {
                $this->session->set_flashdata('error', 'Please tick the checkbox to confirm.');
                redirect('motor-insurance');
            }

            $scriptsIE = jsScripts($this->scripts_array_ie);
            $scripts = jsScripts($this->scripts_array);

            $header_data = array('title' => 'Motor Quote Intoduction Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
            $this->load->view('user/header', $header_data);
            $this->template->load('motor_template', 'index');
            $this->load->view('user/footer');
        } else {
            redirect('motor-insurance/get_quote');
        }
    }

    /**
     * Get Quote Page
     */
    public function get_quote($token = null) {
        $this->motor_model->update_allowed_motor_quote_tab(0, true);

        if (!is_numeric($this->session->userdata('motor_step'))) {
            redirect('motor-insurance');
        }
        $this->session->set_userdata('motor_step', 1);

        $this->form_validation->set_error_delimiters('<div class="error_msg"><p>', '</p></div>');
        $this->form_validation->set_message('required', '{field} is required.');

        if ($this->input->post('submit') == 'Save') {
            if ($this->input->post("contact_information")['email_address'] != '') {
                unset($_POST['submit']);
                $data = array_replace_recursive($this->motor_model->default_quote_values(), $this->input->post());

                $token = $this->motor_model->save_quote($data);
                $this->motor_model->send_saved_quote_mail($data);

                $this->session->set_userdata('saved', 'Your session has been saved. An email has been sent for you to click next time you want to re-stablish your session');
                redirect(site_url('motor-insurance/get_quote/' . $token));
            } else {
                $this->form_validation->set_rules("contact_information[email_address]", 'Email Address', 'required|max_length[50]');
                if ($this->form_validation->run() == FALSE) {
                    $data = $this->motor_model->initialize_get_quote_form();

                    $this->scripts_array[] = jsUrl('core/chosen.jquery.min.js');
                    $this->scripts_array[] = jsUrl('core/rangeslider.js');
                    $scriptsIE = jsScripts($this->scripts_array_ie);
                    $scripts = jsScripts($this->scripts_array);

                    $header_data = array('title' => 'Motor Get Quote Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
                    $this->load->view('user/header', $header_data);
                    $this->template->preload = TRUE;
                    $this->template->load('motor_template', 'get_quote', $data);
                    $this->load->view('user/footer');
                } else {
                    $this->motor_model->do_get_quote();
                }
            }
        } else {
            if (!$this->input->post()) {
                if ($token != NULL) {
                    $values = $this->motor_model->get_motor_saved_quote_values_by_token($token);
                    $_POST = json_decode($values, TRUE);
                }
            }
            $rules = $this->motor_rules_model->get_quote_rules();
            $this->form_validation->set_rules($rules);
            $this->form_validation->set_rules('contact_information[telephone]', 'Telephone', 'callback_telephone_check[' . $this->input->post('contact_information')['mobile'] . ']');

            if ($this->input->post('vehicle_information')['insurance_type'] != COMPREHENSIVE && $this->input->post('vehicle_information')['insurance_type'] != COMPREHENSIVE_AON) {
                $this->form_validation->set_rules('vehicle_information[is_new]', 'Is your car brand new', 'required|max_length[1]');
            }

            if (isset($this->input->post('vehicle_information')['model_name']) && $this->input->post('vehicle_information')['model_name'] == 'Other') {
                $this->form_validation->set_rules('vehicle_information[other_model]', 'Other model', 'required|max_length[250]');
            }

            if ($this->form_validation->run($this) == FALSE) {
                $data = $this->motor_model->initialize_get_quote_form();

                $this->scripts_array[] = jsUrl('core/chosen.jquery.min.js');
                $this->scripts_array[] = jsUrl('core/rangeslider.js');
                $scriptsIE = jsScripts($this->scripts_array_ie);
                $scripts = jsScripts($this->scripts_array);

                $header_data = array('title' => 'Motor Get Quote Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
                $this->load->view('user/header', $header_data);
                $this->template->preload = TRUE;
                $this->template->load('motor_template', 'get_quote', $data);
                $this->load->view('user/footer');
            } else {
                $this->motor_model->do_get_quote();
            }
        }
    }

    /**
     * Validates telephone and mobile
     * @param $telephone
     * @param $mobile
     * @return bool
     */
    public function telephone_check($telephone, $mobile) {
        if (empty($telephone) && empty($mobile)) {
            $this->form_validation->set_message('telephone_check', 'Either Telephone Number or Mobile Number is required.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Get back prompt page
     */
    public function get_back_prompt() {
        $this->session->set_userdata('motor_quotes_pk', null);
        $scriptsIE = jsScripts($this->scripts_array_ie);
        $scripts = jsScripts($this->scripts_array);
        $header_data = array('title' => 'Motor Quote Get Back Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
        $this->load->view('user/header', $header_data);
        $this->template->load('motor_template', 'get_back_prompt');
        $this->load->view('user/footer');
    }

    /**
     * Quote Summary page
     */
    public function quote_summary($token = null) {
        if ($token != NULL) {
            $this->motor_model->quote_tab_redirect_handler(1);
            $this->motor_model->update_allowed_motor_quote_tab(2);

            if (!is_numeric($this->session->userdata('motor_step'))) {
                redirect('motor-insurance');
            }
            $this->session->set_userdata('motor_step', 1);

            $scriptsIE = jsScripts($this->scripts_array_ie);
            $scripts = jsScripts($this->scripts_array);

            $data = array();
            $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($token), TRUE);
            $motor_info = $this->vehicle_model->get_motor_for_computation($motor_saved_quotes['motors_pk']);
            $quote_premiums = $this->motor_model->quote_premiums($motor_info, $motor_saved_quotes);
            $data = array_merge($motor_saved_quotes, $motor_info, $quote_premiums);

            $header_data = array('title' => 'Motor Quote Summary Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
            $this->load->view('user/header', $header_data);
            $this->template->load('motor_template', 'quote_summary', ['data' => (object)$data]);
            $this->load->view('user/footer');
            $this->motor_quote_model->generate_formal_quote($data['token'], true);
        } else {
            redirect("motor-insurance");
        }
    }

    public function generate_formal_quote($token) {
        $is_ctpl = ($this->uri->segment(4)) ? TRUE : FALSE;
        $this->motor_quote_model->generate_formal_quote($token, FALSE);
    }

    public function summary_confirmation($token) {
        if ($token != NULL) {
            $this->motor_model->quote_tab_redirect_handler(5);

            if (!is_numeric($this->session->userdata('motor_step'))) {
                redirect('motor-insurance');
            }
            $this->session->set_userdata('motor_step', 1);

            $scriptsIE = jsScripts($this->scripts_array_ie);
            $scripts = jsScripts($this->scripts_array);

            $data = array();
            $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($token), TRUE);
            $motor_info = $this->vehicle_model->get_motor_for_computation($motor_saved_quotes['motors_pk']);
            $quote_premiums = $this->motor_model->quote_premiums($motor_info, $motor_saved_quotes);
            $data = array_merge($motor_saved_quotes, $motor_info, $quote_premiums);

            $header_data = array('title' => 'Motor Summary Confirmation Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
            $this->load->view('user/header', $header_data);
            $this->template->load('motor_template', 'summary_confirmation', ['data' => (object)$data]);
            $this->load->view('user/footer');
            $this->motor_quote_model->generate_formal_quote($data['token'], true);
        } else {
            redirect("motor-insurance");
        }
    }

    /*
	|--------------------------------------------------------------------------
	| VEHICLE INFORMATION
	|--------------------------------------------------------------------------
	*/
    public function vehicle_information($token) {
        if ($token != NULL) {
            $this->motor_model->quote_tab_redirect_handler(2);

            if (!is_numeric($this->session->userdata('motor_step'))) {
                redirect('motor-insurance');
            }
            $this->session->set_userdata('motor_step', 2);

            $rules = $this->motor_rules_model->vehicle_information_rules();
            $this->form_validation->set_rules($rules);

            if (in_array($this->input->post('vehicle_information')['insurance_type'], [COMPREHENSIVE, COMPREHENSIVE_CTPL, COMPREHENSIVE_CTPL_AON])) {
//                $this->form_validation->set_rules('vehicle_information[overnight_parking]', 'Overnight parking', 'required|max_length[50]');
            }

            if ($this->input->post('vehicle_information')['color'] == 'Other') {
                $this->form_validation->set_rules('vehicle_information[other_color]', 'Other color', 'required|max_length[250]');
            }

            if (in_array($this->input->post('vehicle_information')['insurance_type'], [CTPL, COMPREHENSIVE_CTPL, COMPREHENSIVE_CTPL_AON])) {
                $this->form_validation->set_rules('vehicle_information[mv_file_number]', 'MV file number', 'required|max_length[50]');
            }
            $this->form_validation->set_error_delimiters('<div class="error_msg"><p>', '</p></div>');
            $this->form_validation->set_message('required', '{field} is required.');

            $config['upload_path'] = OR_CR_FOLDER;
            $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|bmp|GIF|JPG|JPEG|PNG|PDF|BMP';
            $config['max_size']	= '50000';
            $config['max_width']  = '5000';
            $config['max_height']  = '5000';
            $config['file_name'] = 'MTR' . date('Y') . $token;

            if ($this->form_validation->run() == FALSE) {
                $this->load->library('upload', $config);

                if(!empty($_FILES['vehicle_information']['name']['or_cr_file'])) {
                    foreach ($_FILES['vehicle_information']['name'] as $key => $image) {
                        $_FILES['or_cr']['name'] = $_FILES['vehicle_information']['name'][$key];
                        $_FILES['or_cr']['type'] = $_FILES['vehicle_information']['type'][$key];
                        $_FILES['or_cr']['tmp_name'] = $_FILES['vehicle_information']['tmp_name'][$key];
                        $_FILES['or_cr']['error'] = $_FILES['vehicle_information']['error'][$key];
                        $_FILES['or_cr']['size'] = $_FILES['vehicle_information']['size'][$key];

                        if ($this->upload->do_upload('or_cr')) {
                            $vehicle_information = $this->motor_model->vehicle_information($token, FALSE);
                        } else {
                            $vehicle_information = $this->motor_model->vehicle_information($token, !$this->input->post('loaded') ? FALSE : TRUE);
                        }
                        break;
                    }
                }
                /**
                 * CTPL no file
                 */
                else {
                    $vehicle_information = $this->motor_model->vehicle_information($token, FALSE);
                }
                $this->scripts_array[] = jsUrl('core/chosen.jquery.min.js');
                $this->scripts_array[] = jsUrl('core/bootstrap-datepicker.min.js');
                $this->scripts_array[] = jsUrl('core/jquery.nicefileinput.js');
                $scriptsIE = jsScripts($this->scripts_array_ie);
                $scripts = jsScripts($this->scripts_array);

                $header_data = array('title' => 'Motor Vehicle Information Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
                $this->load->view('user/header', $header_data);
                $this->template->preload = TRUE;
                $this->template->load('motor_template', 'vehicle_information', $vehicle_information);
                $this->load->view('user/footer');
            }
            else {
                $data = $this->input->post();
                $this->load->library('upload', $config);//dd($_FILES);

                if(!empty($_FILES['vehicle_information']['name']['or_cr_file'])) {
                    foreach ($_FILES['vehicle_information']['name'] as $key => $image) {
                        $_FILES['or_cr']['name'] = $_FILES['vehicle_information']['name'][$key];
                        $_FILES['or_cr']['type'] = $_FILES['vehicle_information']['type'][$key];
                        $_FILES['or_cr']['tmp_name'] = $_FILES['vehicle_information']['tmp_name'][$key];
                        $_FILES['or_cr']['error'] = $_FILES['vehicle_information']['error'][$key];
                        $_FILES['or_cr']['size'] = $_FILES['vehicle_information']['size'][$key];

                        if ($this->upload->do_upload('or_cr')) {
                            $orCr = $this->upload->data();
                            $data['vehicle_information']['or_cr'] = $orCr['file_name'];
                            $this->motor_model->do_vehicle_information($data, $token);
                        } else {
                            if (count($_FILES)) {
                                $vehicle_information = $this->motor_model->vehicle_information($token, FALSE);

                                $this->scripts_array[] = jsUrl('core/chosen.jquery.min.js');
                                $this->scripts_array[] = jsUrl('core/bootstrap-datepicker.min.js');
                                $this->scripts_array[] = jsUrl('core/jquery.nicefileinput.js');
                                $scriptsIE = jsScripts($this->scripts_array_ie);
                                $scripts = jsScripts($this->scripts_array);

                                $header_data = array('title' => 'Motor Vehicle Information Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
                                $this->load->view('user/header', $header_data);
                                $this->template->preload = TRUE;
                                $this->template->load('motor_template', 'vehicle_information', $vehicle_information);
                                $this->load->view('user/footer');
                            } else {
                                $this->motor_model->do_vehicle_information($data, $token);
                            }
                        }
                        break;
                    }
                } else {
                    $vehicle_information = $this->motor_model->vehicle_information($token, FALSE);
                    if ((!empty($vehicle_information['vehicle_information']['or_cr']) && count($_FILES) == 0) || $this->input->post('vehicle_information')['insurance_type'] == COMPREHENSIVE) {
                        $this->motor_model->do_vehicle_information($data, $token);
                    } else {
                        $this->scripts_array[] = jsUrl('core/chosen.jquery.min.js');
                        $this->scripts_array[] = jsUrl('core/bootstrap-datepicker.min.js');
                        $this->scripts_array[] = jsUrl('core/jquery.nicefileinput.js');
                        $scriptsIE = jsScripts($this->scripts_array_ie);
                        $scripts = jsScripts($this->scripts_array);

                        $header_data = array('title' => 'Motor Vehicle Information Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
                        $this->load->view('user/header', $header_data);
                        $this->template->preload = TRUE;
                        $this->template->load('motor_template', 'vehicle_information', $vehicle_information);
                        $this->load->view('user/footer');
                    }
                }
            }
        } else {
            redirect("motor-insurance");
        }
    }

    /*
	|--------------------------------------------------------------------------
	| COVERAGE DETAILS
	|--------------------------------------------------------------------------
	*/
    public function coverage_details($token = NULL) {
        if ($token != NULL) {
            $this->motor_model->quote_tab_redirect_handler(3);

            if (!is_numeric($this->session->userdata('motor_step'))) {
                redirect('motor-insurance');
            }
            $this->session->set_userdata('motor_step', 3);

            $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($token), TRUE);

            if ($this->input->post()) {
                if (!$motor_saved_quotes['vehicle_information']['is_new']) {
                    $plate_number_combo = [11, '02', '03', '04', '05', '06', '07', '08', '09', 10];

                    $start_date = date("Y-") . $plate_number_combo[((int) substr($motor_saved_quotes['vehicle_information']['plate_number'], -1))] . "-01";
                    $end_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime($start_date)) . " + 3 years"));

                    $_POST['coverage_details']['ctpl_start_date'] = $start_date;
                    $_POST['coverage_details']['ctpl_end_date'] = $end_date;
                }

                $this->motor_model->do_coverage_details($motor_saved_quotes);
            } else {
                $coverage_details = $motor_saved_quotes['coverage_details'];
                $coverage_details['vehicle_information']['insurance_type'] = $motor_saved_quotes['vehicle_information']['insurance_type'];
                $coverage_details['vehicle_information']['is_new'] = $motor_saved_quotes['vehicle_information']['is_new'];

                if (!$motor_saved_quotes['vehicle_information']['is_new']) {
                    $plate_number_combo = [11, '02', '03', '04', '05', '06', '07', '08', '09', 10];

                    $start_date = date("Y-") . $plate_number_combo[((int) substr($motor_saved_quotes['vehicle_information']['plate_number'], -1))] . "-01";
                    $end_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime($start_date)) . " + 3 years"));

                    $coverage_details['coverage_details']['ctpl_start_date'] = $start_date;
                    $coverage_details['coverage_details']['ctpl_end_date'] = $end_date;

                }
                $_POST = $coverage_details;
            }


            /**
             * Load Scripts and header
             */
            $scriptsIE = jsScripts($this->scripts_array_ie);
            $scripts = jsScripts($this->scripts_array);
            $header_data = array('title' => 'Motor Coverage Details Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
            $this->load->view('user/header', $header_data);

            /**
             * Load Main Content
             */
            $this->template->preload = TRUE;
            $this->template->load('motor_template', 'coverage_details', $_POST);

            /**
             * Load Footer
             */
            $this->load->view('user/footer');
        } else {
            redirect("motor-insurance");
        }
    }

    /*
	|--------------------------------------------------------------------------
	| PERSONAL INFORMATION
	|--------------------------------------------------------------------------
	*/
    public function personal_information($token = NULL) {
        if ($token != NULL) {
            $this->motor_model->quote_tab_redirect_handler(4);

            if (!is_numeric($this->session->userdata('motor_step'))) {
                redirect('motor-insurance');
            }
            $this->session->set_userdata('motor_step', 4);

            $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($token), TRUE);

            if ($this->input->post()) {
                $data = $this->motor_model->do_personal_info($motor_saved_quotes);
                $data['first_load'] = false;
            } else {
                $data = $motor_saved_quotes;
                $data['personal_information']['nationality'] = 'Filipino';
                $data['first_load'] = true;
            }

            $agent = $motor_saved_quotes['agent'];
            if ($agent) {
                $data['agent']['agent_type'] = $agent['code'] ? 'Agent Code' : 'Agent Name';
                $data['agent']['with_agent'] = 'on';
            }


            if ($data['mailing_address']['province'] == "") {
                $data['mailing_address']['province'] = $data['vehicle_information']['address']['province'];
            }
            if ($data['mailing_address']['city'] == "") {
                $data['mailing_address']['city'] = $data['vehicle_information']['address']['city'];
            }
            if ($data['mailing_address']['barangay'] == "") {
                $data['mailing_address']['barangay'] = $data['vehicle_information']['address']['barangay'];
            }

            /**
             * Set initial values
             */
            if ($data) {
                $_POST = $data;
            }

            /**
             * Load Header and Scripts
             */
            $this->scripts_array[] = jsUrl('core/chosen.jquery.min.js');
            $scriptsIE = jsScripts($this->scripts_array_ie);
            $scripts = jsScripts($this->scripts_array);
            $header_data = array('title' => 'Motor Personal Information Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
            $this->load->view('user/header', $header_data);

            /*
             * Load Main Content
             */
            $this->load->model('agent/agent_model');
            
            $this->template->preload = TRUE;
            $this->template->load('motor_template', 'personal_information', [
                'suffixes' => $this->suffix_model->fetch(),
                'salutations' => $this->salutation_model->fetch(),
                'nationalities' => $this->nationality_model->fetch(),
                'provinces' => $this->place_model->provinces(),
                'cities' => set_value('mailing_address[province]') ? $this->place_model->cities(set_value('mailing_address[province]')) : $data['vehicle_information']['address']['province'],
                'barangays' => set_value('mailing_address[city]') ? $this->place_model->barangays(set_value('mailing_address[city]')) : $data['vehicle_information']['address']['city'],
                'occupations' => $this->occupation_model->fetch(),
                'motor_quote' => (object)$data,
                'default_places' => $this->place_model->get_label_in_ids([$data['vehicle_information']['address']['province'], $data['vehicle_information']['address']['city'], $data['vehicle_information']['address']['barangay']]),
                'first_load' => $data['first_load'],
                'agencies'   => $this->agent_model->get_agencies()
            ]);

            /**
             * Load Footer
             */
            $this->load->view('user/footer');
        } else {
            redirect("motor-insurance");
        }
    }

    public function generate_policy_schedule($id) {
        $is_ctpl = ($this->uri->segment(4)) ? TRUE : FALSE;
        $this->motor_quote_model->generate_policy_schedule($id, $is_ctpl, FALSE);
    }

    public function generate_bank_certificate($id) {
        $this->motor_quote_model->generate_bank_certificate($id);
    }

    public function generate_motor_quote_request($id) {
        $this->motor_quote_model->generate_motor_quote_request($id);
    }

    public function generate_invoice($id) {
        $is_ctpl = ($this->uri->segment(4)) ? TRUE : FALSE;
        $this->motor_quote_model->generate_invoice($id, $is_ctpl);
    }

    public function generate_confirmation_of_cover($id) {
        $this->motor_quote_model->generate_confirmation_of_cover($id);
    }

    /**
     * Displays the verification form to create the password
     */
    public function verify() {
        $data['token'] = ($this->uri->segment(3) != FALSE) ? $this->uri->segment(3) : '';
        $user = $this->client_model->get_user_by_token($data['token']);

        if($this->input->post()) {
            if ($this->client_rules_model->validate($this->form_validation, 'verify') != FALSE) {
                if ($this->client_model->change_password($user, $this->input->post('password'))) {
                    $this->client_model->login($user['email_address'],$this->input->post('password'));
                }
            }
        }
        $scriptsIE = jsScripts($this->scripts_array_ie);
        $scripts = jsScripts($this->scripts_array);

        if ($user != false) {
            $data['email_address'] = $user['email_address'];

            $header_data = array('title' => 'Verify Account Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
            $this->load->view('user/header', $header_data);
            $this->template->load('motor_template', 'verify', $data);
            $this->load->view('user/footer');
        } else {
            $header_data = array('title' => 'Verify Nothing Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
            $this->load->view('user/header', $header_data);
            $this->template->load('motor_template', 'verify_nothing');
            $this->load->view('user/footer');
        }
    }

    public function send_to_dragonpay($token) {

        $saved_quotes = $this->motor_model->get_motor_saved_quote_by_token($token);
        $motor_saved_quotes = json_decode($saved_quotes['values'], TRUE);

        $motor_info = $this->vehicle_model->get_motor_for_computation($motor_saved_quotes['motors_pk']);
        $quote_premiums = $this->motor_model->quote_premiums($motor_info, $motor_saved_quotes);
        $data = array_merge($motor_saved_quotes, $motor_info, $quote_premiums);

        $txnid       = "MTR-" . (date("y")) . "-" . recordLengthAlign($saved_quotes['pk'], 6, "R");//$data['quote_id'];MC-MNP-HO-15-000001-01
        $email       = $data['contact_information']['email_address'];
        $description = $data['vehicle_information']['year'] . " " . $data['vehicle_information']['brand'] . " " . $data['vehicle_information']['model_name'] . " Premium for " . $data['personal_information']['lname'] . ",  " . $data['personal_information']['fname'];
        $ccy         = "PHP";


        $amount      = number_format($data['premium'], 2, '.', '');
        $digest_str  = DP_MERCHANT.":$txnid:$amount:$ccy:$description:$email:".DP_SECRET_KEY;
        $digest      = sha1($digest_str);

        $params = "merchantid=" . urlencode(DP_MERCHANT) .
            "&txnid=" .  urlencode($txnid) .
            "&amount=" . urlencode($amount) .
            "&ccy=" . urlencode($ccy) .
            "&description=" . urlencode($description) .
            "&email=" . urlencode($email) .
            "&digest=" . urlencode($digest) .
            "&param1=" . 'motor' .
            "&param2=" . urlencode($token);
        $url = DP_PAYMENT_URL;

        if ( TEST ) {
            file_get_contents(base_url('transaction/post_callback?txnid='.urlencode($txnid).'&refno=mtr'.urlencode($txnid).'&status=S&message='.urlencode($description).'&digest=mtr'.urlencode($txnid).'&param1=motor&param2='.urlencode($txnid)));
            redirect(base_url('transaction/return_callback?txnid='.urlencode($txnid).'&refno=mtr'.urlencode($txnid).'&status=S&message='.urlencode($description).'&digest=mtr'.urlencode($txnid).'&param1=motor&param2='.urlencode($txnid)));
        }
        header("Location: $url?$params");
    }

    public function success() {
        $this->session->set_userdata('motor_step', NULL);

        $scriptsIE = jsScripts($this->scripts_array_ie);
        $scripts = jsScripts($this->scripts_array);

        $motor_quote_pk = $this->uri->segment(3);
        $motor_quote = $this->motor_model->get_motor_quote_by_pk($motor_quote_pk);
        $data = $motor_quote;
        $data['transaction'] = $this->transaction_model->get_transaction(MOTOR, $motor_quote_pk);
        $header_data = array('title' => 'Successful Transactions Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
        $this->load->view('user/header', $header_data);
        $this->template->load('motor_template', 'success', $data);
        $this->load->view('user/footer');
    }

    public function failed() {
        $scriptsIE = jsScripts($this->scripts_array_ie);
        $scripts = jsScripts($this->scripts_array);
        $header_data = array('title' => 'Successful Transactions Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
        $this->load->view('user/header', $header_data);
        $this->template->load('motor_template', 'failed');
        $this->load->view('user/footer');
    }

}