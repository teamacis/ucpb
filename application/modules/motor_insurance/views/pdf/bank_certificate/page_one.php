<style>
    * { font-size: 12px; line-height: 18px; }
    .max-width { width: 100%; }
    .large-width { width: 75%; }
    .half-width { width: 50%; }
    .one-third-width { width: 33%; }
    .one-fourth-width { width: 25%; }
    .center { margin: 0 auto; }

    .text-center { text-align: center; }
    .text-right  { text-align: right; }
    .colon { width: 5%; }

    .header { border-top: 1px dashed black; border-bottom: 1px dashed black; line-height:20px; }
    .header-two { border-bottom: 1px dashed black; line-height: 15px; text-align: center; }

    table tr td { line-height: 25px; }

    h1 { font-size: 18px; }
</style>

<br><br><br><br><br><br><br>
<h1 class="text-center">CONFIRMATION</h1>
<br><br>
<div class="text-center">
    This is to confirm that the vehicle under Policy No. ____________________ is <br>
    insured with UCPB GEN. INSURANCE CO., INC.
</div>
<br><br>

<table class="max-width">
    <tr>
        <td class="one-fourth-width">ASSURED</td>
        <td class="colon">:</td>
        <td style="width: 66%">&nbsp;&nbsp;&nbsp;<?= $data->fname . ' ' . $data->lname; ?></td>
    </tr>
    <tr>
        <td>EFFECTIVITY</td>
        <td>:</td>
        <td>&nbsp;&nbsp;&nbsp;<?= date('F d, Y', strtotime($data->effectivity)); ?></td>
    </tr>
    <tr>
        <td>UNIT</td>
        <td>:</td>
        <td>&nbsp;&nbsp;&nbsp;<?= $data->brand; ?> <?= $data->year; ?> <?= $data->model_name; ?></td>
    </tr>
    <tr>
        <td>PLATE NO.</td>
        <td>:</td>
        <td>&nbsp;&nbsp;&nbsp;<?= $data->plate_number; ?></td>
    </tr>
    <tr>
        <td>COLOR</td>
        <td>:</td>
        <td>&nbsp;&nbsp;&nbsp;<?= $data->color; ?></td>
    </tr>
    <tr>
        <td>SERIAL. NO</td>
        <td>:</td>
        <td>&nbsp;&nbsp;&nbsp;<?= $data->chassis_number; ?></td>
    </tr>
    <tr>
        <td>MOTOR NO.</td>
        <td>:</td>
        <td>&nbsp;&nbsp;&nbsp;<?= $data->engine_number; ?></td>
    </tr>
    <tr>
        <td>COVERAGE</td>
        <td>:</td>
        <td class="max-width">
            <table>
                <?php if(!in_array($data->vehicle_information['insurance_type'], [COMPREHENSIVE_AON, COMPREHENSIVE])) { ?>
                    <tr>
                        <td>Compulsary Third Party Liability</td>
                        <td>PHP <?= number_format($data->ctpl, 2, '.', ','); ?></td>
                    </tr>
                <?php }; ?>
                <?php if($data->vehicle_information['insurance_type'] != CTPL): ?>
                <tr>
                    <td>OWN DAMAGE</td>
                    <td>PHP <?= number_format($data->ownDamage, 2, '.', ','); ?></td>
                </tr>
                <tr>
                    <td>THEFT</td>
                    <td>PHP <?= number_format($data->ownDamage, 2, '.', ','); ?></td>
                </tr>
                <tr>
                    <td>VTPL - BODILY INJURY</td>
                    <td>PHP <?= number_format($data->bodilyInjury, 2, '.', ','); ?></td>
                </tr>
                <tr>
                    <td>VTPL - PROPERTY DAMAGE</td>
                    <td>PHP <?= number_format($data->propertyDamage, 2, '.', ','); ?></td>
                </tr>
                <tr>
                    <td>AUTO PERSONAL ACCIDENT</td>
                    <td>PHP <?= number_format($data->autoPersonalAccident, 2, '.', ','); ?></td>
                </tr>
                <?php endif; ?>
                <?php if(in_array($data->insurance_type, [COMPREHENSIVE_AON, COMPREHENSIVE_CTPL_AON])): ?>
                <tr>
                    <td>ACTS OF NATURE</td>
                    <td>PHP <?= number_format($data->aon, 2, '.', ','); ?></td>
                </tr>
                <?php endif; ?>
            </table>
        </td>
    </tr>
</table>

<br><br>
<div class="text-center">
    This further confirms that the aforesaid insurance policy is in full force and effect and loss <br>
    and/or damage, if any, shall be payable to UCPB - HEAD OFFICE or its assigns as their <br>
    interest may appear, subject to the terms and conditions, clauses and warranties of the policy. <br>
</div>

<span style="display: none;">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Issued this <?= ordinal(date('j'))?> day of <?= date('F')?>, <?= date('Y') ?> at Makati City

<br>

<div class="text-center">**This is a computer generated form and does not require any signature**</div>