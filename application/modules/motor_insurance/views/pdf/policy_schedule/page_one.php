<style>
    * { font-size: 10px; }
    .max-width { width: 100%; }
    .half-width { width: 50%; }
    .one-third-width { width: 33%; }
    .one-fourth-width { width: 25%; }

    .text-center { text-align: center; }
    .text-right  { text-align: right; }
    .colon { width: 5%; }

    .header { border-top: 1px dashed black; border-bottom: 1px dashed black; line-height:20px; }
    .header-two { border-bottom: 1px dashed black; line-height: 15px; text-align: center; }

    table tr td { line-height: 15px; }
</style>

<div class="text-center">
    <b>MOTOR CAR POLICY SCHEDULE</b>
</div>


<br>

<table class="max-width">
    <tr>
        <td class="half-width">
            <table>
                <tr>
                    <td>Line</td>
                    <td class="colon">:</td>
                    <td class="max-width">MOTOR CAR</td>
                </tr>
                <tr>
                    <td>Subline</td>
                    <td>:</td>
                    <td>PRIVATE CAR NON-FLEET</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Issue Date</td>
                    <td>:</td>
                    <td><?=date('M d, Y',strtotime($data->issued))?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Policy No</td>
                    <td>:</td>
                    <td><?php echo $data->policy_id; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Insured</td>
                    <td>:</td>
                    <td><?=$data->fname . ' ' . $data->lname;?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>:</td>
                    <td><?php echo $data->locationOfVehicle; ?></td>
                </tr>
            </table>
        </td>
        <td>
            <table class="max-width">
                <tr>
                    <td class="text-right">Policy Period</td>
                    <td class="text-right"></td>
                </tr>
                <tr>
                    <td class="text-right">From:</td>
                    <td class="text-right"><?= ($data->is_ctpl) ? $data->ctpl_start_date : $data->non_ctpl_start_date?> 12:00:00 NOON</td>
                </tr>
                <tr>
                    <td class="text-right">To:</td>
                    <td class="text-right"><?= ($data->is_ctpl) ? $data->ctpl_end_date : $data->non_ctpl_end_date?> 12:00:00 NOON</td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
            </table>
            <br>
            <table class="max-width">
                <tr>
                    <td class="half-width">PREMIUM</td>
                    <td class="colon">:</td>
                    <td class="half-width text-right ">PHP <?= ($data->is_ctpl) ? number_format($data->ctplNetPremium, 2, '.', ',') : number_format($data->netPremium, 2, '.', ','); ?></td>
                </tr>
                <tr>
                    <td>DOCUMENT STAMPS</td>
                    <td></td>
                    <td class="text-right"><?= ($data->is_ctpl) ? number_format($data->ctplDocStamps, 2, '.', ',') : number_format($data->docStamps, 2, '.', ','); ?></td>
                </tr>
                <tr>
                    <td>VAT</td>
                    <td>:</td>
                    <td class="text-right"><?= ($data->is_ctpl) ? number_format($data->ctplVat, 2, '.', ',') : number_format($data->vat, 2, '.', ','); ?></td>
                </tr>
                <tr>
                    <td>LOCAL GOVERNMENT TAX</td>
                    <td>:</td>
                    <td class="text-right"><?= ($data->is_ctpl) ? number_format($data->ctplLocalGovernmentTax, 2, '.', ',') : number_format($data->localGovernmentTax, 2, '.', ','); ?></td>
                </tr>
                <?php if($data->is_ctpl) {?>
                <tr>
                    <td>INTERCONNECTIVITY FEE</td>
                    <td>:</td>
                    <td class="text-right"><?= number_format($data->ctplInterconnectivity, 2, '.', ','); ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td class="text-right">--------------------------</td>
                </tr>
                <tr>
                    <td>AMOUNT DUE</td>
                    <td>:</td>
                    <td class="text-right"><?= ($data->is_ctpl) ? number_format($data->ctplGrossPremium, 2, '.', ',') : number_format($data->grossPremium, 2, '.', ','); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td class="text-right">===============</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table>
    <tr>
        <td class="one-fourth-width">&nbsp;</td>
        <td class="colon"></td>
        <td class="half-width"></td>
    </tr>
    <?php if(!$data->is_ctpl)  { ?>
    <tr>
        <td>Total Sum Insured</td>
        <td>:</td>
        <td><b><?php
            echo strtoupper(convert_number_to_words($data->market_value));
        ?> IN PHILIPPINE PESO (<?=number_format($data->market_value, 2, '.', ',')?>)</b></td>
    </tr>
    <?php } ?>
</table>
<br><br>
<div class="header">
    <b>INSURED</b>
</div>
<br>
<table class="max-width">
    <tr>
        <td class="half-width">
            <table class="max-width">
                <tr>
                    <td>Vehicle Insured</td>
                    <td class="colon">:</td>
                    <td><?= $data->model_name; ?></td>
                </tr>
                <tr>
                    <td>Model Year</td>
                    <td>:</td>
                    <td><?= $data->year; ?></td>
                </tr>
                <tr>
                    <td>Make</td>
                    <td>:</td>
                    <td><?= $data->model; ?></td>
                </tr>
                <tr>
                    <td>Type of Body</td>
                    <td>:</td>
                    <td><?= $data->body?></td>
                </tr>
                <tr>
                    <td>Color</td>
                    <td>:</td>
                    <td><?= $data->color; ?></td>
                </tr>
                <tr>
                    <td>BLT File No.</td>
                    <td>:</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Motor/Engine No.</td>
                    <td>:</td>
                    <td><?= $data->engine_number; ?></td>
                </tr>
                <tr>
                    <td>Serial/Chassis No.</td>
                    <td>:</td>
                    <td><?= $data->chassis_number; ?></td>
                </tr>
                <tr>
                    <td>Vehicle Type</td>
                    <td>:</td>
                    <td>PRIVATE CAR</td>
                </tr>
                <tr>
                    <td>Accessories</td>
                    <td>:</td>
                    <td>AIRCON<br>STEREO<br>SPEAKER<br>MARWHEEL</td>
                </tr>
                <?php if (!empty($data->non_standard)) { ?>
                <tr>
                    <td>Non-Standard Accessories</td>
                    <td>:</td>
                    <td><?php
                        if (!empty($data->non_standard)) {
                            foreach ($data->non_standard as $key =>  $val) {
                                echo $val['non_standard_accessories'] . '<br>';
                            }
                        }
                        ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Item TSI</td>
                    <td>:</td>
                    <td>PHP <?= number_format($data->market_value, 2, '.', ',')?></td>
                </tr>
                <tr>
                    <td>Item Premium</td>
                    <td>:</td>
                    <td>PHP <?= ($data->is_ctpl) ? number_format($data->ctplNetPremium, 2, '.', ',') : number_format($data->netPremium, 2, '.', ','); ?></td>
                </tr>
            </table>
        </td>
        <td>
            <table class="max-width">
                <tr><td>&nbsp;</td><td></td><td></td></tr>
                <tr><td>&nbsp;</td><td></td><td></td></tr>
                <tr><td>&nbsp;</td><td></td><td></td></tr>
                <tr>
                    <td>Area of Operation</td>
                    <td class="colon">:</td>
                    <td>NATIONWIDE</td>
                </tr>
                <tr>
                    <td>Plate No.</td>
                    <td>:</td>
                    <td><?= $data->plate_number; ?></td>
                </tr>
                <tr>
                    <td>Authorized Capacity</td>
                    <td>:</td>
                    <td><?= $data->seating_capacity; ?></td>
                </tr>
                <tr>
                    <td>Unladen Weight</td>
                    <td>:</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Motor Type Desc</td>
                    <td>:</td>
                    <td>LIGHT</td>
                </tr>
                <tr>
                    <td>COC No</td>
                    <td>:</td>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<br><br>

<table class="max-width">
    <tr>
        <td class="one-third-width">
            <div class="header-two">
                Peril/s
            </div>
            <br>
            <?php if($data->is_ctpl)  { ?>
            COMPULSORY THIRD PAR <br>
            <?php } ?>
            <?php if(!$data->is_ctpl)  { ?>
            OWN DAMAGE <br>
            THEFT <br>
            VTPL - BODILY INJURY <br>
            VTPL - PROPERTY DAMAGE <br>
            ACTS OF NATURE <br>
            <?php } ?>
        </td>
        <td class="one-third-width">
            <div class="header-two">
                Sum Insured
            </div>
            <br>
            <?php if($data->is_ctpl)  { ?>
                <?php if(!in_array($data->insurance_type, [COMPREHENSIVE_AON, COMPREHENSIVE])) { ?>
                PHP <?= number_format($data->ctpl, 2, '.', ','); ?><br>
                <?php } ?>
            <?php } ?>
            <?php if(!$data->is_ctpl)  { ?>
                <?php if($data->insurance_type != CTPL) { ?>
                PHP <?= number_format($data->ownDamage, 2, '.', ','); ?><br>
                PHP <?= number_format($data->ownDamage, 2, '.', ','); ?><br>
                PHP <?= number_format($data->bodilyInjury, 2, '.', ','); ?><br>
                PHP <?= number_format($data->propertyDamage, 2, '.', ','); ?><br>
                <?php } ?>
                <?php if(in_array($data->insurance_type, [COMPREHENSIVE_AON, COMPREHENSIVE_CTPL_AON])) { ?>
                PHP <?= number_format($data->aon, 2, '.', ','); ?>
                <?php } ?>
            <?php } ?>
        </td>
        <td class="one-third-width">
            <div class="header-two">
                Premium
            </div>
            <br>
            <?php if($data->is_ctpl)  { ?>
                <?php if(!in_array($data->insurance_type, [COMPREHENSIVE_AON, COMPREHENSIVE])) { ?>
                PHP <?= number_format($data->ctpl, 2, '.', ','); ?><br>
                <?php } ?>
            <?php } ?>
            <?php if(!$data->is_ctpl)  { ?>
                <?php if($data->insurance_type != CTPL) { ?>
                PHP <?= number_format($data->ownDamageCoverage, 2, '.', ','); ?><br>
                PHP <?= number_format($data->ownDamageCoverage, 2, '.', ','); ?><br>
                PHP <?= number_format($data->bodilyInjuryCoverage, 2, '.', ','); ?><br>
                PHP <?= number_format($data->propertyDamageCoverage, 2, '.', ','); ?><br>
                <?php } ?>
                <?php if(in_array($data->insurance_type, array(COMPREHENSIVE_AON, COMPREHENSIVE_CTPL_AON))) { ?>
                PHP <?= number_format($data->aon, 2, '.', ','); ?>
                <?php } ?>
            <?php } ?>
        </td>
    </tr>
</table>
