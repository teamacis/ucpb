<style>
    * { font-size: 10px; }
    .font-big { font-size: 14px; }

    .max-width { width: 100%; }
    .large-width { width: 75%; }
    .half-width { width: 50%; }
    .one-third-width { width: 33%; }
    .one-fourth-width { width: 25%; }

    .text-center { text-align: center; }
    .text-right  { text-align: right; }
    .colon { width: 5%; }

    .header { border-top: 1px dashed black; border-bottom: 1px dashed black; line-height:20px; }
    .header-two { border-bottom: 1px dashed black; line-height: 15px; text-align: center; }

    table tr td { line-height: 15px; }

</style>

<b>DEDUCTIBLES</b>
<br>
<table style="width: 100%;">
    <tr>
        <td class="one-fourth-width">
            <div class="header-two">
                Deductible/s
            </div>
            <br>

        </td>
        <td class="one-fourth-width">
            <div class="header-two">
                Amount
            </div>
            <br>

        </td>
        <td class="one-fourth-width">
            <div class="header-two">
                Rate (%)
            </div>
            <br>

        </td>
        <td class="one-fourth-width">
            <div class="header-two">
                Peril/s
            </div>
            <br>

        </td>
    </tr>
</table>

<br><br><br>

<table>
    <?php if(!$data->is_ctpl)  { ?>
    <tr>
        <td class="one-fourth-width">Deductibles</td>
        <td class="colon">:</td>
        <td><?= number_format($data->deductible, 2, '.', ',')?></td>
    </tr>
    <?php } ?>
    <tr>
        <td>Towing</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td>Authorized Repair Limit</td>
        <td>:</td>
        <td></td>
    </tr>
</table>

<br><br><br>

<div class="header">
    <b>WARRANTIES AND CLAUSES</b>
</div>
<br>
<b>DRUNKEN DRIVERS CLAUSE</b> <br>
<b>NON CASA OR NON DEALER REPAIR SHOP CLAUSE</b> <br>
<b>RSCC ENDORSEMENT</b> <br>
<b>MOTOR CAR ACCESSORY CLAUSE</b> <br>
<b>ACTS OF NATURE ENDORSEMENT</b> <br>
<b>AIRBAG CLAUSE</b>

<br><br><br>

<div class="header">
    <b>GENERAL INFORMATION</b>
</div>

<br><br><br><br>
<table class="max-width">
    <tr>
        <td style="width: 60%">
            <?=date('F d, Y', strtotime($data->issued))?> <br>
            <?php if(isset($data->code)) echo $data->code . '<br>';?>
            <?php if(isset($data->code) && isset($data->fname)) echo '/'; ?>
            <?php if(isset($data->code)) echo $data->fname . ' ' . $data->lname . '<br>';?>
            Policy ID : <?= $data->policy_id?>
        </td>
        <td class="text-center" style="width: 40%">
            <b class="font-big"><em>UCPB General Insurance Co. Inc.</em></b>
            <br><br><br>
            <b>Isabelo P. Africa</b> <br>
            <b>President</b>
            <div style="border-top: 1px solid black; line-height: 20px;">
                Authorized Signatory
            </div>
        </td>
    </tr>
</table>