<style>
    * { font-size: 11px; line-height: 18px; }
    .black-header { background-color: black; color: white; }
    .text-center { text-align: center; }
    .text-right { text-align: right; }
    small { font-size: 9px; }
</style>

<table class="black-header">
    <tr>
        <td>
            &nbsp;
            <br>
            &nbsp;&nbsp;&nbsp;<strong>UCPB General Insurance Company Inc.</strong> <br>
            &nbsp;&nbsp;&nbsp;5/F UCPB Corporate Offices 7907 Makati Avenue <br>
            &nbsp;&nbsp;&nbsp;Makati City 1200 Philippines
            <br>
            &nbsp;
        </td>
        <td>

        </td>
    </tr>
</table>

<br>
<div class="text-right">
    An ISO 9001-2008 Certified Company
</div>

<div class="text-center">
    <h3>INVOICE</h3>
    <hr>
</div>

<table>
    <tr>
        <td style="width: 150px;"><strong>VAT. REG. T.I.N.</strong></td>
        <td style="width: 220px;">000-432-798-000</td>
        <td style="width: 280px;">&nbsp;</td>
    </tr>
    <tr>
        <td><strong>BIR Permit No.</strong></td>
        <td>0612-116-00205-CBA/AR</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><strong>Insured</strong></td>
        <td><?=$data->fname . ' ' . $data->lname?></td>
        <td>
            <table style="width: 100%;">
                <tr style="width: 20%;">
                    <td><strong>Date of Insurance</strong></td>
                    <td><?= date('d F Y', strtotime($data->issued))?></td>
                </tr>
                <tr style="width: 80%;">
                    <td><strong>Invoice No.</strong></td>
                    <td>HO-0000000029396</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><strong>Address</strong></td>
        <td><?=$data->locationOfVehicle;?></td>
        <td>
            <table style="width: 100%;">
                <tr style="width: 20%;">
                    <td><strong>Producer</strong></td>
                    <td>UCPB GENERAL</td>
                </tr>
                <tr style="width: 80%;">
                    <td><strong>Code</strong></td>
                    <td>41</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><strong>TIN</strong></td>
        <td>660356300</td>
    </tr>
</table>
<br>
<hr>
&nbsp;
<br>
<table>
    <tr>
        <td style="width: 150px;"><strong>Class</strong></td>
        <td style="width: 220px;">LAND TRANSPORTATION OPERATORS</td>
        <td style="width: 280px;">&nbsp;</td>
    </tr>
    <tr>
        <td><strong>Policy No.</strong></td>
        <td><?= $data->policy_id?></td>
        <td>
            <table style="width: 100%;">
                <tr style="width: 20%;">
                    <td><strong>CURRENCY</strong></td>
                    <td class="text-right">PHILIPPINE PESO(PHP)</td>
                </tr>
                <tr style="width: 80%;">
                    <td><strong>PREMIUM(VATABLE)</strong></td>
                    <td class="text-right"><?= ($data->is_ctpl) ? number_format($data->ctplNetPremium, 2, '.', ',') : number_format($data->netPremium, 2, '.', ',')?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><strong>Sum Insured</strong></td>
        <td><?= number_format($data->sum_insured, 2, '.', ',')?></td>
        <td>
            <table style="width: 100%;">
                <tr style="width: 20%;">
                    <td><strong>LOCAL GOVERNMENT</strong></td>
                    <td class="text-right"><?= ($data->is_ctpl) ? number_format($data->ctplLocalGovernmentTax, 2, '.', ',') : number_format($data->localGovernmentTax, 2, '.', ',');?></td>
                </tr>
                <tr style="width: 80%;">
                    <td><strong>VAT</strong></td>
                    <td class="text-right"><?= ($data->is_ctpl) ? number_format($data->ctplVat, 2, '.', ',') : number_format($data->vat, 2, '.', ',')?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><strong>Period of Insurance</strong></td>
        <td>
<strong>FROM</strong> 12:00 NOON of <?= ($data->is_ctpl) ? $data->ctpl_start_date : $data->non_ctpl_start_date?> <br>
<strong>TO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong> 12:00 NOON of <?= ($data->is_ctpl) ? $data->ctpl_end_date : $data->non_ctpl_end_date?>
        </td>
        <td>
            <table style="width: 100%;">
                <tr style="width: 100%;">
                    <td colspan="2" style="border-bottom: 1px solid black;"></td>
                </tr>
                <tr style="width: 20%;">
                    <td><strong>TOTAL AMOUNT DUE</strong></td>
                    <td class="text-right"><?= ($data->is_ctpl) ? number_format($data->ctplNetPremium + $data->ctplLocalGovernmentTax + $data->ctplVat, 2, '.', ',') : number_format($data->netPremium + $data->localGovernmentTax + $data->vat, 2, '.', ','); ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<br><br><br><br>

<div class="text-center">**This is a computer generated form and does not require any signature**</div>

<br><br><br>
<strong><em>Important:</em></strong>
<br>
<small>
<em>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Should the policy be cancelled or endorsed to a lower value, the insured is still liable to pay the full amount of the documentary stamps as stipulated<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;in the policy prior to cancellation/endorsement.
</em>
</small>