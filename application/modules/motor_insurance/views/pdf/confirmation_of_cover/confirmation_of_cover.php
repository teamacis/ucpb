<style>
    * { font-size: 9px; line-height: 20px; }
    .border { border: 1px solid black; }
    .border-side { border-right: 1px solid black; border-left: 1px solid black; }
    .border-bottom { border-bottom: 1px solid black; }
    .text-center { text-align: center; }
    .text-right { text-align: right; }
    h1 { font-size: 30px; }
    h2 { font-size: 18px; }
    .large-text { font-size: 15px; }
</style>

<div>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <table>
        <tr>
            <td style="width: 95px;">
                <br><br><br>
                <img src="<?= imgUrl('site_logo.png'); ?>" style="width: 75px;"/>
            </td>
            <td style="width:200px;">
                <br><br><br>
                <h1>UCPB GEN</h1>
            </td>
            <td class="text-left" style="width: 340px;">
                <h2 class="text-center">
                    "ORIGINAL COPY"
                </h2>
                <div class="large-text text-center">
                    CONFIRMATION OF COVER <br>
                    NON-LAND TRANSPORTATION OPERATORS <br>
                    VEHCILE <br>
                    <strong>AF-NO.</strong>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
</div>
<br>

<table style="width: 100%;">
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="border">
            POLICY NO.
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="3" rowspan="4" class="border">
NAME AND ADDRESS OF INSURED
<br/>
<?= $lname; ?>, <?= $fname; ?>
<br/>
<?= $locationOfVehicle; ?>
<br/>
<br/>
        </td>
        <td class="border">
BUSINESS/PROFESSION
<br>
        </td>
        <td class="border">
CONFIRMATION OF COVER NO.
<br>
        </td>
    </tr>
    <tr>
        <td class="border">
DATE ISSUED <br>
<?= $date; ?>
        </td>
        <td class="border">
OFFICIAL RECEIPT <br>
        </td>
    </tr>
    <tr>
        <td class="border text-center" colspan="2">PERIOD OF INSURANCE</td>
    </tr>
    <tr>
        <td class="border">FROM 12:00 NOON<br><?= $ctpl_start_date ?></td>
        <td class="border">TO 12:00 NOON<br><?= $ctpl_end_date ?></td>
    </tr>
    <tr>
        <td colspan="5" class="border">SCHEDULED VEHICLE</td>
    </tr>
    <tr>
        <td class="border">
MODEL<br>
<?= $model_name; ?>
        </td>
        <td class="border">
MAKE<br>
<?= $year . ' ' . $brand . ' ' . $model_name; ?>
        </td>
        <td class="border">
TYPE OF BODY<br>
<?= $body; ?>
        </td>
        <td class="border">
COLOR<br>
<?= $color; ?>
        </td>
        <td class="border">
BLT FILE NO.<br>
        </td>
    </tr>
    <tr>
        <td class="border">
PLATE NO.<br>
<?= $plate_number; ?>
        </td>
        <td class="border">
SERIAL / CHASSIS NO.<br>
<?= $chassis_number; ?>
        </td>
        <td class="border">
MOTOR NO.<br>
<?= $engine_number; ?>
</td>
        <td class="border">
AUTHORIZED CAPACITY<br>
<?= $seating_capacity; ?>
        </td>
        <td class="border">
UNLADEN WEIGHT
<div class="text-right">kgs.</div>
        </td>
    </tr>
</table>
<table class="border-side border-bottom">
    <tr>
        <td colspan="2" class="text-center"><strong>SECTION I / II</strong></td>
        <td colspan="2" class="text-center"><strong>LIMITS OF LIABILITY</strong></td>
        <td colspan="2" class="text-center"><strong>PREMIUM PAID</strong></td>
    </tr>
    <tr>
        <td rowspan="2" colspan="2" class="border">
            THIRD PARTY LIABILITY
        </td>
        <td colspan="2" class="border">
            LIMITS OF LIABILITY
        </td>
        <td colspan="2" class="border">
            P <?= number_format($ctpl, '2', '.', ',') ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="border">
            PREMIUM PAID
        </td>
        <td colspan="2" class="border">
            P <?= number_format($premium, '2', '.', ',') ?>
        </td>
    </tr>
    <tr>
        <td colspan="4">
SUBJECT TO THE SCHEDULE OF INDEMNITIES SHOWN AT THE BACK HEREOF: <br>
            <small>
                &nbsp;&nbsp;&nbsp;This Confirmation of COver is evidence of the pol.icy of Insurance required under Chapter <br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VI - Compulsory Motor Vehicle Liability Insurance of the Insurance Code as amended by <br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Presidential Decree No. 1814. <br>
            </small>
        </td>
        <td colspan="2">
            <br><br><br>
            _________________________________________ <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Authorized Signature
        </td>
    </tr>
</table>
<div class="text-center">
    UCPB General Insurance Co., Inc. <br>
    5th Floor UCPB Bldg, 7907 Makati Ave., Makati City 1200 Philippines - MCPO Box No. 1009 - Tel. No.:(632) 811-1788 - Fax No.:(632) 811-3333 - VAT Reg. TIN 000-432-798-000
</div>