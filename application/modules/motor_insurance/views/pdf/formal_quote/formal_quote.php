<style>
</style>
<img src="<?= imgUrl('letter_head.png') ?>">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td colspan="2"><?=date('F d, Y')?><br /><br /></td>
    </tr>
    <tr>
        <td colspan="2">Ms./Mr. <?=ucfirst($personal_information['fname'])?> <?=ucfirst($personal_information['lname'])?></td>
    </tr>
    <tr>
        <td colspan="2"><?=$locationOfVehicle?><br /><br /></td>
    </tr>
    <tr>
        <td colspan="2">Dear Ms./Mr. <?=ucfirst($personal_information['lname'])?><br /></td>
    </tr>
    <tr>
        <td colspan="2">We are pleased to submit our Motor Car insurance quotation as follows:</td>
    </tr>
    <tr>
        <td width="40%">
            Insured's Name
        </td>
        <td width="60%">: &nbsp;&nbsp;&nbsp;&nbsp;<?=ucfirst($personal_information['fname']) . ' ' . ucfirst($personal_information['lname'])?></td>
    </tr>
    <tr>
        <td>
            Insured's Unit
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;<?=ucfirst($year) . ' ' . ucfirst($brand) . ' ' . ucfirst($model_name)?></td>
    </tr>
    <?php if (in_array($vehicle_information['insurance_type'], [CTPL, COMPREHENSIVE, COMPREHENSIVE_AON])) { ?>
    <tr>
        <td>
            Period of Insurance
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;
            <?php
            if($vehicle_information['insurance_type'] == COMPREHENSIVE){
                echo '1 year';
            }else if($vehicle_information['insurance_type'] == CTPL){
                if($vehicle_information['is_new']) {
                    echo '3 years';
                } else {
                    echo '1 year';
                }
            }
            ?>
        </td>
    </tr>
    <?php } else { ?>
    <tr>
        <td>
            Comprehensive Period of Insurance
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;
            1 year
        </td>
    </tr>
    <tr>
        <td>
            CTPL Period of Insurance
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;
            <?php
            if($vehicle_information['is_new']) {
                echo '3 years';
            } else {
                echo '1 year';
            }
            ?>
        </td>
    </tr>
    <?php } ?>
    <tr>
        <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;<em>Coverage:</em></td>
    </tr>
    <?php
    if($vehicle_information['insurance_type'] == COMPREHENSIVE){ ?>
    <tr>
        <td>
            Own Damage and Theft
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($ownDamageCoverage, 2, '.', ',')?></td>
    </tr>
    <tr>
        <td>
            Bodily Injury
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php 500,000.00</td>
    </tr>
    <tr>
        <td>
            Property Damage
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php 500,000.00</td>
    </tr>
    <tr>
        <td>
            Auto Personal Accident
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($autoPersonalAccidentCoverage, 2, '.', ',')?></td>
    </tr>
    <?php
    } else if($vehicle_information['insurance_type'] == CTPL){ ?>
    <tr>
        <td>
            Compulsary Third Party Liability
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($ctpl, 2, '.', ',')?></td>
    </tr>
    <?php
    } else if($vehicle_information['insurance_type'] == COMPREHENSIVE_CTPL){ ?>
    <tr>
        <td>
            Compulsary Third Party Liability
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($ctpl, 2, '.', ',')?></td>
    </tr>
    <tr>
        <td>
            Own Damage and Theft
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($ownDamageCoverage, 2, '.', ',')?></td>
    </tr>
    <tr>
        <td>
            Bodily Injury
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php 500,000.00</td>
    </tr>
    <tr>
        <td>
            Property Damage
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php 500,000.00</td>
    </tr>
    <tr>
        <td>
            Auto Personal Accident
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($autoPersonalAccidentCoverage, 2, '.', ',')?></td>
    </tr>
    <?php
    } else if($vehicle_information['insurance_type'] == COMPREHENSIVE_AON){ ?>
    <tr>
        <td>
            Own Damage and Theft
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($ownDamageCoverage, 2, '.', ',')?></td>
    </tr>
    <tr>
        <td>
            Bodily Injury
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php 500,000.00</td>
    </tr>
    <tr>
        <td>
            Property Damage
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php 500,000.00</td>
    </tr>
    <tr>
        <td>
            Auto Personal Accident
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php '.number_format($data['autoPersonalAccident'], 2, '.', ',').'</td>
    </tr>
    <tr>
        <td>
            Acts of Nature
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php '.number_format($data['aon'], 2, '.', ',').'</td>
    </tr>
    <?php
    } else if($vehicle_information['insurance_type'] == COMPREHENSIVE_CTPL_AON){ ?>
    <tr>
        <td>
            Compulsary Third Party Liability
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($ctpl, 2, '.', ',')?></td>
    </tr>
    <tr>
        <td>
            Own Damage and Theft
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($ownDamageCoverage, 2, '.', ',')?></td>
    </tr>
    <tr>
        <td>
            Bodily Injury
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php 500,000.00</td>
    </tr>
    <tr>
        <td>
            Property Damage
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php 500,000.00</td>
    </tr>
    <tr>
        <td>
            Auto Personal Accident
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($autoPersonalAccidentCoverage, 2, '.', ',')?></td>
    </tr>
    <tr>
        <td>
            Acts of Nature
        </td>
        <td>: &nbsp;&nbsp;&nbsp;&nbsp;Php <?=number_format($aon, 2, '.', ',')?></td>
    </tr>
    <?php
    } ?>

    <tr>
        <td><br />&nbsp;&nbsp;&nbsp;&nbsp;<em>Annual Premium</em><br /></td>
        <td><br />: &nbsp;&nbsp;&nbsp;&nbsp;<b>Php <?=number_format($premium, 2, '.', ',')?></b><br /></td>
    </tr>
    <?php if($vehicle_information['insurance_type'] != CTPL){ ?>
    <tr>
        <td><br />&nbsp;&nbsp;&nbsp;&nbsp;<em>Deductible</em><br /></td>
        <td><br />: &nbsp;&nbsp;&nbsp;&nbsp;<b>Php <?=number_format($deductible, 2, '.', ',')?></b><br /></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;<em>* AUPA - for <?=$seating_capacity?> passengers including the driver</em></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;
            <em>> Accidental Death & Disablement (A&D) – Php 50,000.00 each</em>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;
            <em>> Medical Reimbursement (MER) – Php 5,000.00 each</em>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;<em>* With 24-hour Road Assistance Protection (KALAKBAY)</em></td>
    </tr>
    <?php } ?>
    <tr>
        <td colspan="2"><br /><br />
            The above quotation is subject to standard policy terms and conditions. Please call our customer service representative at 811-1788.
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center"><br /><br />**This is a computer generated form and does not require any signature**</td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <br />
            <img src="<?=imgUrl('slogan.png')?>" width="500" />
        </td>
    </tr>
</table>