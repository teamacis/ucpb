<h2>Personal Information</h2>

<h3>Agent Information</h3>
<strong>Do you have an agent with UCPB Gen?</strong><br>
<?= $agent['with_agent']== 'on' ? 'Yes' : 'No'; ?>

<?php if($agent['with_agent']== 'Yes'): ?>
<strong>Agent Type</strong><br>
<?= $agent['agent_type']; ?>

<?php if($agent['agent_type ']== 'Agent Code'): ?>
<strong>Agent Code</strong><br>
<?= $agent['agent_code']; ?>
<?php else: ?>
<strong>Agent Name</strong><br>
<?= $agent['agent_fname']; ?> <?= $agent['agent_lname']; ?>
<?php endif; ?>
<br><br>
<?php endif; ?>

<br>
<h3>Personal Information</h3>
<strong>Salutation</strong><br>
<?= $personal_information['salutation']; ?>
<br><br>
<strong>Last Name</strong><br>
<?= $personal_information['lname']; ?>
<br><br>
<strong>First Name</strong><br>
<?= $personal_information['fname']; ?>
<br><br>
<strong>Middle Initial</strong><br>
<?= $personal_information['mname']; ?>
<br><br>
<strong>Suffix</strong><br>
<?= $personal_information['suffix']; ?>
<br><br>
<strong>Gender</strong><br>
<?= $personal_information['gender']; ?>
<br><br>
<strong>Birth Date</strong><br>
<?= $personal_information['birthdate']; ?>
<br><br>
<strong>Nationality</strong><br>
<?= $personal_information['nationality']; ?>
<br><br>
<strong>Occupation</strong>
<?= $personal_information['occupation']; ?>
<br><br>
<?php if($personal_information['with_tin']): ?>
<strong>Tin</strong><br>
<?= $personal_information['tin']; ?>
<?php else: ?>
<strong>ID Type</strong><br>
<?= $personal_information['id_type']; ?>
<br><br>
<strong>ID Number</strong><br>
<?= $personal_information['id_number']; ?>
<?php endif; ?>

<br>
<h3>Mailing Address</h3>
<strong>Same location with my car</strong><br>
<?= $same_location == 'on' ? 'Yes' : 'No'; ?>
<br><br>

<strong>Province</strong><br>
<?= $this->place->get_by_field('pk', $vehicle_information['address']['province'])->place; ?>
<br><br>
<strong>City</strong><br>
<?= $this->place->get_by_field('pk', $vehicle_information['address']['city'])->place; ?>
<br><br>
<strong>Barangay</strong><br>
<?= $this->place->get_by_field('pk', $vehicle_information['address']['barangay'])->place; ?>
<br><br>
<strong>House No., Street Name, Subdivision Name</strong><br>
<?= $mailing_address['house_number']; ?>

<br>
<h3>Contact Information</h3>
<strong>Email Address</strong><br>
<?= $contact_information['email_address']; ?>
<br><br>
<strong>Telephone</strong><br>
<?= $contact_information['telephone']; ?>
<br><br>
<strong>Mobile</strong><br>
<?= $contact_information['mobile']; ?>
<br><br>
<strong>Delivery Policy</strong><br>
<?= $delivery == 'on' ? 'Yes' : 'No'; ?>