<h2>Coverage Details</h2>

<?php if ($vehicle_information['insurance_type'] != CTPL) {?>
<strong>Comprehensive Policy Period</strong><br><br>
Start Date <br>
<?= $coverage_details['comprehensive_start_date']; ?>
<br><br>
End Date <br>
<?= $coverage_details['comprehensive_end_date']; ?>
<br><br><br>
<?php } ?>


<?php if ($vehicle_information['insurance_type'] != COMPREHENSIVE && $vehicle_information['insurance_type'] != COMPREHENSIVE_AON) { ?>
<strong>CTPL Policy Period</strong><br><br>
Start Date <br>
<?= $coverage_details['ctpl_start_date']; ?>
<br><br>
End Date <br>
<?= $coverage_details['ctpl_end_date']; ?>
<?php } ?>