<h2>Quote Request</h2>
<?php //dd($personal_information); ?>
<h3>Personal Information</h3>
<strong>Last Name</strong><br>
<?= $personal_information['lname']; ?>
<br><br>
<strong>First Name</strong><br>
<?= $personal_information['fname']; ?>

<br>
<h3>Contact Information</h3>
<strong>Email Address</strong><br>
<?= $contact_information['email_address']; ?>
<br><br>
<strong>Telehpone Number</strong><br>
<?= $contact_information['telephone']; ?>
<br><br>
<strong>Mobile Number</strong><br>
<?= $contact_information['mobile']; ?>

<br>
<h3>Location of Vehicle</h3>
<strong>Province</strong><br>
<?= $this->place->get_by_field('pk', $vehicle_information['address']['province'])->place; ?>
<br><br>
<strong>City</strong><br>
<?= $this->place->get_by_field('pk', $vehicle_information['address']['city'])->place; ?>
<br><br>
<strong>Barangay</strong><br>
<?= $this->place->get_by_field('pk', $vehicle_information['address']['barangay'])->place; ?>
<?//= $locationOfVehicle; ?>

<br>
<h3>Vehicle Information</h3>
<strong>Insurance Type</strong><br>
<?= unserialize(INSURANCE_LABELS)[$vehicle_information['insurance_type']]; ?>

<?php if($vehicle_information['insurance_type'] != COMPREHENSIVE && $vehicle_information['insurance_type'] != COMPREHENSIVE_AON): ?>
<br><br>
<strong>Is your car brand new?</strong><br>
<?= $vehicle_information['is_new'] == 1 ? 'Yes' : 'No'; ?>
<?php endif; ?>

<br><br>
<strong>Year</strong><br>
<?= $vehicle_information['year']; ?>

<br><br>
<strong>Brand</strong><br>
<?= $vehicle_information['brand']; ?>

<br><br>
<strong>Model</strong><br>
<?= $vehicle_information['model_name'] != 'Other' ? $vehicle_information['model_name'] : $vehicle_information['other_model']; ?>

<br><br>
<strong>Market Value</strong><br>
<?= $vehicle_information['market_value']; ?>

<br><br>
<strong>Original Market Value</strong><br>
<?= $vehicle_information['original_market_value']; ?>