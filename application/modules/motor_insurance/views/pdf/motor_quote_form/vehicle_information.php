<h2>Vehicle Information</h2>

<h3>Information</h3>
<strong>Year</strong><br>
<?= $vehicle_information['year']; ?>
<br><br>
<strong>Brand</strong><br>
<?= $vehicle_information['brand']; ?>
<br><br>
<strong>Model Name</strong><br>
<?= $vehicle_information['model_name']; ?>
<br><br>
<strong>Seating Capacity</strong><br>
<?= $seating_capacity; ?>
<br><br>
<strong>Market Value</strong><br>
<?= $vehicle_information['market_value']; ?>

<br>

<?php if($vehicle_information['insurance_type'] != 'CTPL'): ?>

<h3>Accessories</h3>
<strong>Other Standard(built-in)</strong><br>
<?php if(isset($vehicle_information['accessory'])): ?>
<?php foreach($vehicle_information['accessory'] as $accessory): ?>
<?= $accessory; ?>
<br>
<?php endforeach; ?>
<?php endif; ?>
<br>
<strong>Non Standard(non-built-in)</strong><br>
<?php if(isset($vehicle_information['non_standard_accessories'])): ?>
<?php foreach($vehicle_information['non_standard_accessories'] as $key =>$accessory): ?>
<?= $accessory; ?> - P<?= $vehicle_information['non_standard_values'][$key]; ?>
<br>
<?php endforeach; ?>
<?php endif; ?>
<?php endif; ?>

<h3>Other Information</h3>
<strong>Plate Number</strong><br>
<?= $vehicle_information['plate_number']; ?>
<br><br>
<strong>Engine Number</strong><br>
<?= $vehicle_information['engine_number']; ?>
<br><br>
<strong>Chassis Number</strong><br>
<?= $vehicle_information['chassis_number']; ?>
<br><br>
<strong>MV File Number</strong><br>
<?= $vehicle_information['mv_file_number']; ?>
<br><br>
<strong>Color</strong><br>
<?= $vehicle_information['color']; ?>
<?php if($vehicle_information['insurance_type'] != CTPL): ?>
<br><br>
<strong>Mortgagee</strong><br>
<?php if(isset($vehicle_information['mortgagees_pk'])): ?>
<?= $this->mortgagee->get_by_field('pk', $vehicle_information['mortgagees_pk'])->mortgagee; ?>
<?php endif; ?>
<?php endif; ?>
<br>

<h3>Previous Insurance Provider</h3>
<?php if(isset($vehicle_information['insurance_providers_pk'])): ?>
<?= $this->insurance_provider->get_by_field('pk', $vehicle_information['insurance_providers_pk'])->provider; ?>
<?php endif; ?>
<br><br>

<h3>OR/CR</h3>
<?php if ($insurance_type != COMPREHENSIVE && $insurance_type != COMPREHENSIVE_AON): ?>
<img src="<?php echo fileUrl('orcr/'.$vehicle_information['or_cr']); ?>" width="200"/>
<?php endif; ?>