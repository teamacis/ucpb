<div class="col-md-12 col-sm-12 full_length">
    <div class="col-md-12 col-xs-12 title_bar">
        <h3>Summary &amp; Confirmation</h3>
        <p></p>
    </div>
    <!-- quote summary -->
    <form class="form_style" action="<?=site_url('motor-insurance/send_to_dragonpay/' .  $this->uri->segment(3))?>" method="post">
        <div class="new_row">
            <div class="col-md-5">
                <table class="table summary_table table_3">
                    <thead>
                    <tr>
                        <th colspan="2">Personal Information</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><strong>Name:</strong><span class="value_2"><?=$data->name?></span></td>
                    </tr>
                    </tbody>
                </table>
                <table class="table summary_table table_3">
                    <thead>
                    <tr>
                        <th colspan="2">Location of Vehicle</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><strong>City:</strong><span class="value_2"><?=$data->locationOfVehicle?></span></td>
                    </tr>
                    </tbody>
                </table>
                <table class="table summary_table table_3">
                    <thead>
                    <tr>
                        <th colspan="2">Contact Information</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><strong>Telephone Number:</strong><span class="value_1"><?=$data->contact_information['telephone']?></span></td>
                    </tr>
                    <tr>
                        <td><strong>Mobile Number:</strong><span class="value_1"><?=$data->contact_information['mobile']?></span></td>
                    </tr>
                    <tr>
                        <td><strong>Email:</strong><span class="value_1"><?=$data->contact_information['email_address']?></span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-7">
                <table class="table summary_table table_3">
                    <thead>
                    <tr>
                        <th colspan="4">Vehicle Information</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="2"><strong>Type of insurance:</strong>
                            <span class="value_2">
                                <?php
                                    if($data->vehicle_information['insurance_type'] == COMPREHENSIVE){
                                        echo "Comprehensive";
                                    }else if($data->vehicle_information['insurance_type'] == CTPL){
                                        echo "CTPL";
                                    }else if($data->vehicle_information['insurance_type'] == COMPREHENSIVE_CTPL){
                                        echo "Comprehensive and CTPL";
                                    }else if($data->vehicle_information['insurance_type'] == COMPREHENSIVE_AON){
                                        echo "Comprehensive with AON";
                                    }else if($data->vehicle_information['insurance_type'] == COMPREHENSIVE_CTPL_AON){
                                        echo "Comprehensive, CTPL, and AON";
                                    }
                                ?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Year:</strong><span class="value_2"><?=$data->year?></span></td>
                    </tr>
                    <tr>
                        <td><strong>Brand:</strong><span class="value_1"><?=$data->brand?></span></td>
                        <td><strong>Model:</strong><span class="value_1"><?=$data->model_name?></span></td>
                    </tr>
                    <?php if ($data->vehicle_information['insurance_type'] != CTPL) { ?>
                    <tr>
                        <td colspan="2"><strong>Market Value:</strong><span class="value_1">Php <?=number_format($data->vehicle_information['market_value'], 2, '.', ',')?></span></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <table class="table summary_table table_3">
                    <thead>
                    <tr>
                        <th colspan="4">Coverage</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if($data->vehicle_information['insurance_type'] != COMPREHENSIVE_AON && $data->vehicle_information['insurance_type'] != COMPREHENSIVE){ ?>
                        <tr>
                            <td colspan="2"><strong>Compulsary Third Party Liability Policy Period:</strong><span class="value_1"><?=date('F d, Y', strtotime($data->coverage_details['ctpl_start_date']))?> - <?=date('F d, Y', strtotime($data->coverage_details['ctpl_end_date']))?></span></td>
                        </tr>
                    <?php } ?>

                    <?php if($data->vehicle_information['insurance_type'] != CTPL){ ?>
                        <tr>
                            <td colspan="2"><strong>Comprehensive Policy Period:</strong><span class="value_1"><?=date('F d, Y', strtotime($data->coverage_details['comprehensive_start_date']))?> - <?=date('F d, Y', strtotime($data->coverage_details['comprehensive_end_date']))?></span></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="new_row row_pad">
            <div class="col-md-6 summary_section table_type_2">
                <div class="section_title title_1">
                    <h5>Coverage</h5>
                </div>
                <div class="col-md-12 no_gutter">
                    <table class="table summary_table table_2">
                        <colgroup>
                            <col width="60%" />
                            <col width="40%" />
                        </colgroup>
                        <thead>
                        <tr>
                            <th colspan="2">Limit of Liability</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $this->load->view('blocks/confirmation_coverages')?>
                        </tbody>
                    </table>
                    <?php if ($data->vehicle_information['insurance_type'] != CTPL) { ?>
                    <div class="disclaimer_section">
                        <p><strong>Deductible</strong> Php <?=number_format($data->deductible, 2, '.', ',')?></p>
                        <p>
                            <strong>Non-standard Accessories</strong> (Note all declared accessories not factory installed)
                        </p>
                        <?php if($data->vehicle_information['insurance_type'] != CTPL && isset($data->vehicle_information['non_standard']) && count($data->vehicle_information['non_standard']) > 0): ?>
                        <?= "<p>" ?>
                        <?php foreach($data->vehicle_information['non_standard'] as $key => $accessory): ?>
                                    <?= $accessory['non_standard_accessories'] ."<br>" ?>
                            <?php endforeach; ?>
                        <?= "</p>" ?>
                        <?php endif; ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-6 summary_section table_type_1">
                <div class="section_title title_1">
                    <h5>Premium Computation</h5>
                </div>
                <div class="col-md-12 no_gutter">
                    <table class="table summary_table table_1">
                        <colgroup>
                            <col width="50%" />
                            <col width="50%" />
                        </colgroup>
                        <?php $this->load->view('blocks/confirmation_premium'); ?>
                        <tfoot>
                        <tr>
                            <th>Total Amount Due</th>
                            <td><span>Php <?=number_format($data->premium, 2, '.', ',')?></span></td>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="disclaimer_section">
                        <h6>Documentary Stamps Tax</h6>
                        <p>
                            Due to BIR implementation of EDST(Electronic Documentary Stamp Tax) system effective July 1, 2010, policy holders are mandated to pay the DST portion of the premium once the policy is issued. Refund on DST for cancelled policies is not allowed.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="new_row row_pad terms_conditions">
            <div class="checkbox">
                <label>
                    <a href="#" data-toggle="modal" data-target="#myModal">Terms and Conditions</a>
                    <!-- <input type="checkbox"> Accept <a href="#" data-toggle="modal" data-target="#myModal">Terms and Conditions</a> -->
                </label>
            </div>
        </div>
        <p class="form_button">
            <button type="submit" class="btn btn-success btn-lg" disabled>Buy</button>
            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal2">Cancel</button>
        </p>
        <!-- /quote summary -->
    </form>

    <!-- Ternms and Conditions modal -->
    <div class="modal fade terms_condition_modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="modal_close_btn" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                        <img src="<?=base_url('assets/images/site_logo2.png')?>" class="logo" />
                        <span>Terms and Conditions</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="tc_content" style="overflow: hidden;">
                        <iframe src="<?=base_url('files/policies/pc/pc.htm')?>" border="0" width="100%" height="100%"></iframe>
                    </div>
                </div>
                <div align="center">
                    <a href="" data-dismiss="modal" onclick="acceptTerms()"><b>Accept Terms and Conditions</b></a><br /><br />
                </div>
            </div>
        </div>
    </div>
    <!-- cancel modal -->
    <div class="modal fade cancel_modal bs-example-modal-sm" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="modal_close_btn" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body">
                    <p class="message">Are you sure you want to cancel your transaction?</p>
                    <p class="form_button">
                        <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal" onclick="cancelTransaction()" >Yes</button><br><br>
                        <button type="button" class="btn btn-success btn-lg"  data-dismiss="modal" aria-label="Close">No</button>   
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function() {

    });

    function acceptTerms(){
        $('[type=submit]').removeAttr('disabled');
    }

    function cancelTransaction(){
        window.location.href = '<?=site_url('motor-insurance')?>';
    }
</script>
