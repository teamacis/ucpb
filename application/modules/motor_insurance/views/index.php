<div class="s_content col-md-6 gq_intro">
    <?php echo form_open('motor_insurance'); ?>
        <h4>This website only issues quotations for private cars.</h4>
        <div class="intro_details">
            <div class="checkbox">
                <label><input type="checkbox" name="confirm" value="1"> I confirm that unit for quotation is <span class="hl">not</span> used to carry fare paying passengers.</label>
            </div>
        </div>
        <div class="intro_box">
            <h5>Example of units <span class="hl">not</span> considered as Private Cars:</h5>
            <ul>
                <li>Yellow plated vehicles such as Taxi and U/V Express Van</li>
                <li>School bus / Van</li>
            </ul>
        </div>
        <p class="form_button">
            <input type="submit" class="btn btn-success btn-lg" name="submit" value="Get Quote">
        </p>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
    <?php if ($this->session->flashdata('error')) { ?>
    common.viewDialog('<?=$this->session->flashdata('error')?>');
    <?php } ?>
    function go_to_get_quote() {
        window.location.href = siteUrl + 'motor-insurance/get_quote';
    }
</script>