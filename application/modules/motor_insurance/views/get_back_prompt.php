<div class="s_content col-md-6">
    <h4>Thank you for requesting a quote from us!</h4>
    <div class="final_message">
        <p>A representative of UCPB Gen will get back to you as soon as possible.</p>
    </div>
    <p class="form_button">
        <button type="button" class="btn btn-success btn-lg" onclick="window.location.href = '<?=site_url('motor-insurance')?>'">Home</button>
    </p>
</div>