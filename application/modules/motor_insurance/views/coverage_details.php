<?php $this->load->view('user/quote_sidebar', array('tab' => 'motor_insurance'));?>
<div class="col-md-9 col-sm-9" ng-controller="coverageDetailsCtrl">
    <div class="col-md-12 col-xs-12 title_bar" >
        <h3>COVERAGE DETAILS</h3>
        <p></p>
    </div>

    <!-- CONTENT FIELD -->
    <div class="col-md-12 content_field">

        <form class="form_style" method="POST" action="<?= site_url('motor-insurance/coverage_details/' . $this->uri->segment(3)); ?>">
            <!-- POLICY PERIOD (Compre and AON) -->
            <div class="alert alert-warning hidden no-loss" role="alert">Selecting dates before the current date gives you warranted no loss</div>
            <?php if ($vehicle_information['insurance_type'] != CTPL) {?>
            <div class="form_content">
                <div class="line_title"><p>Comprehensive Policy Period</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control calendar_input required" placeholder="Start Date" readonly name="coverage_details[comprehensive_start_date]" value="<?= set_value('coverage_details[comprehensive_start_date]') ?>" />
                        <?= form_error('coverage_details[comprehensive_start_date]'); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control" value="12:00 P.M." readonly="readonly" />
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control calendar_input required" placeholder="End Date" readonly name="coverage_details[comprehensive_end_date]" value="<?= set_value('coverage_details[comprehensive_end_date]') ?>" />
                        <?= form_error('coverage_details[comprehensive_end_date]'); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control" value="12:00 P.M." readonly="readonly" />
                    </div>
                </div>
            </div>
            <?php } ?>
            <!-- /END OF POLICY PERIOD (Compre and AON)  -->
            <!-- CTPL POLICY PERIOD -->
            <?php if ($vehicle_information['insurance_type'] != COMPREHENSIVE && $vehicle_information['insurance_type'] != COMPREHENSIVE_AON) {?>
            <div class="form_content">
                <div class="line_title"><p>CTPL Policy Period</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control calendar_input required" placeholder="Start Date" readonly name="coverage_details[ctpl_start_date]" value="<?= set_value('coverage_details[ctpl_start_date]') ?>" />
                        <?= form_error('coverage_details[ctpl_start_date]'); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control" value="12:00 P.M." readonly="readonly" />
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control calendar_input required" placeholder="End Date" readonly name="coverage_details[ctpl_end_date]" value="<?= set_value('coverage_details[ctpl_end_date]') ?>" />
                        <?= form_error('coverage_details[ctpl_end_date]'); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control" value="12:00 P.M." readonly="readonly" />
                    </div>
                </div>
            </div>
            <?php } ?>
            <!-- /END OF CTPL POLICY PERIOD -->
            <p class="form_button">
                <button type="button" class="btn btn-success btn-lg back">Back</button>
                <button type="submit" class="btn btn-success btn-lg">Next</button>
            </p>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("[name='coverage_details[comprehensive_start_date]']").datepicker({ dateFormat: 'yy-mm-dd', });
        <?php
            if ($vehicle_information['is_new']) {
        ?>
        $("[name='coverage_details[ctpl_start_date]']").datepicker({ dateFormat: 'yy-mm-dd', });
        <?php } ?>


        /**
         * Comprehensive on change
         */
        $("[name='coverage_details[comprehensive_start_date]']").change(function() {
            // Add 1 year
            var newDate = moment($(this).val(), 'YYYY-MM-DD').add(1, 'year');
            var curDate = moment();
            if (moment($(this).val()) < moment(curDate.format('YYYY-MM-DD'))) {
                $('.no-loss').removeClass('hidden');
            } else {
                $('.no-loss').addClass('hidden');
            }
            // Update End Date
            $("[name='coverage_details[comprehensive_end_date]']").val(newDate.format('YYYY-MM-DD'));
        });

        /**
         * CTPL on change
         */
        $("[name='coverage_details[ctpl_start_date]']").change(function() {
            var newVehicle = '<?= $vehicle_information['is_new']; ?>';
            var yearsToAdd = newVehicle == 1 ? 3 : 1;
            var newDate;

            // Add year
            newDate = moment($(this).val(), 'YYYY-MM-DD').add(yearsToAdd, 'year');
            var curDate = moment();
            if (moment($(this).val()) < moment(curDate.format('YYYY-MM-DD'))) {
                $('.no-loss').removeClass('hidden');
            } else {
                $('.no-loss').addClass('hidden');
            }

            // Update End Date
            $("[name='coverage_details[ctpl_end_date]']").val(newDate.format('YYYY-MM-DD'));
        });

        $('.back').click(function() {
            window.location.href = '<?=site_url('motor-insurance/vehicle_information/'.$this->uri->segment(3))?>';
        });
    });
</script>