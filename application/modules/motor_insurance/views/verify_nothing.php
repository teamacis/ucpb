<div class="container motor_content pass_page box">
    <div class="row no_gutter">
        <form method="POST" action="<?=site_url('client/clientrequest/login?return_uri=' . $this->input->get('return_uri'))?>">
            <div class="s_content col-md-6">
                <div class="acc_details">
                    <p>Your account has already been verified.</p>
                </div>
                <div class="acc_box">
                    <?= $this->session->flashdata('form_error'); ?>
                    <p><span class="glyphicon glyphicon-lock"></span> User Login</p>
                    <div class="col-md-12 form_input">
                        <input type="text" class="form-control" name="username" placeholder="Username" />
                        <?= form_error('username'); ?>
                    </div>
                    <div class="col-md-12 form_input">
                        <input type="password" class="form-control" name="password" placeholder="Password" />
                        <?= form_error('password'); ?>
                    </div>
                </div>
                <p class="form_button">
                    <button type="submit" class="btn btn-success btn-lg">Login</button>
                    <a href="#" class="pass_forgot">Did you forget your Password?</a>
                </p>
            </div>
        </form>
    </div>
</div>