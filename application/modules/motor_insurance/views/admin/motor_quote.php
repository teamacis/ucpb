<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$title?></title>
    <link href="<?=cssUrl("bootstrap.min.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("style.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("font-face.css")?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <?php if(isset($scriptsIE)) echo $scriptsIE;?>
    <![endif]-->
    <?php if(isset($scripts)) echo $scripts;?>
</head>
<body>
<div id="content"><br />
    <div class="container motor_content box">
        <div class="row no_gutter">
            <div class="col-md-12 col-sm-12 full_length">
                <div class="col-md-12 col-xs-12 title_bar">
                    <h3>QUOTE SUMMARY</h3>

                    <div class="pull-right" style="margin-top: -13px; margin-left: 10px;">
                        <a href="javascript:void(0)" id="view-pdf">View PDF</a>
                    </div>
                    <div class="pull-right" style="margin-top:-25px; width: 250px;">
                        <select name="pdf" class="form-control chosen-select" data-placeholder="Select a PDF">
                            <option value=""></option>
                            <?php if(file_exists(FORMAL_QUOTES_FOLDER.$data->quote_id.'.pdf')) { ?>
                                <option value="<?= FORMAL_QUOTES_FOLDER.$data->quote_id.'.pdf' ?>">Formal Quote</option>
                            <?php } ?>

                            <?php if($data->status != 'ACTIVE') { ?>
                                <?php if(file_exists(MOTOR_QUOTE_FILES_QUOTE_REQUEST.'quote-request-'.$data->quote_id.'.pdf')) { ?>
                                    <option value="<?= MOTOR_QUOTE_FILES_QUOTE_REQUEST.'quote-request-'.$data->quote_id.'.pdf' ?>">Quote Request</option>
                                <?php } ?>

                                <?php if(file_exists(MOTOR_QUOTE_FILES_BANK_CERTIFICATE.'bank-certificate-'.$data->quote_id.'.pdf')) { ?>
                                    <option value="<?= MOTOR_QUOTE_FILES_BANK_CERTIFICATE.'bank-certificate-'.$data->quote_id.'.pdf' ?>">Bank Certificate</option>
                                <?php } ?>

                                <?php if(file_exists(MOTOR_QUOTE_CONFIRMATION_OF_COVER.'confirmation-of-cover-'.$data->quote_id.'.pdf')) { ?>
                                    <option value="<?= MOTOR_QUOTE_CONFIRMATION_OF_COVER.'confirmation-of-cover-'.$data->quote_id.'.pdf' ?>">Confirmation of Cover</option>
                                <?php } ?>

                                <?php if ($data->insurance_type == CTPL) { ?>
                                    <?php if(file_exists(POLICIES_FOLDER.'policy-schedule-'.$data->quote_id.'-ctpl.pdf')) { ?>
                                        <option value="<?= POLICIES_FOLDER.'policy-schedule-'.$data->quote_id.'-ctpl.pdf' ?>">Policy Schedule - CTPL</option>
                                    <?php } ?>
                                <?php } else if ($data->insurance_type == COMPREHENSIVE_AON || $data->insurance_type == COMPREHENSIVE) { ?>

                                    <?php if(file_exists(POLICIES_FOLDER.'policy-schedule-'.$data->quote_id.'-comprehensive.pdf')) { ?>
                                        <option value="<?= POLICIES_FOLDER.'policy-schedule-'.$data->quote_id.'-comprehensive.pdf' ?>">Policy Schedule - Comprehensive</option>
                                    <?php } ?>

                                <?php } else { ?>

                                    <?php if(file_exists(POLICIES_FOLDER.'policy-schedule-'.$data->quote_id.'-ctpl.pdf')) { ?>
                                        <option value="<?= POLICIES_FOLDER.'policy-schedule-'.$data->quote_id.'-ctpl.pdf' ?>">Policy Schedule - CTPL</option>
                                    <?php } ?>

                                    <?php if(file_exists(POLICIES_FOLDER.'policy-schedule-'.$data->quote_id.'-comprehensive.pdf')) { ?>
                                        <option value="<?= POLICIES_FOLDER.'policy-schedule-'.$data->quote_id.'-comprehensive.pdf' ?>">Policy Schedule - Comprehensive</option>
                                    <?php } ?>

                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <!-- CONTENT FIELD -->
                <div class="col-md-12 content_field summary_field">
                    <!-- PREMIUM COMPUTATION -->
                    <div class="summary_info computation">
                        <div class="summary_title title-1"><h4>Premium Computation</h4></div>
                        <div class="summary_content">
                            <?php $this->load->view('motor_insurance/blocks/premium'); ?>
                        </div>
                        <div class="summary_note">
                            <div class="col-md-8">
                                <strong>Documentary Stamps Tax</strong>
                                <p>Due to BIR implementation of EDST(Electronic Documentary Stamp Tax) system effective July 1, 2010, policy holders are mandated to pay the DST portion of the premium once the policy is issued. Refund on DST for cancelled policies is not allowed.</p>
                            </div>
                        </div>
                    </div>
                    <!-- END OF PREMIUM COMPUTATION -->
                    <!-- COVERAGE -->
                    <div class="summary_info coverage">
                        <div class="summary_title title-1"><h4>Coverage</h4></div>
                        <div class="summary_content">
                            <div class="col-md-8 summary_group">
                                <div class="summary_line">
                                    <div class="col-md-12 info_field"><strong class="label_item">Limit of Liability</strong></div>
                                </div>
                                <?php $this->load->view('motor_insurance/blocks/coverages'); ?>
                            </div>
                        </div>
                        <div class="summary_note">
                            <div class="col-md-8">
                                <p><strong>Deductible</strong>
                                    <span>
                                        Php <?php number_format($data->deductible, 2, '.', ','); ?>
                                    </span>
                                </p>
                                <p><strong>Non-standard Accessories</strong><span>(Note all declared accessories not factory installed)</span></p>
                            </div>
                        </div>
                    </div>
                    <!-- END OF COVERAGE -->
                    <!-- INFORMATION -->
                    <div class="summary_info insurance_info">
                        <div class="summary_title title-2"><h4>Insured and Vehicle Information</h4></div>
                        <div class="summary_content">
                            <div class="col-md-8 summary_group">
                                <div class="line_title"><p>Personal Information</p></div>
                                <div class="summary_line">
                                    <div class="col-md-12 info_field">
                                        <strong class="label_item">Name:</strong>
                                        <span class="value_item">
                                            <?php echo $data->name; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 summary_group">
                                <div class="line_title"><p>Location of Vehicle</p></div>
                                <div class="summary_line">
                                    <div class="col-md-12 info_field">
                                        <strong class="label_item">Address:</strong>
                                        <span class="value_item">
                                            <?php echo $data->locationOfVehicle; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 summary_group">
                                <div class="line_title"><p>Contact Information</p></div>
                                <div class="summary_line">
                                    <div class="col-md-12 info_field">
                                        <strong class="label_item">Telephone Number:</strong>
                                        <span class="value_item">
                                            <?php echo $data->telephone; ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="summary_line">
                                    <div class="col-md-12 info_field">
                                        <strong class="label_item">Mobile Number:</strong>
                                        <span class="value_item">
                                            <?php echo $data->mobile; ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="summary_line">
                                    <div class="col-md-12 info_field">
                                        <strong class="label_item">Email:</strong>
                                        <span class="value_item">
                                            <?php echo $data->email_address; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 summary_group">
                                <div class="line_title"><p>Vehicle Information</p></div>
                                <div class="summary_line">
                                    <div class="col-md-12 info_field"><strong class="label_item">Type of insurance:</strong>
                            <span class="value_item">
                                <?php echo unserialize(INSURANCE_LABELS)[$data->insurance_type]; ?>
                            </span>
                                    </div>
                                </div>
                                <div class="summary_line">
                                    <div class="col-md-12 info_field">
                                        <strong class="label_item">Year:</strong>
                                        <span class="value_item">
                                            <?php echo $data->year; ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="summary_line">
                                    <div class="col-md-6 info_field">
                                        <strong class="label_item">Brand:</strong>
                                        <span class="value_item">
                                            <?php echo $data->brand; ?>
                                        </span>
                                    </div>
                                    <div class="col-md-6 info_field">
                                        <strong class="label_item">Model:</strong>
                                        <span class="value_item">
                                            <?php echo $data->model_name; ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="summary_line">
                                    <div class="col-md-12 info_field">
                                        <strong class="label_item">Market Value:</strong>
                                        <span class="value_item">
                                            Php <?php echo number_format($data->market_value, 2, '.', ','); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END OF INFORMATION -->
                </div>
            </div>
        </div>
    </div>
    <script>
        $('.chosen-select').chosen({width:"100%", disable_search_threshold: 10, search_contains: true});

        $("#view-pdf").click(function() {
            var pdfUrl = $('[name=pdf]').val();

            if(pdfUrl) {
                window.open('<?= base_url(); ?>'+pdfUrl);
            }
        })
    </script>
</body>
</html>