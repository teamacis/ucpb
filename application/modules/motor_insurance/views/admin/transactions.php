<script>
    $(document).ready(function() {
        /**
         * Filter by Status
         */
        $('[name=status]').change(function() {
            var status = $(this).val() == 'ALL' ? '' : '/'+$(this).val();
            window.location.href = "<?= site_url('admin/transactions'); ?>"+status;
        });

        /**
         * Update Status
         */
        $('.update-status').click(function() {
            var form   = $(this).parents('form');
            var status = $(this).attr('data-value');
            form.find('[name=status]').val(status);
            form.submit();
        });

        var pageOne = $('.table_pagination').find('a').eq(0);
        pageOne.attr('href', '?per_page=1');
    });
</script>


<div id="content" class="admin_dashboard">
    <!-- page top -->
    <div class="admin_page_top box ">
        <div class="container">
            <h4 class="page_title_top col-md-6">Admin Transactions</h4>
            <div class="site_date col-md-6">
                <div class="date_container">
                    <a href="#" class="btn_info"></a>
                    <div class="date_content"><span class="today_is">Today is:</span><strong class="today_date"><?php echo date('l, F d, Y'); ?></strong></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page top-->
    <!-- additional title -->
    <div class="container additional_title">
        <h5>Transactions</h5>
    </div>
    <!-- / additional title -->
    <div class="container dashboard_content box">
        <div class="row">
            <div class="col-md-1 side_menu">
                <ul class="dash_menu text-hide">
                    <li><a href="<?=site_url(L_DASHBOARD)?>" class="menu_link link_dashboard" title="Dashboard">Dashboard</a></li>
                    <li><a href="<?=site_url(L_TRANSACTIONS_MOTOR)?>" class="menu_link link_transaction" title="Transactions">Transactions</a></li>
                    <li><a href="#" class="menu_link link_reports" title="Reports">Reports</a></li>
                    <li><a href="#" class="menu_link link_resourcecenter" title="Resource Center">Resource Center</a></li>
                    <li><a href="#" class="menu_link link_search" title="Research">Research</a></li>
                </ul>
                <ul class="dash_menu text-hide">
                    <li><a href="#" class="menu_link link_profile" title="Dashboard">My Profile</a></li>
                    <li><a href="#" class="menu_link link_settings" title="Settings">Settings</a></li>
                </ul>
            </div>
            <div class="col-md-11">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#" aria-controls="motor" role="tab" data-toggle="tab"><b>MOTOR</b></a></li>
                    <li role="presentation"><a href="#" aria-controls="itp" role="tab" data-toggle="tab"><b>ITP</b></a></li>
                    <li role="presentation"><a href="#" aria-controls="fire" role="tab" data-toggle="tab"><b>HEP+</b></a></li>
                </ul>
                <!-- Tab panels -->
                <h2>Motor Quote List
                    <div class="pull-right">
                        <!-- <label class="pull-left"><small>Status Filter</small></label> -->
                        <select class="form-control pull-right" name="status">
                            <option <?php if(!$filters['motor_quotes.status']) { ?> selected <?php } ?>>ALL</option>
                            <option <?php if($filters['motor_quotes.status'] == STATUS_ACTIVE) { ?> selected <?php } ?>><?=STATUS_ACTIVE?></option>
                            <option <?php if($filters['motor_quotes.status'] == STATUS_APPROVED) { ?> selected <?php } ?>><?=STATUS_APPROVED?></option>
                            <option <?php if($filters['motor_quotes.status'] == STATUS_DECLINED) { ?> selected <?php } ?>><?=STATUS_DECLINED?></option>
                            <option <?php if($filters['motor_quotes.status'] == STATUS_EXPIRED) { ?> selected <?php } ?>><?=STATUS_EXPIRED?></option>
                            <option <?php if($filters['motor_quotes.status'] == STATUS_FOR_APPROVAL) { ?> selected <?php } ?>><?=STATUS_FOR_APPROVAL?></option>
                            <option <?php if($filters['motor_quotes.status'] == STATUS_PENDING) { ?> selected <?php } ?>><?=STATUS_PENDING?></option>
                        </select>
                    </div>
                </h2><hr />
                <!-- DASHBOARD TABLE DATABASE -->
                <div class="col-md-12 dashtable content_field">
                    <!-- <div class="top_head">
                        <div class="checkbox">
                            <label><input type="checkbox" checked>Select All</label>
                        </div>
                    </div> -->
                    <!-- TABLE HEADER -->
                    <div class="table_head">
                        <div class="col-md-5 col col_first"><strong>Client Name / Transaction ID</strong></div>
                        <div class="col-md-3 col"><strong>Status</strong></div>
                        <div class="col-md-1 col"><strong>View</strong></div>
                        <div class="col-md-1 col"><strong>Action</strong></div>
                        <div class="col-md-2 col"><strong>Date</strong></div>
                    </div>
                    <!-- /TABLE HEADER -->
                    <!-- TABLE CONTENT -->
                    <div class="table_content">

                        <?php if($motors == false || count($motors) == 0) { ?>
                            <div class="table_item">
                                <div class="col-md-12 col">
                                    <span class="tabledate text-danger text-center">No Records Found.</span>
                                </div>
                            </div>
                        <?php } else { ?>
                            <?php foreach($motors as $motor) { ?>
                                <div class="table_item">
                                    <div class="col-md-5 col col_first">
                                        <div class="client">
                                            <div class="client_con">
                                                <p class="client_name">
                                                    <?php if($motor->personal_fname) { ?>
                                                        <?= $motor->personal_fname; ?> <?= $motor->personal_lname; ?>
                                                    <?php } else { ?>
                                                        <?= $motor->fname; ?> <?= $motor->lname; ?>
                                                    <?php } ?>
                                                </p>
                                                <span class="status active">
                                                    <?= $motor->quote_id; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col">
                                        <p class="<?= unserialize(STATUS_CLASS)[$motor->status]; ?>">
                                            <b><?= $motor->status; ?></b>
                                        </p>
                                    </div>
                                    <div class="col-md-1 col">
                                        <a href="<?= site_url("admin/motor_quote/$motor->pk"); ?>" class="btn_view" target="_blank"></a>
                                    </div>
                                    <div class="col-md-1 col">
                                        <?php if($motor->status == STATUS_FOR_APPROVAL) { ?>
                                            <!-- action -->
                                            <form method="POST" action="<?= site_url('admin/adminrequest/update_status/'.$motor->quote_id); ?>">
                                                <input type="hidden" name="status">
                                                <?php if($motor->personal_email_address) { ?>
                                                    <input type="hidden" name="email_address" value="<?= $motor->personal_email_address; ?>">
                                                <?php } else { ?>
                                                    <input type="hidden" name="email_address" value="<?= $motor->email_address; ?>">
                                                <?php } ?>

                                                <input type="hidden" name="per_page" value="<?= isset($_GET['per_page']) ? $_GET['per_page'] : 1; ?>">
                                                <div class="btn-group action">
                                                    <button type="button" class="btn btn-default dropdown-toggle tabletoggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <img src="<?=imgUrl('icon_action.png')?>" /><span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="javascript:void(0)" data-value="APPROVED" class="update-status">Approve</a></li>
                                                        <li><a href="javascript:void(0)" data-value="DECLINED" class="update-status">Decline</a></li>
                                                    </ul>
                                                </div>
                                            </form>
                                            <!-- /action -->
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-2 col">
                                        <!-- date -->
                                        <span class="tabledate">
                                            <?= $motor->date_created; ?>
                                        </span>
                                        <!-- /date -->
                                    </div>
                                </div>
                            <?php } ?>

                        <?php } ?>

                        <?php if($motors && count($motors) > 0 && $totalPage > 1) { ?>
                        <div class="table_foot">
                            <div class="col-md-1">
                                <?php if(isset($_GET['per_page']) && $_GET['per_page'] > 1) { ?>
                                    <ul class="page_button pull-left">
                                            <li><a href="?per_page=1" class="btn_page btn_prev"><span class="glyphicon glyphicon-triangle-left"></span></a></li>
                                            <li><a href="?per_page=<?= $_GET['per_page'] - 1; ?>" class="btn_page btn_first"><span class="glyphicon glyphicon-backward"></span></a></li>
                                    </ul>
                                <?php } ?>
                            </div>
                            <div class="col-md-10">
                                <div class="table_pagination">
                                    <ul>
                                        <?= $this->pagination->create_links(); ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <?php if(isset($_GET['per_page']) && $_GET['per_page'] < $totalPage) { ?>
                                    <ul class="page_button pull-right">
                                        <li><a href="?per_page=<?= $_GET['per_page'] + 1; ?>" class="btn_page btn_last"><span class="glyphicon glyphicon-forward"></span></a></li>
                                        <li><a href="?per_page=<?= $totalPage; ?>" class="btn_page btn_next"><span class="glyphicon glyphicon-triangle-right"></span></a></li>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <!-- END OF DASHBOARD TABLE DATABASE -->
                </div>
            </div>
        </div>
    </div>