<script>
    $(document).ready(function() {
        /**
         * Filter by Status
         */
        $('[name=status]').change(function() {
            var status = $(this).val() == 'ALL' ? '' : '/'+$(this).val();
            window.location.href = "<?= site_url('admin/delivery'); ?>"+status;
        });

        /**
         * Update Status
         */
        $('.update-status').click(function() {
            var form   = $(this).parents('form');
            var status = $(this).attr('data-value');
            form.find('[name=status]').val(status);
            form.submit();
        });

        var pageOne = $('.table_pagination').find('a').eq(0);
        pageOne.attr('href', '?per_page=1');
    });
</script>


<div id="content" class="admin_dashboard">
    <!-- page top -->
    <div class="admin_page_top box ">
        <div class="container">
            <h4 class="page_title_top col-md-6">Admin Transactions</h4>
            <div class="site_date col-md-6">
                <div class="date_container">
                    <a href="#" class="btn_info"></a>
                    <div class="date_content"><span class="today_is">Today is:</span><strong class="today_date"><?php echo date('l, F d, Y'); ?></strong></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page top-->
    <!-- additional title -->
    <div class="container additional_title">
        <h5>Transactions</h5>
    </div>
    <!-- / additional title -->
    <div class="container dashboard_content box">
        <div class="row">
            <div class="col-md-1 side_menu">
                <ul class="dash_menu text-hide">
                    <li><a href="<?=site_url(L_DASHBOARD)?>" class="menu_link link_dashboard" title="Dashboard">Dashboard</a></li>
                    <li><a href="<?=site_url(L_TRANSACTIONS_MOTOR)?>" class="menu_link link_transaction" title="Transactions">Transactions</a></li>
                    <li><a href="#" class="menu_link link_reports" title="Reports">Reports</a></li>
                    <li><a href="#" class="menu_link link_resourcecenter" title="Resource Center">Resource Center</a></li>
                    <li><a href="#" class="menu_link link_search" title="Research">Research</a></li>
                </ul>
                <ul class="dash_menu text-hide">
                    <li><a href="#" class="menu_link link_profile" title="Dashboard">My Profile</a></li>
                    <li><a href="#" class="menu_link link_settings" title="Settings">Settings</a></li>
                </ul>
            </div>
            <div class="col-md-11">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#" aria-controls="motor" role="tab" data-toggle="tab"><b>MOTOR</b></a></li>
                    <li role="presentation"><a href="#" aria-controls="itp" role="tab" data-toggle="tab"><b>ITP</b></a></li>
                    <li role="presentation"><a href="#" aria-controls="fire" role="tab" data-toggle="tab"><b>HEP+</b></a></li>
                </ul>
                <!-- Tab panels -->
                <h2>
                    Delivery List
                </h2>
                <div class="pull-right" style="margin-top: -40px;">
                    <!-- <label class="pull-left"><small>Status Filter</small></label> -->
                    <select class="form-control pull-right" name="status">
                        <option value="" <?php if($filters['status'] == ''): ?> selected <?php endif; ?>>All</option>
                        <option value="<?= POLICY_NOT_DELIVERED; ?>" <?php if($filters['status'] == POLICY_NOT_DELIVERED): ?> selected <?php endif; ?>>Not Delivered</option>
                        <option value="<?= POLICY_DELIVERED; ?>" <?php if($filters['status'] == POLICY_DELIVERED): ?> selected <?php endif; ?>>Delivered</option>
                    </select>
                </div>
                <hr />
                <!-- DASHBOARD TABLE DATABASE -->
                <div class="col-md-12 dashtable content_field">
                    <!-- <div class="top_head">
                        <div class="checkbox">
                            <label><input type="checkbox" checked>Select All</label>
                        </div>
                    </div> -->
                    <!-- TABLE HEADER -->
                    <div class="table_head">
                        <div class="col-md-4 col col_first"><strong>Client Name / Transaction ID</strong></div>
                        <div class="col-md-2 col"><strong>Agent</strong></div>
                        <div class="col-md-2 col"><strong>Policy ID</strong></div>
                        <div class="col-md-2 col"><strong>Date Delivered</strong></div>
                        <div class="col-md-2 col"><strong>Date Created</strong></div>
                    </div>
                    <!-- /TABLE HEADER -->
                    <!-- TABLE CONTENT -->
                    <div class="table_content">

                        <?php if($policies == false || count($policies) == 0) { ?>
                            <div class="table_item">
                                <div class="col-md-12 col">
                                    <span class="tabledate text-danger text-center">No Records Found.</span>
                                </div>
                            </div>
                        <?php } else { ?>
                            <?php foreach($policies as $policy) { ?>
                                <div class="table_item">
                                    <div class="col-md-4 col col_first">
                                        <div class="client">
                                            <div class="client_con">
                                                <p class="client_name">
                                                    <?= $policy->fname; ?> <?= $policy->lname; ?>
                                                </p>
                                                <span class="status active">
                                                    <?= $policy->quote_id; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col">
                                        <?= $policy->agent_fname; ?> <?= $policy->agent_lname; ?>
                                    </div>
                                    <div class="col-md-2 col policy-id">
                                        <?= $policy->policy_id; ?>
                                    </div>
                                    <div class="col-md-2 col">
                                        <?php if($policy->date_delivered): ?>
                                            <?= substr($policy->date_delivered, 0, -9); ?>
                                        <?php else: ?>
                                            <a href="#" class='update-delivery-date-modal-open-btn' data-toggle="modal" data-target="#update-delivery-date-modal">Update Delivery Date</a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-2 col">
                                        <?= $policy->date_created; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>

                        <?php if($policies && count($policies) > 0 && $totalPage > 1) { ?>
                            <div class="table_foot">
                                <div class="col-md-1">
                                    <?php if(isset($_GET['per_page']) && $_GET['per_page'] > 1) { ?>
                                        <ul class="page_button pull-left">
                                            <li><a href="?per_page=1" class="btn_page btn_prev"><span class="glyphicon glyphicon-triangle-left"></span></a></li>
                                            <li><a href="?per_page=<?= $_GET['per_page'] - 1; ?>" class="btn_page btn_first"><span class="glyphicon glyphicon-backward"></span></a></li>
                                        </ul>
                                    <?php } ?>
                                </div>
                                <div class="col-md-10">
                                    <div class="table_pagination">
                                        <ul>
                                            <?php  echo $this->pagination->create_links(); ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <?php if(isset($_GET['per_page']) && $_GET['per_page'] < $totalPage) { ?>
                                        <ul class="page_button pull-right">
                                            <li><a href="?per_page=<?= $_GET['per_page'] + 1; ?>" class="btn_page btn_last"><span class="glyphicon glyphicon-forward"></span></a></li>
                                            <li><a href="?per_page=<?= $totalPage; ?>" class="btn_page btn_next"><span class="glyphicon glyphicon-triangle-right"></span></a></li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <!-- END OF DASHBOARD TABLE DATABASE -->
                </div>
            </div>
        </div>
    </div>

    <!-- Delivery Date Modal -->
    <div class="modal fade terms_condition_modal delivery-date-modal" id="update-delivery-date-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 500px;">
            <div class="modal-content">
                <button type="button" class="modal_close_btn" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                        <img src="<?=base_url('assets/images/site_logo2.png')?>" class="logo" />
                        <span class="modal-title-text"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <form action="<?= site_url('admin/adminrequest/update_delivery'); ?>" method="POST">
                        <div class="alert alert-danger" style="display: none;">Please specify a delivery address</div>
                        Delivery Date <br>
                        <input type="text" name="date_delivered" class="form-control" readonly><br>
                        <div class="row" style="display: none;">
                            Time <br>
                            <div class="col-md-5">
                                <input type="text" name="" id="" class="form-control" placeholder="Hours (00-24)">
                            </div>
                            <div class="col-md-5">
                                <input type="text" name="" id="" class="form-control" placeholder="Minutes (00-59)">
                            </div>
                        </div>
                        <br>
                        <input type="button" value="Update Delivery Address" class="btn btn-primary btn-sm" name="update-delivery-address-submit-btn">
                        <input type="hidden" name="policy_id">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var modalTitleText = 'Update $policyId delivery date';

            $('[name=date_delivered]').datepicker({
                dateFormat: 'yy-mm-dd',
                autoclose: true,
                maxDate: moment().format('YYYY-MM-DD')
            });

            $('.update-delivery-date-modal-open-btn').click(function() {
                var modalTitleText = 'Update $policyId delivery date';
                var policyId = $(this).parent().siblings('.policy-id').text();

                $('.modal-title-text').text(modalTitleText.replace(/\$policyId/, policyId));
                $('.alert-danger').hide();
                $('[name=date_delivered]').val('');
                $('[name=policy_id]').val($.trim(policyId));
            });

            $('[name=update-delivery-address-submit-btn]').click(function() {
                var date = $.trim($('[name=date_delivered]').val());

                $('.alert-danger').hide();
                if(!date) {
                    $('.alert-danger').show();
                }
                else {
                    $(this).parents('form').submit();
                }
            })
        });
    </script>