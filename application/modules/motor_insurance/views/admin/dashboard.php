<div id="content" class="admin_dashboard">
    <!-- page top -->
    <div class="admin_page_top box ">
        <div class="container">
            <h4 class="page_title_top col-md-6">Dashboard</h4>
            <div class="site_date col-md-6">
                <div class="date_container">
                    <a href="#" class="btn_info"></a>
                    <div class="date_content"><span class="today_is">Today is:</span><strong class="today_date">Monday, July 6, 2015</strong></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page top-->
    <!-- additional title -->
    <div class="container additional_title">
        <h5>Dashboard</h5>
    </div>
    <!-- / additional title -->
    <div class="container dashboard_content box">
        <div class="row no_gutter">
            <!-- SIDE MENU -->
            <div class="col-md-1 side_menu">
                <ul class="dash_menu text-hide">
                    <li><a href="#" class="menu_link link_dashboard" title="Dashboard">Dashboard</a></li>
                    <li><a href="#" class="menu_link link_transaction" title="Transactions">Transactions</a></li>
                    <li><a href="#" class="menu_link link_reports" title="Reports">Reports</a></li>
                    <li><a href="#" class="menu_link link_resourcecenter" title="Resource Center">Resource Center</a></li>
                    <li><a href="#" class="menu_link link_search" title="Research">Research</a></li>
                </ul>
                <ul class="dash_menu text-hide">
                    <li><a href="#" class="menu_link link_profile" title="Dashboard">My Profile</a></li>
                    <li><a href="#" class="menu_link link_settings" title="Settings">Settings</a></li>
                </ul>
            </div>
            <!-- END OF SIDE MENU -->
            <!-- MAIN CONTENT -->
            <div class="col-md-11">
                <div class="welcome_msg col-md-12">
                    Hi Admin! What would you like to do today?
                </div>
                <div class="col-md-12 dashboard_control">
                    <ul class="dashboard_navigation">
                        <li class="on"><a href="#" class="link_about">About Us</a></li>
                        <li><a href="#" class="link_lastest">Latest News</a></li>
                        <li><a href="#" class="link_events">Events</a></li>
                        <li><a href="#" class="link_special">Special Offers</a></li>
                        <li><a href="#" class="link_newsletter">Newsletter</a></li>
                    </ul>
                </div>
            </div>
            <!-- END OF MAIN CONTENT -->
        </div>
    </div>
</div>