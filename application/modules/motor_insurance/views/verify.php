<div class="s_content col-md-6">
    <form method="post" action="<?=site_url('motor-insurance/verify/' . $this->uri->segment(3))?>">
        <div class="pass_details">
            <p>Please enter your new password for your <span class="account_email"><?=$email_address?></span> account</p>
        </div>
        <div class="password_box">
            <p>Password length is a minimum of 8 and a maximum of 20 characters.</p>
            <div class="col-md-12 form_input">
                <input type="password" class="form-control" placeholder="Password" name="password" />
<!--                <div class="ok_msg"><p>Password is strong</p></div>-->

                <?php echo form_error('password'); ?>
            </div>
            <div class="col-md-12 form_input">
                <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password" />
<!--                <div class="error_msg"><p>Password did not match</p></div>-->
                <?php echo form_error('confirm_password'); ?>
            </div>
        </div>
        <p class="form_button">
            <button type="submit" class="btn btn-success btn-lg">Submit</button>
        </p>
    </form>
</div>
<style>
    .password_box > div:last-child {
        margin-top: 20px;
    }
    .password_box > div:last-child.error {
        margin-top: auto;
    }
</style>