<div class="s_content col-md-6">
    <h4>Thank you for trusting <strong>UCPB Gen</strong>!</h4>
    <div class="transaction_details">
        <p>Your purchase from <strong>UCPB Gen</strong> is <strong>COMPLETE</strong></p>
        <p>Your transaction ID for this payment is : <span class="trans_id"><?=$this->input->get('txnid')?></span></p>
    </div>
    <div class="documents_section">
        <ul class="doc_list">
            <li>
                <a href="<?=base_url(FORMAL_QUOTES_FOLDER . $token . '.pdf')?>" target="_blank" class="type_pdf">Formal Quote</a>
            </li>
            <li>
                <a href="<?=base_url(MOTOR_QUOTE_FILES_QUOTE_REQUEST. 'quote-request-' . $token . '.pdf')?>" target="_blank" class="type_pdf">Quote Request</a>
            </li>
            <li>
                <a href="<?=base_url(MOTOR_QUOTE_FILES_BANK_CERTIFICATE. 'bank-certificate-' . $token . '.pdf')?>" target="_blank" class="type_pdf">Bank Certificate</a>
            </li>
            <li>
                <a href="<?=base_url(MOTOR_QUOTE_CONFIRMATION_OF_COVER. 'confirmation-of-cover-' . $token . '.pdf')?>" target="_blank" class="type_pdf">Confirmation of Cover</a>
            </li>
            <li>
                <a href="<?=base_url('files/policies/pc/pc.pdf')?>" target="_blank" class="type_pdf">Terms and Conditions</a>
            </li>
        </ul>
        <ul class="doc_list">
            <?php if ($insurance_type == CTPL || $insurance_type == COMPREHENSIVE_CTPL || $insurance_type == COMPREHENSIVE_CTPL_AON) { ?>
            <li>
                <a href="<?=base_url(POLICIES_FOLDER . 'policy-schedule-' . $token  . '-ctpl.pdf')?>" target="_blank" class="type_pdf">CTPL Policy</a>
            </li>
            <?php } ?>
            <?php if ($insurance_type != CTPL) { ?>
            <li>
                <a href="<?=base_url(POLICIES_FOLDER . 'policy-schedule-' . $token  . '-comprehensive.pdf')?>" target="_blank" class="type_pdf">Comprehensive Policy</a>
            </li>
            <?php } ?>
            <?php if ($insurance_type == CTPL || $insurance_type == COMPREHENSIVE_CTPL || $insurance_type == COMPREHENSIVE_CTPL_AON) { ?>
                <li>
                    <a href="<?=base_url(MOTOR_QUOTE_FILES_INVOICE . 'invoice-' . $token  . '-ctpl.pdf')?>" target="_blank" class="type_pdf">CTPL Invoice</a>
                </li>
            <?php } ?>
            <?php if ($insurance_type != CTPL) { ?>
                <li>
                    <a href="<?=base_url(MOTOR_QUOTE_FILES_INVOICE . 'invoice-' . $token  . '-comprehensive.pdf')?>" target="_blank" class="type_pdf">Comprehensive Invoice</a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="final_message">
        <p>Keep this receipt, in case you need customer support from Dragonpay or UCPB Gen. We will send a copy of this receipt and confirmation email to <span class="email_id"><?= $email_address; ?></span>.</p>
    </div>
    <p class="form_button">
        <a href="<?= site_url('motor-insurance'); ?>" type="button" class="btn btn-success btn-lg" style="line-height: 25px;">Home</a>
    </p>
</div>