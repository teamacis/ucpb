<?php $this->load->view('user/quote_sidebar', array('tab' => 'motor_insurance'));?>
<div class="col-md-9 col-sm-9">
    <div class="col-md-12 col-xs-12 title_bar">
        <h3>QUOTE SUMMARY</h3>
        <p></p>
    </div>
    <!-- CONTENT FIELD -->
    <div class="col-md-12 content_field summary_field">
        <!-- PREMIUM COMPUTATION -->
        <div class="summary_info computation">
            <div class="summary_title title-1"><h4>Premium Computation</h4></div>
            <div class="summary_content">
                <?php $this->load->view('motor_insurance/blocks/premium'); ?>
            </div>
            <div class="summary_note">
                <div class="col-md-8">
                    <strong>Documentary Stamps Tax</strong>
                    <p>Due to BIR implementation of EDST(Electronic Documentary Stamp Tax) system effective July 1, 2010, policy holders are mandated to pay the DST portion of the premium once the policy is issued. Refund on DST for cancelled policies is not allowed.</p>
                </div>
            </div>
        </div>
        <!-- END OF PREMIUM COMPUTATION -->
        <!-- COVERAGE -->
        <div class="summary_info coverage">
            <div class="summary_title title-1"><h4>Coverage</h4></div>
            <div class="summary_content">
                <div class="col-md-8 summary_group">
                    <div class="summary_line">
                        <div class="col-md-12 info_field"><strong class="label_item">Limit of Liability</strong></div>
                    </div>
                    <?php $this->load->view('motor_insurance/blocks/coverages'); ?>
                </div>
            </div>
            <?php if ($data->vehicle_information['insurance_type'] != CTPL) { ?>
            <div class="summary_note">
                <div class="col-md-8">
                    <p><strong>Deductible</strong>
                                    <span>
                                        Php <?=number_format($data->deductible, 2, '.', ',')?>
                                    </span>
                    </p>
                    <p><strong>Non-standard Accessories</strong><span>(Note all declared accessories not factory installed)</span></p>
                </div>
            </div>
            <?php } ?>
        </div>
        <!-- END OF COVERAGE -->
        <!-- INFORMATION -->
        <div class="summary_info insurance_info">
            <div class="summary_title title-2"><h4>Insured and Vehicle Information</h4></div>
            <div class="summary_content">
                <div class="col-md-8 summary_group">
                    <div class="line_title"><p>Personal Information</p></div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Name:</strong>
                                        <span class="value_item">
                                            <?php echo $data->name; ?>
                                        </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 summary_group">
                    <div class="line_title"><p>Location of Vehicle</p></div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Address:</strong>
                                        <span class="value_item">
                                            <?php echo $data->locationOfVehicle; ?>
                                        </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 summary_group">
                    <div class="line_title"><p>Contact Information</p></div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Telephone Number:</strong>
                                        <span class="value_item">
                                            <?php echo $data->contact_information['telephone']; ?>
                                        </span>
                        </div>
                    </div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Mobile Number:</strong>
                                        <span class="value_item">
                                            <?php echo $data->contact_information['mobile']; ?>
                                        </span>
                        </div>
                    </div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Email:</strong>
                                        <span class="value_item">
                                            <?php echo $data->contact_information['email_address']; ?>
                                        </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 summary_group">
                    <div class="line_title"><p>Vehicle Information</p></div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field"><strong class="label_item">Type of insurance:</strong>
                            <span class="value_item">
                                <?php echo unserialize(INSURANCE_LABELS)[$data->vehicle_information['insurance_type']]; ?>
                            </span>
                        </div>
                    </div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Year:</strong>
                                        <span class="value_item">
                                            <?php echo $data->year; ?>
                                        </span>
                        </div>
                    </div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Brand:</strong>
                                        <span class="value_item">
                                            <?php echo $data->brand; ?>
                                        </span>
                        </div>
                    </div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Model:</strong>
                                        <span class="value_item">
                                            <?php echo $data->model_name; ?>
                                        </span>
                        </div>
                    </div>
                    <?php if ($data->vehicle_information['insurance_type'] != CTPL) { ?>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Market Value:</strong>
                            <span class="value_item">
                                Php <?php echo number_format($data->vehicle_information['market_value'], 2, '.', ','); ?>
                            </span>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- END OF INFORMATION -->
        <p class="form_button">
            <button type="button" class="btn btn-primary btn-lg" onclick="view_formal_quote()">View Formal Quote</button>
            <button type="button" class="btn btn-success btn-lg" onclick="go_to_vehicle_info()">Buy</button>
        </p>
    </div>
</div>
<!-- END OF INFORMATION -->

<script type="text/javascript">
    $(document).ready(function() {

    });

    function view_formal_quote() {
        window.open("<?=site_url("motor-insurance/generate_formal_quote/" . $data->token)?>", "_blank");
    }

    function go_to_vehicle_info() {
        window.location.href = '<?=site_url("motor-insurance/vehicle_information/" . $data->token)?>';
    }
</script>