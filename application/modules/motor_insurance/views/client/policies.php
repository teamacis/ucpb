<div class="client_page">
    <div class="container box client_box">
        <div class="col-md-12 rb_mb npr npl ">
            <div class="client_tabs">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#motor" aria-controls="motor" role="tab" data-toggle="tab">Motor</a></li>
                    <li role="presentation"><a href="#fire" aria-controls="fire" role="tab" data-toggle="tab">Fire</a></li>
                    <li role="presentation"><a href="#itp" aria-controls="itp" role="tab" data-toggle="tab">ITP</a></li>
                </ul>
                <!-- Tab panels -->
                <div class="tab-content round_box">
                    <div role="tabpanel" class="tab-pane active col-md-12" id="motor">
                        <!--                        <div class="search_tab">-->
                        <!--                            <button class="searchtab_btn glyphicon glyphicon-search"></button>-->
                        <!--                            <input type="text" class="search_input" placeholder="Search Keywords" />-->
                        <!--                        </div>-->
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th><a href="#">Policy ID</a></th>
                                <th><a href="#">Status <span class="sort_icon glyphicon glyphicon-triangle-bottom"></span></a></th>
                                <th><a href="#">Type of Insurance <span class="sort_icon glyphicon glyphicon-triangle-bottom"></span></a></th>
                                <th><a href="#">Net Premium</a></th>
                                <th><a href="#">Policy Period <span class="sort_icon glyphicon glyphicon-triangle-top"></span></a></th>
                                <th><a href="#">CTPL Policy Period <span class="sort_icon glyphicon glyphicon-triangle-top"></span></a></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($motor_quotes as $key=>$quote) { ?>
                                <?php
                                    $startDate = $endDate = $startDateArray = $endDateArray = [];

                                    if($quote->start_date) {
                                        $startDateArray = explode(',', $quote->start_date);

                                        foreach($startDateArray as $sd) {
                                            $sd = explode('~', $sd);
                                            $sd[1] = new DateTime($sd[1]);
                                            $startDate[$sd[0] == 0 ? 'not_ctpl' : 'ctpl'] = $sd[1]->format('Y-m-d');
                                        }
                                    }
                                    if($quote->end_date) {
                                        $endDateArray = explode(',', $quote->end_date);

                                        foreach($endDateArray as $ed) {
                                            $ed = explode('~', $ed);
                                            $ed[1] = new DateTime($ed[1]);
                                            $endDate[$ed[0] == 0 ? 'not_ctpl' : 'ctpl'] = $ed[1]->format('Y-m-d');
                                        }
                                    }
                                ?>
                                <tr>
                                    <td><span class="order"><?= $key+1; ?></span></td>
                                    <td><a href="#" class="policy_id"><?= $quote->quote_id; ?></a></td>
                                    <td><span class="policy_status"><span class="<?= unserialize(STATUS_CLASS)[$quote->status]; ?>"><?= $quote->status; ?></span></span></td>
                                    <td><span class="insure_type"><?= unserialize(INSURANCE_LABELS)[$quote->insurance_type]; ?></span></td>
                                    <td>
                                        <span class="net_premium">
                                            Php <?= number_format($quote->gross_premium, 2, '.', ','); ?>
                                        </span>
                                    </td>
                                    <td>
                                        <span class="policy_period">
                                            <?= isset($startDate['not_ctpl']) ? $startDate['not_ctpl']. ' -' : ''; ?>
                                            <?= isset($endDate['not_ctpl']) ? $endDate['not_ctpl'] : ''; ?>
                                        </span>
                                    </td>
                                    <td>
                                        <span class="policy_period">
                                            <?= isset($startDate['ctpl']) ? $startDate['ctpl']. ' -' : ''; ?>
                                            <?= isset($endDate['ctpl']) ? $endDate['ctpl'] : ''; ?>
                                        </span>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
<!--                        <nav>-->
<!--                            <ul class="pagination">-->
<!--                                <li class="disabled">-->
<!--                                    <a href="#" aria-label="Previous"><span aria-hidden="true">Prev</span></a>-->
<!--                                </li>-->
<!--                                <li><a href="#">1</a></li>-->
<!--                                <li><a href="#">2</a></li>-->
<!--                                <li><a href="#">3</a></li>-->
<!--                                <li><a href="#">4</a></li>-->
<!--                                <li><a href="#">5</a></li>-->
<!--                                <li>-->
<!--                                    <a href="#" aria-label="Next"><span aria-hidden="true">Next</span></a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </nav>-->
                    </div>
                    <div role="tabpanel" class="tab-pane col-md-12" id="fire">
                        <div class="search_tab">
                            <button class="searchtab_btn glyphicon glyphicon-search"></button>
                            <input type="text" class="search_input" placeholder="Search Keywords" />
                        </div>
<!--                        <table class="table table-striped">-->
<!--                            <thead>-->
<!--                            <tr>-->
<!--                                <th></th>-->
<!--                                <th><a href="#">Policy ID</a></th>-->
<!--                                <th><a href="#">Status <span class="sort_icon glyphicon glyphicon-triangle-bottom"></span></a></th>-->
<!--                                <th><a href="#">Type of Insurance <span class="sort_icon glyphicon glyphicon-triangle-bottom"></span></a></th>-->
<!--                                <th><a href="#">Net Premium</a></th>-->
<!--                                <th><a href="#">Policy Period <span class="sort_icon glyphicon glyphicon-triangle-top"></span></a></th>-->
<!--                            </tr>-->
<!--                            </thead>-->
<!--                            <tbody>-->
<!--                            <tr>-->
<!--                                <td><span class="order">1</span></td>-->
<!--                                <td><a href="#" class="policy_id">MTR2015867756</a></td>-->
<!--                                <td><span class="policy_status active">Active</span></td>-->
<!--                                <td><span class="insure_type">CTPL</span></td>-->
<!--                                <td><span class="net_premium">Php 470,000.00</span></td>-->
<!--                                <td><span class="policy_period">06/25/15 - 06/25/16</span></td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td><span class="order">2</span></td>-->
<!--                                <td><a href="#" class="policy_id">MTR2015234994</a></td>-->
<!--                                <td><span class="policy_status active">Active</span></td>-->
<!--                                <td><span class="insure_type">CTPL</span></td>-->
<!--                                <td><span class="net_premium">Php 1,267,980.00</span></td>-->
<!--                                <td><span class="policy_period">03/18/15 - 03/18/16</span></td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td><span class="order">3</span></td>-->
<!--                                <td><a href="#" class="policy_id">MTR2013986544</a></td>-->
<!--                                <td><span class="policy_status inactive">Inactive</span></td>-->
<!--                                <td><span class="insure_type">CTPL</span></td>-->
<!--                                <td><span class="net_premium">Php 290,000.00</span></td>-->
<!--                                <td><span class="policy_period">08/08/13 - 08/08/14</span></td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td><span class="order">4</span></td>-->
<!--                                <td><a href="#" class="policy_id">MTR2013654542</a></td>-->
<!--                                <td><span class="policy_status inactive">Inactive</span></td>-->
<!--                                <td><span class="insure_type">CTPL</span></td>-->
<!--                                <td><span class="net_premium">Php 170,000.00</span></td>-->
<!--                                <td><span class="policy_period">04/23/13 - 04/23/14</span></td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td><span class="order">5</span></td>-->
<!--                                <td><a href="#" class="policy_id">MTR2013986544</a></td>-->
<!--                                <td><span class="policy_status inactive">Inactive</span></td>-->
<!--                                <td><span class="insure_type">CTPL</span></td>-->
<!--                                <td><span class="net_premium">Php 290,000.00</span></td>-->
<!--                                <td><span class="policy_period">08/08/13 - 08/08/14</span></td>-->
<!--                            </tr>-->
<!--                            </tbody>-->
<!--                        </table>-->
<!--                        <nav>-->
<!--                            <ul class="pagination">-->
<!--                                <li class="disabled">-->
<!--                                    <a href="#" aria-label="Previous"><span aria-hidden="true">Prev</span></a>-->
<!--                                </li>-->
<!--                                <li><a href="#">1</a></li>-->
<!--                                <li><a href="#">2</a></li>-->
<!--                                <li><a href="#">3</a></li>-->
<!--                                <li><a href="#">4</a></li>-->
<!--                                <li><a href="#">5</a></li>-->
<!--                                <li>-->
<!--                                    <a href="#" aria-label="Next"><span aria-hidden="true">Next</span></a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </nav>-->
                    </div>
                    <div role="tabpanel" class="tab-pane col-md-12" id="itp">
                        <div class="search_tab">
                            <button class="searchtab_btn glyphicon glyphicon-search"></button>
                            <input type="text" class="search_input" placeholder="Search Keywords" />
                        </div>
<!--                        <table class="table table-striped">-->
<!--                            <thead>-->
<!--                            <tr>-->
<!--                                <th></th>-->
<!--                                <th><a href="#">Policy ID</a></th>-->
<!--                                <th><a href="#">Status <span class="sort_icon glyphicon glyphicon-triangle-bottom"></span></a></th>-->
<!--                                <th><a href="#">Type of Insurance <span class="sort_icon glyphicon glyphicon-triangle-bottom"></span></a></th>-->
<!--                                <th><a href="#">Net Premium</a></th>-->
<!--                                <th><a href="#">Policy Period <span class="sort_icon glyphicon glyphicon-triangle-top"></span></a></th>-->
<!--                            </tr>-->
<!--                            </thead>-->
<!--                            <tbody>-->
<!--                            <tr>-->
<!--                                <td><span class="order">1</span></td>-->
<!--                                <td><a href="#" class="policy_id">MTR2015867756</a></td>-->
<!--                                <td><span class="policy_status active">Active</span></td>-->
<!--                                <td><span class="insure_type">CTPL</span></td>-->
<!--                                <td><span class="net_premium">Php 470,000.00</span></td>-->
<!--                                <td><span class="policy_period">06/25/15 - 06/25/16</span></td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td><span class="order">2</span></td>-->
<!--                                <td><a href="#" class="policy_id">MTR2015234994</a></td>-->
<!--                                <td><span class="policy_status active">Active</span></td>-->
<!--                                <td><span class="insure_type">CTPL</span></td>-->
<!--                                <td><span class="net_premium">Php 1,267,980.00</span></td>-->
<!--                                <td><span class="policy_period">03/18/15 - 03/18/16</span></td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td><span class="order">3</span></td>-->
<!--                                <td><a href="#" class="policy_id">MTR2013654542</a></td>-->
<!--                                <td><span class="policy_status inactive">Inactive</span></td>-->
<!--                                <td><span class="insure_type">CTPL</span></td>-->
<!--                                <td><span class="net_premium">Php 170,000.00</span></td>-->
<!--                                <td><span class="policy_period">04/23/13 - 04/23/14</span></td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td><span class="order">4</span></td>-->
<!--                                <td><a href="#" class="policy_id">MTR2013986544</a></td>-->
<!--                                <td><span class="policy_status inactive">Inactive</span></td>-->
<!--                                <td><span class="insure_type">CTPL</span></td>-->
<!--                                <td><span class="net_premium">Php 290,000.00</span></td>-->
<!--                                <td><span class="policy_period">08/08/13 - 08/08/14</span></td>-->
<!--                            </tr>-->
<!--                            </tbody>-->
<!--                        </table>-->
<!--                        <nav>-->
<!--                            <ul class="pagination">-->
<!--                                <li class="disabled">-->
<!--                                    <a href="#" aria-label="Previous"><span aria-hidden="true">Prev</span></a>-->
<!--                                </li>-->
<!--                                <li><a href="#">1</a></li>-->
<!--                                <li><a href="#">2</a></li>-->
<!--                                <li><a href="#">3</a></li>-->
<!--                                <li><a href="#">4</a></li>-->
<!--                                <li><a href="#">5</a></li>-->
<!--                                <li>-->
<!--                                    <a href="#" aria-label="Next"><span aria-hidden="true">Next</span></a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </nav>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container box client_box">
        <div class="col-md-8 npl">
            <div class="round_box">
                <h4 class="box_title">Personal Information</h4>
                <div class="client_personal_info">
                    <div class="col-md-3 profile_img">
                        <div class="client_img">
                            <a href="#" class="img"><img src="<?= imgUrl('client_001.png'); ?>" alt="Michael Angelo Bautista" /></a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="info_grp">
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Full Name </div>
                                <div class="col-md-8 info_val"><?= $this->session->userdata('name'); ?></div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Mobile Number</div>
                                <div class="col-md-8 info_val"><?= $this->session->userdata('mobile'); ?></div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Email Address</div>
                                <div class="col-md-8 info_val"><?= $this->session->userdata('email_address'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 npr">
            <?php $this->load->view('client/blocks/contact_us'); ?>
        </div>
    </div>
</div>