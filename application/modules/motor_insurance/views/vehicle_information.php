<?php $this->load->view('user/quote_sidebar', array('tab' => 'motor_insurance'));?>
<div class="col-md-9 col-sm-9" id="vehicleinfoform">
    <div class="col-md-12 col-xs-12 title_bar">
        <h3>VEHICLE INFORMATION</h3>
        <p></p>
    </div>
    <!-- CONTENT FIELD -->
    <div class="col-md-12 content_field">
        <form name="vehicleform" id="vehicleform" class="form_style" method="post" action="<?=site_url('motor-insurance/vehicle_information/' . $this->uri->segment(3))?>" enctype="multipart/form-data">
            <input type="hidden" name="vehicle_information[insurance_type]" value="<?= $vehicle_information['insurance_type']?>">
            <!-- VEHICLE INFO -->
            <div class="form_content">
                <div class="line_title"><p>Vehicle Information</p></div>
                <div class="form_line fnt_style">
                    <div class="col-md-4 form_input"><strong>Year:</strong><span class="value_2"><?=$year?></span></div>
                </div>
                <div class="form_line fnt_style">
                    <div class="col-md-4 form_input"><strong>Brand:</strong><span class="value_1"><?=$brand?></span></div>
                    <div class="col-md-8 form_input"><strong>Model:</strong><span class="value_1"><?=$model_name?></span></div>
                </div>
                <div class="form_line fnt_style">
                    <div class="col-md-4 form_input"><strong>Seating Capacity:</strong><span class="value_1"><?=$seating_capacity?> Seater</span></div>
                    <div class="col-md-8 form_input ">
                        <strong>Gross Premium Value:</strong>
                        <span class="value_1 premium">Php <?=number_format($gross_premium, 2, '.', ',')?></span>
                        <input type="hidden" value="<?=$gross_premium?>" id="premium">
                    </div>
                </div>
                <?php if ($vehicle_information['insurance_type'] != CTPL) { ?>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <select class="form-control chosen-select " data-placeholder="Overnight Parking" name="vehicle_information[overnight_parking]">
                            <option></option>
                            <option value="Locked garage" <?php if (isset($this->input->post('vehicle_information')['overnight_parking']) && $this->input->post('vehicle_information')['overnight_parking'] == 'Locked garage') echo set_select('vehicle_information[overnight_parking]', 'Locked garage', TRUE); ?>>Locked garage</option>
                            <option value="Street parking" <?php if (isset($this->input->post('vehicle_information')['overnight_parking']) && $this->input->post('vehicle_information')['overnight_parking'] == 'Street parking') echo set_select('vehicle_information[overnight_parking]', 'Street parking', TRUE); ?>>Street parking</option>
                        </select>
                        <?php echo form_error('vehicle_information[overnight_parking]'); ?>
                    </div>
                </div>
                <?php } ?>
            </div>
            <!-- END OF VEHICLE INFO -->
            <!-- ACCESSORIES INFO -->

            <?php if ($vehicle_information['insurance_type'] != CTPL) { ?>
            <div class="form_content">
                <div class="line_title"><p>Accessories</p></div>
                <div class="form_line list_style">
                    <div class="col-md-4 form_input">
                        <p class="n_title">Standard (built in)</p>
                        <ol>
                            <li>Aircon</li>
                            <li>Stereo</li>
                            <li>Magwheels</li>
                            <li>Speaker</li>
                        </ol>
                    </div>
                    <div class="col-md-4 form_input">
                        <p class="n_title">Other Standard (built in)</p>
                        <select class="form-control chosen-select" multiple data-placeholder="Accessory" id="accessory" name="vehicle_information[other_accessories_selected][]"  search-drop>
                            <option></option>
                            <?php
                            foreach ($accessories as $val) {
                                echo '<option value="'.$val['accessory'].'"';
                                if (isset($vehicle_information['other_accessories_selected']) && in_array($val['accessory'], $vehicle_information['other_accessories_selected'])) {
                                    echo set_select('vehicle_information[other_accessories_selected]', $val['accessory'], TRUE);
                                }
                                echo '>'.$val['accessory'].'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-8 form_input non-standard-accessories">
                        <p class="n_title">Non-Standard -(nonbuilt in)</p>
                        <?php
                        if (!empty($vehicle_information['non_standard'])) {
                            foreach ($vehicle_information['non_standard'] as $key =>  $val) {
                               echo '<div class="item_choice">
                                    <span class="value_accessory">' . $val['non_standard_accessories'] . '</span>
                                    <span class="value_price">Php ' . number_format($val['non_standard_values'], 2, '.', ',') . '</span>
                                    <input type="hidden" name="non_standard_accessories[]" value="' . $val['non_standard_accessories'] . '">
                                    <input type="hidden" name="non_standard_values[]" value="' .$val['non_standard_values'] . '">
                                    <a href="javascript:void(0)" class="btn_close"></a>
                                </div>';
                            }
                        }
                        ?>

                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <select class="form-control chosen-select" data-placeholder="Accessory" name="non_standard_accessory">
                            <option></option>
                            <?php
                            foreach ($accessories as $val) {
                                echo '<option value="'.$val['accessory'].'"';
                                echo '>'.$val['accessory'].'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="number" class="form-control" placeholder="Value" name="non_standard_accessory_value"/>
                    </div>
                </div>
                <div class="form_line">
                    <button type="button" class="btn btn_add" onclick="add_accessories()"></button>
                </div>
            </div>
            <?php } ?>
            <!-- END OF ACCESSORIES INFO -->
            <!-- OTHER INFO -->
            <div class="form_content">
                <div class="line_title"><p>Other Info</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Plate Number" name="vehicle_information[plate_number]" value="<?=set_value('vehicle_information[plate_number]')?>"/>
                        <?php echo form_error('vehicle_information[plate_number]'); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Engine Number" name="vehicle_information[engine_number]" value="<?=set_value('vehicle_information[engine_number]')?>"/>
                        <?php echo form_error('vehicle_information[engine_number]'); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Chassis Number" name="vehicle_information[chassis_number]" value="<?=set_value('vehicle_information[chassis_number]')?>"/>
                        <?php echo form_error('vehicle_information[chassis_number]'); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control <?=in_array($vehicle_information['insurance_type'], [CTPL, COMPREHENSIVE_CTPL, COMPREHENSIVE_CTPL_AON]) ? "required" : "" ?>" placeholder="MV File Number" name="vehicle_information[mv_file_number]"  value="<?=set_value('vehicle_information[mv_file_number]')?>"/>
                        <?php echo form_error('vehicle_information[mv_file_number]'); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <select class="form-control chosen-select" data-placeholder="Color" name="vehicle_information[color]">
                            <option></option>
                            <?php
                            foreach ($colors as $val) {
                                echo '<option value="'.$val['color'].'"';
                                if (isset($this->input->post('vehicle_information')['color'])) {
                                    echo set_select('vehicle_information[color]', $val['color'], TRUE);
                                }
                                echo '>'.$val['color'].'</option>';
                            }
                            ?>
                            <option value="Other" <?php
                                if (isset($this->input->post('vehicle_information')['color'])) {
                                    echo set_select('vehicle_information[color]', 'Other', TRUE);
                                }
                            ?>>Other</option>
                        </select>
                        <?php echo form_error('vehicle_information[color]'); ?>
                    </div>
                    <div class="col-md-4 form_input <?php
                        if (!isset($this->input->post('vehicle_information')['color']) || $this->input->post('vehicle_information')['color'] != 'Other') {
                            echo 'hidden';
                        }
                    ?>">
                        <input type="text" class="form-control required" placeholder="Other Color" name="vehicle_information[other_color]" value="<?=set_value('vehicle_information[other_color]')?>"/>
                        <?php echo form_error('vehicle_information[other_color]'); ?>
                    </div>
                </div>
                <?php if ($vehicle_information['insurance_type'] != CTPL) { ?>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <select class="form-control chosen-select" data-placeholder="Mortgagee" name="vehicle_information[mortgagees_pk]">
                            <option></option>
                            <?php
                            foreach ($mortgagees as $val) {
                                echo '<option value="'.$val['pk'].'"';
                                if ($this->input->post('vehicle_information')['mortgagees_pk']) {
                                    echo set_select('vehicle_information[mortgagees_pk]', $val['pk'], TRUE);
                                }
                                echo '>'.$val['mortgagee'].'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <?php } ?>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <select class="form-control chosen-select" data-placeholder="Previous Insurance Provider" name="vehicle_information[insurance_providers_pk]">
                            <option></option>
                            <?php
                            foreach ($insurance_providers as $val) {
                                echo '<option value="'.$val['pk'].'"';
                                if ($this->input->post('vehicle_information')['insurance_providers_pk']) {
                                    echo set_select('vehicle_information[insurance_providers_pk]', $val['pk'], TRUE);
                                }
                                echo '>'.$val['provider'].'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <?php if ($vehicle_information['insurance_type'] != COMPREHENSIVE && $vehicle_information['insurance_type'] != COMPREHENSIVE_AON) { ?>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <div class="line_title"><p>Upload OR / CR</p></div>
                        <div class="upload_box" <?php if((isset($vehicle_information['or_cr']) && !empty($vehicle_information['or_cr']) && count($_FILES) == 0)) { ?> style='display: none;'<?php } ?>>
                            <input type="file" class="nice" name="vehicle_information[or_cr_file]" <?php if(isset($vehicle_information['or_cr']) && !empty($vehicle_information['or_cr'])  && count($_FILES) == 0) { ?> disabled <?php } ?>/>
                            <span class="btn_close"></span>
                        </div>
                        <div class="image-preview" <?php if((isset($vehicle_information['or_cr']) && empty($vehicle_information['or_cr'])) ||
                                                            (!empty($vehicle_information['or_cr']) && count($_FILES) > 0)) { ?> style='display: none;'<?php } ?>>
                            <img src="<?php echo fileUrl('orcr/'. (isset($vehicle_information['or_cr']) ? $vehicle_information['or_cr'] : '')); ?>" style="width: 100%;"/>
                            <div class="text-right" style="margin-top: 10px;">
                                <input type="button" class="btn btn-success btn-sm remove-image" value="Remove Image">
                            </div>
                        </div>

                        <?php if ($this->input->post() && $file_failed) {
                            echo '<div class="error_msg"><p>Need a valid file</p></div>';
                        }?>

                    </div>
                </div>
                <p class="small_text">Allowed types: PNG, JPG, GIF, BMP, PDF </p>
                <?php } ?>
            </div>
            <!-- END OF INSURANCE PROVIDER-->
            <p class="form_button">
                <input type="submit" class="btn btn-success btn-lg" value="Next">
            </p>

            <input type="hidden" name="loaded" value="true">
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.chosen-select').chosen({width:"100%", disable_search_threshold: 10, search_contains: true});

        $("[name='vehicle_information[or_cr_file]']").nicefileinput({
            label : ' '
        });

        $("[name='vehicle_information[color]']").change(function() {
            if ($(this).val() != 'Other') {
                $("[name='vehicle_information[other_color]']").val('').parent().addClass('hidden');
            } else {
                $("[name='vehicle_information[other_color]']").parent().removeClass('hidden');
            }
        });

        $('.remove-image').click(function() {
            $('.image-preview').hide();
            $('.upload_box').show();
            $('.nice').removeAttr('disabled');
        });
    });

    function add_accessories() {
        var div_count = $('.non-standard-accessories').find('.item_choice').length;
        var html = '<div class="item_choice">'+
                    '<span class="value_accessory">' + $('[name=non_standard_accessory]').val() + '</span>'+
                    '<span class="value_price">Php ' + $('[name=non_standard_accessory_value]').val() + '</span>'+
                    '<input type="hidden" name="non_standard_accessories[]" value="' + $('[name=non_standard_accessory]').val() + '">'+
                    '<input type="hidden" name="non_standard_values[]" value="' + $('[name=non_standard_accessory_value]').val() + '">'+
                    '<a href="javascript:void(0)" class="btn_close"></a>'+
                '</div>';
        var gross = parseFloat($('#premium').val()) + parseFloat($('[name=non_standard_accessory_value]').val());
        $('.premium').html('Php ' + common.number_format(gross, 2, '.', ','));
        $('#premium').val(gross);
        $('.non-standard-accessories').append(html);
        $('[name=non_standard_accessory]').val('');
        $('[name=non_standard_accessory_value]').val('');
        $('[name=non_standard_accessory]').trigger("chosen:updated");
    }

    $(document).on('click', '.btn_close', function() {
        var gross = parseFloat($('#premium').val()) - parseFloat($(this).parent().find("[name='vehicle_information[non_standard_values][]']").val());
        $('.premium').html('Php ' + common.number_format(gross, 2, '.', ','));
        $('#premium').val(gross);
        $(this).parent().remove();
    })
</script>