<?php  if($data->vehicle_information['insurance_type'] == CTPL) { ?>
    <tr>
        <th><span class="pre_ltr">A.</span><strong>Compulsary Third Party Liability</strong></th>
        <td><span><?=number_format($data->ctpl, 2, '.', ',')?></span></td>
    </tr>
<?php } ?>

<?php
if($data->vehicle_information['insurance_type'] == COMPREHENSIVE) {
    ?>
    <tr>
        <th><span class="pre_ltr">A.</span><strong>Own Damage and Theft</th>
        <td><span><?=number_format($data->ownDamageCoverage, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">B.</span><strong>Bodily Injury</strong></th>
        <td><span><?=number_format($data->bodily_injury_max, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">C.</span><strong>Property Damage</strong></th>
        <td><span><?=number_format($data->property_damage_max, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">D.</span><strong>Auto Personal Accident</strong></th>
        <td><span><?=number_format($data->autoPersonalAccidentCoverage, 2, '.', ',')?></span></td>
    </tr>
<?php
}
?>

<?php
if($data->vehicle_information['insurance_type']  == COMPREHENSIVE_CTPL) {
    ?>
    <tr>
        <th><span class="pre_ltr">A.</span><strong>Compulsary Third Party Liability</strong></th>
        <td><span><?=number_format($data->ctpl, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">B.</span><strong>Own Damage and Theft</th>
        <td><span><?=number_format($data->ownDamageCoverage, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">C.</span><strong>Bodily Injury</strong></th>
        <td><span><?=number_format($data->bodily_injury_max, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">D.</span><strong>Property Damage</strong></th>
        <td><span><?=number_format($data->property_damage_max, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">E.</span><strong>Auto Personal Accident</strong></th>
        <td><span><?=number_format($data->autoPersonalAccidentCoverage, 2, '.', ',')?></span></td>
    </tr>
<?php
}
?>

<?php
if($data->vehicle_information['insurance_type']  == COMPREHENSIVE_AON) {
    ?>
    <tr>
        <th><span class="pre_ltr">A.</span><strong>Own Damage and Theft</th>
        <td><span><?=number_format($data->ownDamageCoverage, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">B.</span><strong>Bodily Injury</strong></th>
        <td><span><?=number_format($data->bodily_injury_max, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">C.</span><strong>Property Damage</strong></th>
        <td><span><?=number_format($data->property_damage_max, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">D.</span><strong>Auto Personal Accident</strong></th>
        <td><span><?=number_format($data->autoPersonalAccidentCoverage, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">E.</span><strong>Acts of Nature</strong></th>
        <td><span><?=number_format($data->aon, 2, '.', ',')?></span></td>
    </tr>
<?php
}
?>

<?php
if($data->vehicle_information['insurance_type']  == COMPREHENSIVE_CTPL_AON) {
    ?>
    <tr>
        <th><span class="pre_ltr">A.</span><strong>Compulsary Third Party Liability</strong></th>
        <td><span><?=number_format($data->ctpl, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">B.</span><strong>Own Damage and Theft</th>
        <td><span><?=number_format($data->ownDamageCoverage, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">C.</span><strong>Bodily Injury</strong></th>
        <td><span><?=number_format($data->bodily_injury_max, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">D.</span><strong>Property Damage</strong></th>
        <td><span><?=number_format($data->property_damage_max, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">E.</span><strong>Auto Personal Accident</strong></th>
        <td><span><?=number_format($data->autoPersonalAccidentCoverage, 2, '.', ',')?></span></td>
    </tr>
    <tr>
        <th><span class="pre_ltr">F.</span><strong>Acts of Nature</strong></th>
        <td><span><?=number_format($data->aon, 2, '.', ',')?></span></td>
    </tr>
<?php
}
?>