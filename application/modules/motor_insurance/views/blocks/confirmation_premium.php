<thead>
<tr>
    <th>Net Premium</th>
    <td><span>Php <?=number_format($data->netPremium + $data->ctplNetPremium, 2, '.', ',')?></span></td>
</tr>
</thead>
<tbody>
<tr>
    <th colspan="2">Plus:</th>
</tr>
<tr>
    <th>Doc Stamps</th>
    <td><span><?=number_format($data->docStamps + $data->ctplDocStamps, 2, '.', ',')?></span></td>
</tr>
<tr>
    <th>VAT</th>
    <td><span><?=number_format($data->vat + $data->ctplVat, 2, '.', ',')?></span></td>
</tr>
<tr>
    <th>Local Government Tax</th>
    <td><span><?=number_format($data->localGovernmentTax + $data->ctplLocalGovernmentTax, 2, '.', ',')?></span></td>
</tr>
<?php if (!in_array($data->vehicle_information['insurance_type'], [COMPREHENSIVE, COMPREHENSIVE_AON])) {?>
<tr>
    <th>Interconnectivity</th>
    <td><span><?=number_format($data->ctplInterconnectivity, 2, '.', ',')?></span></td>
</tr>
<?php } ?>
</tbody>