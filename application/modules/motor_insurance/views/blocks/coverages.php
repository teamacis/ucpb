<?php if($data->vehicle_information['insurance_type'] == CTPL) { ?>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">A.</span><strong class="label_item">Compulsary Third Party Liability</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->ctpl, 2, '.', ',')?></span></div>
    </div>
<?php } ?>

<?php if($data->vehicle_information['insurance_type'] == COMPREHENSIVE) { ?>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">A.</span><strong class="label_item">Own Damage and Theft</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->ownDamageCoverage, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">B.</span><strong class="label_item">Bodily Injury</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->bodily_injury_max, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">C.</span><strong class="label_item">Property Damage</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->property_damage_max, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">D.</span><strong class="label_item">Auto Personal Accident</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->autoPersonalAccidentCoverage, 2, '.', ',')?></span></div>
    </div>
<?php } ?>

<?php if($data->vehicle_information['insurance_type'] == COMPREHENSIVE_CTPL) { ?>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">A.</span><strong class="label_item">Compulsary Third Party Liability</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->ctpl, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">B.</span><strong class="label_item">Own Damage and Theft</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->ownDamageCoverage, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">C.</span><strong class="label_item">Bodily Injury</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->bodily_injury_max, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">D.</span><strong class="label_item">Property Damage</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->property_damage_max, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">E.</span><strong class="label_item">Auto Personal Accident</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->autoPersonalAccidentCoverage, 2, '.', ',')?></span></div>
    </div>
<?php } ?>

<?php  if($data->vehicle_information['insurance_type'] == COMPREHENSIVE_AON) { ?>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">A.</span><strong class="label_item">Own Damage and Theft</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->ownDamageCoverage, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">B.</span><strong class="label_item">Bodily Injury</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->bodily_injury_max, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">C.</span><strong class="label_item">Property Damage</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->property_damage_max, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">D.</span><strong class="label_item">Auto Personal Accident</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->autoPersonalAccidentCoverage, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">E.</span><strong class="label_item">Acts of Nature</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->aon, 2, '.', ',')?></span></div>
    </div>
<?php } ?>

<?php
if($data->vehicle_information['insurance_type'] == COMPREHENSIVE_CTPL_AON) {
    ?>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">A.</span><strong class="label_item">Compulsary Third Party Liability</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->ctpl, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">B.</span><strong class="label_item">Own Damage and Theft</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->ownDamageCoverage, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">C.</span><strong class="label_item">Bodily Injury</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->bodily_injury_max, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">D.</span><strong class="label_item">Property Damage</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->property_damage_max, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">E.</span><strong class="label_item">Auto Personal Accident</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->autoPersonalAccidentCoverage, 2, '.', ',')?></span></div>
    </div>
    <div class="summary_line">
        <div class="col-md-8 info_field"><span class="order">F.</span><strong class="label_item">Acts of Nature</strong></div>
        <div class="col-md-4 info_field text-right"><span class="value_item"><?=number_format($data->aon, 2, '.', ',')?></span></div>
    </div>
<?php } ?>

