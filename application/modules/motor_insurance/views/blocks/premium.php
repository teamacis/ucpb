<div class="col-md-8 top_head">
    <div class="col-md-6 info_field"><strong class="label_item">Net Premium</strong></div>
    <div class="col-md-6 info_field text-right">
        <span class="value_item">
            Php <?= number_format($data->netPremium + $data->ctplNetPremium, 2, '.', ','); ?>
        </span>
    </div>
</div>
<div class="col-md-8 summary_group">
    <div class="summary_line">
        <div class="col-md-12 info_field"><strong class="label_item">Plus:</strong></div>
    </div>
    <div class="summary_line">
        <div class="col-md-6 info_field"><strong class="label_item">Doc Stamps</strong></div>
        <div class="col-md-6 info_field text-right">
            <span class="value_item">
                <?= number_format($data->docStamps + $data->ctplDocStamps, 2, '.', ','); ?>
            </span>
        </div>
    </div>
    <div class="summary_line">
        <div class="col-md-6 info_field"><strong class="label_item">VAT</strong></div>
        <div class="col-md-6 info_field text-right">
            <span class="value_item">
                <?= number_format($data->vat + $data->ctplVat, 2, '.', ','); ?>
            </span>
        </div>
    </div>
    <div class="summary_line">
        <div class="col-md-6 info_field"><strong class="label_item">Local Government Tax</strong></div>
        <div class="col-md-6 info_field text-right">
            <span class="value_item">
                <?= number_format($data->localGovernmentTax + $data->ctplLocalGovernmentTax, 2, '.', ','); ?>
            </span>
        </div>
    </div>
    <?php if (in_array($data->vehicle_information['insurance_type'], [CTPL, COMPREHENSIVE_CTPL, COMPREHENSIVE_CTPL_AON])) { ?>
    <div class="summary_line">
        <div class="col-md-6 info_field"><strong class="label_item">Interconnectivity</strong></div>
        <div class="col-md-6 info_field text-right">
            <span class="value_item">
                <?= number_format($data->ctplInterconnectivity, 2, '.', ','); ?>
            </span>
        </div>
    </div>
    <?php } ?>
</div>
<div class="col-md-8 bottom_foot">
    <div class="col-md-6 info_field"><strong class="label_item">Gross Premium</strong></div>
    <div class="col-md-6 info_field text-right">
        <span class="value_item">
            Php <?= number_format($data->premium, 2, '.', ','); ?>
        </span>
    </div>
</div>