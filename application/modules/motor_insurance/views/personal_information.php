<?php $this->load->view('user/quote_sidebar', array('tab' => 'motor_insurance'));?>

<div class="col-md-9 col-sm-9">
    <div class="col-md-12 col-xs-12 title_bar">
        <h3>Agent Information</h3>
        <p></p>
    </div>

    <form name="personalform" id="personalform" class="form_style" method="post" action="<?=site_url('motor-insurance/personal_information/' . $this->uri->segment(3))?>">
        <!-- CONTENT FIELD -->
        <div class="col-md-12 content_field">
            <!-- AGENT TYPE -->
            <div class="form_content">
                <div class="radio_type form_input">
                    <p class="radio_type_title">Do you have an agent with <strong>UCPB Gen</strong>?</p>
                    <div class="radio_group">
                        <label class="radio-inline">
                            <input type="radio" name="agent[with_agent]" value="on" <?php if(set_value('agent[with_agent]') == 'on') { ?> checked <?php } ?>> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="agent[with_agent]" value="off" <?php if(set_value('agent[with_agent]') != 'on') { ?> checked <?php } ?>> No
                        </label>
                    </div>
                </div>
            </div>
            <!-- END OF AGENT TYPE -->
            <!-- AGENT INFO -->
            <div class="form_content agent-info" <?php if(set_value('agent[with_agent]') != 'on') { ?> style="display: none;" <?php } ?>>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <select class="form-control chosen-select" data-placeholder="Agent Type" name="agent[type]">
                            <option></option>
                            <option <?php if($this->input->post('agent')['type'] == 'Agent Code'): ?> selected <?php endif; ?>>Agent Code</option>
                            <option <?php if($this->input->post('agent')['type'] == 'Agent Name'): ?> selected <?php endif; ?>>Agent Name</option>
                            <option <?php if($this->input->post('agent')['type'] == 'Agency Name'): ?> selected <?php endif; ?>>Agency Name</option>
                        </select>
                        <?= form_error('agent[type]'); ?>
                    </div>
                    <div class="col-md-4 form_input agent-name" <?php if(!set_value('agent[type]') || set_value('agent[type]') != 'Agent Name') { ?>style="display: none; <?php } ?>">
                        <input type="text" class="form-control required" name="agent[fname]" placeholder="First Name" value="<?= set_value('agent[fname]'); ?>" />
                        <?= form_error('agent[fname]'); ?>
                        <?php if($this->session->flashdata('agent_name_error')): ?>
                            <div class="error_msg"><p><?= $this->session->flashdata('agent_name_error'); ?></p></div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4 form_input agent-name" <?php if(!set_value('agent[type]') || set_value('agent[type]') != 'Agent Name') { ?>style="display: none; <?php } ?>">
                        <input type="text" class="form-control required" name="agent[lname]" placeholder="Last Name" value="<?= set_value('agent[lname]'); ?>" />
                        <?= form_error('agent[lname]'); ?>
                        <?php if($this->session->flashdata('agent_name_error')): ?>
                            <div class="error_msg"><p><?= $this->session->flashdata('agent_name_error'); ?></p></div>
                        <?php endif; ?>
                    </div>
                    <!-- ====== HIDDEN AGENT CODE ====== -->
                    <div class="col-md-4 form_input agent-code" <?php if(!set_value('agent[type]') || set_value('agent[type]') != 'Agent Code') { ?>style="display: none; <?php } ?>">
                        <input type="text" class="form-control required" name="agent[code]" placeholder="Agent's Code" value="<?= set_value('agent[code]'); ?>" />
                        <?= form_error('agent[code]'); ?>
                        <?php if($this->session->flashdata('agent_code_error')): ?>
                            <div class="error_msg"><p><?= $this->session->flashdata('agent_code_error'); ?></p></div>
                        <?php endif; ?>
                    </div>
                     <!-- ====== END OF HIDDEN AGENT CODE ====== -->
                    <div class="col-md-4 form_input agent-agency" <?php if(!set_value('agent[type]') || set_value('agent[type]') != 'Agency Name') { ?>style="display: none; <?php } ?>">
                        <input type="text" class="form-control required" name="agent[agency]" placeholder="Agency Name" value="<?= set_value('agent[agency]'); ?>" />
                        <?= form_error('agent[agency]'); ?>
                        <?php if($this->session->flashdata('agency_name_error')): ?>
                            <div class="error_msg"><p><?= $this->session->flashdata('agency_name_error'); ?></p></div>
                        <?php endif; ?>
                    </div>
                    <!-- ====== END OF HIDDEN AGENT CODE ====== -->
                </div>
            </div>
            <!-- /END OF AGENT INFO -->
        </div>
        <!-- END OF CONTENT FIELD -->
        <div class="col-md-12 title_bar">
            <h3>Personal Information</h3>
            <!-- <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor.</p> -->
        </div>
        <div class="col-md-12 content_field">
            <!-- PERSONAL INFO -->
            <div class="form_content">
                <div class="line_title"><p>Personal Information</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <select class="form-control chosen-select" data-placeholder="Salutation" name="personal_information[salutation]">
                            <option></option>
                            <?php foreach ($salutations as $salutation) { ?>
                                <option value="<?= $salutation->salutation; ?>" <?= $this->input->post('personal_information')['salutation'] ? set_select('personal_information[salutation]', $salutation->salutation, TRUE) : ''; ?>><?= $salutation->salutation; ?></option>';
                            <?php } ?>
                            <option value="">NONE</option>
                        </select>
                        <?= form_error('personal_information[salutation]'); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Last Name" name="personal_information[lname]" value="<?= set_value('personal_information[lname]') ? set_value('personal_information[lname]') : $motor_quote->lname; ?>" />
                        <?= form_error('personal_information[lname]'); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="First Name" name="personal_information[fname]" value="<?= set_value('personal_information[fname]') ? set_value('personal_information[fname]') : $motor_quote->fname; ?>" />
                        <?= form_error('personal_information[fname]'); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Middle Initial" name="personal_information[mname]" value="<?= set_value('personal_information[mname]'); ?>" maxlength="1"/>
                        <?= form_error('personal_information[mname]'); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <select class="form-control chosen-select required" name="personal_information[suffix]" data-placeholder="Suffix">
                            <option></option>
                            <?php foreach ($suffixes as $suffix) { ?>
                                <option value="<?= $suffix->suffix; ?>" <?= $this->input->post('personal_information')['suffix'] ? set_select('personal_information[suffix]', $suffix->suffix, TRUE) : ''; ?>><?= $suffix->suffix; ?></option>;
                            <?php } ?>
                        </select>
                        <?= form_error('personal_information[suffix]'); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="radio_type form_input">
                        <p class="radio_type_title">Gender</p>
                        <div class="radio_group" style="width: 150px;">
                            <label class="radio-inline">
                                <input type="radio" value="Male" name="personal_information[gender]" <?php if(set_value('personal_information[gender]') == 'Male') { ?> checked <?php } ?>> Male
                            </label>
                            <label class="radio-inline">
                                <input type="radio" value="Female" name="personal_information[gender]" <?php if(set_value('personal_information[gender]') == 'Female') { ?> checked <?php } ?>> Female
                            </label>
                            <?= form_error('personal_information[gender]'); ?>
                        </div>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control calendar_input required" placeholder="Date of Birth" name="personal_information[birthdate]" value="<?= set_value('personal_information[birthdate]'); ?>" readonly />
                        <?= form_error('personal_information[birthdate]'); ?>
                    </div>
                    <div class="col-md-4 form_input required">
                        <select class="form-control chosen-select required" name="personal_information[nationality]" data-placeholder="Nationality">
                            <option></option>
                            <?php foreach ($nationalities as $nationality) { ?>
                                <option value="<?= $nationality->nationality; ?>" <?= $this->input->post('personal_information')['nationality'] ? set_select('personal_information[nationality]', $nationality->nationality, TRUE) : ''; ?>><?= $nationality->nationality; ?></option>;
                            <?php } ?>
                        </select>
                        <?= form_error('personal_information[nationality]'); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <input type="hidden" name="personal_information[with_tin]" value="<?= set_value('personal_information[with_tin]'); ?>">
                        <select class="form-control chosen-select required" name="personal_information[occupation]" data-placeholder="Occupation">
                            <option></option>
                            <?php foreach ($occupations as $occupation) { ?>
                                <option data-with-tin='<?= $occupation->with_tin; ?>' value='<?= $occupation->occupation; ?>' <?= $this->input->post('personal_information')['occupation'] ? set_select('personal_information[occupation]', $occupation->occupation, TRUE) : ''; ?>>
                                    <?= $occupation->occupation; ?>
                                </option>
                            <?php } ?>
                        </select>
                        <?= form_error('personal_information[occupation]'); ?>
                    </div>
                    <div class="col-md-4 form_input with-tin" <?php if(set_value('personal_information[with_tin]') != 1): ?>style="display: none;"<?php endif; ?>>
                        <input type="text" class="form-control required" placeholder="Tax Identification Number" name="personal_information[tin]" value="<?= set_value('personal_information[tin]'); ?>"/>
                        <?= form_error('personal_information[tin]'); ?>
                    </div>
                </div>

                <div class="form_line without-tin" <?php if(set_value('personal_information[with_tin]') != 0): ?>style="display: none;"<?php endif; ?>>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Type of ID" name="personal_information[id_type]" />
                        <?= form_error('personal_information[id_type]'); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="ID Number" name="personal_information[id_number]" />
                        <?= form_error('personal_information[id_number]'); ?>
                    </div>
                </div>
            </div>
            <!-- /END OF PERSONAL INFO -->
            <!-- MAILING INFO -->
            <div class="form_content">
                <div class="line_title"><p>Mailing Address</p></div>
                <div class="form_line">
                    <div class="col-md-8 form_input">
                        <div class="checkbox">
                            <label><input type="checkbox" name="same_location"
                                    <?= $first_load || $this->input->post('same_location') == 'on' ? 'checked' : ''; ?>> Same location with my car</label>
                        </div>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <?php
                            $placeholder = 'Province';
                            $disabled    = '';
                            if($first_load || $this->input->post('same_location') == 'on') {
                                $placeholder = $default_places[0]->place;
                                $disabled = 'disabled';
                            }
                        ?>
                        <select class="form-control chosen-select location required" name="mailing_address[province]" onchange="getCities()"
                                data-placeholder="<?= $placeholder; ?>" <?= $disabled; ?>>
                            <option></option>
                            <?php foreach ($provinces as $province) { ?>
                                <option value="<?= $province['pk']; ?>" <?= $this->input->post('mailing_address')['province'] ? set_select('mailing_address[province]', $province['pk'], TRUE) : ''; ?>>
                                    <?= $province['place']; ?>
                                </option>'
                            <?php } ?>
                        </select>
                        <?= form_error('mailing_address[province]'); ?>
                        <input type="hidden" name="vehicle_information[address][province]" value="<?= $motor_quote->vehicle_information['address']['province']; ?>">
                    </div>
                    <div class="col-md-4 form_input required">
                        <?php
                            $placeholder = 'City';
                            $disabled    = '';
                            if($first_load || $this->input->post('same_location') == 'on') {
                                $placeholder = $default_places[1]->place;
                                $disabled = 'disabled';
                            }
                        ?>
                        <select class="form-control chosen-select location required" name="mailing_address[city]" onchange="getBarangays()"
                                data-placeholder="<?= $placeholder; ?>" <?= $disabled; ?>>
                            <option></option>
                            <?php if($cities) { ?>
                                <?php foreach ($cities as $city) { ?>
                                    <option value="<?= $city['pk']; ?>" <?= set_select('mailing_address[city]', $city['pk'], TRUE); ?>>
                                        <?= $city['place']; ?>
                                    </option>'
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?= form_error('mailing_address[city]'); ?>
                        <input type="hidden" name="vehicle_information[address][city]" value="<?= $motor_quote->vehicle_information['address']['city']; ?>">
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <?php
                            $placeholder = 'Barangay';
                            $disabled    = '';
                            if($first_load || $this->input->post('same_location') == 'on') {
                                $placeholder = $default_places[2]->place;
                                $disabled = 'disabled';
                            }
                        ?>
                        <select class="form-control chosen-select location required" name="mailing_address[barangay]"
                                data-placeholder="<?= $placeholder; ?>" <?= $disabled; ?>>
                            <option></option>
                            <?php if($barangays) { ?>
                                <?php foreach ($barangays as $barangay) { ?>
                                    <option value="<?= $barangay['pk']; ?>" <?= set_select('mailing_address[barangay]', $barangay['pk'], TRUE); ?>>
                                        <?= $barangay['place']; ?>
                                    </option>'
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?= form_error('mailing_address[barangay]'); ?>
                        <input type="hidden" name="vehicle_information[address][barangay]" value="<?= $motor_quote->vehicle_information['address']['barangay']; ?>">
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-8 form_input">
                        <input type="text" class="form-control required" placeholder="House No., Street Name, Subdivision Name" name="mailing_address[house_number]" value="<?= set_value('mailing_address[house_number]'); ?>"/>
                        <?= form_error('mailing_address[house_number]'); ?>
                    </div>
                </div>
            </div>
            <!-- END OF MAILING INFO -->
            <!-- CONTACT INFO -->
            <div class="form_content">
                <div class="line_title"><p>Contact Information</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="email" class="form-control required" placeholder="Email Address" name="contact_information[email_address]" maxlength="50" data-ng-model="formdata.email_address" data-ng-disabled="true"
                               value="<?= set_value('contact_information[email_address]') ? set_value('contact_information[email_address]') : $motor_quote->contact_information['email_address']; ?>"/>
                        <?= form_error('contact_information[email_address]'); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Mobile" name="contact_information[mobile]" value="<?= set_value('contact_information[mobile]') ? set_value('contact_information[mobile]') : $motor_quote->contact_information['mobile']; ?>" />
                        <?= form_error('contact_information[mobile]'); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Telephone" name="contact_information[telephone]" value="<?= set_value('contact_information[telephone]') ? set_value('contact_information[telephone]') : $motor_quote->contact_information['telephone']; ?>" />
                        <?= form_error('contact_information[telephone]'); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-8 form_input">
                        <div class="checkbox">
                            <label><input type="checkbox" name="delivery" <?= $this->input->post('delivery') ? 'checked' : ''; ?> /> Deliver hardcopy of my policy</label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OF CONTACT INFO -->
        </div>
        <!-- END OF CONTENT FIELD -->
        <p class="form_button">
            <button type="button" class="btn btn-success btn-lg back"">Back</button>
            <button type="submit" class="btn btn-success btn-lg"">Next</button>
        </p>
    </form>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("[name='personal_information[birthdate]']").datepicker({ dateFormat: 'yy-mm-dd', maxDate: '-18y' });
        $('.chosen-select').chosen({width:"100%", disable_search_threshold: 10, search_contains: true});
        $('.back').click(function() {
            window.location.href = '<?=site_url('motor-insurance/coverage_details/' . $this->uri->segment(3))?>';
        });

        /**
         * Hides the Agent Info
         */
        $("[name='agent[with_agent]']").change(function() {
            if($(this).val() == 'off') {
                $('.agent-info').hide();
            }
            else {
                $('.agent-info').show();
            }
        });

        /**
         * Toggles Agent Code and Agent Name
         */
        $("[name='agent[type]']").change(function() {
            $('.agent-code, .agent-name, .agent-agency').hide();
            if($(this).val() == 'Agent Code') {
                 $('.agent-code').show();
            }
            else if ($(this).val() == 'Agent Name'){
                $('.agent-name').show();
            } else {
                $('.agent-agency').show();
            }
        });

        /**
         * Toggles TIN and ID information
         */
        $("[name='personal_information[occupation]'").change(function() {
            tinControl();
        });

        function tinControl() {
            var tin = parseInt($("[name='personal_information[occupation]'").find(":selected").attr('data-with-tin'));
            $("[name='personal_information[with_tin]']").val(tin);

            $('.with-tin, .without-tin').hide();
            if(tin) {
                $('.with-tin').show();
            }
            else if(tin == '0' || tin == 0){
                $('.without-tin').show();
            }
        }

        // Must run on first load to control tin.
        tinControl();

        /**
         * Mailing Address
         */
        var defaultLabels = {
            province: '<?= $default_places[0]->place; ?>',
            city:     '<?= $default_places[1]->place; ?>',
            barangay: '<?= $default_places[2]->place; ?>'
        };
        var locationUserInput = {};

        $("[name=same_location]").click(function() {
            checkSameLocation();
        });

        function checkSameLocation() {
            var location = $('.location');
            if($("[name=same_location]").prop('checked')) {
                location.attr('disabled', 'disabled');
                updateLocationUserInput(true);
            }
            else {
                location.removeAttr('disabled');
                updateLocationUserInput();
            }

            location.trigger('chosen:updated');
        }

        function updateLocationUserInput(setDefault) {
            var province = $("[name='mailing_address[province]']");
            var city     = $("[name='mailing_address[city]']");
            var barangay = $("[name='mailing_address[barangay]']");

            if(setDefault) {
                locationUserInput.province = province.val();
                locationUserInput.city     = city.val();
                locationUserInput.barangay = barangay.val();

                province.val('').attr('data-placeholder', defaultLabels.province);
                city.val('').attr('data-placeholder', defaultLabels.city);
                barangay.val('').attr('data-placeholder', defaultLabels.barangay);
            }
            else {
                province.val(locationUserInput.province).attr('data-placeholder', 'Province');
                city.val(locationUserInput.city).attr('data-placeholder', 'City');
                barangay.val(locationUserInput.barangay).attr('data-placeholder', 'Barangay');
            }
        }

    });

    /**
     * LOCATION FETCH
     */
    function getCities() {
        common.ajax('<?=site_url('place/cities')?>', {province: $("[name='mailing_address[province]']").val()},
            function(response) {
                if (response.length > 0) {
                    var options = '<option></option>';
                    $.each(response, function(i, v) {
                        options += '<option value="' + v.pk + '">' + v.place + '</option>';
                    });
                    $("[name='mailing_address[city]']").html(options).prop("disabled", false).trigger("chosen:updated");
                    $("[name='mailing_address[barangay]']").val(null).prop("disabled", true).trigger("chosen:updated");
                }
            });
    }
    function getBarangays() {
        common.ajax('<?=site_url('place/barangays')?>', {city: $("[name='mailing_address[city]']").val()},
            function(response) {
                if (response.length > 0) {
                    var options = '<option></option>';
                    $.each(response, function(i, v) {
                        options += '<option value="' + v.pk + '">' + v.place + '</option>';
                    });
                    $("[name='mailing_address[barangay]']").html(options).prop("disabled", false).trigger("chosen:updated");
                }
            });
    }
</script>