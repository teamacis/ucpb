<?php $this->load->view('user/quote_sidebar', array('tab' => 'motor_insurance'));?>
<div class="col-md-9 col-sm-9" data-ng-controller="getQuoteCtrl" >
    <div class="col-md-12 col-xs-12 title_bar">
        <h3>QUOTE REQUEST</h3>
        <p></p>
    </div>
    <!-- CONTENT FIELD -->
    <div class="col-md-12 content_field">
<!--        --><?php //echo validation_errors(); ?>
        <?php if ($this->session->userdata('saved')) { ?>
            <div class="alert alert-success" role="alert">
                <?=$this->session->userdata('saved')?>
                <?=$this->session->unset_userdata('saved')?>
            </div>
        <?php } ?>
        <form name="quoteform" id="quoteform" class="form_style" method="post" action="<?=site_url('motor-insurance/get_quote/' . $this->uri->segment(3))?>">
            <!-- PERSONAL INFO -->
            <div class="form_content">
                <div class="line_title"><p>Personal Information</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Last Name" name="personal_information[lname]" maxlength="50" value="<?=set_value("personal_information[lname]")?>"/>
                        <?php echo form_error("personal_information[lname]"); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="First Name" name="personal_information[fname]" maxlength="50" value="<?=set_value("personal_information[fname]")?>"/>
                        <?php echo form_error("personal_information[fname]"); ?>
                    </div>
                </div>
            </div>
            <!-- /END OF PERSONAL INFO -->
            <!-- CONTACT INFO -->
            <div class="form_content">
                <div class="line_title"><p>Contact Information</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Email Address" name="contact_information[email_address]" maxlength="50" value="<?=set_value("contact_information[email_address]")?>"/>
                        <?php echo form_error("contact_information[email_address]"); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" telnum="telephone" placeholder="Telephone Number" name="contact_information[telephone]" maxlength="11" value="<?=set_value("contact_information[telephone]")?>"/>
                        <?php echo form_error("contact_information[telephone]"); ?>
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" telnum="mobile"  placeholder="Mobile Number" name="contact_information[mobile]" maxlength="11" value="<?=set_value("contact_information[mobile]")?>"/>
                    </div>
                </div>
                <p class="small_text">Note: Please provide either Telephone Number or Mobile Number</p>
            </div>
            <!-- END OF CONTACT INFO -->
            <!-- VEHICLE LOCATION INFO -->
            <div class="form_content">
                <div class="line_title"><p>Location of Vehicle</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <select class="form-control chosen-select" data-placeholder="Province" name="vehicle_information[address][province]" onchange="getCities()">
                            <option></option>
                            <?php
                            foreach ($provinces as $val) {
                                echo '<option value="'.$val['pk'].'"';
                                if ($this->input->post("vehicle_information")['address']['province']) {
                                    echo  set_select("vehicle_information[address][province]", $val['pk'], TRUE);
                                }
                                echo '>'.$val['place'].'</option>';
                            }
                            ?>
                        </select>
                        <?php echo form_error("vehicle_information[address][province]"); ?>
                    </div>
                    <div class="col-md-4 form_input required">
                        <select class="form-control chosen-select" data-placeholder="City"  name="vehicle_information[address][city]" onchange="getBarangays()" <?php if (empty($cities)) echo 'disabled';?>>
                            <option></option>
                            <?php
                            if (!empty($cities)) {
                                foreach ($cities as $val) {
                                    echo '<option value="' . $val['pk'] . '"';
                                    if ($this->input->post("vehicle_information")['address']['city']) {
                                        echo  set_select("vehicle_information[address][city]", $val['pk'], TRUE);
                                    }
                                    echo '>' . $val['place'] . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <?php echo form_error("vehicle_information[address][city]"); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <select class="form-control chosen-select" data-placeholder="Barangay" name="vehicle_information[address][barangay]" <?php if (empty($barangays)) echo 'disabled';?>>
                            <option></option>
                            <?php
                            if (!empty($barangays)) {
                                foreach ($barangays as $val) {
                                    echo '<option value="' . $val['pk'] . '"';
                                    if ($this->input->post("vehicle_information")['address']['barangay']) {
                                        echo  set_select("vehicle_information[address][barangay]", $val['pk'], TRUE);
                                    }
                                    echo '>' . $val['place'] . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <?php echo form_error("vehicle_information[address][barangay]"); ?>
                    </div>
                </div>
            </div>
            <!-- END OF VEHICLE LOCATION INFO -->
            <!-- VEHICLE INFO -->
            <div class="form_content">
                <div class="line_title"><p>Vehicle Information</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <!-- Insurance Info -->
                        <select class="form-control chosen-select" data-placeholder="Type of Insurance" name="vehicle_information[insurance_type]" onchange="askIfNew()">
                            <option></option>
                            <?php
                            foreach ($insurance_types as $val) {
                                echo '<option value="'.$val[0].'"';
                                if ($this->input->post("vehicle_information")['insurance_type']) {
                                    echo  set_select("vehicle_information['insurance_type']", $val[0], TRUE);
                                }

                                echo '>'.$val[1].'</option>';
                            }
                            ?>
                        </select>
                        <?php echo form_error("vehicle_information[insurance_type]"); ?>
                        <!-- /Insurance Info -->
                    </div>
                </div>
                <div class="form_line is_new <?php
                    if ($this->input->post("vehicle_information")['insurance_type'] != NULL) {
                        if ($this->input->post("vehicle_information")['insurance_type'] == COMPREHENSIVE || $this->input->post("vehicle_information")['insurance_type'] == COMPREHENSIVE_AON) {
                            echo 'hidden';
                        }
                    } else {
                        echo 'hidden';
                    }
                ?>">
                    <div class="col-md-4 form_input">
                        <div class="radio_type">
                            <div class="radio_type_title">
                                Is your car brand new?<span class="hl">*</span>
                                <div class="visible-xs-inline-block pull-right">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                                        <label class="onoffswitch-label" for="myonoffswitch">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="radio_group hidden-xs">
                                <label class="radio-inline">
                                    <input type="radio" name="vehicle_information[is_new]" id="inlineRadio1" value="1" <?=set_checkbox("vehicle_information[is_new]", 1)?>/> Yes
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="vehicle_information[is_new]" id="inlineRadio2" value="0" <?=set_checkbox("vehicle_information[is_new]", 0)?>/> No
                                </label>
                            </div>
                            <?php echo form_error("vehicle_information[is_new]"); ?>
                        </div>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <!-- Car Year -->
                        <select class="form-control chosen-select" data-placeholder="Year Acquired" name="vehicle_information[year]" onchange="getBrands()">
                            <option></option>
                            <?php
                            foreach ($year as $val) {
                                echo '<option value="'.$val.'"';
                                if ($this->input->post("vehicle_information")['year']) {
                                    echo set_select("vehicle_information[year]", $val, TRUE);
                                }
                                echo '>'.$val.'</option>';
                            }
                            ?>
                        </select>
                        <?php echo form_error("vehicle_information[year]"); ?>
                        <!-- /Car Year -->
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <!-- Brand -->
                        <select class="form-control chosen-select" data-placeholder="Brand" name="vehicle_information[brand]" onchange="getModels()" <?php if (empty($brands)) echo 'disabled';?>>
                            <option></option>
                            <?php
                            if (!empty($brands)) {
                                foreach ($brands as $val) {
                                    echo '<option value="' . $val['brand'] . '"';
                                    if (isset($this->input->post("vehicle_information")['brand'])) {
                                        echo set_select("vehicle_information[brand]", $val['brand'], TRUE);
                                    }
                                    echo '>' . $val['brand'] . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <?php echo form_error("vehicle_information[brand]"); ?>
                        <!-- /Brand -->
                    </div>
                </div>
                <div class="form_line ">
                    <div class="col-md-4 form_input required">
                        <fieldset>
                            <select id="disabledSelect" class="form-control chosen-select" data-placeholder="Model Name" name="vehicle_information[model_name]" onchange="selectModel()" <?php if (empty($models)) echo 'disabled';?>>
                                <option></option>
                                <?php
                                if (!empty($models)) {
                                    foreach ($models as $val) {
                                        echo '<option value="' . $val['model_name'] . '"';
                                        if ($this->input->post("vehicle_information")['model_name']) {
                                            echo set_select("vehicle_information[model_name]", $val['model_name'], TRUE);
                                        }
                                        echo ' data-pk="' . $val['pk'] . '">' . $val['model_name'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                            <?php echo form_error("vehicle_information[model_name]"); ?>
                        </fieldset>
                    </div>
                    <div class="col-md-4 form_input <?php
                    if (isset($this->input->post("vehicle_information")['model_name'])) {
                        if ($this->input->post("vehicle_information")['model_name'] != 'Other') {
                            echo 'hidden';
                        }
                    } else {
                        echo 'hidden';
                    }
                    ?>">
                        <input type="text" class="form-control required" name="vehicle_information[other_model]" placeholder="Other Model Name" value="<?=set_value("vehicle_information[other_model]")?>"/>
                        <?php echo form_error("vehicle_information[other_model]"); ?>
                    </div>
                </div>
            </div>
            <!-- END OF  VEHICLE INFO -->
            <!-- MARKET VALUE -->
            <div class="form_content ma <?php
            if (!isset($model) || $this->input->post("vehicle_information")['other_model'] == CTPL) {
                echo 'hidden';
            }
            ?>">
                <div class="line_title"><p>Market Value</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input market_value_slider">
                        <div class="market_value_slider-inner">
                            <input type="range" min="<?php if (isset($min)) echo $min;?>" max="<?php if (isset($max)) echo $max;?>" step="1" data-rangeslider name="vehicle_information[market_value]" value="<?=set_value("vehicle_information[market_value]")?>"/>
                            <span class="value_left value_range">Php <span class="min-value"><?php if (isset($min)) echo number_format($min, 2, '.', ',');?></span></span>
                            <span class="value_right value_range">Php <span class="max-value"><?php if (isset($max)) echo number_format($max, 2, '.', ',');?></span></span>
                        </div>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control market_value" placeholder="Market Value" readonly="readonly"/>
                        <input type="hidden" name="vehicle_information[original_market_value]" value="<?=set_value("vehicle_information[original_market_value]")?>"/>
                    </div>
                </div>
            </div>
            <!-- END OF MARKET VALUE -->
            <div class="buttons">
                <p class="form_button">
                    <input type="hidden" name="token" value="<?=$this->uri->segment(3)?>"/>
                    <input type="submit" class="btn btn-primary btn-lg" name="submit" value="Save">
                    <input type="submit" class="btn btn-success btn-lg" name="submit" value="Get Quote">
                </p>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.chosen-select').chosen({width:"100%", disable_search_threshold: 10, search_contains: true});
        $('[data-rangeslider]').rangeslider({
            polyfill: false,
            rangeClass: 'rangeslider',
            fillClass: 'rangeslider__fill',
            handleClass: 'rangeslider__handle',
            onInit: function() {},
            onSlide: function (position, value) {
                $(".market_value").val('Php '+common.number_format(value, 2, '.', ','));
            },
            onSlideEnd: function(position, value) {}
        });
    });
    function getCities() {
        common.ajax('<?=site_url('place/cities')?>', {province: $("[name='vehicle_information[address][province]']").val()},
        function(response) {
            if (response.length > 0) {
                var options = '<option></option>';
                $.each(response, function(i, v) {
                    options += '<option value="' + v.pk + '">' + v.place + '</option>';
                });
                $("[name='vehicle_information[address][city]']").html(options).prop("disabled", false).trigger("chosen:updated");
                $("[name='vehicle_information[address][barangay]']").val(null).prop("disabled", true).trigger("chosen:updated");
            }
        });
    }
    function getBarangays() {
        common.ajax('<?=site_url('place/barangays')?>', {city: $("[name='vehicle_information[address][city]']").val()},
        function(response) {
            if (response.length > 0) {
                var options = '<option></option>';
                $.each(response, function(i, v) {
                    options += '<option value="' + v.pk + '">' + v.place + '</option>';
                });
                $("[name='vehicle_information[address][barangay]']").html(options).prop("disabled", false).trigger("chosen:updated");
            }
        });
    }
    function getBrands() {
        common.ajax('<?=site_url('vehicle/brands')?>', {year: $("[name='vehicle_information[year]']").val()},
        function(response) {
            if (response.length > 0) {
                var options = '<option></option>';
                $.each(response, function(i, v) {
                    options += '<option value="' + v.brand + '">' + v.brand + '</option>';
                });
                $("[name='vehicle_information[brand]']").html(options).prop("disabled", false).trigger("chosen:updated");
                $("[name='vehicle_information[model_name]']").val(null).prop("disabled", true).trigger("chosen:updated");
                $("[name='vehicle_information[other_model]']").parent().addClass('hidden');
                $("[name='vehicle_information[market_value]']").parent().parent().parent().parent().addClass('hidden');
            }
        });
    }
    function getModels() {
        common.ajax('<?=site_url('vehicle/models')?>', {year: $("[name='vehicle_information[year]']").val(), brand: $("[name='vehicle_information[brand]']").val()},
        function(response) {
            if (response.length > 0) {
                var options = '<option></option>';
                $.each(response, function(i, v) {
                    options += '<option value="' + v.model_name + '" data-pk="' + v.pk + '">' + v.model_name + '</option>';
                });
                options += '<option value="Other" pk="Other">Other</option>';
                $("[name='vehicle_information[model_name]']").html(options).prop("disabled", false).trigger("chosen:updated");
                $("[name='vehicle_information[other_model]']").parent().addClass('hidden');
                $("[name='vehicle_information[market_value]']").parent().parent().parent().parent().addClass('hidden');
            }
        });
    }
    function loadMarketValue() {
        common.ajax('<?=site_url('vehicle/model')?>', {motor_pk: $("[name='vehicle_information[model_name]']").find('option:selected', this).attr('data-pk')},
            function (response) {
                $("[name='vehicle_information[market_value]']").rangeslider('destroy');
                $("[name='vehicle_information[market_value]']").parent().parent().parent().parent().removeClass('hidden');
                $("[name='vehicle_information[other_model]']").parent().addClass('hidden');

                $("[name='vehicle_information[market_value]']").attr('max', (parseFloat(response.sum_insured) * 1.1).toFixed(0));
                var max = parseFloat((response.sum_insured * 1.1).toFixed(0));
                $(".max-value").html(common.number_format(max, 2, '.', ','));
                $("[name='vehicle_information[market_value]']").attr('min', (parseFloat(response.sum_insured) * 0.9).toFixed(0));
                var min = parseFloat((response.sum_insured * 0.9).toFixed(0));
                $(".min-value").html(common.number_format(min, 2, '.', ','));
                $("[name='vehicle_information[original_market_value]']").val(response.sum_insured);

                $("[name='vehicle_information[market_value]']").rangeslider({
                    polyfill: false,
                    rangeClass: 'rangeslider',
                    fillClass: 'rangeslider__fill',
                    handleClass: 'rangeslider__handle',
                    onInit: function () {
                        $("[name='vehicle_information[market_value]']").val(response.sum_insured);
                    },
                    onSlide: function (position, value) {
                        $(".market_value").val('Php ' + common.number_format(value, 2, '.', ','));
                    }
                });
                $("[name='vehicle_information[market_value]']").val(response.sum_insured).change();
            });
    }

    function selectModel() {
        if ($("[name='vehicle_information[model_name]']").val() != "Other") {
            if($("[name='vehicle_information[insurance_type]']").val() != <?=CTPL?>) {
                loadMarketValue();
            } else {
                $("[name='vehicle_information[other_model]']").parent().addClass('hidden');
                $("[name='vehicle_information[market_value]']").parent().parent().parent().parent().addClass('hidden');
                $("[name='vehicle_information[market_value]']").rangeslider('destroy');
            }
        } else {
            $("[name='vehicle_information[other_model]']").parent().removeClass('hidden');
            $("[name='vehicle_information[market_value]']").parent().parent().parent().parent().addClass('hidden');
            $("[name='vehicle_information[market_value]']").rangeslider('destroy');
        }
    }
    function askIfNew() {
        if ($("[name='vehicle_information[insurance_type]']").val() != '<?=COMPREHENSIVE?>' && $("[name='vehicle_information[insurance_type]']").val() != '<?=COMPREHENSIVE_AON?>') {
            $('.is_new').removeClass('hidden');
        } else {
            $('.is_new').addClass('hidden');
        }

        if ($("[name='vehicle_information[insurance_type]']").val() == <?=CTPL?>) {
            $("[name='vehicle_information[other_model]']").parent().addClass('hidden');
            $("[name='vehicle_information[market_value]']").parent().parent().parent().parent().addClass('hidden');
            $("[name='vehicle_information[market_value]']").rangeslider('destroy');
        } else {
            if ($("[name='vehicle_information[model_name]']").val() != "") {
                loadMarketValue();
            }
        }
    }
</script>