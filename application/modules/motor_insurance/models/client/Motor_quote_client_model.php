<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_quote_client_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->model('motor_insurance/motor_model', 'motor_insurance');
        $this->load->model('vehicle/vehicle_model', 'vehicle');
    }

    public function fetch() {
        $query = $this->db->select([
            'motor_quotes.pk',
            'motor_quotes.quote_id',
            'motor_quotes.status',
            'motor_quotes.insurance_type',
            'motor_quotes_computations.gross_premium',
            'motor_quotes_computations.ctpl_net_premium',
            "GROUP_CONCAT(CONCAT(motor_quotes_coverages.is_ctpl, '~',  motor_quotes_coverages.start_date)) as start_date",
            "GROUP_CONCAT(CONCAT(motor_quotes_coverages.is_ctpl, '~', motor_quotes_coverages.end_date)) as end_date",
            'motor_quotes.date_created'
        ])
        ->join('motor_quotes_computations', 'motor_quotes_computations.motor_quotes_pk = motor_quotes.pk', 'left')
        ->join('motor_quotes_coverages', 'motor_quotes_coverages.motor_quotes_pk = motor_quotes.pk', 'left')
        ->join('users_quotes', 'users_quotes.quotes_pk = motor_quotes.pk', 'left')
        ->group_by('motor_quotes.quote_id')
        ->order_by('motor_quotes.date_created DESC, motor_quotes_coverages.is_ctpl ASC')
        ->where('users_quotes.users_pk', $this->session->userdata('pk'))
        ->get('motor_quotes');

        $quotes = $query->result();

        if($query->num_rows() > 0) {
            foreach($quotes as $key => $quote) {
                if($quote->status != 'ACTIVE') {
                    $motorQuote = $this->motor->get_motor_quote_by_pk($quote->pk);
                    $motor = $this->vehicle->get_motor_for_computation($motorQuote['motors_pk']);
                    $data = array_merge($this->motor->quote_premiums($motor, $motorQuote), $motorQuote);

                    $quotes[$key]->net_premium = $data['netPremium'];
                    $quotes[$key]->ctpl_net_premium = $data['ctplNetPremium'];
                }
            }
        }

        return $quotes;
    }

}
