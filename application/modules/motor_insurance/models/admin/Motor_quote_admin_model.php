<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_quote_admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Admin Motor List
     *
     * @param array $filters
     * @return mixed
     */
    public function fetch($filters = []) {
        /**
         * Main Query
         */
        $offset = isset($_GET['per_page']) ? $_GET['per_page'] - 1 : 0;

        $query = $this->db->select(array(
            'personal_informations.fname as personal_fname',
            'personal_informations.lname as personal_lname',
            'users.email_address as personal_email_address',
            'motor_quotes.pk',
            'motor_quotes.fname',
            'motor_quotes.lname',
            'motor_quotes.email_address',
            'motor_quotes.status',
            'motor_quotes.date_created',
            'motor_quotes.quote_id'
        ))
        ->order_by('pk DESC')
        ->limit(TOTAL_ROWS)
        ->offset($offset)
//        ->join('motor_quotes_coverages', 'motor_quotes.pk = motor_quotes_coverages.motor_quotes_pk', 'left')
        ->join('users_quotes', 'users_quotes.pk = motor_quotes.users_quotes_pk', 'left')
        ->join('personal_informations', 'personal_informations.users_quotes_pk = users_quotes.pk', 'left')
        ->join('users', 'users.pk = users_quotes.users_pk', 'left');
//        ->where('motor_quotes_coverages.is_ctpl', 0);

        /**
         * Filters Loop
         */
        foreach($filters as $key=>$filter) {
            if($filter) {
                $query->where($key, $filter);
            }
        }

        /**
         * Return Results
         */
        $query = $query->get('motor_quotes');
        if($query->num_rows()) {
            return $query->result();
        }

        return false;
    }

    /**
     * @param $quoteId
     * @param $status
     * @return boolean
     */
    public function update_status($quoteId, $status) {

        $query = $this->db->where('quote_id', $quoteId)
                          ->update('motor_quotes', ['status' => $status]);

        return $query;

    }

    /**
     * Get total count of motor_insurance quotes
     * @return mixed
     */
    public function total_rows($filters = []) {
        /**
         * Main Query
         */
        $query = $this->db->select('COUNT(*) as total_rows');

        /**
         * Filters Loop
         */
        foreach($filters as $key=>$filter) {
            if($filter) {
                $query->where($key, $filter);
            }
        }

        /**
         * Return Results
         */
        $query = $query->get('motor_quotes');
        if($query->num_rows()) {
            $result = $query->row_object();
            return $result->total_rows;
        }

        return false;
    }

}
