<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_computation_admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('place/place_model', 'place');
    }

    /**
     * Single Motor Quote Computation
     */
    public function fetch_by_pk($pk, $motorQuote) {

        $query = $this->db->select([
            'ctpl_net_premium as ctplNetPremium',
            'ctpl_gross_premium as ctplGrossPremium',
            'ctpl_vat as ctplVat',
            'ctpl_doc_stamps as ctplDocStamps',
            'ctpl_local_government_tax as ctplLocalGovernmentTax',
            'ctpl_Interconnectivity as ctplInterconnectivity',
            'ctpl',
            'net_premium as netPremium',
            'gross_premium as grossPremium',
            'doc_stamps as docStamps',
            'vat',
            'local_government_tax as localGovernmentTax',
            'own_damage as ownDamage',
            'own_damage_coverage as ownDamageCoverage',
            'bodily_injury as bodilyInjury',
            'property_damage as propertyDamage',
            'auto_personal_accident as autoPersonalAccident',
            'auto_personal_accident_coverage as autoPersonalAccidentCoverage',
            'aon',
            'deductible'
        ])
        ->where('motor_quotes_pk', $pk)
        ->get('motor_quotes_computations');

        /**
         * Fetch Computations
         */
        $computation = $query->row_array();
        $computation['premium'] = $computation['ctplGrossPremium'] + $computation['grossPremium'];

        /**
         * Setup Motor Details.
         */
        $motorQuotePk = $motorQuote['pk'];
        $motorQuote['locationOfVehicle'] = $this->place->location($motorQuote['province'], $motorQuote['city'], $motorQuote['barangay']);
        $motorQuote['name'] = $motorQuote['fname'] . " " . $motorQuote['lname'];
        $motorQuote['transaction'] = $motorQuotePk;

        /**
         * Merge Computations and Motors.
         */
        $computation = array_merge($computation, $motorQuote);

        return $computation;

    }

}


