<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_rules_model extends CI_Model {

    public function get_quote_rules() {
        return array(
            array(
                'field' => 'personal_information[lname]',
                'label' => 'Last name',
                'rules' => 'required|max_length[50]'
            ),
            array(
                'field' => 'personal_information[fname]',
                'label' => 'First name',
                'rules' => 'required|max_length[50]'
            ),
            array(
                'field' => 'contact_information[email_address]',
                'label' => 'Email address',
                'rules' => 'required|valid_email|max_length[50]'
            ),
            array(
                'field' => 'vehicle_information[address][province]',
                'label' => 'Province',
                'rules' => 'required|max_length[11]'
            ),
            array(
                'field' => 'vehicle_information[address][city]',
                'label' => 'City',
                'rules' => 'required|max_length[11]'
            ),
            array(
                'field' => 'vehicle_information[address][barangay]',
                'label' => 'Barangay',
                'rules' => 'required|max_length[11]'
            ),
            array(
                'field' => 'vehicle_information[insurance_type]',
                'label' => 'Insurance Type',
                'rules' => 'required|max_length[1]'
            ),
            array(
                'field' => 'vehicle_information[year]',
                'label' => 'Year',
                'rules' => 'required|max_length[4]'
            ),
            array(
                'field' => 'vehicle_information[brand]',
                'label' => 'Brand',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'vehicle_information[model_name]',
                'label' => 'Model name',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'vehicle_information[market_value]',
                'label' => 'Market value',
                'rules' => 'required|max_length[20]'
            )
        );
    }

    public function vehicle_information_rules() {
        return array(
            array(
                'field' => 'vehicle_information[plate_number]',
                'label' => 'Plate number',
                'rules' => 'required|max_length[50]'
            ),
            array(
                'field' => 'vehicle_information[engine_number]',
                'label' => 'Engine number',
                'rules' => 'required|max_length[50]'
            ),
            array(
                'field' => 'vehicle_information[chassis_number]',
                'label' => 'Chassis number',
                'rules' => 'required|max_length[50]'
            ),
//            array(
//                'field' => 'vehicle_information[mv_file_number]',
//                'label' => 'MV file number',
//                'rules' => 'max_length[50]'
//            ),
            array(
                'field' => 'vehicle_information[color]',
                'label' => 'Color',
                'rules' => 'required|max_length[100]'
            ),
//            array(
//                'field' => 'or_cr_file',
//                'label' => 'OR CR file',
//                'rules' => 'required'
//            ),
            array(
                'field' => 'vehicle_information[mortgagees]',
                'label' => 'Mortgagee',
                'rules' => 'max_length[11]'
            ),
            array(
                'field' => 'vehicle_information[insurance_providers]',
                'label' => 'Insurance provider',
                'rules' => 'max_length[11]'
            ),
        );
    }

}
