<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_quote_agent_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save($fields) {
        $insert = [];

        $insert['motor_quotes_pk'] = $this->session->userdata('motor_quotes_pk');

        if($fields['agent_type'] == 'Agent Name') {
            $insert['is_name'] = true;
            $insert['fname'] = $fields['agent_fname'];
            $insert['lname'] = $fields['agent_lname'];
        }
        else {
            $insert['is_name'] = false;
            $insert['code'] = $fields['agent_code'];

            $query  = $this->db->where(['code' => $fields['agent_code']])->get('agents');
            $result = $query->row_object();
            $insert['fname'] = $result->fname;
            $insert['lname'] = $result->lname;
        }

        return $this->db->insert('motor_quotes_agents', $insert);

    }

    public function destroy_by_field($field, $value) {
        return $this->db->delete('motor_quotes_agents', [$field => $value]);
    }

    public function fetch_by_field($field, $value) {
        $return = false;

        $this->db->where($field, $value);
        $this->db->from('motor_quotes_agents');

        $query = $this->db->get();
        if($query->num_rows()) {
            return $query->row_array();
        }
        return $return;
    }

}