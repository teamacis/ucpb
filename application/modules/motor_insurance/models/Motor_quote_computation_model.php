<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_quote_computation_model extends CI_Model {
    protected $pk = '';
    protected $motor_quotes_pk = '';
    protected $net_premium = '';
    protected $ctpl_net_premium = '';
    protected $gross_premium = '';
    protected $ctpl_gross_premium = '';
    protected $doc_stamps = '';
    protected $ctpl_doc_stamps = '';
    protected $vat = '';
    protected $ctpl_vat = '';
    protected $local_government_tax = '';
    protected $ctpl_local_government_tax = '';
    protected $own_damage = '';
    protected $bodily_injury = '';
    protected $property_damage = '';
    protected $auto_personal_accident = '';
    protected $aon = '';
    protected $deductible = '';
    protected $ctpl_interconnectivity = '';

    public function __construct() {
        parent::__construct();
    }

    public function set($field, $value) {
        if (isset($this->$field)) {
            $this->$field = $value;
        }
    }

    public function get($field) {
        if (isset($this->$field)) {
            return $this->$field;
        } else {
            return NULL;
        }
    }

    public function get_data() {
        $reflect = new ReflectionClass($this);
        return get_object_vars($this);
    }

    public function set_data($data) {
        foreach($data as $field => $value) {
            $this->set($field, $value);
        }
    }
}
