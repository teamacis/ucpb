<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_quote_model extends CI_Model {
    protected $pk = '';
    protected $quote_id = '';
    protected $users_quotes_pk = '';
    protected $lname = '';
    protected $fname = '';
    protected $email_address = '';
    protected $telephone = '';
    protected $mobile = '';
    protected $type = 1;
    protected $province = '';
    protected $city = '';
    protected $barangay = '';
    protected $insurance_type = '';
    protected $is_new = '';
    protected $motors_pk = '';
    protected $year = '';
    protected $brand = '';
    protected $model_name = '';
    protected $market_value = '';
    protected $original_market_value = '';
    protected $delivery = '';
    protected $status = '';
    protected $is_requested = '';
    protected $token = '';
    protected $date_created = '';

    public function __construct() {
        parent::__construct();

        $this->load->model('motor_insurance/motor_model', 'motor_insurance');
        $this->load->model('vehicle/vehicle_model', 'vehicle');
        $this->load->model('motor_insurance/admin/motor_computation_admin_model', 'motor_computation');
        $this->load->model('mortgagee/mortgagee_model', 'mortgagee');
        $this->load->model('insurance_provider/insurance_provider_model', 'insurance_provider');
        $this->load->model('place/place_model', 'place');
        $this->load->model('policy/policy_model');
        $this->load->model('motor_quotes_file/motor_quotes_file_model', 'motor_quote_file');
        $this->load->model('personal_information/personal_information_model');
        $this->load->model('motor_insurance/motor_quote_agent_model');
        $this->load->model('place/place_model');

    }

    public function set($field, $value) {
        if (isset($this->$field)) {
            $this->$field = $value;
        }
    }

    public function get($field) {
        if (isset($this->$field)) {
            return $this->$field;
        } else {
            return NULL;
        }
    }

    public function get_data() {
        $reflect = new ReflectionClass($this);
        return get_object_vars($this);
    }

    public function set_data($data) {
        foreach($data as $field => $value) {
            $this->set($field, $value);
        }
    }

    public function update_delivery($pk, $delivery) {
        $query = $this->db->where('pk', $pk)
                          ->update('motor_quotes', ['delivery' => $delivery]);
    }

    /*
	|--------------------------------------------------------------------------
	| POLICY SCHEDULE
	|--------------------------------------------------------------------------
	*/
    public function generate_policy_schedule($motor_quotes_pk, $is_ctpl = FALSE, $createFile = false) {

        $motorQuote = $this->motor_model->get_motor_quote_by_pk($motor_quotes_pk);
        $motor_computation = $this->vehicle_model->get_motor_for_computation($motorQuote['motors_pk']);

        $data =  $motorQuote;//array_merge($motor_computation, $motorQuote);

        $data['is_ctpl'] = $is_ctpl;

        // Fetch Coverage
        $coverage = $this->motor_model->get_coverage($motor_quotes_pk, $is_ctpl);
        unset($coverage['pk']);

        // Fetch Motor Model
        $motors            = $this->vehicle_model->get_filtered_model($data);

        // Merge Coverage
        $data =  array_merge($coverage, $data, $motors);

        // Fetch Motor Information
        $motor_info        = $this->motor_model->get_vehicle_information($motor_quotes_pk);
        $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($motorQuote['token']), TRUE);
        $motor_premium = $this->motor_model->quote_premiums($motor_computation, $motor_saved_quotes);

        // Fetch Computation
//        $motor_premium     = $this->motor_insurance->quote_premiums($motor_computation, $data);
        $data['non_standard'] = $this->motor_model->get_accessories_selected($motorQuote['pk']);

        // Fetch Agent
        $agent               = $this->motor_quote_agent_model->fetch_by_field('motor_quotes_pk', $motorQuote['pk']);
        if (!$agent) {
            $agent = [];
        }

        $policy = $this->policy_model->get_policy_by_quote_id($motor_quotes_pk);
        $ctpl_coverages = $this->motor_model->get_coverage($motorQuote['pk'], true);
        $ctpl_coverages['ctpl_start_date'] = date('M d, Y',strtotime($ctpl_coverages['ctpl_start_date']));
        $ctpl_coverages['ctpl_end_date']   = date('M d, Y',strtotime($ctpl_coverages['ctpl_end_date']));

        $non_ctpl_coverages = $this->motor_model->get_coverage($motorQuote['pk'], false);
        $non_ctpl_coverages['non_ctpl_start_date'] = date('M d, Y',strtotime($non_ctpl_coverages['non_ctpl_start_date']));
        $non_ctpl_coverages['non_ctpl_end_date']   = date('M d, Y',strtotime($non_ctpl_coverages['non_ctpl_end_date']));

        // Merge All Data
        $data              = (object) array_merge($data, $motor_premium, $motor_info, $agent, $ctpl_coverages, $non_ctpl_coverages, $policy);
        $data->issued = date('Y-m-d');

        $pdf = new Pdf();

        $pdf->generate_pages([
            $this->load->view('motor_insurance/pdf/policy_schedule/page_one', ['data' => $data], true),
            $this->load->view('motor_insurance/pdf/policy_schedule/page_two', [], true)
        ]);

        $name = 'policy-schedule-' . $motorQuote['token'] . '-' . ($is_ctpl ? 'ctpl' : 'comprehensive') . '.pdf';
        $path = POLICIES_FOLDER.$name;

        $pdf->create($path, $createFile, [
            'name' => $name,
            'type' => 'policy-schedule-' . ($is_ctpl ? 'ctpl' : 'comprehensive'),
            'motor_saved_quotes_token' => $motorQuote['token']
        ]);
    }

    /*
	|--------------------------------------------------------------------------
	| BANK CERTIFICATE
	|--------------------------------------------------------------------------
	*/
    public function generate_bank_certificate($motor_quotes_pk, $createFile = false) {
        $motorQuote = $this->motor_model->get_motor_quote_by_pk($motor_quotes_pk);

        $motorInformation = $this->motor_model->get_vehicle_information($motor_quotes_pk);
        $motor_computation = $this->vehicle_model->get_motor_for_computation($motor_quotes_pk);

        $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($motorQuote['token']), TRUE);

        $premium = $this->motor_model->quote_premiums($motor_computation, $motor_saved_quotes);

        $data              = (object) array_merge($premium, $motorQuote, $motorInformation);
        $data->effectivity = date('Y-m-d');

        $pdf = new Pdf();

        $pdf->generate_pages([
            $this->load->view('motor_insurance/pdf/bank_certificate/page_one', ['data' => $data], true)
        ]);

        $name = "bank-certificate-{$motorQuote['token']}.pdf";
        $path = MOTOR_QUOTE_FILES_BANK_CERTIFICATE.$name;

        $pdf->create($path, $createFile, [
            'name' => $name,
            'type' => 'bank-certificate',
            'motor_saved_quotes_token' => $motorQuote['token']
        ]);

    }

    /*
	|--------------------------------------------------------------------------
	| MOTOR QUOTE FILE
	|--------------------------------------------------------------------------
	*/
    public function generate_motor_quote_request($motor_quotes_pk, $createFile = false) {
        $motorQuote = $this->motor_model->get_motor_quote_by_pk($motor_quotes_pk);

        $motorInformation = $this->motor_model->get_vehicle_information($motor_quotes_pk);
        $motor_computation = $this->vehicle_model->get_motor_for_computation($motor_quotes_pk);

        $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($motorQuote['token']), TRUE);

        $premium = $this->motor_model->quote_premiums($motor_computation, $motor_saved_quotes);

        $condition = array(
            'year' => $motorQuote['year'],
            'brand' => $motorQuote['brand'],
            'model_name' => $motorQuote['model_name']
        );
        $motors = $this->vehicle_model->get_filtered_model($condition);

        $data              = (object) array_merge($premium, $motorQuote, $motorInformation, $motors);

        $pdf = new Pdf();


        $pages[] = $this->load->view("motor_insurance/pdf/motor_quote_form/get_quote", $data, true);
        $pages[] = $this->load->view("motor_insurance/pdf/motor_quote_form/vehicle_information", $data, true);
        $pages[] = $this->load->view("motor_insurance/pdf/motor_quote_form/coverage_details", $data, true);
        $pages[] = $this->load->view("motor_insurance/pdf/motor_quote_form/personal_information", $data, true);

        $pdf->generate_pages($pages);

        $name = "quote-request-{$motorQuote['token']}.pdf";
        $path = MOTOR_QUOTE_FILES_QUOTE_REQUEST.$name;

        $pdf->create($path, $createFile, [
            'name' => $name,
            'type' => 'quote-request',
            'motor_saved_quotes_token' => $motorQuote['token']
        ]);
    }

    public function generate_invoice($motor_quotes_pk, $is_ctpl = FALSE, $createFile = false) {
        $motorQuote = $this->motor_model->get_motor_quote_by_pk($motor_quotes_pk);

        $motor_computation = $this->vehicle_model->get_motor_for_computation($motorQuote['motors_pk']);


//        $motorQuote = $this->motor_insurance->get_motor_quote_by_field('quote_id', $quoteId);
        $data =  array_merge($motor_computation, $motorQuote);

        $data['is_ctpl'] = $is_ctpl;

        // Fetch Coverage
//        $coverage = $this->motor_insurance->get_coverage($motorQuote['pk'], $is_ctpl);
//        unset($coverage['pk']);

        // Fetch Motor Model
        $motors            = $this->vehicle_model->get_filtered_model($data);

        // Merge Coverage
//        $data =  array_merge($coverage, $data);

        // Fetch Motor Information
        $motor_info        = $this->motor_model->get_vehicle_information($motor_quotes_pk);
        $policy = $this->policy_model->get_policy_by_quote_id($motor_quotes_pk);
        $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($motorQuote['token']), TRUE);
        $motor_premium = $this->motor_model->quote_premiums($motor_computation, $motor_saved_quotes);


        $data['non_standard'] = $this->motor_model->get_accessories_selected($motorQuote['pk']);
//        $data['non_standard'] = $this->motor_insurance->quote_premiums($motor_computation, $data);
//dd($data['non_standard']);
        // Fetch Agent
        $agent               = $this->motor_quote_agent_model->fetch_by_field('motor_quotes_pk', $motorQuote['pk']);
        if (!$agent) {
            $agent = [];
        }

        $condition = array(
            'year' => $motorQuote['year'],
            'brand' => $motorQuote['brand'],
            'model_name' => $motorQuote['model_name']
        );
//        $motors = $this->motor_model->get_filtered_model($condition);
        $ctpl_coverages = $this->motor_model->get_coverage($motorQuote['pk'], true);
        $ctpl_coverages['ctpl_start_date'] = date('M d, Y',strtotime($ctpl_coverages['ctpl_start_date']));
        $ctpl_coverages['ctpl_end_date']   = date('M d, Y',strtotime($ctpl_coverages['ctpl_end_date']));

        $non_ctpl_coverages = $this->motor_model->get_coverage($motorQuote['pk'], false);
        $non_ctpl_coverages['non_ctpl_start_date'] = date('M d, Y',strtotime($non_ctpl_coverages['non_ctpl_start_date']));
        $non_ctpl_coverages['non_ctpl_end_date']   = date('M d, Y',strtotime($non_ctpl_coverages['non_ctpl_end_date']));

        // Merge All Data
        $data              = (object) array_merge($data, $motors, $motor_computation, $motor_premium, $motor_info, $agent, $ctpl_coverages, $non_ctpl_coverages, $policy);


        $data->issued = date('Y-m-d');

        $pdf = new Pdf();

        $pdf->generate_pages([
            $this->load->view("motor_insurance/pdf/invoice/invoice", ['data' => $data]    , true)
        ]);

        $name = 'invoice-' . $motorQuote['token'] . '-' . ($is_ctpl ? 'ctpl' : 'comprehensive') . '.pdf';
        $path = MOTOR_QUOTE_FILES_INVOICE.$name;

        $pdf->create($path, $createFile, [
            'name' => $name,
            'type' => 'invoice',
            'motor_saved_quotes_token' => $motorQuote['token']
        ]);
    }

    public function generate_confirmation_of_cover($motor_quotes_pk, $createFile = false) {

        $motorQuote = $this->motor_model->get_motor_quote_by_pk($motor_quotes_pk);

        $motorInformation = $this->motor_model->get_vehicle_information($motor_quotes_pk);
        $motor_computation = $this->vehicle_model->get_motor_for_computation($motor_quotes_pk);

        $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($motorQuote['token']), TRUE);

        $premium = $this->motor_model->quote_premiums($motor_computation, $motor_saved_quotes);

        $condition = array(
            'year' => $motorQuote['year'],
            'brand' => $motorQuote['brand'],
            'model_name' => $motorQuote['model_name']
        );
        $motors = $this->vehicle_model->get_filtered_model($condition);
        $ctpl_coverages = $this->motor_model->get_coverage($motorQuote['pk'], true);
        $ctpl_coverages['ctpl_start_date'] = date('M d, Y',strtotime($ctpl_coverages['ctpl_start_date']));
        $ctpl_coverages['ctpl_end_date']   = date('M d, Y',strtotime($ctpl_coverages['ctpl_end_date']));

        $data             = (object) array_merge($this->motor_computation->fetch_by_pk($motorQuote['pk'], $motorQuote), $motorQuote, $motorInformation, $motors, $ctpl_coverages, $premium);
        $data->date       = date('Y-m-d');



        $pdf = new Pdf();

        $pdf->generate_pages([
            $this->load->view("motor_insurance/pdf/confirmation_of_cover/confirmation_of_cover", $data, true)
        ]);

        $name = "confirmation-of-cover-{$motorQuote['token']}.pdf";
        $path = MOTOR_QUOTE_CONFIRMATION_OF_COVER.$name;

        $pdf->create($path, $createFile, [
            'name' => $name,
            'type' => 'confirmation-of-cover',
            'motor_saved_quotes_token' => $motorQuote['token']
        ]);
    }

    /**
     * Generates formal quote pdf
     * @param $quote_id
     * @param bool $createFile
     */
    public function generate_formal_quote($token, $createFile = false) {
        $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($token), TRUE);
        $motor_info = $this->vehicle_model->get_motor_for_computation($motor_saved_quotes['motors_pk']);
        $quote_premiums = $this->motor_model->quote_premiums($motor_info, $motor_saved_quotes);
        $data = array_merge($motor_saved_quotes, $motor_info, $quote_premiums);

        // Load PDF
        $pdf = new Pdf;

        // Generate Pages
        $pdf->generate_pages([
            $this->load->view('motor_insurance/pdf/formal_quote/formal_quote', $data, true)
        ]);

        $name = $token.'.pdf';
        $path = FORMAL_QUOTES_FOLDER.$name;

        // Generate the PDF
        $pdf->create($path, $createFile, [
            'name' => $name,
            'type' => 'formal-quote',
            'motor_saved_quotes_token' => $token
        ]);
    }

//    public function update_status($txnid) {
//        return $this->db->update('motor_quotes', array('status' => STATUS_APPROVED), 'quote_id = \'' . $txnid . '\'');
//    }
}
