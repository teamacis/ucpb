<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->quotes_table = 'quotes';
    }

    /**
     * Redirects to correct tab process of the get quote steps
     * @param $tabNumber
     */
    public function quote_tab_redirect_handler($tabNumber) {
        $tabs       = ['get_quote', 'get_quote', 'vehicle_information', 'coverage_details', 'personal_information', 'summary_confirmation'];
        $allowedTab = $this->session->userdata('allowed_motor_quote_tab');

        if($tabNumber > $allowedTab || $allowedTab < 1) {
            redirect('motor-insurance/'.$tabs[$allowedTab]);
        }
    }

    /**
     * Updates allowed motor_insurance quote tab
     * @param $tabNumber
     * @param bool $force
     */
    public function update_allowed_motor_quote_tab($tabNumber, $force = false) {
        $allowedTab = $this->session->userdata('allowed_motor_quote_tab');

        if($tabNumber > $allowedTab || $force) {
            $this->session->set_userdata('allowed_motor_quote_tab', $tabNumber);
        }
    }

    /**
     * Initializes default data for the get quote form
     * @return array
     */
    public function initialize_get_quote_form() {
        $data = array(
            'provinces' => [],
            'cities' => [],
            'barangays' => [],
            'year' => [],
            'brands' => [],
            'models' => [],
            'max' => 0,
            'min' => 0,
        );

        $data['provinces'] = $this->place_model->provinces();
        if ($this->input->post('vehicle_information')['address']['province'] != NULL) {
            $data['cities'] = $this->place_model->cities($this->input->post('vehicle_information')['address']['province']);
        }
        if ($this->input->post('vehicle_information')['address']['province'] != NULL && isset($this->input->post('vehicle_information')['address']['city'])) {
            $data['barangays'] = $this->place_model->barangays($this->input->post('vehicle_information')['address']['city']);
        }
        $data['insurance_types'] = array(
            array(COMPREHENSIVE, "Comprehensive"),
            array(CTPL, "CTPL"),
            array(COMPREHENSIVE_CTPL, "Comprehensive and CTPL"),
            array(COMPREHENSIVE_AON, "Comprehensive with AON"),
            array(COMPREHENSIVE_CTPL_AON, "Comprehensive with AON and CTPL")
        );
        for ($i = 0, $year = date("Y"); $i < 20; $i++) {
            $data['year'][] = $year - $i;
        }
        if ($this->input->post('vehicle_information')['year'] != NULL) {
            $data['brands'] = $this->vehicle_model->get_brands_by_field('year', $this->input->post('vehicle_information')['year']);
        }
        if ($this->input->post('vehicle_information')['year'] != NULL && isset($this->input->post('vehicle_information')['brand'])) {
            $data['models'] = $this->vehicle_model->models($this->input->post('vehicle_information')['year'], $this->input->post('vehicle_information')['brand']);
            $data['models'][] = array('pk' => 'Other', 'model_name' => 'Other', 'pc_cv' => PC);
        }
        if ($this->input->post('vehicle_information')['year'] != NULL && isset($this->input->post('vehicle_information')['brand']) && $this->input->post('vehicle_information')['model_name'] != NULL && $this->input->post('vehicle_information')['model_name'] != 'Other') {
            $data['model'] = $this->vehicle_model->get_model($this->input->post('vehicle_information')['year'], $this->input->post('vehicle_information')['brand'], $this->input->post('vehicle_information')['model_name']);
            $data['max'] = $data['model']['sum_insured'] * 1.1;
            $data['min'] = $data['model']['sum_insured'] * 0.9;
        }
        return $data;
    }

    /**
     * Processes save quote
     */
    public function save_quote($param) {
        if (isset($param['token']) && !empty($param['token'])) {
            $does_saved_quote_exist = $this->_does_saved_quote_exist_by_token($param['token']);
            if ($does_saved_quote_exist) {
                $this->_update_saved_quote($param);
            } else {
                $param['token'] = random_string('sha1');
                $this->_insert_save_quote($param);
            }
        } else {
            $param['token'] = generateRandomString(50);
            $this->_insert_save_quote($param);
        }
        return $param['token'];
    }

    /**
     * Fetches motor_insurance saved quotes values
     * @param $token
     * @return array
     */
    public function get_motor_saved_quote_values_by_token($token) {
        $saved_quote = $this->db->where("token", $token)
                ->get($this->quotes_table);
        if ($saved_quote->num_rows()) {
            return $saved_quote->row_array()['values'];
        }
        return [];
    }

    /**
     * Fetches motor_insurance saved quotes
     * @param $token
     * @return array
     */
    public function get_motor_saved_quote_by_token($token) {
        $saved_quote = $this->db->where("token", $token)
            ->get($this->quotes_table);
        if ($saved_quote->num_rows()) {
            return $saved_quote->row_array();
        }
        return [];
    }

    /**
     * Fetches motor_insurance saved quotes
     * @param $token
     * @return array
     */
    public function get_motor_saved_quote_by_id($id) {
        $saved_quote = $this->db->where("pk", $id)
            ->get($this->quotes_table);
        if ($saved_quote->num_rows()) {
            return $saved_quote->row_array();
        }
        return [];
    }

    /**
     * Default values for quotation
     * @return array
     */
    public function default_quote_values() {
        return array(
            "coverage_details" => array(
                "comprehensive_start_date"          => "",
                "comprehensive_end_date"            => "",
                "ctpl_start_date"                   => "",
                "ctpl_end_date"                     => ""
            ),
            "contact_information" => array(
                "email_address"                     => "",
                "telephone"                         => "",
                "mobile"                            => ""
            ),
            "agent" => array(
                "type"                              => "",
                "code"                              => "",
                "fname"                             => "",
                "lname"                             => "",
                "with_agent" => ""
            ),
            "personal_information" => array(
                "salutation"                        => "",
                "lname"                             => "",
                "fname"                             => "",
                "mname"                             => "",
                "suffix"                            => "",
                "gender"                            => "",
                "birthdate"                         => "",
                "nationality"                       => "",
                "occupation"                        => "",
                "id_type"                           => "",
                "id_number"                         => "",
                "with_tin"                          => 0
            ),
            "mailing_address" => array(
                "province"                          => "",
                "city"                              => "",
                "barangay"                          => "",
            ),
            "vehicle_information" => array(
                "insurance_type"                    => "",
                "is_new"                            => 0,
                "year"                              => "",
                "brand"                             => "",
                "model_name"                        => "",
                "other_model"                       => "",
                "market_value"                      => "",
                "original_market_value"             => "",
                "overnight_parking"                 => "",
                "other_accessories_selected"        => array(),
                "non_standard"                      => array(),
                "plate_number"                      => "",
                "engine_number"                     => "",
                "chassis_number"                    => "",
                "mv_file_number"                    => "",
                "color"                             => "",
                "mortgagees_pk"                     => "",
                "insurance_providers_pk"            => "",
                "or_cr"                             => "",
                "address" => array(
                    "province"                      => "",
                    "city"                          => "",
                    "barangay"                      => "",
                    "house_number"                  => ""
                )
            ),
            "type"                                  => 1,
            "delivery"                              => 0,
            "employer"                              => "",
            "business_nature"                       => "",
            "same_location"                         => "on"
        );
    }

    /**
     * Processes the get quote
     */
    public function do_get_quote() {
        $post = $this->input->post();

        if (isset($post['vehicle_information']['model_name']) && $post['vehicle_information']['model_name'] == "Other") {
            $post['vehicle_information']['model_name'] = $post['vehicle_information']['other_model'];
        }

        $data = $post;
        $data['type'] = USER_TYPE_INDIVIDUAL;

        $condition = array(
            'year' => $post['vehicle_information']['year'],
            'brand' => $post['vehicle_information']['brand'],
            'model_name' => $post['vehicle_information']['model_name']
        );

        $model_row = $this->vehicle_model->get_filtered_model($condition);

        $active_user = $this->client_model->check_active_user($data['contact_information']['email_address']);
        $is_place_blacklisted = $this->motor_model->is_place_blacklisted($data['vehicle_information']['address']['barangay']);

        $data['motors_pk'] = ($model_row['pk'] != false) ? $model_row['pk'] : NULL;
        $data['status'] = STATUS_PENDING;
        if ($data['vehicle_information']['market_value'] > 5000000 || !in_array($model_row['pc_cv'], [PC, CV]) || $data['motors_pk'] == NULL || $is_place_blacklisted != 0) {
            $data['status'] = STATUS_FOR_APPROVAL;
        }

        unset($data['submit']);

        $data = array_replace_recursive($this->default_quote_values(), $data);

        $motor_saved_quotes_token = $this->save_quote($data);

        $this->session->set_userdata('motor_saved_quotes_token', $motor_saved_quotes_token);

        $this->update_allowed_motor_quote_tab(1);

        $this->session->set_userdata('motor_get_quote', $this->input->post());

        if ($data['vehicle_information']['market_value'] > 5000000 || !in_array($model_row['pc_cv'], [PC, CV]) || $data['motors_pk'] == NULL || $is_place_blacklisted != 0) {
            $this->_send_get_back_mail($data);
            redirect('motor-insurance/get_back_prompt');
        } else if ($active_user) {
            redirect('client/login?return_uri=motor-insurance/quote_summary');
        } else {
            redirect('motor-insurance/quote_summary/' . $motor_saved_quotes_token);
        }
    }

    /**
     * Sends email to user after save quote processes
     * @param array $data
     */
    public function send_saved_quote_mail($data = array()) {
        $msg = '<p>'
            . 'Dear ' . ((isset($data['personal_information']['fname']) && !empty($data['personal_information']['fname'])) ? $data['personal_information']['fname'] : "User ({$data['contact_information']['email_address']})") . ',<br>'
            . '<br>'
            . 'Thank you for securing a quotation from UCPB GEN.<br>'
            . 'You can retrieve your quotation anytime at<br>'
            . '<a href="' . site_url('motor-insurance/get_quote/' . $data['token']) . '">' . site_url('motor-insurance/get_quote/' . $data['token']) . '</a>'
            . '</p>';

        $this->email->clear();
        $this->email->from('motor_insurance@ucpbgen.com', 'MOTOR UCPBGEN');
        $this->email->to($data['contact_information']['email_address']);
        $this->email->subject('UCPBGEN Motor Quote Session');
        $this->email->message($msg);
        $this->email->send();
    }

    /**
     * Get quote premiums
     * @param $motorInfo
     * @param $data
     * @return mixed
     */
    public function quote_premiums($motorInfo, $data) {
        $accessoriesTotalAmount = 0;
        if ($data['vehicle_information']['insurance_type'] != CTPL && isset($data['vehicle_information']['non_standard']) && count($data['vehicle_information']['non_standard']) > 0) {
            foreach($data['vehicle_information']['non_standard'] as $key => $accessory) {
                $accessoriesTotalAmount += $accessory['non_standard_values'];
            }
        }

        $data['locationOfVehicle'] = $this->place_model->location($data['vehicle_information']['address']['province'], $data['vehicle_information']['address']['city'], $data['vehicle_information']['address']['barangay']);
        $data['name'] = $data['personal_information']['fname'] . " " . $data['personal_information']['lname'];

        $data['ctpl'] = 0;
        $data['ownDamage'] = 0;
        $data['bodilyInjury'] = 0;
        $data['propertyDamage'] = 0;
        $data['autoPersonalAccident'] = 0;
        $data['aon'] = 0;
        $data['deductible'] = 0;

        $data['ctplNetPremium'] = 0;
        $data['ctplInterconnectivity'] = 0;
        $data['ctplVat'] = 0;
        $data['ctplDocStamps'] = 0;
        $data['ctplLocalGovernmentTax'] = 0;
        $data['ctplGrossPremium'] = 0;

        $config = $this->motor_configurations();
        $data = array_merge($data, $config);

        if (in_array($data['vehicle_information']['insurance_type'], [CTPL, COMPREHENSIVE_CTPL_AON, COMPREHENSIVE_CTPL])) {
            $data['ctpl'] = 100000;
        }
        $data['vehicle_information']['market_value'] += $accessoriesTotalAmount;
        if ($data['vehicle_information']['insurance_type'] != CTPL) {
            if ($motorInfo['pc_cv'] == PC) {
                $data['ownDamageCoverage'] = $data['vehicle_information']['market_value'];
                $data['ownDamage'] = $data['vehicle_information']['market_value'] * ($config['pc_own_damage_percentage'] / 100);
                $data['bodilyInjury'] = $config['pc_bodily_injury'];
                $data['bodilyInjuryCoverage'] = $config['bodily_injury_max'];
                $data['propertyDamage'] = $config['pc_property_damage'];
                $data['propertyDamageCoverage'] = $config['property_damage_max'];
                $data['autoPersonalAccident']  = $config['pc_auto_personal_accident_multiplier'] * $motorInfo['seating_capacity'];
                $data['autoPersonalAccidentCoverage'] = $config['auto_personal_accident'] * $motorInfo['seating_capacity'];
                $deductible = ($data['vehicle_information']['market_value'] * ($config['deductible_percentage'] / 100));
                $data['deductible'] = $deductible >= $config['pc_deductible_min'] ? $deductible : $config['pc_deductible_min'];
            } else {
                if ($motorInfo['pc_cv'] == CV) {
                    $data['ownDamageCoverage'] = $data['vehicle_information']['market_value'];
                    $data['ownDamage'] = $data['vehicle_information']['market_value'] * ($config['cv_own_damage_percentage'] / 100);
                    $data['bodilyInjury'] = $config['cv_bodily_injury'];
                    $data['bodilyInjuryCoverage'] = $config['bodily_injury_max'];
                    $data['propertyDamage'] = $config['cv_property_damage'];
                    $data['propertyDamageCoverage'] = $config['property_damage_max'];
                    $data['autoPersonalAccident'] = $config['cv_auto_personal_accident_multiplier'] * $motorInfo['seating_capacity'];
                    $data['autoPersonalAccidentCoverage'] = $config['auto_personal_accident'] * $motorInfo['seating_capacity'];
                    $deductible = ($data['vehicle_information']['market_value'] * ($config['deductible_percentage'] / 100));
                    $data['deductible'] = $deductible >= $config['cv_deductible_min'] ? $deductible : $config['cv_deductible_min'];
                }
            }
        }
        if (in_array($data['vehicle_information']['insurance_type'], [COMPREHENSIVE_CTPL_AON, COMPREHENSIVE_AON])) {
            $data['aon'] = $data['vehicle_information']['market_value'] * ($config['aon_percentage'] / 100);
        }

        if ($data['bodilyInjury'] > $config['bodily_injury_max']) {
            $data['bodilyInjury'] = $config['bodily_injury_max'];
        }

        if ($data['propertyDamage'] > $config['property_damage_max']) {
            $data['propertyDamage'] = $config['property_damage_max'];
        }

        $data['netPremium'] = $data['ownDamage'] + $data['bodilyInjury'] + $data['propertyDamage'] +
            $data['autoPersonalAccident'] + $data['aon'];

        $data['vat'] = $data['netPremium'] * ($config['tax'] / 100);

        /**
         * Doc Stamp
         */
        $docStamp  = floor($data['netPremium'] / 4) * .50;
        $remainder = fmod($data['netPremium'], 4);
        $data['docStamps'] = !$remainder ? $docStamp : $docStamp+.50;

        /**
         * Agent Percentage
         */
        $this->load->model('motor_quote_agent/motor_quote_agent_model', 'motor_quote_agent');
        $this->load->model('agent/agent_model', 'agent');
        $this->load->model('rate/rate_model', 'rate');

        // Fetch Quote Agent First
        $agent           = $data['agent']['type'] == 'Agent Name' ? $this->agent->fetch_by_name($data['agent']['fname'], $data['agent']['lname'])
            : $this->agent->fetch_by_field('code', $data['agent']['code']);
        // Then Fetch the Rate
        $rate            = $this->rate->get_rate_by_rate_location_pk($agent['rate_location_pk']);
        // Get the percentage!
        $percentage      = $rate ? ($rate->rate * .01) : 0.002;

        $data['localGovernmentTax'] = $data['netPremium'] * $percentage;

        $data['grossPremium'] = $data['netPremium'] + $data['vat'] + $data['docStamps'] + $data['localGovernmentTax'];

        if (in_array($data['vehicle_information']['insurance_type'], [CTPL, COMPREHENSIVE_CTPL, COMPREHENSIVE_CTPL_AON])) {


            //if new for 1 year else for 3 years
            $data['ctplNetPremium'] = $data['vehicle_information']['is_new'] ? $config['ctpl_first_year_net_premium'] : $config['ctpl_third_year_net_premium'];
            $data['ctplVat'] = $data['ctplNetPremium'] * ($config['tax'] / 100);

            $docStamp  = floor($data['ctplNetPremium'] / 4) * .50;
            $remainder = fmod($data['ctplNetPremium'], 4);
            $data['ctplDocStamps'] = !$remainder ? $docStamp : $docStamp+.50;

            $data['ctplLocalGovernmentTax'] = $data['ctplNetPremium'] * $percentage;
            $data['ctplInterconnectivity'] = $config['interconnectivity'];

            $data['ctplGrossPremium'] = $data['ctplNetPremium'] + $data['ctplVat'] + $data['ctplDocStamps'] + $data['ctplLocalGovernmentTax'] + $data['ctplInterconnectivity'];
        }

        $data['premium'] = $data['grossPremium'] + $data['ctplGrossPremium'];

        return $data;
    }

    /**
     * Fetches vehicle information for quote
     * @param $token
     * @param bool $does_file_failed
     */
    public function vehicle_information($token, $does_file_failed = FALSE) {

        $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($token), TRUE);
        $motor_info = $this->vehicle_model->get_motor_for_computation($motor_saved_quotes['motors_pk']);
        $quote_premiums = $this->motor_model->quote_premiums($motor_info, $motor_saved_quotes);
        $data = array_merge($motor_saved_quotes, $motor_info, $quote_premiums);
        $data['file_failed'] = $does_file_failed;
        $accessories_sum = 0;
        if ($data['vehicle_information']['insurance_type'] != CTPL) {
        }
        if (!$this->input->post()) {
            $_POST = array_merge($data, $_POST);
        }

        if (isset($this->input->post('vehicle_information')['non_standard'])) {
            foreach ($this->input->post('vehicle_information')['non_standard'] as $key => $val) {
                $accessories_sum += $val['non_standard_values'];
            }
        }
        $data['accessories'] = $this->vehicle_model->accessories();
        $data['colors'] = $this->vehicle_model->colors();
        $data['mortgagees'] = $this->client_model->mortgagees();
        $data['insurance_providers'] = $this->client_model->insurance_providers();
        $data['gross_premium'] = $data['premium'] + $accessories_sum;

        return $data;
    }

    /**
     * Process the vehicle information form
     * @param $data
     * @param $motor_saved_quotes_token
     */
    public function do_vehicle_information($data, $motor_saved_quotes_token) {
        $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($motor_saved_quotes_token), TRUE);

        $motor_saved_quotes['vehicle_information']['other_accessories_selected'] = array();
        $motor_saved_quotes['vehicle_information']['non_standard'] = array();
		if (isset($data['non_standard_accessories']) && count($data['non_standard_accessories']) > 0) {
			foreach($data['non_standard_accessories'] as $key => $val) {
				$data['vehicle_information']['non_standard'][] = array(
					"non_standard_accessories" => $val,
					"non_standard_values" => $data['non_standard_values'][$key]
				);
			}
		}
//        dd($data);

        unset($data['accessory'], $data['non_standard_accessories'], $data['non_standard_values']);
        $data = array_replace_recursive($motor_saved_quotes, $data);
        $this->save_quote($data);

        $this->update_allowed_motor_quote_tab(3);
        redirect('motor-insurance/coverage_details/' . $motor_saved_quotes_token);
    }

    /**
     * Coverage Details POST Request
     */
    public function do_coverage_details($motor_saved_quotes) {
        $_POST['vehicle_information']['insurance_type'] = $motor_saved_quotes['vehicle_information']['insurance_type'];
        $_POST['vehicle_information']['is_new'] = $motor_saved_quotes['vehicle_information']['is_new'];
        $validated = $this->coverage_details_rules_model->validate($this->form_validation, 'save', (object) $this->input->post());
        if($validated) {
            $this->coverage_details_model->save($this->input->post(), $motor_saved_quotes);
            $this->update_allowed_motor_quote_tab(4);
            redirect('motor-insurance/personal_information/' . $motor_saved_quotes['token']);
        }
    }

    /**
     * Personal Information Request
     */
    public function do_personal_info($motor_saved_quotes) {
        if ($this->input->post('same_location') == 'on') {
            $_POST['mailing_address']['province'] = $this->input->post('vehicle_information')['address']['province'];
            $_POST['mailing_address']['city'] = $this->input->post('vehicle_information')['address']['city'];
            $_POST['mailing_address']['barangay'] = $this->input->post('vehicle_information')['address']['barangay'];
        } else {
            $_POST['same_location'] = "";
        }
        if ($this->input->post('delivery') == NULL) {
            $_POST['delivery'] = "";
        }
        $validated = $this->personal_information_rules_model->validate($this->form_validation, 'save', (object) $this->input->post());
        if($validated) {
//            $this->db->trans_start();
            
            /**
             * Save Motor Agent
             */
            $this->motor_quote_agent_model->destroy_by_field('motor_quotes_pk', $this->session->userdata('motor_quotes_pk'));
            if($this->input->post('with_agent') == 'on') {
                $insert = [];

                $insert['motor_quotes_pk'] = $this->session->userdata('motor_quotes_pk');

                if($this->input->post('agent_type') == 'Agent Name') {
                    $insert['is_name'] = true;
                    $insert['fname'] = $this->input->post('agent')['fname'];
                    $insert['lname'] = $this->input->post('agent')['lname'];
                }
                else {
                    $insert['is_name'] = false;
                    $insert['code'] = $this->input->post('agent')['code'];

                    $query  = $this->db->where(['code' => $this->input->post('agent')['code']])->get('agents');
                    $result = $query->row_object();
                    $insert['fname'] = $result->fname;
                    $insert['lname'] = $result->lname;
                }

                $motor_saved_quotes['agent'] = array(
                    "type" => $this->input->post('agent')['type'],
                    "code" => $insert['code'],
                    "fname" => $insert['fname'],
                    "lname" => $insert['lname'],
                    "with_agent" => $this->input->post('agent')['with_agent']
                );
            }

//            $motor_saved_quotes = array_merge($motor_saved_quotes, $this->input->post());
//            $this->coverage_details_model->save($this->input->post(), $motor_saved_quotes);

            $data = array_replace_recursive($motor_saved_quotes, $this->input->post());
//            $data = array_replace_recursive($motor_saved_quotes, $data);
            $this->save_quote($data);

            $this->motor_model->update_allowed_motor_quote_tab(5);
            redirect('motor-insurance/summary_confirmation/' . $motor_saved_quotes['token']);
        }

        $data = array_replace_recursive($motor_saved_quotes, $this->input->post());
        return $data;
    }

    /**
     * Checks if saved quote token exists
     * @param string $token
     * @return bool
     */
    private function _does_saved_quote_exist_by_token($token = '') {
        $count = $this->db->where("token", $token)
            ->get($this->quotes_table)
            ->num_rows();
        return $count > 0 ? TRUE : FALSE;
    }

    /**
     * Updates the saved quote in the database
     * @param array $param
     */
    private function _update_saved_quote($param = array()) {
        $data = array(
            'token' => $param['token'],
            'values' => json_encode($param)
        );
        $this->db->where('token', $data['token'])
            ->update($this->quotes_table, $data);
    }

    /**
     * Inserts save quote in the database
     * @param array $param
     */
    private function _insert_save_quote($param = array()) {
        $data = array(
            'token' => $param['token'],
            'values' => json_encode($param),
            'line'  => 'motor'
        );
        $this->db->insert($this->quotes_table, $data);
    }

    /**
     * Sends email that the quote is ground for approval
     * @param array $data
     */
    private function _send_get_back_mail($data = array()) {
        $msg = '<p>'
            . 'Dear  ' . $data['personal_information']['fname'] . ',<br>'
            . '<br>'
            . 'Thank you for requesting a quotation from UCPB GEN. For the meantime, we will review your application. Your request will be available within 48 (sample only) hours from the receipt of this email.<br>'
            . '<br>'
            . 'Have a good day!.<br>'
            . '</p>';

        $this->email->clear();
        $this->email->from('motor_insurance@ucpbgen.com', 'MOTOR UCPBGEN');
        $this->email->to($data['contact_information']['email_address']);
        $this->email->subject('Request for UCPB GEN Insurance Quotation');
        $this->email->message($msg);
        $this->email->send();
    }

    /**
     * Inserts motor_insurance quote
     * @param array $params
     * @return bool
     */
    private function _insert_motor_quote($params = array()) {
        $return = FALSE;
        $params['date_created'] = date('Y-m-d H:i:s');

        unset($params['other_model']);
        unset($params['submit']);
        if ($this->db->insert('motor_quotes', $params)) {
            $motor_quotes_pk = $this->db->insert_id();
            $this->db->where(array('pk' => $motor_quotes_pk))
                ->update('motor_quotes', array('quote_id' => 'MTR' . date('Y') . recordLengthAlign($motor_quotes_pk, 6, "R")));

            if ($this->db->insert('users_quotes', array('quotes_pk' => $motor_quotes_pk))) {
                $users_quotes_pk = $this->db->insert_id();
                $this->db->where(array('pk' => $users_quotes_pk))
                    ->update('motor_quotes', array('users_quotes_pk' => $users_quotes_pk));
            }
            $return = $motor_quotes_pk;
        }

        return $return;
    }















    public function get_motor_quote_by_token($token) {
        $return = false;
        $this->db->where("token", $token);
        $this->db->from('motor_quotes');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        }
        return $return;
    }

    public function get_motor_quote_by_pk($pk) {
        $return = false;

        $this->db->where("motor_quotes.pk", $pk);
        $this->db->from('motor_quotes');

        $query = $this->db->get();
        if($query->num_rows()) {
            return $query->row_array();
        }
        return $return;
    }

    public function get_motor_quote_by_field($field, $value) {
        $return = false;

        $this->db->where($field, $value);
        $this->db->from('motor_quotes');

        $query = $this->db->get();
        if($query->num_rows()) {
            return $query->row_array();
        }
        return $return;
    }

    public function get_motor_quote_by_quote_id($quote_id) {
        $return = false;

        $this->db->where("motor_quotes.quote_id", $quote_id);
        $this->db->from('motor_quotes');

        $query = $this->db->get();
        if($query->num_rows()) {
            return $query->row_array();
        }
        return $return;
    }

    public function get_motor_quote_by_txnid($txnid) {
        $return = false;

        $query = $this->db->where("dt.transaction_id", $txnid)

                        ->join('dragonpay_transactions dt', 'dt.quotes_pk = mq.pk')
                        ->select('mq.*')
                        ->get('motor_quotes as mq');
        if($query->num_rows()) {
            return $query->row_array();
        }
        return $return;
    }

    public function is_place_blacklisted($barangay) {
        return $this->db->where(array(
                'pk' => $barangay,
                'is_blacklisted' => 1
            ))
            ->get('places')
            ->num_rows();
    }

    public function motor_configurations() {
        return $this->db->get('motor_configurations')->row_array();
    }

//    public function get_pc_percentage(){
//        $return = false;
//        $this->db->select('pc');
//        $this->db->from('pc_percentage');
//        $query = $this->db->get();
//        if ($query->num_rows()) {
//            $return = $query->row_array();
//        }
//        return $return;
//    }
//
//    public function get_tax() {
//        $return = false;
//        $this->db->select('tax');
//        $this->db->from('tax_percentage');
//        $query = $this->db->get();
//        if ($query->num_rows()) {
//            $return = $query->row_array();
//        }
//        return $return;
//    }

    public function does_have_request_session() {
        if (!$this->session->has_userdata('motor_quotes_pk')) {
            redirect('motor-insurance/get_quote');
        }
    }

    public function get_vehicle_information($motor_quotes_pk) {
        return $this->db->select(array(
                            'mqi.*'
                        ))
                        ->join('motor_quotes_informations as mqi', 'mqi.motor_quotes_pk = mq.pk', 'left')
                        ->where(array(
                            'mq.pk' => $motor_quotes_pk
                        ))
                        ->get('motor_quotes as mq')
                        ->row_array();
    }

    public function get_accessories_selected($motorQuotePk = null) {
        return $this->db->where("motor_quotes_pk", $motorQuotePk)
                ->select('motor_accessories as non_standard_accessories')
                ->select('value as non_standard_values')
                ->from('motor_quotes_non_standards')
                ->get()
                ->result_array();
    }

    public function get_other_accessories_selected($motorQuotePk = null) {
        return $this->db->select('motor_accessories as accessory')
                    ->where("motor_quotes_pk",$motorQuotePk)
                    ->from('motor_quotes_other_standards')
                    ->get()
                    ->result_array();
    }

//    public function insert_other_information($motor_quote_pk = null, $params = array()) {
//        $return = false;
//        $data = $params;
//        $data['motor_quotes_pk'] = $motor_quote_pk;
//        $information = $this->get_vehicle_information($motor_quote_pk);
//        if ($information['motor_quotes_pk'] == NULL) {
//            unset($data['pk']);
//            if ($this->db->insert('motor_quotes_informations', $data)){
//                $return = true;
//            }
//        } else {
//            unset($data['pk']);
//            $this->db->where('motor_quotes_pk', $motor_quote_pk);
//            if ($this->db->update('motor_quotes_informations', $data)){
//                $return = true;
//            }
//        }
//
//        return $return;
//    }

    public function insert_other_standards($motor_quote_pk = null, $params = array()) {
        $return = false;
        $data = $params;
        $this->db->delete('motor_quotes_other_standards', array('motor_quotes_pk' => $motor_quote_pk));
        if (count($data) > 0) {
            foreach ($data as $key => $val) {
                if ($this->db->insert('motor_quotes_other_standards', $val)) {
                    $return = true;
                }
            }
        }
        return $return;
    }

    public function insert_non_standards($motor_quote_pk = null, $params = array()) {
        $return = false;
        $data = $params;
        $this->db->delete('motor_quotes_non_standards', array('motor_quotes_pk' => $motor_quote_pk));
        if (count($data) > 0) {
            foreach ($data as $key => $accessory) {
                unset($data[$key]['pk']);
                if ($this->db->insert('motor_quotes_non_standards', $data[$key])) {
                    $return = true;
                }
            }
        }
        return $return;
    }

    public function get_coverage($motor_quote_pk = null, $isCtpl = true) {
        $return = [];

        $query = $this->db->select(['mqc.*'])
                          ->join('motor_quotes_coverages as mqc', 'mqc.motor_quotes_pk = mq.pk
                                                                   AND is_ctpl= ' . $this->db->escape($isCtpl) . '
                                                                   AND motor_quotes_pk= ' . $this->db->escape($motor_quote_pk), 'left')
                          ->limit(1)
                          ->order_by('mqc.pk', 'DESC')
                          ->get('motor_quotes as mq');

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $data   = [];

            if ($isCtpl) {
                $data['ctpl_start_date'] = date("Y-m-d", strtotime($result['start_date']));
                $data['ctpl_end_date']   = date("Y-m-d", strtotime($result['end_date']));
            }
            else {
                $data['non_ctpl_start_date'] = date("Y-m-d", strtotime($result['start_date']));
                $data['non_ctpl_end_date']   = date("Y-m-d", strtotime($result['end_date']));
            }

            return $data;
        }

        return $return;
    }

    public function insert_premium($params = array()) {
        $return = false;

        $motor_quote_pk = $this->session->userdata('motor_quotes_pk');
        $data['motor_quotes_pk'] = $motor_quote_pk;

        $data['net_premium'] = isset($params['netPremium']) ? $params['netPremium'] : "";
        $data['gross_premium'] = isset($params['premium']) ? $params['premium'] : "";
        $data['doc_stamps'] = isset($params['docStamps']) ? $params['docStamps'] : "";
        $data['vat'] = isset($params['vat']) ? $params['vat'] : "";
        $data['local_government_tax'] = isset($params['localGovernmentTax']) ? $params['localGovernmentTax'] : "";
        $data['own_damage'] = isset($params['ownDamage']) ? $params['ownDamage'] : "";
        $data['bodily_injury'] = isset($params['bodilyInjury']) ? $params['bodilyInjury'] : "";
        $data['property_damage'] = isset($params['propertyDamage']) ? $params['propertyDamage'] : "";
        $data['auto_personal_accident'] = isset($params['autoPersonalAccident']) ? $params['autoPersonalAccident'] : "";
        $data['deductible'] = isset($params['deductible']) ? $params['deductible'] : "";

        $data['own_damage_coverage'] = isset($params['ownDamageCoverage']) ? $params['ownDamageCoverage'] : "";
        $data['auto_personal_accident_coverage'] = isset($params['autoPersonalAccidentCoverage']) ? $params['autoPersonalAccidentCoverage'] : "";

        $data['ctpl_net_premium'] = isset($params['ctplNetPremium']) ? $params['ctplNetPremium'] : "";
        $data['ctpl_gross_premium'] = isset($params['ctplGrossPremium']) ? $params['ctplGrossPremium'] : "";
        $data['ctpl_doc_stamps'] = isset($params['ctplDocStamps']) ? $params['ctplDocStamps'] : "";
        $data['ctpl_vat'] = isset($params['ctplVat']) ? $params['ctplVat'] : "";
        $data['ctpl_local_government_tax'] = isset($params['ctplLocalGovernmentTax']) ? $params['ctplLocalGovernmentTax'] : "";
        $data['ctpl_interconnectivity'] = isset($params['ctplInterconnectivity']) ? $params['ctplInterconnectivity'] : "";

        $data['aon'] = isset($params['aon']) ? $params['aon'] : "";

        if ($this->_does_premium_exist($motor_quote_pk)) {
            $this->db->where("motor_quotes_pk",$motor_quote_pk);
            if ($this->db->update('motor_quotes_computations', $data)){
                $return = true;
            }
        } else if ($this->db->insert('motor_quotes_computations', $data)){
            $return = true;
        }

        return $return;
    }

    private function _does_premium_exist($motor_quote_pk) {
        $return = false;
        $this->db->where(array(
            'motor_quotes_pk' => $motor_quote_pk
        ));
        $this->db->from('motor_quotes_computations');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return true;
        }
        return $return;
    }
}

