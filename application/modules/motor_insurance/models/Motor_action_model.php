<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_action_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function save_request($data = array(), $is_saved = FALSE) {
        if ($data) {
            if ($motor_quote_pk = $this->motor_model->insert_motor_quote($data, $is_saved)) {
                //echo json_encode(array("status" => 200, "msg" => array('save' => TRUE)));

                    if ($is_saved) {
                    $msg = '<p>'
                        . 'Dear ' . $data['fname'] . ',<br>'
                        . '<br>'
                        . 'Thank you for securing a quotation from UCPB GEN.<br>'
                        . 'You can retrieve your quotation anytime at<br>'
                        . '<a href="' . site_url('motor-insurance/get_quote/' . $data['token']) . '">' . site_url('motor-insurance/get_quote/' . $data['token']) . '</a>'
                        . '</p>';

                    $this->email->clear();
                    $this->email->set_header('Content-type', 'text/html; charset=iso-8859-1');
                    $this->email->from('motor_insurance@ucpbgen.com', 'MOTOR UCPBGEN');
                    $this->email->to($data['email_address']);
                    $this->email->subject('UCPBGEN Motor Quote Session');
                    $this->email->message($msg);
                    $this->email->send();
                }

                return $motor_quote_pk;

            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function get_back_email($data = array()) {
        if ($data) {
            $msg = '<p>'
                . 'Dear  ' . $data['fname'] . ',<br>'
                . '<br>'
                . 'Thank you for requesting a quotation from UCPB GEN. For the meantime, we will review your application. Your request will be available within 48 (sample only) hours from the receipt of this email.<br>'
                . '<br>'
                . 'Have a good day!.<br>'
                . '</p>';

            $this->email->clear();
            $this->email->set_header('Content-type', 'text/html; charset=iso-8859-1');
            $this->email->from('motor_insurance@ucpbgen.com', 'MOTOR UCPBGEN');
            $this->email->to($data['email_address']);
            $this->email->subject('Request for UCPB GEN Insurance Quotation');
            $this->email->message($msg);
            $this->email->send();
        }
    }
}
