<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_quote_information_model extends CI_Model {
    protected $pk = '';
    protected $motor_quotes_pk = '';
    protected $mortgagees_pk = '';
    protected $insurance_providers_pk = '';
    protected $overnight_parking = '';
    protected $plate_number = '';
    protected $engine_number = '';
    protected $chassis_number = '';
    protected $mv_file_number = '';
    protected $color = '';
    protected $or_cr = '';

    public function __construct() {
        parent::__construct();
    }

    public function set($field, $value) {
        if (isset($this->$field)) {
            $this->$field = $value;
        }
    }

    public function get($field) {
        if (isset($this->$field)) {
            return $this->$field;
        } else {
            return NULL;
        }
    }

    public function get_data() {
        $reflect = new ReflectionClass($this);
        return get_object_vars($this);
    }

    public function set_data($data) {
        foreach($data as $field => $value) {
            $this->set($field, $value);
        }
    }
}
