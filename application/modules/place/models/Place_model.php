<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Place_model extends MY_Model {
    public function __construct() {
        parent::__construct();
    }

    public function provinces() {
        $return = false;
        $this->db->order_by("place");
        $this->db->where("pk NOT REGEXP '[0-9]{1,2}0000000'");
        $this->db->where("pk REGEXP '[0-9]{3,4}00000'");
        $this->db->from('places');
        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->result_array();
        }
        return $return;
    }

    public function cities($prov = '') {
        $return = false;
        $province = substr($prov, 0, -5);
        $this->db->order_by("place");
        $this->db->where("pk NOT LIKE '%".$prov."%'");
        $this->db->where("pk REGEXP '" . $province . "[0-9]{2}000'");
        $this->db->from('places');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        }
        return $return;
    }

    public function barangays($cty = '') {
        $return = false;
        $city = substr($cty, 0, -3);
        $this->db->order_by("place");
        $this->db->where("pk NOT LIKE '%".$cty."%'");
        $this->db->where("pk REGEXP '" . $city . "[0-9]{3}'");
        $this->db->from('places');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        }
        return $return;
    }

    public function location($province = "", $city = "", $barangay = "") {
        $this->db->where("pk", $province);
        $this->db->from('places');
        $this->db->select('place');
        $query = $this->db->get();
        if ($query->num_rows()) {
            $province = $query->row_array();
            $province = $province['place'];
        }

        $this->db->where("pk", $city);
        $this->db->from('places');
        $this->db->select('place');
        $query = $this->db->get();
        if ($query->num_rows()) {
            $city = $query->row_array();
            $city = $city['place'];
        }

        $this->db->where("pk", $barangay);
        $this->db->from('places');
        $this->db->select('place');
        $query = $this->db->get();
        if ($query->num_rows()) {
            $barangay = $query->row_array();
            $barangay = $barangay['place'];
        }

        return ucfirst(strtolower($barangay)) . ", " . ucfirst(strtolower($city)) . ", " . ucfirst(strtolower($province));
    }

    public function get_label_in_ids($ids) {
        $query = $this->db->select('place')
                          ->where_in('pk', $ids)
                          ->get('places');


        if ($query->num_rows()) {
            return $query->result();
        }
    }

    public function get_by_field($field, $value) {
        $return = false;

        $this->db->where($field, $value);
        $this->db->from('places');

        $query = $this->db->get();
        if($query->num_rows()) {
            return $query->row_object();
        }
        return $return;
    }

    public function save($where=NULL,$data=array())
    {
        if ( is_array($data) AND count($data) > 0 ) {
            if ( ! is_null($where) ) {
                return parent::update('places', $data, $where);
            } else {
                return parent::insert('places', $data);
            }
        }

        return FALSE;
    }

    public function get($type=NULL,$place=NULL,$keyword=NULL,$limit=NULL,$offset=NULL,$order_by=NULL,$sort=NULL)
    {
        if ( ! is_null($type) ) {
            $where = array();

            if ( $type == 'province' ) {
                $where[] = "pk NOT REGEXP '[0-9]{1,2}0000000'";
                $where[] = "pk REGEXP '[0-9]{3,4}00000'";
            } elseif ( $type == 'city' AND ! is_null($place) ) {
                $province = substr($place, 0, -5);
                $where[] = "pk NOT LIKE '%{$place}%'";
                $where[] = "pk REGEXP '{$province}[0-9]{2}000'";
            } elseif ( $type == 'barangay' AND ! is_null($place) ) {
                $city = substr($place, 0, -3);
                $where[] = "pk NOT LIKE '%{$place}%'";
                $where[] = "pk REGEXP '{$city}[0-9]{3}'";
            }

            $table = "places";

            if ( count($where) > 0 ) {
                $options = [
                    "where"  => $where,
                    "order"  => ! is_null($order_by) ? $order_by : "place"
                ];

                return parent::get($table,$options);
            }      
        }

        return FALSE;
    }
}