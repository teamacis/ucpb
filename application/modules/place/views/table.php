<div class="col-md-12 dashbox  content_field">
	<form class="form_style">
		<?php echo form_hidden('type', $type); ?>
		<?php echo form_hidden('title', $title); ?>
		<div class="form_content">
			<div class="col-md-6">
				<div class="line_title"><p>Search</p></div>
				<div class="form_line form_input"><input type="text" class="form-control required" placeholder="Type Keywords" /></div>
			</div>
			<?php if ( isset($dropdown) ): ?>
				<div class="col-md-4">
					<div class="line_title"><p><?php echo $type == 'City' ? 'Provinces' : 'Cities'; ?></p></div>
					<div class="form_line form_input"><?php echo $dropdown; ?></div>
				</div>
			<?php endif; ?>
			<div class="col-md-2 no_linetitle"><button class="btn_create">Search</button></div>
		</div>
	</form>
</div>
<div class="col-md-12 dashtable content_field">
	<div class="top_head">
		<div class="checkbox">
			<label><input type="checkbox">Select All</label>
		</div>
	</div>
	<!-- TABLE HEADER -->
	<div class="table_head">
		<div class="col-md-10 col col_first"><strong><?php echo $type; ?> Name</strong></div>
		<div class="col-md-2 col"><strong>Blacklisted</strong></div>
	</div>
	<!-- /TABLE HEADER -->

	<!-- TABLE CONTENT -->
	<div class="table_content">
		<!-- ====== TABLE ITEM ======  -->
		<?php if (is_object($places) AND $places->count > 0): ?>
			<?php foreach ($places->data AS $place): ?>
				<div class="table_item">
					<div class="col-md-10 col col_first">
						<div class="checkbox">
							<label><input type="checkbox"></label>
						</div>
						<div class="client">
							<p class="client_name">
								<?php $title = ucwords(strtolower($place->place)); ?>
								<?php if ( $type == 'Province' ): ?>
									<?php echo anchor(base_url("admin/settings/places/cities/{$place->pk}"), $title); ?>
								<?php elseif ($type == 'City'): ?>
									<?php echo anchor(base_url("admin/settings/places/barangays/{$place->pk}"), $title); ?>
								<?php else: ?>		
									<?php echo $title; ?>
								<?php endif; ?>
							</p>
						</div>
					</div>
					<div class="col-md-2 col">
						<input type="checkbox" data-type="<?php echo $type; ?>" data-item="place-blacklist" data-size="mini" data-on-text="Yes" data-off-text="No" data-place-id="<?php echo $place->pk; ?>" <?php echo $place->is_blacklisted == 0 ? "" : "checked"; ?>>
					</div>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
<script type="text/javascript">
	function place_blacklist(iPlaceId,sType,sState)
	{
		if ( iPlaceId && sType ) {
			var oData = {
				api_key 	: apiKey,
				place_id	: iPlaceId,
				type 		: sType,
				state 		: sState
			};
			console.log(oData);
			$.ajax({
				type : 'POST',
				url : baseUrl + 'place/ajax_blacklist',
				data : oData
			});
		}
	}
	
	$(document).ready(function(){
		var config = {
		  	'.chosen-select'           : {width:"100%",disable_search_threshold: 10},
		}
		for (var selector in config) {
		  	$(selector).chosen(config[selector]);
		}
		$("[data-item=place-blacklist]").bootstrapSwitch();
		$("[data-item=place-blacklist]").on('switchChange.bootstrapSwitch', function(event, state) {
			var iPlaceId 	= $(this).attr("data-place-id"),
				sType		= $(this).attr("data-type"),
				sState 		= state;

			place_blacklist(iPlaceId,sType,sState);
		});
		$("select[name=parent_id]").on("change", function(){
			var sType 	= $("input[name=title]").val().toLowerCase();
			var iValue	= $(this).val();

			if ( sType && iValue ) {
				window.location.href = baseUrl + 'admin/settings/places/' + sType + '/' + iValue;
			}
			
		});
	});
</script>