<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Place extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('place_model');
    }
//
//    /**
//     * Displays the list of provinces in object format
//     */
//    public function provinces() {
//        $query = $this->place_model->provinces();
//        if (!$query) {
//            echo json_encode([]);
//        } else {
//            echo json_encode($query->result());
//        }
//    }
//
//    /**
//     * Displays the list of cities from a given province parameter in object format
//     */
    public function cities() {
        $province = $this->input->post('province');
        $cities = $this->place_model->cities($province);
        if (!$cities) {
            echo json_encode([]);
        } else {
            echo json_encode($cities);
        }
    }
//
//    /**
//     * Display the list of barangays from a given city parameter in object format
//     */
    public function barangays() {
        $city = $this->input->post('city');
        $barangays = $this->place_model->barangays($city);
        if (!$barangays) {
            echo json_encode([]);
        } else {
            echo json_encode($barangays);
        }
    }

    public function ajax_blacklist()
    {
        if ( $this->input->is_ajax_request() ) {
            $return = [];
            $api_key = $this->input->post('api_key');

            if ( $api_key == API_KEY ) {
                $place_id   = $this->input->post('place_id');
                $state      =  $this->input->post('state');
                $type       = $this->input->post('type');

                $type_of_places = ["Province","City","Barangay"];  

                if ( $place_id AND $state AND in_array($type,$type_of_places) ) {
                    $data = [
                        "is_blacklisted" => $state == "true" ? 1 : 0
                    ];

                    if ( $this->place_model->save(["pk" => $place_id], $data) ) {
                        $method = $state == "true" ? "blacklisted" : "remove to blacklist";
                        $return = [
                            "status"    => TRUE,
                            "message"   => "{$type} was succesfully {$method}"
                        ];
                    } else {
                        $return = [
                            "status"    => FALSE,
                            "message"   => "Unable to update, please try again"
                        ];
                    }
                } else {
                     $return = [
                        "status"    => FALSE,
                        "message"   => "Invalid data"
                    ];
                }

            } else {
                $return = [
                    "status"    => FALSE,
                    "message"   => "Invalid API Key"
                ];
            }

            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($return));
        }
    }

    public function get_provinces($api_key=NULL)
    {
        if ( $api_key == API_KEY ) {
            $provinces = $this->place_model->get("province");
            echo json_encode($provinces);
        }
        
    }

    public function get_cities($api_key=NULL,$province=NULL)
    {
        if ( $api_key == API_KEY ) {
            $cities = $this->place_model->get("city",$province);
            echo json_encode($cities);
        }
    }

    public function get_barangays($api_key=NULL,$city=NULL)
    {
        if ( $api_key == API_KEY ) {
            $barangays = $this->place_model->get("barangay",$city);
            echo json_encode($barangays);
        }
    }
}
