<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function fetch_by_field($field, $val) {
        $query = $this->db->join("agent_codes","agent_codes.agent_pk = agents.pk","left")
                          ->join("agencies","agencies.pk = agents.agency_pk","left")
                          ->where($field, $val)
                          ->get('agents');
      
        if($query->num_rows() > 0) {
            return $query->row_array();
        }
        else {
            return false;
        }
    }

    public function fetch_by_name($fname, $lname) {
        $query = $this->db->where(['fname' => strtoupper($fname), 'lname' => strtoupper($lname)])
                          ->get('agents');

        if($query->num_rows() > 0) {
            return $query->row_array();
        }
        else {
            return false;
        }
    }

    public function get_agencies()
    {
        return $this->db->get("agencies")->result_array();
    }
}
