<div id="content" class="admin_dashboard">
    <!-- page top -->
    <div class="login_page_top box ">
        <div class="container">
            <div class="title_top">
                <h4 class="page_title">
                    <img src="<?= imgUrl('site_logo3.png'); ?>" alt="UCPB Gen" class="logo pull-left" />
                    <p class="pull-left">Welcome to<br /> UCPB General Insurance Co., Inc.</p>
                </h4>
            </div>
        </div>
    </div>
    <!-- /page top-->
    <div class="container admin_login box">
        <form method="POST" action="<?= site_url('admin/adminrequest/login'); ?>">
            <div class="row no_gutter">
                <div class="col-md-6 col-md-offset-3 line_space">
                    <strong class="login_title">User Login</strong>
                    <br><br>
                    <?php if($this->session->flashdata('form_error')): ?>
                    <div class="alert alert-danger" role="alert"><?= $this->session->flashdata('form_error'); ?></div>
                    <?php endif; ?>
                </div>

                <div class="col-md-6 col-md-offset-3 line_space">

                    <div class="col-md-6 login_input">
                        <div class="input_box">
                            <input type="text" class="form-control" placeholder="Username" name="username" />
                            <?= form_error('username'); ?>
                        </div>
                    </div>
                    <div class="col-md-6 login_input">
                        <div class="input_box">
                            <input type="password" class="form-control" placeholder="Password" name="password" />
                            <?= form_error('password'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-md-offset-3">
                    <div class="col-md-6"><a href="#" class="forgot_pw">Did you forget your Password?</a></div>
                    <div class="col-md-6"><button type="submit" class="pull-right login_btn col-md-8 col-xs-12">Login</button></div>
                </div>
            </div>
        </form>
    </div>
</div>