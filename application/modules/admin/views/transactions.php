 <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation"<?php echo $active == "motor" ? ' class="active"' : ''; ?>><a href="<?php echo base_url("admin/transactions/motor") ?>" aria-controls="motor" role="tab"><b>MOTOR</b></a></li>
    <li role="presentation"<?php echo $active == "itp" ? ' class="active"' : ''; ?>><a href="<?php echo base_url("admin/transactions/itp") ?>" aria-controls="itp" role="tab"><b>ITP</b></a></li>
    <li role="presentation"<?php echo $active == "hep" ? ' class="active"' : ''; ?>><a href="#" aria-controls="fire" role="tab" data-toggle="tab"><b>HEP+</b></a></li>
</ul>
<!-- Tab panels -->
<h2><?php echo $title; ?> Quote List
    <div class="pull-right">
        <!-- <label class="pull-left"><small>Status Filter</small></label> -->
        <?php echo form_dropdown('status', $statuses, $status, 'class="form-control pull-right" data-return-url="'.$return_url.'"'); ?>  
    </div>
</h2><hr />
<!-- END OF DASHBOARD CONTROL 1 -->
<?php echo $quotes_table; ?>

<script type="text/javascript">
    function urlencode(str) {
      //       discuss at: http://phpjs.org/functions/urlencode/
      //      original by: Philip Peterson
      //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      //      improved by: Brett Zamir (http://brett-zamir.me)
      //      improved by: Lars Fischer
      //         input by: AJ
      //         input by: travc
      //         input by: Brett Zamir (http://brett-zamir.me)
      //         input by: Ratheous
      //      bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      //      bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      //      bugfixed by: Joris
      // reimplemented by: Brett Zamir (http://brett-zamir.me)
      // reimplemented by: Brett Zamir (http://brett-zamir.me)
      //             note: This reflects PHP 5.3/6.0+ behavior
      //             note: Please be aware that this function expects to encode into UTF-8 encoded strings, as found on
      //             note: pages served as UTF-8
      //        example 1: urlencode('Kevin van Zonneveld!');
      //        returns 1: 'Kevin+van+Zonneveld%21'
      //        example 2: urlencode('http://kevin.vanzonneveld.net/');
      //        returns 2: 'http%3A%2F%2Fkevin.vanzonneveld.net%2F'
      //        example 3: urlencode('http://www.google.nl/search?q=php.js&ie=utf-8&oe=utf-8&aq=t&rls=com.ubuntu:en-US:unofficial&client=firefox-a');
      //        returns 3: 'http%3A%2F%2Fwww.google.nl%2Fsearch%3Fq%3Dphp.js%26ie%3Dutf-8%26oe%3Dutf-8%26aq%3Dt%26rls%3Dcom.ubuntu%3Aen-US%3Aunofficial%26client%3Dfirefox-a'

      str = (str + '')
        .toString();

      // Tilde should be allowed unescaped in future versions of PHP (as reflected below), but if you want to reflect current
      // PHP behavior, you would need to add ".replace(/~/g, '%7E');" to the following.
      return encodeURIComponent(str)
        .replace(/!/g, '%21')
        .replace(/'/g, '%27')
        .replace(/\(/g, '%28')
        .replace(/\)/g, '%29')
        .replace(/\*/g, '%2A')
        .replace(/%20/g, '+');
    }

	$(document).ready(function(){
		$("select[name=status]").change(function(){
            var sStatus = urlencode($(this).val());
			window.location = $(this).data("return-url") + '/' + sStatus;
		});
	});
</script>