<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Redirects control of Client Module
     *
     * @param $logged_in
     *   defines if the page must be or must not be logged in.
     */
    public function logged_in($logged_in) {
        if($logged_in) {
            if(!$this->session->userdata('pk') || $this->session->userdata('user_type') != USER_ADMIN) {
                redirect('admin/login');
            }
        }
        else {
            if($this->session->userdata('pk') && $this->session->userdata('user_type') == USER_ADMIN) {
                redirect('admin');
            }
        }
    }

    /**
     * Login Function
     *
     * @param $username
     * @param $password
     */
    public function login($username, $password) {
        $query = $this->db->select(array(
                'users.pk',
                'users.password',
                'users.type',
                'users.user_type',
                'users.email_address'
        ))
        ->where('email_address', $username)
        ->where('status', USER_ACTIVE)
        ->where('user_type', USER_ADMIN)
        ->get('users');

        if($query->num_rows()) {
            $user = $query->row_object();

            if(password_verify($password, $user->password)) {
                $this->session->set_userdata(array(
                    'pk'            => $user->pk,
                    'email_address' => $user->email_address,
                    'type'          => $user->type,
                    'user_type'     => $user->user_type
                ));

                redirect('admin');
            }
        }

        $this->session->set_flashdata('form_error', 'Invalid username or password.');
        $this->view->login();
    }

}
