<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_view_model extends CI_Model {
    private $header_data;

    public function __construct() {
        parent::__construct();

        $scriptsIE = jsScripts(array(
            jsUrl('core/html5shiv.min.js'),
            jsUrl('core/respond.min.js')
        ));

        $scripts = jsScripts(array(
            jsUrl('core/jquery.min.js'),
            jsUrl('core/bootstrap.min.js'),
            jsUrl('core/chosen.jquery.min.js'),
            jsUrl('core/moment.js'),
            jsUrl('core/bootstrap-datepicker.min.js'),
            jsUrl('common.js')
        ));

        $this->header_data = array(
            'scripts'   => $scripts,
            'scriptsIE' => $scriptsIE
        );
    }

    /**
     * Renders the login page
     */
    public function login() {
        $this->header_data['title'] = 'Admin Login';

        $this->load->view('admin/header', $this->header_data);
        $this->load->view('login');
        $this->load->view('admin/footer');
    }

    /**
     * Renders the dashboard page.
     */
    public function dashboard() {
        $this->header_data['title'] = 'Admin Dashboard';

        $this->load->view('admin/header', $this->header_data);
        $this->load->view('motor_insurance/admin/dashboard');
        $this->load->view('admin/footer');
    }

    /**
     * Renders the transactions page.
     *
     * @param $motors
     * @param $filters
     * @param $totalPage
     */
    public function transactions($motors, $filters, $totalPage) {
        $this->header_data['title'] = 'Motor Transactions';

        $this->load->view('admin/header', $this->header_data);
        $this->load->view('motor_insurance/admin/transactions', ['motors' => $motors, 'filters' => $filters, 'totalPage' => $totalPage]);
        $this->load->view('admin/footer');
    }

    /**
     * View Single Motor Quote
     *
     * @param $data
     */
    public function motor_quote($data) {
        $this->header_data['title'] = 'Motor';

        $this->load->view('admin/header', $this->header_data);
        $this->load->view('motor_insurance/admin/motor_quote', [
            'data' => $data
        ]);
        $this->load->view('admin/footer');
    }

    /**
     * Delivery List
     *
     * @param $policies
     * @param $filters
     * @param $totalPage
     */
    public function delivery($policies, $filters, $totalPage) {
        $this->header_data['title'] = 'For Delivery List';

        $this->load->view('admin/header', $this->header_data);
        $this->load->view('motor_insurance/admin/delivery', ['policies' => $policies, 'filters' => $filters, 'totalPage' => $totalPage]);

        $this->load->view('admin/footer');
    }

}