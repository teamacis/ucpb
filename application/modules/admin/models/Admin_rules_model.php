<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_rules_model extends CI_Model {

    /**
     * Client Rule Factory Setups
     *
     * @param $formValidation
     * @param $type
     * @return bool
     */
    public function validate($formValidation, $type) {

        if(method_exists($this, $type)) {
            $formValidation->set_rules($this->$type());
            $formValidation->set_error_delimiters('<div class="error_msg"><p>', '</p></div>');
            $formValidation->set_message('require\d', '{field} is required.');

            return $formValidation->run();
        }

        return false;
    }

    /**
     * Login Validation
     * @return array
     */
    private function login() {
        return array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|max_length[50]'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|max_length[50]'
            )
        );
    }

}
