<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->admin->logged_in(true);
	}

	public function index($status=NULL) {
		$this->itp($status);
	}

	public function itp($status=NULL)
	{
		$status = urldecode($status);
		$quotes_table = Modules::run("quotes/table", API_KEY, "itp", $status);
		$statuses = array("" => "All") + $this->load->config("status", TRUE);

		$data = [
			"quotes_table"	=> $quotes_table,
			"statuses"		=> $statuses,
			"status"		=> $status,
			"return_url"	=> base_url("admin/transactions/itp"),
			"active"		=> "itp",
			"title"			=> "ITP"
		];

		$this->render("transactions", $data,"Transactions","ITP");
	}

	public function motor($status=NULL)
	{
		$status = urldecode($status);
		$quotes_table = Modules::run("quotes/table", API_KEY, "motor", $status);
		$statuses = array("" => "All") + $this->load->config("status", TRUE);

		$data = [
			"quotes_table"	=> $quotes_table,
			"statuses"		=> $statuses,
			"status"		=> $status,
			"return_url"	=> base_url("admin/transactions/motor"),
			"active"		=> "motor",
			"title"			=> "Motor"
		];

		$this->render("transactions", $data,"Transactions","Motor");
	}

	public function detail($quote_pk=NULL) {
		$detail = Modules::run("quotes/detail", API_KEY, $quote_pk);
		
		$this->render("transaction_detail", ["detail" => $detail],"Transactions","Detail");
	}
}

/* End of file Transactions.php */
/* Location: ./application/modules/admin/controllers/Transactions.php */