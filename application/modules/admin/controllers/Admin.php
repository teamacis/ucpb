<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('pagination');

        $this->load->model('admin_model', 'admin');
        $this->load->model('admin_view_model', 'view');

        $this->load->model('motor_insurance/motor_model', 'motor_insurance');
        $this->load->model('vehicle/vehicle_model', 'vehicle');
        $this->load->model('motor_insurance/admin/motor_quote_admin_model', 'motor_quote');
        $this->load->model('motor_insurance/admin/motor_computation_admin_model', 'motor_computation');

        $this->load->model('policy/admin/policy_admin_model', 'policy');
    }

    /**
     * Login
     */
    public function login() {
        $this->admin->logged_in(false);
        $this->view->login();
    }

    /**
     * Logout Function
     */
    public function logout() {
        session_destroy();
        redirect('admin/login');
    }

    /**
     * Dashboard
     */
    public function index() {
        $this->admin->logged_in(true);
        $this->view->dashboard();
    }

    /**
     * Transactions
     */
    public function transactions($status = null) {
        $this->admin->logged_in(true);

        $filters = [
            'motor_quotes.status' => $status
        ];

        $motors    = $this->motor_quote->fetch($filters);
        $totalRows = $this->motor_quote->total_rows($filters);

        $this->pagination->initialize([
            'page_query_string' => true,
            'use_page_numbers'  => true,
            'total_rows'        => $totalRows,
            'per_page'          => TOTAL_ROWS,
            'num_links'         => 5,
            'cur_tag_open'      => '<li class="on"><a href="javascript:void(0)" class="btn_page">',
            'cur_tag_close'     => '</a></li>',
            'num_tag_open'      => '<li>',
            'num_tag_close'     => '</li>',
            'first_link'        => false,
            'last_link'         => false,
            'next_link'         => false,
            'prev_link'         => false,
            'attributes'     => [
                'class' => 'btn_page'
            ]
        ]);

        $totalPage = ceil($totalRows / TOTAL_ROWS);

        $this->view->transactions($motors, $filters, $totalPage);
    }

    /**
     * View Motor
     */
    public function motor_quote($pk) {
        $this->admin->logged_in(true);

        // Fetch Motor Quote and Motor Information
        $motorQuote = $this->motor->get_motor_quote_by_pk($pk);

        /**
         * Computation of Motor from the system's computation
         */
        if($motorQuote['status'] != STATUS_ACTIVE) {
            $motor = $this->vehicle->get_motor_for_computation($motorQuote['motors_pk']);
            $data = (object) array_merge($this->motor->quote_premiums($motor, $motorQuote), $motorQuote);
        }
        /**
         * Computation of Motor from the database
         */
        else {
            $data = (object) array_merge($this->motor_computation->fetch_by_pk($pk, $motorQuote), $motorQuote);
        }

        $this->view->motor_quote($data);

    }

    /**
     * Delivery Page
     */
    public function delivery($status = []) {
        $this->admin->logged_in(true);

        $filters = [
            'status' => $status
        ];

        $policies  = $this->policy->get_policies('motor_insurance', $filters);
        $totalRows = $this->policy->total_rows($filters);

        $this->pagination->initialize([
            'page_query_string' => true,
            'use_page_numbers'  => true,
            'total_rows'        => $totalRows,
            'per_page'          => TOTAL_ROWS,
            'num_links'         => 5,
            'cur_tag_open'      => '<li class="on"><a href="javascript:void(0)" class="btn_page">',
            'cur_tag_close'     => '</a></li>',
            'num_tag_open'      => '<li>',
            'num_tag_close'     => '</li>',
            'first_link'        => false,
            'last_link'         => false,
            'next_link'         => false,
            'prev_link'         => false,
            'attributes'     => [
                'class' => 'btn_page'
            ]
        ]);

        $totalPage = ceil($totalRows / TOTAL_ROWS);

        $this->view->delivery($policies, $filters, $totalPage);
    }

}