<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Places extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$provinces =  json_decode(Modules::run("place/get_provinces", API_KEY));
		$this->_display($provinces, "Provinces");
	}

	public function cities($province=NULL)
	{
		$cities 	= json_decode(Modules::run("place/get_cities", API_KEY, $province));
		$this->_display($cities, "Cities", $province);
	}

	public function barangays($city=NULL)
	{
		$barangays = json_decode(Modules::run("place/get_barangays", API_KEY, $city));
		$this->_display($barangays, "Barangays", $city);
	}

	private function _display($data=array(),$title=NULL,$active=NULL)
	{
		$this->load->helper('inflector');
		$type = singular($title);

		$dropdown = "";
		if ( $type == 'City' ) {;
			$provinces 	= json_decode(Modules::run("place/get_provinces", API_KEY),TRUE);
			$filter_data = array("" => "- Please Select -");
			if ( $provinces ) {
				$filter_data 	+= array_column($provinces['data'],"place","pk");
				$dropdown 		= form_dropdown('parent_id', $filter_data, $active, 'class="form-control  chosen-select required" data-placeholder="- Please Select -"');
			}
		} elseif ( $type == 'Barangay' ) {
			$cities 	= json_decode(Modules::run("place/get_cities", API_KEY, $active),TRUE);
			$filter_data = array("" => "- Please Select -");
		
			if ( $cities ) {
				$filter_data 	+= array_column($cities['data'],"place","pk");
				$dropdown 		= form_dropdown('parent_id', $filter_data, $active, 'class="form-control  chosen-select required" data-placeholder="- Please Select -"');
			}
		}
		
		$this->render("place/table",["places" => $data, "type" => $type , "title" => $title, "dropdown" => $dropdown], "Settings", $title);
	}
}

/* End of file Places.php */
/* Location: ./application/modules/admin/controllers/settings/Places.php */