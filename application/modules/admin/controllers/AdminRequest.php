<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminrequest extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('admin_model', 'admin');
        $this->load->model('policy/admin/policy_admin_model', 'policy');
        $this->load->model('admin_view_model', 'view');
        $this->load->model('admin_rules_model', 'rule');

        $this->load->model('motor_insurance/admin/motor_quote_admin_model', 'motor_quote');
    }

    public function login() {
        $validated = $this->rule->validate($this->form_validation, 'login');

        if($validated) {
            $this->admin->login($this->input->post('username'), $this->input->post('password'));
        }
        else {
            $this->view->login();
        }
    }

    public function update_status($quoteId) {
        $status = $this->input->post('status');
        $this->motor_quote->update_status($quoteId, $status);
        $this->send_mail_status($this->input->post('email_address'), $status);

        redirect('admin/transactions?per_page='.$this->input->post('per_page'));
    }

    public function update_delivery() {
        $post = $this->input->post();

        $this->policy->update_delivery($post['date_delivered'], $post['policy_id']);

        redirect('admin/delivery');
    }

    private function send_mail_status($emailAddress, $status) {
        $this->email->clear();
        $this->email->set_header('Content-type', 'text/html; charset=iso-8859-1');
        $this->email->from('motor_insurance@ucpbgen.com', 'MOTOR UCPBGEN');
        $this->email->to($emailAddress);
        $this->email->subject('UCPBGEN Motor Quote Status - '.$status);
        $this->email->message('Sample Message');
        $this->email->send();
    }

}