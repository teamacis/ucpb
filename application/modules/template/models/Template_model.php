<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function load($template, $view, $params = array()) {
        $this->$template($view, $params);
    }

    private function motor_template($view, $params = array()) {
        $this->load->view('template/motor_header');
        $this->load->view($view, $params);
        $this->load->view('template/motor_footer');
    }

}