<div class="guide_step box mobile_gs">
    <?php if ($this->uri->segment(2) != 'login' && $this->uri->segment(3) != 'login') { ?>
        <div class="container">
            <div class="guide_step_content mobile_gsc">
                <?php if($this->uri->segment(1) == 'motor-insurance') { ?>
                    <div class="steps <?php echo ($this->session->userdata('motor_step') > 1) ? "done" : "";?> <?php echo ($this->session->userdata('motor_step') == 1) ? "active" : "";?>"><a href="#">Quote Request</a></div>
                    <div class="steps <?php echo ($this->session->userdata('motor_step') > 2) ? "done" : "";?> <?php echo ($this->session->userdata('motor_step') == 2) ? "active" : "";?>"><a href="#">Vehicle Info</a></div>
                    <div class="steps <?php echo ($this->session->userdata('motor_step') > 3) ? "done" : "";?> <?php echo ($this->session->userdata('motor_step') == 3) ? "active" : "";?>"><a href="#">Coverage Details</a></div>
                    <div class="steps <?php echo ($this->session->userdata('motor_step') > 4) ? "done" : "";?> <?php echo ($this->session->userdata('motor_step') == 4) ? "active" : "";?>"><a href="#">Personal Info</a></div>
                    <div class="steps last <?php echo ($this->session->userdata('motor_step') > 5) ? "done" : "";?> <?php echo ($this->session->userdata('motor_step') == 5) ? "active" : "";?>"><a href="#">Summary &amp;<br/> Confirmation</a></div>
                <?php } else { ?>
                    <div class="steps done"><a href="#">Quote Request</a></div>
                    <div class="steps done"><a href="#">Vehicle Info</a></div>
                    <div class="steps done"><a href="#">Coverage Details</a></div>
                    <div class="steps done"><a href="#">Personal Info</a></div>
                    <div class="steps done last"><a href="#">Summary &amp;<br/> Confirmation</a></div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>

<!-- /process steps -->
<div class="container motor_content box <?php
echo ($this->uri->segment(2) == "success" || $this->uri->segment(2) == "get_back_prompt") ? "success_page" : "";
echo ($this->uri->segment(2) == "index" || $this->uri->segment(2) == "") ? "intro_page" : "";
echo ($this->uri->segment(2) == "return_callback") ? "success_page" : "";
echo ($this->uri->segment(2) == "verify") ? "pass_page" : "";
?>">
    <div class="row no_gutter">