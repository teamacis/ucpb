<div class="guide_step box mobile_gs">
    <?php if ($this->uri->segment(2) != 'login' && $this->uri->segment(3) != 'login') { ?>
        <div class="container">
            <div class="guide_step_content mobile_gsc">
                <div class="steps <?php echo ($this->session->userdata('itp_step') > 1) ? "done" : "";?> <?php echo ($this->session->userdata('itp_step') == 1) ? "active" : "";?>"><a href="#">Quote Request</a></div>
                <div class="steps <?php echo ($this->session->userdata('itp_step') > 2) ? "done" : "";?> <?php echo ($this->session->userdata('itp_step') == 2) ? "active" : "";?>"><a href="#">Personal Info</a></div>
                <div class="steps last <?php echo ($this->session->userdata('itp_step') == 'finish') ? "done" : "";?> <?php echo ($this->session->userdata('itp_step') == 3) ? "active" : "";?>"><a href="#">Summary &amp;<br/> Confirmation</a></div>
            </div>
        </div>
    <?php } ?>
</div>

<!-- /process steps -->
<div class="container motor_content box <?php
echo ($this->uri->segment(2) == "success" || $this->uri->segment(2) == "get_back_prompt") ? "success_page" : "";
echo ($this->uri->segment(2) == "index" || $this->uri->segment(2) == "") ? "intro_page" : "";
echo ($this->uri->segment(2) == "return_callback") ? "success_page" : "";
echo ($this->uri->segment(2) == "verify") ? "pass_page" : "";
?>">
    <div class="row no_gutter">