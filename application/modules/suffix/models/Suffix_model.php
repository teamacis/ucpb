<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suffix_model extends MY_Model {

	public $rules = [
		[
			'field'	=> 'suffix',
			'label'	=> 'Suffix',
			'rules'	=> 'trim|required'
		]
	];

    public function __construct() {
        parent::__construct();
    }

    public function fetch() {
        $query = $this->db->get('suffix');
        return $query->result();
    }

    public function save($pk=NULL,$data=array())
    {
    	if ( is_array($data) AND count($data) > 0 ) {
    		if ( ! is_null($pk) ) {
    			return parent::update('suffix', $data, ["pk" => $pk]);
    		} else {
    			return parent::insert('suffix', $data);
    		}
    	}

    	return FALSE;
    }
}