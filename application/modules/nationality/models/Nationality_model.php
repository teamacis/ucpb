<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nationality_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function fetch() {
        $query = $this->db->get('nationalities');
        return $query->result();
    }

}