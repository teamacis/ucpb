<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal_information_rules_model extends CI_Model {

    protected $fields;

    /**
     * Rule Factory Setups
     *
     * @param $formValidation
     * @param $type
     * @return bool
     */
    public function validate($formValidation, $type, $fields = []) {
        $this->fields = $fields;

        if(method_exists($this, $type)) {
            $formValidation->set_rules($this->$type());
            $formValidation->set_error_delimiters('<div class="error_msg"><p>', '</p></div>');
            $formValidation->set_message('required', '{field} is required.');

            return $formValidation->run();
        }

        return false;
    }

    /**
     * Save Personal Information Validation
     * @return array
     */
    private function save() {
        $rules = [
            [
                'field' => 'agent[with_agent]',
                'label' => 'With Agent',
                'rules' => 'required'
            ],
            [
                'field' => 'personal_information[salutation]',
                'label' => 'Salutation',
                'rules' => 'required'
            ],
            [
                'field' => 'personal_information[lname]',
                'label' => 'Last Name',
                'rules' => 'required|max_length[50]'
            ],
            [
                'field' => 'personal_information[fname]',
                'label' => 'First Name',
                'rules' => 'required|max_length[50]'
            ],
            [
                'field' => 'personal_information[mname]',
                'label' => 'Middle Initial',
                'rules' => 'trim|max_length[1]'
            ],
            [
                'field' => 'personal_information[gender]',
                'label' => 'Gender',
                'rules' => 'required'
            ],
            [
                'field' => 'personal_information[birthdate]',
                'label' => 'Date of Birth',
                'rules' => 'required'
            ],
            [
                'field' => 'personal_information[occupation]',
                'label' => 'Occupation',
                'rules' => 'required'
            ],
            [
                'field' => 'mailing_address[province]',
                'label' => 'Province',
                'rules' => 'required'
            ],
            [
                'field' => 'mailing_address[city]',
                'label' => 'City',
                'rules' => 'required'
            ],
            [
                'field' => 'mailing_address[barangay]',
                'label' => 'Barangay',
                'rules' => 'required'
            ],
            [
                'field' => 'mailing_address[house_number]',
                'label' => 'House Number',
                'rules' => 'required'
            ],
            [
                'field' => 'contact_information[email_address]',
                'label' => 'Email Address',
                'rules' => 'required'
            ],
            [
                'field' => 'personal_information[nationality]',
                'label' => 'Nationality',
                'rules' => 'required'
            ]
        ];

        /**
         * Places Rules
         */
        if(isset($this->fields->same_location) && $this->fields->same_location != 'on') {
            $rules[] = [
                'field' => 'mailing_address[province]',
                'label' => 'Province',
                'rules' => 'required'
            ];
            $rules[] = [
                'field' => 'mailing_address[city]',
                'label' => 'City',
                'rules' => 'required'
            ];
            $rules[] = [
                'field' => 'mailing_address[barangay]',
                'label' => 'Barangay',
                'rules' => 'required'
            ];
        }

        /**
         * Agent Rules
         */
        if($this->fields->agent['with_agent'] == 'on') {
            $rules[] = [
                'field' => 'agent[type]',
                'label' => 'Agent Type',
                'rules' => 'required'
            ];

            /**
             * If agent code add rule for AGENT CODE
             */
            if($this->fields->agent['type'] == 'Agent Code') {
                $rules[] = [
                    'field' => 'agent[code]',
                    'label' => 'Agent Code',
                    'rules' => 'required|max_length[50]'
                ];
            } elseif($this->fields->agent['type'] == 'Agency Name') {
                $rules[] = [
                    'field' => 'agent[agency]',
                    'label' => 'Agency Name',
                    'rules' => 'required|max_length[100]'
                ];
            }
            /**
             * If not agent code add rule for AGENT'S FIRST NAME and LAST NAME
             */
            else {
                $rules[] = [
                    'field' => 'agent[fname]',
                    'label' => 'Agent\'s First Name',
                    'rules' => 'required|max_length[50]'
                ];
                $rules[] = [
                    'field' => 'agent[lname]',
                    'label' => 'Agent\'s Last Name',
                    'rules' => 'required|max_length[50]'
                ];
            }
        }

        /**
         * Occupation and Tin Rules
         */
        if($this->fields->personal_information['with_tin'] == 1) {
            $rules[] = [
                'field' => 'personal_information[tin]',
                'label' => 'Tin',
                'rules' => 'required|max_length[50]'
            ];
        }
        else {
            $rules[] = [
                'field' => 'personal_information[id_type]',
                'label' => 'ID Type',
                'rules' => 'required|max_length[50]'
            ];
            $rules[] = [
                'field' => 'personal_information[id_number]',
                'label' => 'ID #',
                'rules' => 'required|max_length[50]'
            ];
        }

        if ($this->uri->segment(1) == "motor_insurance") {
            if((!isset($this->fields->contact_information['telephone']) || !$this->fields->contact_information['telephone']) && (!isset($this->fields->contact_information['mobile']) || !$this->fields->contact_information['mobile'])) {
                $rules[] = [
                    'field' => 'contact_information[mobile]',
                    'label' => 'Mobile',
                    'rules' => 'required|max_length[50]',
                    'errors' => [
                        'required' => 'Either telephone or mobile is required.'
                    ]
                ];
            }
        } else {
            /**
             * Phone Number Rules
             */
            if((!isset($this->fields->telephone) || !$this->fields->telephone) && (!isset($this->fields->mobile) || !$this->fields->mobile)) {
                $rules[] = [
                    'field' => 'contact_information[mobile]',
                    'label' => 'Mobile',
                    'rules' => 'required|max_length[50]',
                    'errors' => [
                        'required' => 'Either telephone or mobile is required.'
                    ]
                ];
            }
        }

        return $rules;
    }

    public function check_agent() {
        if($this->fields->agent['with_agent'] == 'on') {
            $this->load->model('agent/agent_model', 'agent');

            /**
             * If agent code add rule for AGENT CODE
             */
            if($this->fields->agent['type'] == 'Agent Code') {
                $result = $this->agent->fetch_by_field('code', $this->fields->agent['code']);
                if(!$result) {
                    $this->session->set_flashdata('agent_code_error', 'Agent code is invalid');
                    return false;
                }
            } elseif ($this->fields->agent['type'] == 'Agency Name') {
                $result = $this->agent->fetch_by_field('agencies.name', $this->fields->agent['agency']);
                if(!$result) {
                    $this->session->set_flashdata('agent_agency_error', 'Agency name is invalid');
                    return false;
                }
            }
            /**
             * If not agent code add rule for AGENT'S FIRST NAME and LAST NAME
             */
            else {
                $result = $this->agent->fetch_by_name($this->fields->agent['fname'], $this->fields->agent['lname']);
                if(!$result) {
                    $this->session->set_flashdata('agent_name_error', 'Agent name is invalid');
                    return false;
                }
            }
        }

        return true;
    }

}