<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_quote_agent_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function fetch_by_field($field, $value) {
        $query = $this->db->where($field, $value)
                          ->get('motor_quotes_agents');

        if($query->num_rows() > 0) {
            return $query->row_object();
        }
        else {
            return false;
        }
    }
}