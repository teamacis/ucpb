<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motor_quotes_file_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save($data) {
        $this->db->delete('motor_quotes_files', ['motor_saved_quotes_token' => $data['motor_saved_quotes_token'], 'type' => $data['type']]);
        $this->db->insert('motor_quotes_files', $data);
    }

}
