<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents_model extends MY_Model {

	private $table_name = 'quote_documents';

	public function __construct()
	{
		parent::__construct();
	}

	public function result($where=array()) {
		$options = [
			"where" => $where
		];
		
		return parent::get($this->table_name, $options);
	}

	public function save($quotek_pk=NULL,$file_name=NULL,$file_type=NULL,$upload_data=NULL)
	{
		if ( ! is_null($quotek_pk) AND ! is_null($file_name) AND ! is_null($file_type) ) {
			$data = [
				"quote_pk"		=> $quotek_pk,
				"file_name"		=> $file_name,
				"file_type"		=> $file_type,
				"data"			=> json_encode($upload_data)
			];
			return parent::insert($this->table_name, $data);
		}

		return FALSE;
	}

	public function remove($where=array()) {
		$document = $this->db->get_where($this->table_name,$where)->row();
		unlink(DOCUMENTS_DIRECTORY . $document->file_name);
		return parent::delete($this->table_name, $where, TRUE);
	}
}

/* End of file Documents_model.php */
/* Location: ./application/modules/quotes/models/Documents_model.php */