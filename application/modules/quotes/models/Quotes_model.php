<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotes_model extends MY_Model {

	private $table_name = "quotes";

	public function __construct()
	{
		parent::__construct();
	}

	public function result($line=NULL,$user_pk=NULL,$status=NULL,$order=NULL,$sort=NULL,$limit=0,$offset=0)
	{
		$allowed_lines = ["itp","motor","fire"];

		$where = array();
		$join  = array();
		$fields = "q.*,p.start_date,p.end_date";

		if ( ! is_null($line) AND in_array($line, $allowed_lines) ) {
			$where[] = "q.line = '{$line}'";
		}

		$join[]		= "LEFT JOIN dragonpay_transactions t ON t.quotes_pk = q.pk";
		$join[] 	= "LEFT JOIN policies p ON p.dragonpay_transaction_pk = t.pk";
		echo $status;
		if ( ! is_null($status) AND array_key_exists($status, $this->load->config('status',TRUE)) ) {
			if ( in_array($status, [STATUS_ISSUED,STATUS_ACTIVE,STATUS_EXPIRED]) ) {

				$status_completed = STATUS_COMPLETED;
				$where[] = "q.status = '{$status_completed}'";

				if ( $status == STATUS_ISSUED ) {
					$where[] = "p.start_date > CURDATE()";
				} elseif ( $status == STATUS_ACTIVE ) {
					$where[] = "CURDATE() BETWEEN p.start_date AND p.end_date";
				} elseif ( $status == STATUS_EXPIRED ) {
					$where[] = "p.end_date < CURDATE()";
				}

			} else {
				$where[] 	= "q.status = '{$status}'";
			}	
		}

		if ( ! is_null($user_pk) ) {
			$join[] 	= "INNER JOIN users_quotes uq ON uq.quotes_pk = q.pk AND uq.line = '{$line}'";
			$where[] 	= "uq.users_pk = '{$user_pk}'";
		} 

		$options = [
			"fields"	=> $fields,
			"where"		=> $where,
			"join"		=> $join,
			"order"		=> $order,
			"sort"		=> $sort,
			"limit"		=> $limit,
			"offset"	=> $offset
		];

		return parent::get($this->table_name . " q",$options);
	}

	public function row($quote_pk=NULL)
	{
		$options = [
			"where"	=> ["pk = '{$quote_pk}'"],
			"row"	=> TRUE
		];

		return parent::get($this->table_name,$options);
	}

	public function token($token=NULL) {

		$quotes = $this->db->get_where($this->table_name, ["token" => $token]);

		if ( $quotes->num_rows() > 0 ) {
			return $quotes->row();
		}

		return FALSE;
	}

	public function save($where=array(),$data=array()) {
		if ( count($where) > 0 ) {
			return parent::update($this->table_name, $data, $where);
		} else {
			return parent::insert($this->table_name, $data);
		}
	}
}

/* End of file Quote_model.php */
/* Location: ./application/modules/quotes/models/Quote_model.php */