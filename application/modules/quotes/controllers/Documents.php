<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('quotes_model');
		$this->load->model('documents_model');
	}

	public function result($api_key=NULL, $token=NULL) {
		if ( $api_key == API_KEY AND ! is_null($token) ) {
			$quote = $this->quotes_model->token($token);
			if ( $quote ) {
				$quote_pk = $quote->pk;
				$where = [
				 	"quote_pk" => $quote_pk
				];

				$documents = $this->documents_model->result($where);

				return $documents;
			}
		}
	}

	public function upload($api_key=NULL, $token=NULL)
	{
		if ( $api_key == API_KEY AND ! is_null($token) ) {
			$quote = $this->quotes_model->token($token);
			if ( $quote ) {
				$config['upload_path']      = DOCUMENTS_DIRECTORY;
	            $config['allowed_types']    = 'jpg|png|pdf|doc';
	            $config['encrypt_name']     = TRUE;

	            $this->load->library('upload', $config);

	            $quote_pk = $quote->pk;

	            if($this->upload->do_upload('documents')) {
	            	$upload_data = $this->upload->data();
	            	$file_name = $upload_data['file_name'];
	            	$file_type = $upload_data['file_type'];

	            	$document_pk = $this->documents_model->save($quote_pk, $file_name, $file_type, $upload_data);
	            	
	            	$initialPreviewConfig = [
	            		"url"	=> base_url("quotes/documents/delete/{$api_key}/{$token}"),
	            		"key"	=> $document_pk,
	            		"caption" => $file_name,
	            		"width"	=> "120px"
	            	];

	            	$icon = "fa-file";

	            	if ( $file_type == "image/jpg" OR $file_type == "image/jpeg" OR $file_type == "image/jpe" OR $file_type == "image/png" ) {
	            		$icon = "fa-file-photo-o text-warning";
	            	} elseif ( $file_type == "application/pdf" ) {
	            		$icon = "fa-file-pdf-o text-danger";
	            	} elseif ( $file_type == "application/msword" ) {
	            		$icon = "fa-file-word-o text-primary";
	            	}

	            	$initialPreview = '<div class="file-preview-other-frame">' . 
											'<div class="file-preview-other">' .
												'<span class="file-icon-4x"><i class="fa '.$icon.'"></i></span>' .
											'</div>' .
										'</div>';
	            	echo json_encode([
	            		'initialPreview' 		=> [$initialPreview],
	            		'initialPreviewConfig' 	=> [$initialPreviewConfig],
	            		'append'				=> true
	            	]);
            					
	            } else {
	            	echo json_encode(["error" => $this->upload->display_errors()]);
	            }
			}
		}
	}

	public function delete($api_key=NULL,$token=NULL)
	{
		if ( $api_key == API_KEY AND ! is_null($token) ) {
			$quote = $this->quotes_model->token($token);
			if ( $quote ) {
				$quote_pk = $quote->pk;
				$document_pk = $this->input->post('key');

				$where = [
					'quote_pk' => $quote_pk
				];

				if ( $document_pk ) {
					$where['pk']	= $document_pk;
				}

				$this->documents_model->remove($where);
				echo json_encode($where);
			}
		}
	}
}

/* End of file Documents.php */
/* Location: ./application/modules/quotes/controllers/Documents.php */