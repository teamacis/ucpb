<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotes extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('quotes_model');
		$this->load->model('documents_model');
	}

	public function table($api_key=NULL,$line="itp",$status=NULL,$order=NULL,$sort=NULL,$limit=0,$offset=0)
	{
		if ( $api_key == API_KEY ) {
			$quotes = $this->quotes_model->result($line,NULL,$status,$order,$sort,$limit,$offset);

			if ( $quotes['count'] > 0 ) {
				foreach ( $quotes['data'] AS $index => $quote ) {
					$where = [
			    		"quote_pk = '{$quote->pk}'"
			    	];
					$quotes['data'][$index]->documents = $this->documents_model->result($where);
				}
			}

			$data = [
				"quotes"	=> $quotes,
				"api_key"	=> API_KEY
			];

			return $this->load->view('table', $data, TRUE);
		}
	}

	public function detail($api_key=NULL,$quote_pk=NULL)
	{
		if ( $api_key == API_KEY ) {
			$saved_quote = $this->quotes_model->row($quote_pk);
			if ( $saved_quote ) {
				$quote =  json_decode($saved_quote->values, TRUE);
				$personal_information 	= $quote['personal_information'];
				$company_information  	= $quote['company_information'];
				$travel_information		= $quote['travel_information'];
				$coverage_type			= $travel_information['coverage_type'];
				$itineraries 			= $quote[strtolower($coverage_type) . '_itinerary']['same'];
				$package_type   		= $quote[strtolower($coverage_type).'_package']['same'];
				$coverages 				= $this->load->config("coverage", TRUE)[strtolower($package_type)];
				//$members 				= isset($quote[strtolower($coverage_type) . '_member']) ? $quote[strtolower($coverage_type) . '_member'] : NULL;
				//$beneficiaries			= isset($quote['beneficiaries']) ? $quote['beneficiaries'] : NULL;

				$this->load->library('travel_insurance/itp_premium', ["itp_quote_pk" => $quote_pk]);
				$this->load->model('travel_information/itp_country_model');
				
				foreach ($itineraries AS $index => $itinerary) {
		        	$itineraries[$index]['country'] = $this->itp_country_model->get_countries($itinerary['destination']);
		        }

				$data = [
					"insurance_cover"		=> $quote['insurance_cover'],
					"personal_information"	=> (object) $personal_information,
					"company_information"	=> (object) $company_information,
					"travel_information"	=> (object) $travel_information,
					"itineraries"			=> $itineraries,
					"coverages"				=> $coverages,
					"premium"     			=> $this->itp_premium->compute(),
					"package_type"			=> $package_type
				];

				$line = $saved_quote->line;

				return $this->load->view("detail/{$line}", $data, TRUE);
			}
		}
	}

	public function update_status($api_key=NULL)
	{
		$status 	= $this->input->post('status');
		$quote_pk	= $this->input->post('quote_pk');
		$remarks    = $this->input->post('remarks');

		if ( $api_key == API_KEY AND ! is_null($quote_pk) AND ! is_null($status) ) {
			$allowed_statuses = [STATUS_APPROVED,STATUS_DISAPPROVED];
			if ( in_array($status, $allowed_statuses) ) {
				$data = ["status"	=> $status, "remarks" => $remarks];
				$where = ["pk" => $quote_pk];

				if ( $this->quotes_model->save($where,$data) ) {
					if ( $status == STATUS_APPROVED ) {
						$this->send_notification_to_user(API_KEY, $quote_pk, $status);
					}
					echo json_encode(["status" => TRUE, "new_status" => ucfirst(strtolower($status)), "message" => "Quote succesfully " . strtolower($status)]);
				} else {
					echo json_encode(["status" => FALSE, "message" => "Unable to update status"]);
				}
			} else {
				echo json_encode(["status" => FALSE, "message" => "Invalid Status"]);
			}
		} else {
			echo json_encode(["status" => FALSE, "message" => "Invalid Data"]);
		}
	}

	public function send_notification_to_user($api_key=NULL, $quote_pk=NULL, $status=NULL)
	{
		$allowed_statuses = [STATUS_APPROVED,STATUS_DISAPPROVED,STATUS_FOR_APPROVAL];
		if ( $api_key == API_KEY AND ! is_null($quote_pk) AND ! is_null($status) AND in_array($status, $allowed_statuses) ) {
			$saved_quote = $this->quotes_model->row($quote_pk);
			if ( $saved_quote ) {
				$quote = json_decode($saved_quote->values);
				$personal_information = $quote->personal_information;

				$email_address = $personal_information->email_address;
				$name          = (strlen($personal_information->fname) > 0) ? $personal_information->fname . ' ' . $personal_information->lname : "User";

				$action = "";
				$message = "";
				
				if ( $status == STATUS_APPROVED ) {
					$action 	= "approved";
					$link   	= base_url("travel_insurance/quote/summary?token={$saved_quote->token}");
					$message = $this->load->view('email/approve_quote', ["name" => $name, "link" => $link], TRUE);
				} elseif ( $status == STATUS_DISAPPROVED ) {
					$action = "dissapproved";
				} elseif ( $status == STATUS_FOR_APPROVAL ) {
					$action = "submitted";
					$message = $this->load->view('email/for_approval_quote', ["name" => $name], TRUE);
				}

				$this->email->clear();
        		$this->email->from(ITP_EMAIL, ITP_EMAIL_NAME);
       		 	$this->email->to($email_address);
        		$this->email->subject("Your quote request was {$action}");
        		$this->email->message($message);

        		$this->email->send();
			}
		}
	}

	public function test()
	{
		$this->_send_notification_to_user(4,STATUS_APPROVED);
	}
}

/* End of file Quotes.php */
/* Location: ./application/modules/quotes/controllers/Quotes.php */