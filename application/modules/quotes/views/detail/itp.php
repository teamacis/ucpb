<div class="col-md-12 col-sm-12 full_length">
	<div class="new_row">
		<div class="col-md-6">
			<?php if ($insurance_cover == 'individual'): ?>
				<table class="table summary_table table_3">
					<thead>
						<tr>
							<th colspan="2">Personal Information</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><strong>Name:</strong><span class="value_2"><?php echo ucwords($personal_information->fname . ' ' . $personal_information->lname); ?></span></td>
						</tr>
					</tbody>
				</table>
			<?php else: ?>
				<table class="table summary_table table_3">
					<thead>
						<tr>
							<th colspan="2">Company Information</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><strong>Name:</strong><span class="value_2"><?php echo ucwords($personal_information->name); ?></span></td>
						</tr>
					</tbody>
				</table>
			<?php endif ; ?>
			<table class="table summary_table table_3">
				<thead>
					<tr>
						<th colspan="2">Contact Information</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($insurance_cover == 'company'): ?>
						<tr>
							<td><strong>Contact Person:</strong><span class="value_1"><?php echo $personal_information->contact_person; ?></span></td>
						</tr>
					<?php endif; ?>
					<tr>
						<td><strong>Telephone Number:</strong><span class="value_1"><?php echo $personal_information->telephone; ?></span></td>
					</tr>
					<tr>
						<td><strong>Mobile Number:</strong><span class="value_1"><?php echo $personal_information->mobile; ?></span></td>
					</tr>
					<tr>
						<td><strong>Email:</strong><span class="value_1"><?php echo $personal_information->email_address; ?></span></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-6">
			<table class="table summary_table table_3">
				<thead>
					<tr>
						<th colspan="4">Travel Information</th>
					</tr>
				</thead>
				<tbody>	
					<tr>
						<td colspan="2"><strong>Purpose of Travel:</strong><span class="value_2"><?php echo $travel_information->purpose_of_travel; ?></span></td>							
					</tr>
					<tr>
						<td colspan="2"><strong>Type of Coverage:</strong><span class="value_2"><?php echo $travel_information->coverage_type; ?></span></td>
					</tr>
					<tr>
						<td colspan="2"><strong>Start Date:</strong><span class="value_2"><?php echo date("m-d-Y",strtotime($premium->departure_date)); ?></span></td>
					</tr>
					<tr>
						<td colspan="2"><strong>End Date:</strong><span class="value_2"><?php echo date("m-d-Y",strtotime($premium->arrival_date)); ?>  (Both dates inclusive)</span></td>
					</tr>
				</tbody>
			</table>
			<table class="table summary_table table_3">
				<thead>
					<tr>
						<th colspan="4">Itineraries</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($itineraries) AND is_array($itineraries) AND count($itineraries) > 0): ?>
						<?php foreach ($itineraries AS $itinerary): ?>
							<?php $itinerary = (object) $itinerary; ?>
							<tr>
								<td><strong>Destination:</strong><span class="value_2"><?php echo $itinerary->country; ?></span></td>
								<td><strong>Date:</strong><span class="value_2"><?php echo date("M d, Y",strtotime($itinerary->start_date)) . ' - ' . date("M d, Y",strtotime($itinerary->end_date)); ?></span></td>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="new_row row_pad">
		<div class="col-md-6 summary_section table_type_1">
			<div class="section_title title_1">
				<h5>Coverage</h5>
			</div>
			<div class="summary_content">
			    <div class="col-md-10 summary_group">
			        <div class="summary_line">
			            <h4><?php echo $package_type; ?> Quotation</h4>
			        </div>

			        <div class="summary_line">
			            <div class="col-md-8 info_field">
			                <span class="order">I.</span>
			                <strong class="label_item"><?php echo $coverages['accidental_death']['label']; ?></strong>
			            </div>
			            <div class="col-md-4 info_field text-right">
			                <span class="value_item"><?php echo $coverages['accidental_death']['value']; ?></span>
			            </div>
			        </div>

			        <div class="summary_line">
			            <div class="col-md-8 info_field">
			                <span class="order">II.</span>
			                <strong class="label_item"><?php echo $coverages['accidental_burial_benefit']['label']; ?></strong>
			            </div>
			            <div class="col-md-4 info_field text-right">
			                <span class="value_item"><?php echo $coverages['accidental_burial_benefit']['value']; ?></span>
			            </div>
			        </div>

			        <div class="summary_line">
			            <div class="col-md-8 info_field">
			                <span class="order">III.</span>
			                <strong class="label_item"><?php echo $coverages['personal_liability']['label']; ?></strong>
			            </div>
			            <div class="col-md-4 info_field text-right">
			                <span class="value_item"><?php echo $coverages['personal_liability']['value']; ?></span>
			            </div>
			        </div>

			        <div class="summary_line">
			            <div class="col-md-8 info_field">
			                <span class="order">IV.</span>
			                <strong class="label_item">Travel Assistance Services</strong>
			            </div>
			        </div>

			        <div style="margin-left: 32px;">
			            <?php if ( isset($coverages['travel_assistant_services']) AND is_array($coverages['travel_assistant_services']) ): ?>
			                <?php foreach ( $coverages['travel_assistant_services'] AS $index => $row ): ?>
			                    <?php $number = $index + 1; ?>
			                    <div class="summary_line">
			                        <div class="col-md-8 info_field">
			                            <span class="order"><?php echo $number; ?>.</span>
			                            <strong class="label_item"><?php echo $row['label']; ?></strong>
			                        </div>
			                        <div class="col-md-4 info_field text-right">
			                            <span class="value_item"><?php echo $row['value']; ?> </span>
			                        </div>
			                    </div>
			                <?php endforeach; ?>
			            <?php endif; ?>
			        </div>
			    </div>
			</div>
			<div class="summary_note">
			    <div class="col-md-8">
			        <p>
			            <span>
			                *** For FAMILY PLAN, coverage for Spouse is equal to the Principal, children 18 years
			                old and below are covered up to 50% of the Medical Expenses benefit.
			                <br><br>
			                Please feel free to call us at tel. No. 811-1788 should there anything you wish to clarify.
			            </span>
			        </p>
			    </div>
			</div>

		</div>
		<div class="col-md-6 summary_section table_type_1">
			<div class="section_title title_1">
				<h5>Premium Computation</h5>
			</div>	
			<div class="col-md-12 no_gutter">
				<table class="table summary_table table_1">
					<colgroup>
						<col width="50%" />
						<col width="50%" />
					</colgroup>
					<thead>
						<tr>
							<th>Net Premium</th>
							<td><span>USD <?php echo number_format($premium->net_premium,2); ?></span></td>
						</tr>	
					</thead>
					<tbody>
						<tr>
							<th colspan="2">Plus:</th>
						</tr>
						<tr>
							<th>Documentary Stamps</th>
							<td><span><?php echo number_format($premium->dst,2); ?></span></td>
						</tr>
						<tr>
							<th>Premium Tax</th>
							<td><span><?php echo number_format($premium->premium_tax,2); ?></span></td>
						</tr>
						<tr>
							<th>Local Government Tax</th>
							<td><span><?php echo number_format($premium->lgt,2); ?></span></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>Total Premium</th>
							<td>
								<span>USD <?php echo number_format($premium->net_total_usd,2); ?></span>
								<br>
								<span>PHP <?php echo number_format($premium->net_total_php,2); ?></span>
							</td>
						</tr>
						<?php if ( $premium->for_delivery ): ?>
							<tr>
								<th>Delivery Charge</th>
								<td><span>PHP <?php echo number_format($premium->delivery_charge,2); ?></span></td>
							</tr>
							<tr>
								<th>Total Amount Due</th>
								<td><span>PHP <?php echo number_format($premium->total_amount,2); ?></span></td>
							</tr>
						<?php endif; ?>	
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>