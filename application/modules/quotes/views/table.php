<div class="col-md-12 dashtable  content_field" id="quote_list">
	<div class="top_head">
		<div class="checkbox">
			<label><input type="checkbox">Select All</label>
		</div>
	</div>
	<!-- TABLE HEADER -->
	<div class="table_head">
		<div class="col-md-7 col col_first"><strong>Client Name / Status</strong></div>
		<div class="col-md-1 col"><strong>Flag</strong></div>
		<div class="col-md-1 col"><strong>View</strong></div>
		<div class="col-md-1 col"><strong>Action</strong></div>
		<div class="col-md-2 col"><strong>Date</strong></div>
	</div>
	<!-- /TABLE HEADER -->
	<!-- TABLE CONTENT -->
	<div class="table_content">
		<?php if ( isset($quotes) AND $quotes['count'] > 0 AND is_array($quotes['data']) ): ?>
			<?php foreach ( $quotes['data'] AS $quote ): ?>
				<?php 
					$row = json_decode($quote->values);
					$client_name = ($quote->line == "itp" AND $row->insurance_cover == "company") ? $row->company_information->name : $row->personal_information->fname . ' ' . $row->personal_information->lname;
					$status = ucfirst(strtolower($quote->status));
					$date = date("F d, Y", strtotime($quote->time_stamp));
				?>
				<!-- ====== TABLE ITEM ======  -->
				<div class="table_item" data-quote-pk="<?php echo $quote->pk; ?>">
					<div class="col-md-7 col col_first">
						<div class="checkbox">
							<label><input type="checkbox"></label>
						</div>
						<div class="client">
							<div class="client_con">
								<p class="client_name"><?php echo ucwords($client_name); ?></p>
								<span data-property="quote_status" class="status active">
									<?php echo $status; ?>
									<?php if ( $status == 'Completed' ): ?>
										<?php if ( strtotime($quote->start_date) > strtotime(date('Y-m-d')) ): ?>
											 - Issued
										<?php elseif ( strtotime($quote->start_date) <= strtotime(date('Y-m-d')) AND strtotime($quote->end_date) >= strtotime(date('Y-m-d')) ): ?>
											 - Active
										<?php elseif ( strtotime($quote->end_date) < strtotime(date('Y-m-d')) ): ?>
											 - Expired
										<?php endif; ?>
									<?php endif; ?>
									<?php if ( $status == 'Disapproved' ): ?>
										- <?php echo $quote->remarks; ?>
									<?php endif; ?>
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-1 col">
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle tabletoggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<img src="<?php echo base_url("assets/images/icon_flag.png"); ?>" /><span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#">Flag</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-1 col">
						<?php if ($quote->line == "itp"): ?>
							<a href="<?php echo base_url("admin/transactions/detail/{$quote->pk}") ?>" class="btn_view"></a>
						<?php else: ?>
							<a href="#" class="btn_view"></a>
						<?php endif; ?>						
					</div>
					<div class="col-md-1 col">
						<?php if ($quote->status == STATUS_FOR_APPROVAL): ?>
							<!-- action -->
							<div class="btn-group action">
								<button type="button" class="btn btn-default dropdown-toggle tabletoggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<img src="<?php echo base_url("assets/images/icon_action.png"); ?>" /><span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" data-method="update_status" data-quote-pk="<?php echo $quote->pk; ?>" data-status="<?php echo STATUS_APPROVED; ?>">Approve</a></li>
									<li><a href="#" data-method="update_status" data-quote-pk="<?php echo $quote->pk; ?>" data-status="<?php echo STATUS_DISAPPROVED; ?>">Disapprove</a></li>
								</ul>
							</div>
							<!-- /action -->
						<?php endif; ?>
					</div>
					<div class="col-md-2 col">
						<!-- date -->
						<span class="tabledate"><?php echo $date; ?></span>
						<!-- /date -->
					</div>
				</div>
				<?php if ( isset($quote->documents) AND $quote->documents['count'] > 0 ): ?>
					<div class="nested_table">
						<!-- REPEATING NESTED -->
						<?php foreach ( $quote->documents['data'] AS $index => $document ): ?>
							<?php $number = $index + 1; ?>
							<div class="nested_box">
								<div class="table_item nested_item">
									<div class="col-md-9 col col_first">
										<div class="checkbox">
											<label><input type="checkbox"></label>
										</div>
										<div class="client">
											<div class="client_doc">
												Document <?php echo $number; ?>
											</div>
										</div>
									</div>
									<div class="col-md-1 col">
										<a target="_blank" href="<?php echo base_url("files/documents/{$document->file_name}"); ?>" class="btn_view"></a>
									</div>
									<div class="col-md-2 col">
										<!-- date -->
										<span class="tabledate"><?php echo date("F d, Y", strtotime($document->date)); ?></span>
										<!-- /date -->
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
<!-- Modal -->
<div id="disaprove_modal" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Disapprove Quote Request</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('', 'id="disapprove_quote" role="form" class="form-horizontal" data-validate', ["quote_pk" => ""]); ?>                 
                    <div class="col-sm-12">
                    	<textarea name="remarks" class="form-control" placeholder="Enter your remarks here" rows="3"></textarea>                
                    </div>
                    <div class="clearfix">&nbsp;</div>
                <?php echo form_close(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-button="disapprove_quote">Disapprove Quote Request</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e){

		function update_status(iQuotePk,sStatus,sRemarks)
		{
			var uiParent    = $("div.table_item[data-quote-pk="+iQuotePk+"]")

        		$.ajax({
	    			type : 'POST',
	    			url : baseUrl + "quotes/update_status/<?php echo $api_key; ?>",
	    			data : { quote_pk : iQuotePk, status : sStatus, remarks : sRemarks },
	    			beforeSend : function () {
	    				$(".p-loader").removeClass('hidden');
	    			},
	    			success : function (sData) {
	    				var oData = $.parseJSON(sData);

	    				if ( oData.status == true ) {
	    					uiParent.find(".action").remove();
	    					uiParent.find("[data-property=quote_status]").html(oData.new_status);
	    					if ( sRemarks ) {
	    						uiParent.find("[data-property=quote_status]").append(" - " + sRemarks);
	    					}
	    					$('#disaprove_modal').modal("hide");
	    					alert(oData.message);
	    				} else {
	    					alert(oData.message);
	    				}
	    			},
	    			complete : function () {
	    				$(".p-loader").addClass('hidden');
	    			}
	    		});
		}

		$("body").on("click",function(e){
        	var uiTarget = $(e.target);
        	if(uiTarget.closest("[data-method=update_status]").length > 0) {
        		e.preventDefault();
        		var uiThis 		= uiTarget.closest("[data-method=update_status]"),
        			iQuotePk 	= uiThis.data("quote-pk"),
    		 		sStatus		= uiThis.data("status");

        		if ( sStatus == 'APPROVED' ) {
        			update_status(iQuotePk, sStatus);
        		} else {
        			$("#disapprove_quote").find("[name=quote_pk]").val(iQuotePk);
        			$("#disaprove_modal").modal("show");
        		}
        	} else if (uiTarget.closest("[data-button=disapprove_quote]").length > 0) {
        		var sRemarks = $("textarea[name=remarks]").val();

        		if ( sRemarks.length == 0 ) {
        			alert("Remarks is required");
        		} else {
        			var iQuotePk = $("#disapprove_quote").find("[name=quote_pk]").val();
        			update_status(iQuotePk, 'DISAPPROVED', sRemarks);
        		}    		
        	}
        });

        $('#disaprove_modal').on('hide.bs.modal', function (e) {
		  	// do something...
		  	$("#disapprove_quote").find("[name=quote_pk]").val("");
		  	$("#disapprove_quote").find("[name=remarks]").val("");
		});
	});
</script>