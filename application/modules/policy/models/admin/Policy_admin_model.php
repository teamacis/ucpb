<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'modules/policy/classes/PolicyUtility.php';

class Policy_admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Fetch policies
     *
     * @param $filter
     */
    public function get_policies($line, $filters) {
        $offset = isset($_GET['per_page']) ? $_GET['per_page'] - 1 : 0;

        $suffix = PolicyUtility::get_suffix($line);

        $query = $this->db->select([
                              'policies.pk',
                              'policies.policy_id',
                              'policies.date_delivered',
                              'policies.date_created',
                              'motor_quotes.quote_id',
                              'personal_informations.fname',
                              'personal_informations.lname',
                              'motor_quotes_agents.fname as agent_fname',
                              'motor_quotes_agents.lname as agent_lname'
                          ])
                          ->where("policies.policy_id LIKE '$suffix-%'")
                          ->join('dragonpay_transactions', 'dragonpay_transactions.pk = policies.dragonpay_transaction_pk', 'left')
                          ->join('motor_quotes', 'motor_quotes.quote_id = dragonpay_transactions.transaction_id', 'left')
                          ->join('motor_quotes_agents', 'motor_quotes_agents.motor_quotes_pk = motor_quotes.pk', 'left')
                          ->join('users_quotes', 'users_quotes.quotes_pk = motor_quotes.pk', 'left')
                          ->join('personal_informations', 'personal_informations.users_quotes_pk = users_quotes.pk', 'left')
                          ->order_by('policies.pk DESC')
                          ->limit(TOTAL_ROWS)
                          ->offset($offset);

        if($filters['status'] == POLICY_DELIVERED) {
            $query = $this->db->where('date_delivered IS NOT NULL');
        }
        else if($filters['status'] == POLICY_NOT_DELIVERED) {
            $query = $this->db->where('date_delivered IS NULL');
        }

        $query = $this->db->get('policies');

        if($query->num_rows() > 0) {
            return $query->result();
        }
        else {
            return false;
        }
    }


    /**
     * Get total count of policies
     * @return mixed
     */
    public function total_rows($filters = []) {
        /**
         * Main Query
         */
        $query = $this->db->select('COUNT(*) as total_rows');

        /**
         * Filters Loop
         */
        if($filters['status'] == POLICY_DELIVERED) {
            $query = $this->db->where('date_delivered IS NOT NULL');
        }
        else if($filters['status'] == POLICY_NOT_DELIVERED) {
            $query = $this->db->where('date_delivered IS NULL');
        }

        /**
         * Return Results
         */
        $query = $query->get('policies');
        if($query->num_rows()) {
            $result = $query->row_object();
            return $result->total_rows;
        }

        return false;
    }


    public function update_delivery($dateDelivered, $policyId) {
        $this->db->where(['policy_id' => $policyId])
                 ->update('policies', ['date_delivered' => $dateDelivered]);
    }

}