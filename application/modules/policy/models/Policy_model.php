<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'modules/policy/classes/PolicyUtility.php';

class Policy_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Insert new Policy
     *
     * @param $fields
     */
    public function insert_policy($fields,$line='motor_insurance',$extra=NULL) {
        $totalLinePolicy = $this->get_total_policy_by_line($line);
        $totalLinePolicy++;

        $policyId = PolicyUtility::get_policy_id($totalLinePolicy, $line, $extra);

        $fields['policy_id'] = $policyId;

        if ($this->db->insert('policies', $fields)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Fetch the total number of policies in each line
     *
     * @param $line
     * @return mixed
     */
    public function get_total_policy_by_line($line) {
        $suffix = PolicyUtility::get_suffix($line);

        $query = $this->db->select('count(*) as total')
                          ->where("policy_id LIKE '$suffix-%'")
                          ->get('policies');

        $result = $query->row_array();
        return $result['total'];
    }

    public function get_policy_by_quote_id($pk) {
        return $this->db->select('p.policy_id,
                            p.is_delivered,
                            p.date_created,
                            p.date_delivered')
                ->where(array('dt.quotes_pk' => $pk))
                ->join('dragonpay_transactions as dt', 'dt.pk = p.dragonpay_transaction_pk')
                ->get('policies as p')
                ->row_array();
    }

    public function get_policy_by_motor_quote_id($pk) {
        return $this->db->select('p.policy_id,
                            p.is_delivered,
                            p.date_created,
                            p.date_delivered')
            ->where(array('dt.quotes_pk' => $pk))
            ->join('dragonpay_transactions as dt', 'dt.pk = p.dragonpay_transaction_pk')
            ->join('motor_saved_quotes as msq', "CONCAT('MTR-" . date('Y') . "-', msq.pk = p.dt.transaction_id")
            ->join('motor_quotes as mq', 'mq.pk = p.dragonpay_transaction_pk')
            ->get('policies as p')
            ->row_array();
    }

    public function get_policy($where=array()) {

        return $this->db->select("p.*")
                ->where($where)
                ->join('dragonpay_transactions dt','dt.pk = p.dragonpay_transaction_pk','left')
                ->get("policies p")->row();
    }

    public function get_many($where=array()) {

        return $this->db->select("p.*")
                ->where($where)
                ->join('dragonpay_transactions dt','dt.pk = p.dragonpay_transaction_pk','left')
                ->get("policies p")->result();
    }

    public function result($line="itp",$user_pk=0,$keywords=NULL,$limit=0,$offset=0)
    {
        $table = "policies p";
        $fields = "p.*";
        $join = [
            "INNER JOIN dragonpay_transactions t ON t.pk = p.dragonpay_transaction_pk",
        ];

        if ( $line == "itp" ) {
            $fields .=",q.status,c.net_total_php AS net_premium,c.departure_date,c.arrival_date,q.coverage_type";
            $join[] = "INNER JOIN itp_quotes q ON q.pk = t.quotes_pk";
            $join[] = "INNER JOIN itp_computation c ON c.itp_quote_pk = q.pk";
        } elseif ( $line == "motor" ) {
            
        } elseif ( $line == "fire" ) {
            
        }

        $where = [
            "p.user_pk = '{$user_pk}'",
            "t.line = '{$line}'"
        ];

        $options = [
            "fields"    => $fields,
            "join"      => $join,
            "where"     => $where,
            "limit"     => $limit,
            "offset"    => $offset
        ];

        return parent::get($table, $options);
    }

    public function row($user_pk=0,$policy_id=NULL)
    {
        $table = "policies p";
        $join = [
            "INNER JOIN dragonpay_transactions t ON t.pk = p.dragonpay_transaction_pk"
        ];

        $where = [
            "p.user_pk = '{$user_pk}'",
            "p.policy_id = '{$policy_id}'"
        ];

        $options = [
            "join"      => $join,
            "where"     => $where,
            "row"       => TRUE
        ];

        return parent::get($table, $options);
    }
}