<div class="new_row">
	<div class="col-md-6">
		<?php if ($itp_quote['insurance_cover'] == 'individual'): ?>
			<table class="table summary_table table_3">
				<thead>
					<tr>
						<th colspan="2">Personal Information</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><strong>Name:</strong><span class="value_2"><?php echo $personal_information->fname . ' ' . $personal_information->lname; ?></span></td>
					</tr>
				</tbody>
			</table>
		<?php else: ?>
			<table class="table summary_table table_3">
				<thead>
					<tr>
						<th colspan="2">Company Information</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><strong>Name:</strong><span class="value_2"><?php echo $personal_information->name; ?></span></td>
					</tr>
				</tbody>
			</table>
		<?php endif ; ?>
		<table class="table summary_table table_3">
			<thead>
				<tr>
					<th colspan="2">Contact Information</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($itp_quote['insurance_cover'] == 'company'): ?>
					<tr>
						<td><strong>Contact Person:</strong><span class="value_1"><?php echo $personal_information->contact_person; ?></span></td>
					</tr>
				<?php endif; ?>
				<tr>
					<td><strong>Telephone Number:</strong><span class="value_1"><?php echo $personal_information->telephone; ?></span></td>
				</tr>
				<tr>
					<td><strong>Mobile Number:</strong><span class="value_1"><?php echo $personal_information->mobile; ?></span></td>
				</tr>
				<tr>
					<td><strong>Email:</strong><span class="value_1"><?php echo $personal_information->email_address; ?></span></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-6">
		<table class="table summary_table table_3">
			<thead>
				<tr>
					<th colspan="4">Travel Information</th>
				</tr>
			</thead>
			<tbody>	
				<tr>
					<td colspan="2"><strong>Purpose of Travel:</strong><span class="value_2"><?php echo $travel_information->purpose_of_travel; ?></span></td>							
				</tr>
				<tr>
					<td colspan="2"><strong>Type of Coverage:</strong><span class="value_2"><?php echo $travel_information->coverage_type; ?></span></td>
				</tr>
				<tr>
					<td colspan="2"><strong>Start Date:</strong><span class="value_2"><?php echo date("m-d-Y",strtotime($premium->departure_date)); ?></span></td>
				</tr>
				<tr>
					<td colspan="2"><strong>End Date:</strong><span class="value_2"><?php echo date("m-d-Y",strtotime($premium->arrival_date)); ?>  (Both dates inclusive)</span></td>
				</tr>
			</tbody>
		</table>
		<table class="table summary_table table_3">
			<thead>
				<tr>
					<th colspan="4">Itineraries</th>
				</tr>
			</thead>
			<tbody>
				<?php if (isset($itineraries) AND is_array($itineraries) AND count($itineraries) > 0): ?>
					<?php foreach ($itineraries AS $itinerary): ?>
						<?php $itinerary = (object) $itinerary; ?>
						<tr>
							<td><strong>Destination:</strong><span class="value_2"><?php echo $itinerary->country; ?></span></td>
							<td><strong>Date:</strong><span class="value_2"><?php echo date("M d, Y",strtotime($itinerary->start_date)) . ' - ' . date("M d, Y",strtotime($itinerary->end_date)); ?></span></td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
		<?php /*if (isset($members) AND is_array($members) AND count($members) > 0): ?>
			<table class="table summary_table table_3">
				<thead>
					<tr>
						<th colspan="4">Member Information</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($members AS $member): ?>
						<tr>
							<td><strong>Name:</strong><span class="value_1"><?php echo $member->first_name . ' ' . $member->last_name; ?></span></td>
							<td><strong>Age:</strong><span class="value_1"><?php echo calculate_age($member->birth_date); ?></span></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif;*/ ?>
	</div>
</div>
<div class="new_row row_pad">
	<div class="col-md-6 summary_section table_type_2">
		<div class="section_title title_1">
			<h5>Coverage</h5>
		</div>	
		<?php $package_type == "Silver" ? $this->load->view('travel_insurance/confirmation/coverage/silver') : ''; ?>
		<?php $package_type == "Gold" ? $this->load->view('travel_insurance/confirmation/coverage/gold') : ''; ?>
		<?php $package_type == "Platinum" ? $this->load->view('travel_insurance/confirmation/coverage/platinum') : ''; ?>
	</div>
	<div class="col-md-6 summary_section table_type_1">
		<div class="section_title title_1">
			<h5>Premium Computation</h5>
		</div>	
		<?php $this->load->view('travel_insurance/confirmation/premium_computation'); ?>

		<div class="col-md-12 no_gutter">
			<h4 style="margin-left:10px">Documents</h4>
			<ol>
				<li><a target="_blank" href="<?php echo base_url("files/policies/policy-schedule-{$policy_id}.pdf"); ?>"><i class="fa fa-file-pdf-o text-warning"></i> Policy Schdule</a></li>
				<li><a target="_blank" href="<?php echo base_url("files/formal_quotes/{$transaction_id}.pdf"); ?>"><i class="fa fa-file-pdf-o text-warning"></i> Formal Quote</a></li>
				<li><a target="_blank" href="<?php echo base_url("files/itp_quote/application/application-{$transaction_id}.pdf"); ?>"><i class="fa fa-file-pdf-o text-warning"></i> Application Form</a></li>
				<?php if ($premium->country_type == 3): ?>
					<li><a target="_blank" href="<?php echo base_url("files/confirmation_cover/confirmation_cover-{$transaction_id}.pdf"); ?>"><i class="fa fa-file-pdf-o text-warning"></i> Confirmation Cover</a></li>
				<?php endif; ?>
			</ol>
		</div>
	</div>
</div>