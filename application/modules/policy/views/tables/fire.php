<div class="search_tab">
	<button class="searchtab_btn glyphicon glyphicon-search"></button>
	<input type="text" class="search_input" placeholder="Search Keywords" />
</div>
<table class="table table-striped">
	<thead>
		<tr>
			<th></th>
			<th><a href="#">Policy ID</a></th>
			<th><a href="#">Status <span class="sort_icon glyphicon glyphicon-triangle-bottom"></span></a></th>
			<th><a href="#">Coverage Type <span class="sort_icon glyphicon glyphicon-triangle-bottom"></span></a></th>
			<th><a href="#">Net Premium</a></th>
			<th><a href="#">Policy Period <span class="sort_icon glyphicon glyphicon-triangle-top"></span></a></th>
		</tr>
	</thead>
	<tbody>
		<?php if (isset($policies) AND $policies['count'] > 0 AND is_array($policies['data'])): ?>
			<?php foreach ($policies['data'] AS $index => $policy): ?>
				<?php $number = $index + 1; ?>
				<tr>
					<td><span class="order"><?php echo $number; ?></span></td>
					<td><a href="#" class="policy_id"><?php echo $policy->policy_id; ?></a></td>
					<td><span class="policy_status active"><?php echo $policy->status; ?></span></td>
					<td><span class="insure_type"><?php echo $policy->coverage_type; ?></span></td>
					<td><span class="net_premium">Php <?php echo $policy->net_premium; ?></span></td>
					<td><span class="policy_period"><?php echo date("m/d/y",strtotime($policy->departure_date)); ?> - <?php echo date("m/d/y",strtotime($policy->arrival_date)); ?></span></td>
				</tr>
			<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="6" align="center">No record found</td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>