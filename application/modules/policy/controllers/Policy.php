<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Policy extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('policy_model');
	}

	public function index()
	{
		
	}

	public function itp($api_key=NULL,$user_pk=0,$keywords=NULL,$limit=0,$offset=0)
	{
		if ( $api_key == API_KEY ) {
			$policies = $this->policy_model->result("itp",$user_pk, $keywords,$limit,$offset);
			return $this->load->view("tables/itp",["policies" => $policies], TRUE);
		}
	}

	public function motor($api_key=NULL,$user_pk=0,$keywords=NULL,$limit=0,$offset=0)
	{
		if ( $api_key == API_KEY ) {
			$policies = $this->policy_model->result("motor",$user_pk, $keywords,$limit,$offset);
			return $this->load->view("tables/motor",["policies" => $policies], TRUE);
		}
	}

	public function fire($api_key=NULL,$user_pk=0,$keywords=NULL,$limit=0,$offset=0)
	{
		if ( $api_key == API_KEY ) {
			$policies = $this->policy_model->result("fire",$user_pk, $keywords,$limit,$offset);
			return $this->load->view("tables/fire",["policies" => $policies], TRUE);
		}
	}

	public function detail($api_key=NULL,$user_pk=0,$policy_id=NULL)
	{
		if ( $api_key == API_KEY ) {
			$policy = $this->policy_model->row($user_pk, $policy_id);
			
			if ( $policy->line == 'itp' ) {
				$itp_quote_pk = $policy->quotes_pk;

				$this->load->model('travel_insurance/itp_quote_model');
				$quote 					= $this->itp_quote_model->get_by_fields(["itp_quotes.pk" => $itp_quote_pk]);
				$policy->status 		= $quote->status;
				
				$itp_quote              = $this->itp_quote_model->get_saved_quotes($quote->token);
            	$itp_quote_request      = json_decode($itp_quote->values,TRUE);
           		$personal_information   = $itp_quote_request['personal_information'];

           		$user_type              = $itp_quote_request['travel_information']['coverage_type'];
		        $members                = isset($itp_quote_request[strtolower($user_type) . '_member']) ? $itp_quote_request[strtolower($user_type) . '_member'] : array();
		        $package_type           = $itp_quote_request[strtolower($user_type).'_package']['same'];
		        $itineraries            = $itp_quote_request[strtolower($user_type).'_itinerary']['same'];
		        $travel_information     = $itp_quote_request['travel_information'];

		        $this->load->model('travel_insurance/itp_country_model');

		        foreach ($itineraries AS $index => $itinerary) {
		             $itineraries[$index]['country'] = $this->itp_country_model->get_countries($itinerary['destination']);
		        }

		        $this->load->library("travel_insurance/itp_premium", ["itp_quote_pk" => $itp_quote->pk]);

		        $data = [
		        	'itp_quote'				=> $itp_quote_request,
		         	'personal_information'	=> (object) $personal_information,
		         	'premium'     			=> $this->itp_premium->compute(),
		            'package_type'          => $package_type,
		         	'members'     			=> $members,
		         	'itineraries' 			=> $itineraries,
		            'travel_information'    => (object) $travel_information,
		            'policy_id'				=> $policy_id,
		            'transaction_id'		=> $policy->transaction_id
		        ];

				$detail = $this->load->view("details/itp", $data, TRUE);
			}
			
			return ["detail" => $detail, "policy" => $policy];
		}
	}
}

/* End of file Policy.php */
/* Location: ./application/modules/policy/controllers/Policy.php */