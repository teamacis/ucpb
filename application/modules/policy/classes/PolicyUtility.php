<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PolicyUtility {

    public function __construct() {
        $this->ci =& get_instance();
    }

    /**
     * Generates the policy id
     *
     * @param $totalLinePolicy
     * @param $line
     * @return string
     */
    public static function get_policy_id($totalLinePolicy, $line, $extra=NULL) {

        $str_array = array();

        $str_array[] = self::get_suffix($line);

        if ( $line == 'itp' ) {
            $coverage = [
                "Individual"    => "I",
                "Family"        => "F",
                "Group"         => "G"
            ];

            if ( isset($coverage[$extra]) ) {
                $str_array[] = $coverage[$extra] . "TP";
            }

        } elseif ($line == 'motor_insurance') {
            $str_array[] = 'MNP';
        }

        $str_array[] = 'HO';
        $str_array[] = date('y');
        $str_array[] = str_pad($totalLinePolicy, 6, "0", STR_PAD_LEFT);
        $str_array[] = '01';

        return implode('-', $str_array);
    }

    /**
     * Returns the suffix of the line for policy
     *
     * @param $line
     * @return string
     */
    public static function get_suffix($line) {
        if($line == 'motor_insurance') {
            return 'MC';
        } elseif ($line == 'itp') {
            return 'PA';
        }
    }

}