<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_information_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	 public function save($fields, $quote_pk) {
        $insert = [];

        /**
         * Fix Generic Fields
         */
        $requiredFields = ['name', 'contact_person', 'incoporation_date', 'industry_type', 'house_number', 'telephone', 'mobile','province','city','barangay'];
        foreach($requiredFields as $f) {
            if(isset($fields[$f])) {
                $insert[$f] = $fields[$f];
            }
        }

        $query = $this->db->select('pk')->where('quotes_pk', $quote_pk)->get('users_quotes');
        $insert['users_pk'] = $query->row_object()->pk;

        $this->db->insert('company_informations', $insert);
    }
}

/* End of file Company_information_model.php */
/* Location: ./application/modules/company/models/Company_information_model.php */