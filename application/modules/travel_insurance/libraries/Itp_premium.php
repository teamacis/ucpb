<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_premium
{
	protected $ci;
	private $itp_quote_pk;
	private $itineraries;
	private $personal_information;
	private $members;
	private $number_of_days;
	private $max_consecutive_days;
	private $coverage_type;
	private $country_type;
	private $premium_type;
	private $net_premium;
	private $principal_age;
	private $dst;
	private $lgt;
	private $premium_tax;
	private $discount;
	private $delivery_charge;
	private $for_delivery;

	public function __construct( $config=array() )
	{
		$this->ci =& get_instance();

		if ( isset($config['itp_quote_pk']) ) {

			$this->itp_quote_pk = $config['itp_quote_pk'];
			$this->initialize();

		} else {
			exit('Invalid ITP Quote PK');
		}
	}

	private function initialize()
	{
		$this->ci->load->model('travel_insurance/itp_configuration_model');
		$this->ci->load->model('travel_insurance/itp_quote_model');

		$quote = $this->ci->itp_quote_model->get_saved_quotes_pk($this->itp_quote_pk);

		if ( ! $quote ) {
			exit('Invalid Quote');
		}

		$request 				= json_decode($quote->values,TRUE);
		$personal_information	= $request['personal_information'];
		
		$this->coverage_type 	= $request['travel_information']['coverage_type'];
		$itp_tineraries 		= isset($request[strtolower($this->coverage_type)."_itinerary"]) ? $request[strtolower($this->coverage_type)."_itinerary"] : NULL;

		if ( is_null($itp_tineraries) ) {
			exit('No itineraries');
		}

		$user_type 					= $request[strtolower($this->coverage_type) . '_package'];
		$this->personal_information = $personal_information;
		$this->itineraries    		= !isset($itp_tineraries['is_same']) || $itp_tineraries['is_same'] == 'yes' ? array_values($itp_tineraries['same']) : array_values($itp_tineraries['different']);
		$this->members 				= isset($request[strtolower($this->coverage_type) . '_member']) ? $request[strtolower($this->coverage_type) . '_member'] : array();
		$this->departure_date 		= $this->_get_departure_date();
		$this->arrival_date 		= $this->_get_arrival_date();
		$this->number_of_days 		= $this->_compute_number_of_days();
		$this->max_consecutive_days = $this->_compute_max_consecutive_days();
		$this->country_type 		= $this->_get_country_type();
		if ( $this->country_type == 3) {
			$this->premium_type			= 'Platinum';
		} else {
			$this->premium_type			= isset($user_type ['same']) ? $user_type ['same'] : $user_type ['different'];
		}
		
		$this->principal_age		= $this->_calculate_age(isset($request['personal_information']['birth_date']) ? $request['personal_information']['birth_date'] : $request['personal_information']['birthdate'], $this->departure_date);
		$this->net_premium 			= $this->_get_net_premium();
		$this->dst 					= $this->_get_dst();
		$this->lgt 					= $this->_get_lgt();
		$this->premium_tax 			= $this->_get_premium_tax();
		$this->delivery_charge 		= $this->_get_delivery_charge();
	}

	private function _compute_number_of_days()
	{

		if ( is_array( $this->itineraries ) ) {
			$start_dates    = array_column($this->itineraries, "start_date");
	        $end_dates      = array_column($this->itineraries, "end_date");

	        sort($start_dates);
	        rsort($end_dates);

	        $start_date = isset($start_dates[0]) ? $start_dates[0] : FALSE;
	        $end_date   = isset($end_dates[0]) ? $end_dates[0] : FALSE;

			if ( $start_date AND $end_date ) {

				$date1 = new DateTime($start_date);
		        $date2 = new DateTime($end_date);
		        $interval = $date1->diff($date2);
		        
		        $number_of_days = $interval->format('%a') + 1;
		       
		        return $number_of_days;
			}
		}		
		
		return FALSE;
	}

	private function _get_departure_date()
	{
		if ( is_array( $this->itineraries ) ) {
			$start_dates    = array_column($this->itineraries, "start_date");
			sort($start_dates);

			return isset($start_dates[0]) ? $start_dates[0] : FALSE;
		}

		return FALSE;
	}

	private function _get_arrival_date()
	{
		if ( is_array( $this->itineraries ) ) {
			$end_dates    = array_column($this->itineraries, "end_date");
			rsort($end_dates);

			return isset($end_dates[0]) ? $end_dates[0] : FALSE;
		}

		return FALSE;
	}

	private function _compute_max_consecutive_days()
	{
		if ( is_array( $this->itineraries ) ) {	
			$number_of_consecutive_days_array = array();
            $number_of_consecutive_days = 1;
            $end_date = false;

            foreach ($this->itineraries AS $itineary) {
                $num_days_from_last = 0;

                $date_start = new DateTime($itineary['start_date']);
                $date_end 	= new DateTime($itineary['end_date']);
                $interval 	= $date_start->diff($date_end);

                if ($end_date) {
                    // get the interval of from the last itinerary end date
                    $date_end_from_last = new DateTime($end_date);
                    $interval_from_last = $date_end_from_last->diff($date_start);
                    $num_days_from_last = $interval_from_last->format('%a');
                }

                if ($num_days_from_last == 0) {
                    $number_of_consecutive_days += $interval->format('%a');
                } else {
                    $number_of_consecutive_days = $interval->format('%a');
                }

                $end_date = $itineary['end_date'];

                $number_of_consecutive_days_array[] = $number_of_consecutive_days;
            }

            return max($number_of_consecutive_days_array);
		}
	}

	private function _get_country_type()
	{
        if ( is_array($this->itineraries) ) {
        	$destinations = array_column($this->itineraries, "destination");
        	$countries = array();
        	foreach ($destinations AS $d) {
        		$countries = array_merge($countries,explode(",", ltrim($d,',')));
        	}
        
        	$this->ci->load->model('travel_insurance/itp_country_model');
        	return $this->ci->itp_country_model->get_highest_type_by_ids($countries);
        }
        
        return FALSE;
	}

	private function _get_net_premium()
	{
		$net_premium = 0;

		$this->ci->load->model('travel_insurance/itp_premium_model');

		$user_type 		= $this->coverage_type == COVERAGE_FAMILY ? COVERAGE_FAMILY : COVERAGE_INDIVIDUAL;

		$where = [
            'user_type'             => $user_type,
            'days >= '              => $this->number_of_days,
            'premium_type'          => $this->premium_type,
            'itp_country_type_pk'   => $this->country_type
        ];

		if ($this->number_of_days >= 180 AND $this->number_of_days < 365) {
            if ($this->number_of_consecutive_days > 90 AND $this->number_of_consecutive_days <= 180) {
                $where["is_max"] = 1;
            } elseif ($this->number_of_consecutive_days <= 90) {
                $where["is_max"] = 0;
            }
        } elseif ($this->number_of_days >= 365) {
            if ($this->number_of_consecutive_days > 180 AND $this->number_of_consecutive_days <= 365) {
                $where["is_max"] = 1;
            } elseif ($this->number_of_consecutive_days <= 180) {
                $where["is_max"] = 0;
            }
        }
       
        $premium  = $this->ci->itp_premium_model->get_by_fields($where); 

        if ($premium) {
        	if ( $this->coverage_type == COVERAGE_INDIVIDUAL ) {

        		$net_premium = $this->principal_age >= AGE_DOUBLE_AMOUNT ? (floatval($premium['ugic']) * 2) : floatval($premium['ugic']);
        	
        	} elseif ( $this->coverage_type == COVERAGE_FAMILY ) {
        		
        		$net_premium = $this->_count_old_members() > 0 ? (floatval($premium['ugic']) * 2) : floatval($premium['ugic']);

        	} elseif ( $this->coverage_type == COVERAGE_GROUP ) {
        		
        		$old_members 	= $this->_count_old_members();
        		$added_amount 	= $old_members > 0 ? (floatval($premium['ugic']) * $old_members) : 0;
        		$net_premium 	= $added_amount > 0 ? ((floatval($premium['ugic']) + $added_amount) * $this->_count_total_members()) : (floatval($premium['ugic']) * $this->_count_total_members());

        	}
            
            return $net_premium;
        }

        return FALSE;
	}

	private function _count_total_members()
	{
		$total = 1;

		if ( $this->members AND is_array($this->members) ) {
			$total += count($this->members);
		}

		return $total;
	}

	private function _calculate_age( $birth_date=NULL, $end_date=NULL )
	{
		if ( ! is_null($birth_date) ) {
			$start = new DateTime($birth_date);
	        $end   = !is_null($end_date) ? new DateTime($end_date) : new DateTime();
	        $interval = $end->diff($start);
	        return $interval->y;
		}

		return FALSE;
	}

	private function _count_old_members()
	{
		if ( is_array($this->members) ) {
			$total = 0;

			if ( $this->principal_age >= AGE_DOUBLE_AMOUNT ) {
				$total += 1;
			}

			foreach ($this->members AS $member) {
                $member_age = calculate_age($member['birth_date'], $this->departure_date);

                if ( $member_age >= AGE_DOUBLE_AMOUNT ) {	
                	$total += 1;
                }
            }
		}

		return FALSE;
	}

	private function _get_dst()
	{
		$this->ci->load->config('premium');
		return $this->ci->config->item('dst')[strtolower($this->coverage_type)][strtolower($this->premium_type)];
	}

	private function _get_lgt()
	{
		$defaut_lgt_rate = $this->ci->itp_configuration_model->get('default_lgt_rate');

		if ( $this->personal_information 
			AND (
				isset($this->personal_information['agent_code']) 
				OR (
					isset($this->personal_information['agent_fname']) 
					AND isset($this->personal_information['agent_lname'])
				)
				OR isset($this->personal_information['agency_name'])
			) 
		) {
			$this->ci->load->model('agent/agent_model');

			if ( $this->personal_information['agent_type'] == 'Agent Code' ) {
				$agent = $this->ci->agent_model->fetch_by_field('code', $this->personal_information['agent_code']);
			} elseif ( $this->personal_information['agent_type'] == 'Agent Name' ) {
				$agent = $this->ci->agent_model->fetch_by_name($this->personal_information['agent_fname'], $this->personal_information['agent_lname']);
			} elseif ( $this->personal_information['agent_type'] == 'Agency Name' ) {
				$agent = $this->ci->agent_model->fetch_by_field('agencies.name', $this->personal_information['agency_name']);
			}
			
			if ( isset($agent) ) {
				$this->ci->load->model('rate/rate_model');
				$rate = $this->ci->rate_model->get_rate_by_rate_location_pk($agent['rate_location_pk']);

				return $this->net_premium * ($rate ? $rate->rate : $defaut_lgt_rate);
			}
		}

		return $this->net_premium * $defaut_lgt_rate;;
	}

	private function _get_premium_tax()
	{
        $premium_tax = $this->ci->itp_configuration_model->get('premium_tax');
        return $this->net_premium * $premium_tax;
	}

	private function _get_discount()
	{
		$discount = 0;

		if ( $this->_count_total_members() >= ITP_TOTAL_MEMBERS_WITH_DISCOUNT ) {
			$discount += $this->net_premium * ITP_DISCOUNT;
		}

		return $discount;
	}

	private function _get_delivery_charge()
	{
		return $this->ci->itp_configuration_model->get('delivery_charge');
	}

	private function _get_peso_value()
	{
       	return $this->ci->itp_configuration_model->get('peso_value');
	}

	private function _is_for_delivery()
	{
		if ( isset($this->personal_information['delivery_policy']) AND $this->personal_information['delivery_policy'] == 'on' ) {
			return TRUE;
		}

		return FALSE;
	}

	public function compute()
	{
		return new Rate([
			"net_premium" 	=> $this->net_premium,
			"dst"			=> $this->dst,
			"lgt"			=> $this->lgt,
			"premium_tax"	=> $this->premium_tax,
			"discount"		=> $this->_get_discount(),
			"peso_value"	=> $this->_get_peso_value(),
			"number_of_days" => $this->number_of_days,
			"delivery_charge" => $this->delivery_charge,
			"for_delivery"	=> $this->_is_for_delivery(),
			"departure_date" => $this->departure_date,
			"arrival_date"	=> $this->arrival_date,
			"country_type"	=> $this->country_type
		]);	
	}
}

class Rate {

	public $net_premium = 0;
	public $net_premium_php = 0;
	public $dst = 0;
	public $lgt = 0;
	public $premium_tax = 0;
	public $net_total_usd = 0;
	public $net_total_php = 0;
	public $discount = 0;
	public $peso_value = 0;
	public $number_of_days = 0;
	public $delivery_charge = 0;
	public $total_amount = 0;
	public $for_delivery;
	public $departure_date;
	public $arrival_date;
	public $country_type;

	public function __construct($data=array()){
		foreach ($data AS $index => $value) {
			$this->$index = $value;
		}

		$this->get_total_amount_due();
	}

	private function get_total_amount_due()
	{
		$this->net_premium_php  = $this->net_premium * $this->peso_value;
		$this->net_total_usd 	= array_sum([($this->net_premium - $this->discount), $this->dst, $this->lgt, $this->premium_tax]);
		$this->net_total_php 	= $this->net_total_usd * ($this->peso_value + 1);
		$this->total_amount 	= $this->for_delivery ? $this->net_total_php + $this->delivery_charge : $this->net_total_php;
	}
}

/* End of file Itp_rate.php */
/* Location: ./application/modules/travel-insurance/libraries/Itp_rate.php */
