<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/Pdf.php';

class Itp_service
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	/**
	 * pdf_formal_quote function
	 *
	 * Generate itp formal quote pdf
	 *
	 * @return void
	**/
	public function pdf_formal_quote ($token=NULL, $generate_file=FALSE)
	{
		if ( ! is_null($token) ) {
			// get itp quote
			$this->ci->load->model('travel_insurance/itp_quote_model');
			$saved_quote =  $this->ci->itp_quote_model->get_saved_quotes($token);

			if ( $saved_quote ) {
				$itp_quote_request 	= json_decode($saved_quote->values,TRUE);
				$user_type      	= $itp_quote_request['travel_information']['coverage_type'];
				$package_type   	= strtolower($itp_quote_request[strtolower($user_type).'_package']['same']);
				$itineraries    	= $itp_quote_request[strtolower($user_type).'_itinerary']['same'];
				$itp_quote_request  = array_merge($itp_quote_request, $this->_get_itineraries_info($itineraries));
				$itp_quote_request['plan_type']		= ucfirst($package_type);

				$this->ci->load->library("travel_insurance/itp_premium", ["itp_quote_pk" => $saved_quote->pk]);
				$itp_quote_request['premium'] = $this->ci->itp_premium->compute();
				//$itp_quote_request['table_coverage'] = $this->ci->load->view("travel_insurance/pdf/coverage/{$package_type}", [], TRUE);

				//$page_one = $this->ci->load->view("travel_insurance/pdf/formal_quote/{$package_type}/page_one", $itp_quote_request , TRUE);
				//$page_two = $this->ci->load->view("travel_insurance/pdf/formal_quote/{$package_type}/page_two", $itp_quote_request , TRUE);
				$this->ci->load->config("coverage");
				$itp_quote_request['coverages'] = $this->ci->config->item($package_type);

				$page = $this->ci->load->view('travel_insurance/pdf/formal_quote', $itp_quote_request, TRUE);

				$pdf = new PDF();

		        // Generate Pages
		        $pdf->generate_pages([
		            $page
		        ]);

		        $txnid = $this->ci->itp_quote_model->generate_transaction_id($saved_quote->pk);

		        $name = $txnid . '.pdf';
		        $path = FORMAL_QUOTES_FOLDER . $name;

		        // Generate the PDF
		        $pdf->create($path, $generate_file);
				// generate pdf
				// download pdf
			}
		}

		return FALSE;
	}

	public function pdf_policy ($itp_quote_pk=NULL) {

		$this->ci->load->model('travel_insurance/itp_quote_model');
		$itp_quote 		= $this->ci->itp_quote_model->get_by_fields(["itp_quotes.pk" => $itp_quote_pk]);

		$this->ci->load->model('travel_insurance/itp_itinerary_model');
		$itineraries 	= $this->ci->itp_itinerary_model->get(["itp_quote_pk" => $itp_quote_pk], TRUE);
		$itinerary_info = $this->_get_itineraries_info($itineraries);

		$this->ci->load->model('travel_insurance/itp_personal_information_model');
		$personal_information = $this->ci->itp_personal_information_model->fetch_by_quote_pk($itp_quote_pk);

		$this->ci->load->model('travel_insurance/itp_member_model');
		$members = $this->ci->itp_member_model->get(["itp_quote_pk" => $itp_quote_pk]);

		$this->ci->load->model('travel_insurance/itp_computation_model');
		$computation = $this->ci->itp_computation_model->get($itp_quote_pk);

		$this->ci->load->model('travel_insurance/itp_member_model');
		$number_of_persons = $this->ci->itp_member_model->number_of_members($itp_quote_pk);

		$this->ci->load->model('travel_insurance/itp_beneficiary_model');
		$beneficiaries = $this->ci->itp_beneficiary_model->get_by_fields(["itp_beneficiaries.itp_quote_pk" => $itp_quote_pk]);

		$this->ci->load->config("coverage");
		$coverage = $this->ci->config->item(strtolower($itp_quote->package));

		$this->ci->load->model('policy/policy_model');
        $policy = $this->ci->policy_model->get_policy(["dt.line" => "itp", "quotes_pk" => $itp_quote->pk]);

		$policy_data = [
			"itinerary_info"	=> $itinerary_info,
			"itp_quote"			=> $itp_quote,
			"personal_info"		=> $personal_information,
			"computation"		=> $computation,
			"number_of_persons" => $number_of_persons,
			"beneficiaries"		=> $beneficiaries,
			"coverage"			=> $coverage,
			"members"			=> $members,
			"policy_id"			=> $policy->policy_id
		];

		$policy_view = $this->ci->load->view("travel_insurance/pdf/policy/page_one", $policy_data , TRUE);

		$pdf = new PDF();

        // Generate Pages
        $pdf->generate_pages([
            $policy_view
        ]);

        log_message('debug', 'Quote PK ' . $itp_quote->pk);

        $name = "policy-schedule-" . $policy->policy_id . '.pdf';
        $path = POLICIES_FOLDER . $name;

        // Generate the PDF
        $pdf->create($path, TRUE);
	}

	/* Get Destination, Departure date and arrival */
	private function _get_itineraries_info($itineraries=array()) {
		if (is_array($itineraries) AND count($itineraries) > 0) {

			$destinations 			= array_column($itineraries,"destination");
			$return_destinations	= [];
			if (is_array($destinations) AND count($destinations) > 0) {
				$this->ci->load->model('travel_insurance/itp_country_model');
				foreach ($destinations AS $country_id) {
					$return_destinations[] = trim($this->ci->itp_country_model->get_countries($country_id));
				}
			}

			$start_dates = array_column($itineraries, "start_date");
            $end_dates =  array_column($itineraries, "end_date");

            sort($start_dates);
            rsort($end_dates);

            $start_date = isset($start_dates[0]) ? $start_dates[0] : FALSE;
            $end_date   = isset($end_dates[0]) ? $end_dates[0] : FALSE;
      
            return [
            	'destinations' 		=> implode(",", $return_destinations),
            	'departure_date'	=> date("F d, Y",strtotime($start_date)),
            	'arrival_date'		=> date("F d, Y",strtotime($end_date))
            ];
		}	

		return FALSE;
	}

	public function pdf_invoice($itp_quote_pk=NULL)
	{
		$this->ci->load->model('travel_insurance/itp_quote_model');
		$itp_quote 		= $this->ci->itp_quote_model->get_by_fields(["itp_quotes.pk" => $itp_quote_pk]);

		$this->ci->load->model('travel_insurance/itp_personal_information_model');
		$personal_information = $this->ci->itp_personal_information_model->fetch_by_quote_pk($itp_quote_pk);

		$this->ci->load->model('travel_insurance/itp_itinerary_model');
		$itineraries 	= $this->ci->itp_itinerary_model->get(["itp_quote_pk" => $itp_quote_pk], TRUE);
		$itinerary_info = $this->_get_itineraries_info($itineraries);

		$this->ci->load->model('travel_insurance/itp_computation_model');
		$computation = $this->ci->itp_computation_model->get($itp_quote_pk);

		$this->ci->load->model('policy/policy_model');
        $policy = $this->ci->policy_model->get_policy(["dt.line" => "itp", "quotes_pk" => $itp_quote->pk]);

		$data = [
			"itp_quote"			=> $itp_quote,
			"personal_info"		=> $personal_information,
			"computation"		=> $computation,
			"itinerary_info"	=> $itinerary_info,
			"policy_id"			=> $policy->policy_id,
			"invoice_number"	=> $this->ci->itp_quote_model->generate_invoice_number($policy->pk)
		];

		$invoice = $this->ci->load->view("travel_insurance/pdf/invoice", $data, TRUE);

		$pdf = new PDF();

        // Generate Pages
        $pdf->generate_pages([
            $invoice
        ]);

        $this->ci->load->model("transaction/transaction_model");
        $transaction = $this->ci->transaction_model->get(["line" => "itp", "quotes_pk" => $itp_quote->pk]);

        log_message('debug', 'Quote PK ' . $itp_quote->pk);

        $name = "invoice-" . $transaction->transaction_id . '.pdf';
        $path = ITP_QUOTE_FILES_INVOICE . $name;

        // Generate the PDF
        $pdf->create($path, TRUE);
	}

	public function pdf_confirmation_cover($itp_quote_pk=NULL) {

		$this->ci->load->model('travel_insurance/itp_quote_model');
		$itp_quote 		= $this->ci->itp_quote_model->get_by_fields(["itp_quotes.pk" => $itp_quote_pk]);

		$this->ci->load->model('travel_insurance/itp_personal_information_model');
		$personal_information = $this->ci->itp_personal_information_model->fetch_by_quote_pk($itp_quote_pk);

		$this->ci->load->model('travel_insurance/itp_itinerary_model');
		$itineraries 	= $this->ci->itp_itinerary_model->get(["itp_quote_pk" => $itp_quote_pk], TRUE);
		$itinerary_info = $this->_get_itineraries_info($itineraries);

		$this->ci->load->config("coverage");
		$coverage = $this->ci->config->item(strtolower($itp_quote->package));

		$this->ci->load->model('policy/policy_model');
        $policy = $this->ci->policy_model->get_policy(["dt.line" => "itp", "quotes_pk" => $itp_quote->pk]);
	
		$data = [
			"itp_quote"			=> $itp_quote,
			"personal_info"		=> $personal_information,
			"coverage"			=> $coverage,
			"itinerary_info"	=> $itinerary_info,
			"policy_id"			=> $policy->policy_id,
		];

		$cover = $this->ci->load->view("travel_insurance/pdf/confirmation_cover", $data, TRUE);

		$pdf = new PDF();

        // Generate Pages
        $pdf->generate_pages([
            $cover
        ]);

        $this->ci->load->model("transaction/transaction_model");
        $transaction = $this->ci->transaction_model->get(["line" => "itp", "quotes_pk" => $itp_quote->pk]);

        $name = "confirmation-cover-" . $transaction->transaction_id . '.pdf';
        $path = ITP_QUOTE_FILES_COVER . $name;

        // Generate the PDF
        $pdf->create($path, TRUE);
	}

	public function pdf_application_form($itp_quote_pk=NULL)
	{
		$this->ci->load->model('travel_insurance/itp_quote_model');
		$itp_quote 		= $this->ci->itp_quote_model->get_by_fields(["itp_quotes.pk" => $itp_quote_pk]);

		$saved_quote 			= $this->ci->itp_quote_model->get_saved_quotes($itp_quote->token);
		$request 				= json_decode($saved_quote->values);
		$personal_info 			= $request->personal_information;

		$this->ci->load->model('travel_insurance/itp_personal_information_model');
		$personal_information = $this->ci->itp_personal_information_model->fetch_by_quote_pk($itp_quote_pk);

		$this->ci->load->model('travel_insurance/itp_itinerary_model');
		$itineraries 	= $this->ci->itp_itinerary_model->get(["itp_quote_pk" => $itp_quote_pk], TRUE);

		$this->ci->load->model('travel_insurance/itp_emergency_contact_model');
		$emergency_contacts		= $this->ci->itp_emergency_contact_model->get_by_fields(["itp_emergency_contacts.itp_quote_pk" => $itp_quote_pk]);

		$this->ci->load->model('travel_insurance/itp_beneficiary_model');
		$beneficiaries			= $this->ci->itp_beneficiary_model->get_by_fields(["itp_beneficiaries.itp_quote_pk" => $itp_quote_pk], TRUE);

		$data = [
			"request"				=> $request,
			"personal_info"			=> $personal_info,
			"personal_information" 	=> $personal_information,
			"itineraries"			=> $itineraries,
			"emergency_contacts"	=> $emergency_contacts,
			"beneficiaries"			=> $beneficiaries
		];

		$application = $this->ci->load->view("travel_insurance/pdf/application_form", $data, TRUE);

		$pdf = new PDF();

        // Generate Pages
        $pdf->generate_pages([
            $application
        ]);

        $this->ci->load->model("transaction/transaction_model");
        $transaction = $this->ci->transaction_model->get(["line" => "itp", "quotes_pk" => $itp_quote->pk]);

        $name = "application-" . $transaction->transaction_id . '.pdf';
        $path = ITP_QUOTE_FILES_APPLICATION . $name;

        // Generate the PDF
        $pdf->create($path, TRUE);
	}

	public function check_user($email=NULL)
    {
        if ( ! is_null($email) AND ! $this->ci->session->userdata('pk') )
        {
            $this->ci->load->model('client/client_model');
            $user = $this->ci->client_model->get_user_by_email($email);
            if ( $user ) {
                $link = base_url("client/login?return_uri=" . urlencode(current_full_url()));
                redirect($link,'refresh');
            }
        }

        return FALSE;
    }

    public function check_if_needs_approval($token=NULL)
    {
    	$saved_quote 			= $this->ci->itp_quote_model->get_saved_quotes($token);

    	if ( $saved_quote ) {
    		$quote 	= json_decode($saved_quote->values,TRUE);

    		$personal_information 	= $quote['personal_information'];
    		$travel_information		= $quote['travel_information'];
    		$type = strtolower($travel_information['coverage_type']);

    		$itineraries 			= array_values($quote[$type.'_itinerary']);

    		$start_dates    = array_column($itineraries, "start_date");
			sort($start_dates);

			$start_date 	= isset($start_dates[0]) ? $start_dates[0] : date('Y-m-d');

	        /**
	         * Check Purpose of Travel
	         */
	        if($travel_information['purpose_of_travel'] == unserialize(TRAVEL)['others']) {
	            return TRUE;
	        }

	        /**
	         * Checks Travellers if age > 69
	         */
	        if ( $quote['insurance_cover'] == 'individual' ) {
	        	$age = calculate_age($personal_information['birthdate'], $start_date);
		        if($age >= AGE_NEEDS_APPROVAL) {
		            return TRUE;
		        }
	        }

	        // Members are only checked if Coverage Type is not Individual
	        
	        if(ucfirst($type) != COVERAGE_INDIVIDUAL AND isset($quote[$type.'_member'])) {
	            $members = array_values($quote[$type.'_member']);

	            // Check Travellers
	            foreach($members as $member) {
	                $age = calculate_age($member['birth_date'], $start_date);
	                if($age >= AGE_NEEDS_APPROVAL) {
	                    return TRUE;
	                }
	            }
	        } 
    	}

    	return FALSE;
    }

    public function get_persons_over_age($token=NULL)
    {
    	$saved_quote = $this->ci->itp_quote_model->get_saved_quotes($token);

    	if ( $saved_quote ) {
    		$quote 	= json_decode($saved_quote->values,TRUE);

    		$persons = array();

    		$travel_information		= $quote['travel_information'];
    		$type = strtolower($travel_information['coverage_type']);

    		$itineraries 	= array_values($quote[$type.'_itinerary']);

    		$start_dates    = array_column($itineraries, "start_date");
			sort($start_dates);

			$start_date 	= isset($start_dates[0]) ? $start_dates[0] : date('Y-m-d');

			if ( $quote['insurance_cover'] == 'individual' ) {
				$personal_information 	= $quote['personal_information'];
	        	$age = calculate_age($personal_information['birthdate'], $start_date);
		        if($age >= AGE_NEEDS_APPROVAL) {
		            $persons[] = ucwords($personal_information['fname'] . ' ' . $personal_information['lname']);
		        }
	        }

	        if(ucfirst($type) != COVERAGE_INDIVIDUAL AND isset($quote[$type.'_member'])) {
	            $members = array_values($quote[$type.'_member']);

	            // Check Travellers
	            foreach($members as $member) {
	                $age = calculate_age($member['birth_date'], $start_date);
	                if($age >= AGE_NEEDS_APPROVAL) {
	                    $persons[] = ucwords($member['first_name'] . ' ' . $member['last_name']);
	                }
	            }
	        }

	        return $persons;
    	}

    	return FALSE;
    }
}

/* End of file Itp_service.php */
/* Location: ./application/modules/travel-insurance/libraries/Itp_service.php */
