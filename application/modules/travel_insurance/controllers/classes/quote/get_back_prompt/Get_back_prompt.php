<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Get_back_prompt extends Quote implements Quote_interface {

    public function get() {
        $this->ci->session->set_userdata('itp_quote', false);
        $this->render('get_back_prompt/get_back_prompt', 'Get Back Prompt');
    }

}