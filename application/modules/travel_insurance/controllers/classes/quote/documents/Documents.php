<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends Quote implements Quote_interface {

	protected $hasRules = true;
    protected $scripts  = [
        'itp/quote-request.js'
    ];
    protected $error_message;
    protected $user_pk;

	public function boot() {
        $this->ci->load->model('travel_insurance/itp_quote_model');
        $this->ci->load->library('travel_insurance/itp_service');
        $this->user_pk = $this->ci->session->userdata('pk'); 
    }

    /**
     * Get Request
     */
    public function get() {

    	$token = $this->ci->input->get('token');
    	if ($token) {
            $this->_display($token);
        } else {
        	redirect(base_url("travel_insurance"));
        }
    }

    private function _display($token)
    {
    	$saved_quote = $this->ci->itp_quote_model->get_saved_quotes($token);

    	if ( $saved_quote->status == STATUS_PENDING AND $this->ci->itp_service->check_if_needs_approval($token) ) {

	    	$persons 	= $this->ci->itp_service->get_persons_over_age($token);
	    	$api_key 	= API_KEY;

	    	$this->ci->load->model('quotes/documents_model');

	    	$where = [
	    		"quote_pk = '{$saved_quote->pk}'"
	    	];

	    	$documents 	= $this->ci->documents_model->result($where);

	    	$preview_config = "";

	    	if ( $documents['count'] > 0 ) {
	    		$initial_preview = array();
	    		$initial_config  = array();
	    		foreach ( $documents['data'] AS $document ) {
	    			$icon = "fa-file";

		        	if ( $document->file_type == "image/jpg" OR $document->file_type == "image/jpeg" OR $document->file_type == "image/jpe" OR $document->file_type == "image/png" ) {
		        		$icon = "fa-file-photo-o text-warning";
		        	} elseif ( $document->file_type == "application/pdf" ) {
		        		$icon = "fa-file-pdf-o text-danger";
		        	} elseif ( $document->file_type == "application/msword" ) {
		        		$icon = "fa-file-word-o text-primary";
		        	}

		        	$initialPreview = '<div class="file-preview-other-frame">' . 
											'<div class="file-preview-other">' .
												'<span class="file-icon-4x"><i class="fa '.$icon.'"></i></span>' .
											'</div>' .
										'</div>';

					$initial_preview[] = $initialPreview;

					$initial_config[] = [
	            		"url"	=> base_url("quotes/documents/delete/{$api_key}/{$token}"),
	            		"key"	=> $document->pk,
	            		"caption" => $document->file_name,
	            		"width"	=> "120px"
	            	];
	    		}

	    		$preview_config = [
	    			"initial_preview" 	=> json_encode($initial_preview),
	    			"initial_config"	=> json_encode($initial_config),
	    			"append"			=> true
	    		];
	    	}
	    	
	    	$this->render('documents/index', 'Required Documents', [
	    	 		'persons'		=> $persons,
	    	 		'total'			=> count($persons),
	    	 		'upload_url'	=> base_url("quotes/documents/upload/{$api_key}/{$token}"),
	    	 		'delete_url'	=> base_url("quotes/documents/delete/{$api_key}/{$token}"),
	    	 		'error_message'	=> $this->error_message,
	    	 		'documents' 	=> $documents,
	    	 		'preview_config' => $preview_config,
	    	 		'api_key'		=> $api_key,
	    	 		'token'			=> $token
	    	 	]
	    	);
	    } else {
	    	redirect(base_url("travel_insurance"));
	    }
    }

    /**
     * Post Request
     */
    public function post() {
    	$token 		= $this->ci->input->post('token');
    	$saved_quote = $this->ci->itp_quote_model->get_saved_quotes($token);
    	$persons 	= $this->ci->itp_service->get_persons_over_age($token);

    	$this->ci->load->model('quotes/documents_model');

    	$where = [
    		"quote_pk = '{$saved_quote->pk}'"
    	];

	    $documents 	= $this->ci->documents_model->result($where);

    	if ( $documents['count'] >= count($persons) ) {
    		$update_fields = [
                'token'     => $token,
                'status'    => STATUS_FOR_APPROVAL
            ];

            if ( $this->ci->itp_quote_model->save($update_fields, TRUE, $token) ) {
            	Modules::run("quotes/send_notification_to_user", API_KEY, $saved_quote->pk, STATUS_FOR_APPROVAL);
            }
            redirect(base_url('travel_insurance/quote/get_back_prompt'),'refresh');
    	} else {
    		$this->error_message = "Please complete all the certificate of good health";
    		$this->_display($token);
    	}
    }
}

/* End of file Documents.php */
/* Location: ./application/modules/travel_insurance/controllers/classes/quote/documents/Documents.php */