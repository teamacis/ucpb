<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Confirmation extends Quote implements Quote_interface {

	public function boot() {
        $this->ci->load->model('travel_insurance/itp_quote_model');
        $this->ci->load->model('travel_insurance/itp_member_model');
        $this->ci->load->model('travel_insurance/itp_itinerary_model');
        $this->ci->load->model('travel_insurance/itp_package_model');
        $this->ci->load->model('travel_insurance/itp_country_model');
        $this->ci->load->model('travel_insurance/itp_premium_model');
        $this->ci->load->model('travel_insurance/itp_configuration_model');
        $this->ci->load->model('travel_insurance/itp_computation_model');
        $this->ci->load->model('travel_insurance/itp_quote_agent_model');
        $this->ci->load->model('travel_insurance/itp_emergency_contact_model');
        $this->ci->load->model('travel_insurance/itp_beneficiary_model');
        $this->ci->load->model('personal_information/personal_information_model');
        $this->ci->load->model('company/company_information_model');
        $this->ci->load->model('client/client_model');
        $this->user_pk = $this->ci->session->userdata('pk'); 
    }

    public function get() {
      
        $token = $this->ci->input->get('token');

        if ($token) {
            $itp_quote              = $this->ci->itp_quote_model->get_saved_quotes($token);
            $itp_quote_request      = json_decode($itp_quote->values,TRUE);
            $personal_information   = $itp_quote_request['personal_information'];
        }

        if ( (!(isset($itp_quote_request)) OR ! $itp_quote_request) OR ( ! isset($personal_information) OR ! $personal_information) ) {
            redirect(base_url("travel-insurance/quote"),'refresh');
        }

        if ( ! $this->ci->session->userdata('itp_step') OR $this->ci->session->userdata('itp_step') < 3 ) {
        	$this->ci->session->set_userdata( 'itp_step', 3 );
        }

        $user_type              = $itp_quote_request['travel_information']['coverage_type'];
        $members                = isset($itp_quote_request[strtolower($user_type) . '_member']) ? $itp_quote_request[strtolower($user_type) . '_member'] : array();
        $package_type           = $itp_quote_request[strtolower($user_type).'_package']['same'];
        $itineraries            = $itp_quote_request[strtolower($user_type).'_itinerary']['same'];
        $travel_information     = $itp_quote_request['travel_information'];

        foreach ($itineraries AS $index => $itinerary) {
             $itineraries[$index]['country'] = $this->ci->itp_country_model->get_countries($itinerary['destination']);
        }

        $this->ci->load->model('itp_configuration_model');
        $peso_value = $this->ci->itp_configuration_model->get('peso_value');

        $this->ci->load->library("itp_premium", ["itp_quote_pk" => $itp_quote->pk]);

        $this->render('confirmation/confirmation','Summary &amp; Confirmation', [
         	'itp_quote'				=> $itp_quote_request,
         	'personal_information'	=> (object) $personal_information,
         	'premium'     			=> $this->ci->itp_premium->compute(),
            'package_type'          => $package_type,
         	'members'     			=> $members,
         	'itineraries' 			=> $itineraries,
            'travel_information'    => (object) $travel_information,
            'token'                 => $token,
            'peso_value'            => $peso_value
        ]);
    }

    public function post() {

        if ( $this->ci->input->post('token') ) {
            $token = $this->ci->input->post('token');

            $saved_quote            = $this->ci->itp_quote_model->get_saved_quotes($token);
            $itp_quote_request      = json_decode($saved_quote->values,TRUE);
            $personal_information   = $itp_quote_request['personal_information'];

            $this->ci->load->library("travel_insurance/itp_premium", ["itp_quote_pk" => $saved_quote->pk]);
            $computation = $this->ci->itp_premium->compute();

            $this->ci->db->trans_begin();

            $itp_quote_request['computation'] = $computation;

            $insert_fields = [
                'token'         => $token,
                'values'        => json_encode($itp_quote_request)
            ];

            $this->ci->itp_quote_model->save($insert_fields,TRUE, $token, $this->user_pk);

            /*$itp_quote = $this->ci->itp_quote_model->get_by_fields(["token" => $token]);

            if ( ! $itp_quote ) {
                // Save Quote
                $itp_quote_pk           = $this->_insert_itp_quote($itp_quote_request, $token);

                $this->_insert_members($itp_quote_pk, $itp_quote_request, $personal_information);
                $this->_insert_itineraries($itp_quote_pk, $itp_quote_request);
                $this->_insert_packages($itp_quote_pk, $itp_quote_request);
                $this->_insert_personal_information($itp_quote_pk, $personal_information, $itp_quote_request['insurance_cover']);

                $computations                    = json_decode($saved_quote->computation,TRUE);
                $computations['itp_quote_pk']    = $itp_quote_pk;

                $this->ci->itp_computation_model->save($computations);

                $itp_quote = $this->ci->itp_quote_model->get_by_fields(["itp_quotes.pk" => $itp_quote_pk]);
            } else {
                $itp_quote_pk = $itp_quote->pk;
            }*/

            $this->ci->db->trans_status() === TRUE ? $this->ci->db->trans_commit() : $this->ci->db->trans_rollback();     

            $txnid       = $this->ci->itp_quote_model->generate_transaction_id($saved_quote->pk);

            $email       = $itp_quote_request['personal_information']['email_address'];
            $description = "Description";
            $ccy         = "PHP";

            $amount      = number_format($computation->net_total_php, 2, '.', '');
            $digest_str  = DP_MERCHANT.":$txnid:$amount:$ccy:$description:$email:".DP_SECRET_KEY;
            $digest      = sha1($digest_str);

            $params = "merchantid=" . urlencode(DP_MERCHANT) .
                "&txnid=" .  urlencode($txnid) .
                "&amount=" . urlencode($amount) .
                "&ccy=" . urlencode($ccy) .
                "&description=" . urlencode($description) .
                "&email=" . urlencode($email) .
                "&digest=" . urlencode($digest) .
                "&param1=" . 'itp' .
                "&param2=" . urlencode($token);

            $url = DP_PAYMENT_URL;

            if ( TEST ) {
                $post_url = base_url() . 'transaction/post_callback?txnid='.urlencode($txnid).'&refno=VCGRXT62&status=S&message=test&digest=f805f78033ba2abbef87da8d3a8f7c56b9a6ec10&param1=itp&param2='.urlencode($token);
                file_get_contents($post_url);
                $return_url = base_url() . 'transaction/return_callback?txnid='.urlencode($txnid).'&refno=VCGRXT62&status=S&message=test&digest=f805f78033ba2abbef87da8d3a8f7c56b9a6ec10&param1=itp&param2='.urlencode($token);
                redirect($return_url);
            }
     
            $dragonpay_url = $url."?".$params;

            if ($this->ci->client_model->get_by_field('email_address', $itp_quote_request['personal_information']['email_address'])) { // check if the user is exist
                $this->ci->client_model->logged_in(true,$dragonpay_url);
            }

            header("Location: $url?$params");
        }
    }

    /**
     * Inserts the ITP Quote
     *
     * @return mixed
     */
    private function _insert_itp_quote($itp_quote=NULL, $token=NULL) {

        if ( ! is_null($itp_quote) ) {
            $personal_information   = $itp_quote['personal_information'];
            $travel_information     = $itp_quote['travel_information'];

            $insert_fields = array_merge($personal_information, $personal_information, $travel_information);
            $insert_fields['status']    = STATUS_PENDING;
            $insert_fields['token']     = $token;

            // OVERRIDE purpose_of_travel with the Others field then UNSET the purpose_travel_others
            if($insert_fields['purpose_of_travel'] == unserialize(TRAVEL)['others']) {
                $insert_fields['purpose_of_travel'] = $insert_fields['purpose_of_travel_others'];
            }
            unset($insert_fields['purpose_of_travel_others']);

            // When coverage type is not GROUP we just set the group_name to empty string because it may have a value in the front end when they switch to Group and switch back to Family or Individual.
            if($insert_fields['coverage_type'] != unserialize(COVERAGE)['group']) {
                $insert_fields['group_name'] = '';
            }

            /**
             * Fix if it's the same itinerary or package.
             * In the front end value is toggled between 'yes' and 'no'
             */
            $type = strtolower($insert_fields['coverage_type']);

            // If is_same is not set meaning INDIVIDUAL is chosen.
            $itinerary = $itp_quote[$type.'_itinerary'];
            $insert_fields['same_itinerary'] = !isset($itinerary['is_same']) || $itinerary['is_same'] == 'yes' ? true : false;

            // If is_same is not set meaning INDIVIDUAL is chosen.
            $package = $itp_quote[$type.'_package'];
            $insert_fields['same_package']   = !isset($package['is_same']) || $package['is_same'] == 'yes' ? true : false;

            return $this->ci->itp_quote_model->save($insert_fields);
        }
        
        return FALSE;
    }

    /**
     * Inserts Members
     *
     * @param $itp_quote_pk
     */
    public function _insert_members($itp_quote_pk, $itp_quote=NULL,$personal_information=NULL) {
        $type = strtolower($itp_quote['travel_information']['coverage_type']);

        if(ucfirst($type) != COVERAGE_INDIVIDUAL) {
            //$insertFields = array_values($itp_quote[$type.'_member']);
            //$insertFields = $this->_add_itp_quote_on_array($insertFields, $itpQuotePk);
            $members = $itp_quote[$type.'_member'];
            if (is_array($members) AND count($members) > 0) {
                foreach ($members AS $index => $member) {
                    $member['itp_quote_pk'] = $itp_quote_pk;
                    $member_id = $this->ci->itp_member_model->insert($member);

                    /* Insert Emergency Contacts */
                    if (!is_null($personal_information) AND isset($personal_information['emergency_contacts'])) {
                        $emergency_contacts = $personal_information['emergency_contacts'];

                        // Emergency Contacts of Members
                        if (isset($emergency_contacts[$index]) AND is_array($emergency_contacts[$index])) {
                            foreach ($emergency_contacts[$index] AS $contact) {
                                $contact['itp_quote_pk']    = $itp_quote_pk;
                                $contact['itp_member_pk']   = $member_id;
                                $this->ci->itp_emergency_contact_model->insert($contact);
                            }
                        }
                    }

                    /* Insert Beneficiaries */
                    if (!is_null($personal_information) AND isset($personal_information['beneficiaries'])) {
                        $beneficiaries = $personal_information['beneficiaries'];

                        // Beneficiaries of Members
                        if (isset($beneficiaries[$index]) AND is_array($beneficiaries[$index])) {
                            foreach ($beneficiaries[$index] AS $beneficiary) {
                                $beneficiary['itp_quote_pk']    = $itp_quote_pk;
                                $beneficiary['itp_member_pk']   = $member_id;
                                $this->ci->itp_beneficiary_model->insert($beneficiary);
                            }
                        }
                    }
                }
            }        
        }
    }

    /**
     * Inserts Itineraries
     *
     * @param $itpQuotePk
     */
    public function _insert_itineraries($itpQuotePk, $itp_quote=NULL) {
        $type         = strtolower($itp_quote['travel_information']['coverage_type']);
        $itineraries  = $itp_quote[$type.'_itinerary'];

        /**
         * If is_same is not set meaning INDIVIDUAL is chosen.
         * array_values is used because the key needs to be messed up in the front end
         */
        $insertFields = !isset($itineraries['is_same']) || $itineraries['is_same'] == 'yes' ? array_values($itineraries['same']) : array_values($itineraries['different']);
        $insertFields = $this->_add_itp_quote_on_array($insertFields, $itpQuotePk, true);

        $this->ci->itp_itinerary_model->save($insertFields);
    }

    /**
     * Inserts Packages
     *
     * @param $itpQuotePk
     */
    public function _insert_packages($itpQuotePk, $itp_quote=NULL) {
        $type         = strtolower($itp_quote['travel_information']['coverage_type']);
        $packages     = $itp_quote[$type.'_package'];

        /**
         * If is_same is not set meaning INDIVIDUAL is chosen.
         * array_values is used because the key needs to be messed up in the front end
         */
        if(!isset($packages['is_same']) || $packages['is_same'] == 'yes') {
            $insertFields = [
                [
                    'package'      => $packages['same'],
                    'itp_quote_pk' => $itpQuotePk
                ]
            ];
        }
        else {
            $insertFields = array_values($packages['different']);
            $insertFields = $this->_add_itp_quote_on_array($insertFields, $itpQuotePk, true);
        }

        $this->ci->itp_package_model->save($insertFields);
    }

    private function _insert_personal_information($itp_quote_pk=0, $personal_information=array(), $insurance_cover='individual') {
        /**
         * Save ITP Agent
         */
        //$this->ci->itp_quote_agent_model->destroy_by_field('itp_quotes_pk', $this->ci->session->userdata('itp_quote')['pk']);
        if($personal_information['with_agent'] == 'on') {
            $this->ci->itp_quote_agent_model->save($itp_quote_pk,$personal_information);
        }

        if ($insurance_cover == 'individual') {
            /**
             * Save Personal Information!
             */
            $this->ci->personal_information_model->save($personal_information, $itp_quote_pk);
        } else {
             /**
             * Save Company Information!
             */
            $this->ci->company_information_model->save($personal_information, $itp_quote_pk);
        }   
        
        /* Insert Emergency Contacts */
        $emergency_contacts = $personal_information['emergency_contacts'];

        if (isset($emergency_contacts[0]) AND is_array($emergency_contacts[0])) {
            foreach ($emergency_contacts[0] AS $contact) {
                $contact['itp_quote_pk']    = $itp_quote_pk;
                $contact['itp_member_pk']   = 0;
                $this->ci->itp_emergency_contact_model->insert($contact);
            }
        }

        /* Insert Beneficiaries */
        $beneficiaries = $personal_information['beneficiaries'];

        if (isset($beneficiaries[0]) AND is_array($beneficiaries[0])) {
            foreach ($beneficiaries[0] AS $beneficiary) {
                $beneficiary['itp_quote_pk']    = $itp_quote_pk;
                $beneficiary['itp_member_pk']   = 0;
                $this->ci->itp_beneficiary_model->insert($beneficiary);
            }
        }
    }

    /**
     * Adds an ITP Quote Pk field on the array.
     *
     * @param $array
     * @param $itpQuotePk
     * @return array
     */
    private function _add_itp_quote_on_array($array, $itpQuotePk, $isMe = false) {
        foreach($array as $key => $field) {
            $array[$key]['itp_quote_pk'] = $itpQuotePk;

            if($isMe) {
                $array[$key]['is_me'] = !isset($array[$key]['is_me']) ? '' : true;
            }
        }

        return $array;
    }
}

/* End of file Confirmation.php */
/* Location: ./application/modules/itp/controllers/classes/quote/confirmation/Confirmation.php */