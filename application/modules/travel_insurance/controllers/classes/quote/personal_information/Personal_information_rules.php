<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Personal_information_rules extends Quote_rule implements Quote_rule_interface {

    public function rules() {

        $rules = array_merge(
            $this->_contact_information(),
            $this->_agent_information()
        );

        if ( $this->post['insurance_cover'] == 'individual' ) {
            $rules = array_merge($rules,$this->_personal_information());
        } else {
            $rules = array_merge($rules,$this->_company_information());
        }

         /* Emergency Contacts */
        if ($this->ci->input->post('emergency_contacts') AND is_array($this->ci->input->post('emergency_contacts'))) {
            $emergency_contacts = $this->ci->input->post('emergency_contacts');
            foreach ($emergency_contacts AS $member_id => $emergency_contact) {
                if (is_array($emergency_contact) AND count($emergency_contact) > 0) {
                    foreach ($emergency_contact AS $index =>$ec) {
                        $rules[] = [
                            "field"     => "emergency_contacts[{$member_id}][$index][name]",
                            "label"     => "Name",
                            "rules"     => "trim|required"
                        ];
                        $rules[] = [
                            "field"     => "emergency_contacts[{$member_id}][$index][contact_number]",
                            "label"     => "Contact Number",
                            "rules"     => "trim|required"
                        ];
                        $rules[] = [
                            "field"     => "emergency_contacts[{$member_id}][$index][relation]",
                            "label"     => "Relationship",
                            "rules"     => "trim|required"
                        ];
                    }
                }
            }
        }

        /* Beneficiaries */
        if ($this->ci->input->post('beneficiaries') AND is_array($this->ci->input->post('beneficiaries'))) {
            $beneficiaries = $this->ci->input->post('beneficiaries');
            foreach ($beneficiaries AS $member_id => $beneficiary) {
                if (is_array($beneficiary) AND count($beneficiary) > 0) {
                    foreach ($beneficiary AS $index =>$b) {
                        $rules[] = [
                            "field"     => "beneficiaries[{$member_id}][$index][name]",
                            "label"     => "Name",
                            "rules"     => "trim|required"
                        ];
                        $rules[] = [
                            "field"     => "beneficiaries[{$member_id}][$index][relation]",
                            "label"     => "Relationship",
                            "rules"     => "trim|required"
                        ];
                    }
                }
            }
        }

        return $rules;
    }

    /**
     * Personal Information Rules
     *
     * @return array
     */
    private function _personal_information() {
         $rules = [
            [
                'field' => 'salutation',
                'label' => 'Salutation',
                'rules' => 'required'
            ],
            [
                'field' => 'lname',
                'label' => 'Last Name',
                'rules' => 'required|max_length[50]'
            ],
            [
                'field' => 'fname',
                'label' => 'First Name',
                'rules' => 'required|max_length[50]'
            ],
            [
                'field' => 'mname',
                'label' => 'Middle Initial',
                'rules' => 'max_length[1]'
            ],
            [
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ],
            [
                'field' => 'birthdate',
                'label' => 'Date of Birth',
                'rules' => 'required'
            ],
            [
                'field' => 'occupation',
                'label' => 'Occupation',
                'rules' => 'required'
            ],
            [
                'field' => 'nationality',
                'label' => 'Nationality',
                'rules' => 'required'
            ]
        ];

        /**
         * Occupation and Tin Rules
         */
        if($this->post['with_tin'] == 1) {
            $rules[] = [
                'field' => 'tin',
                'label' => 'Tin',
                'rules' => 'required|max_length[50]'
            ];
        }
        else {
            $rules[] = [
                'field' => 'id_type',
                'label' => 'ID Type',
                'rules' => 'required|max_length[50]'
            ];
            $rules[] = [
                'field' => 'id_number',
                'label' => 'ID #',
                'rules' => 'required|max_length[50]'
            ];
        }

         if($this->post['occupation'] == 'Others') {
            $rules[] = [
                'field' => 'other_occupation',
                'label' => 'Others Occupation',
                'rules' => 'required|max_length[50]'
            ];
        }

        return $rules;
    }

    private function _agent_information() {
        $rules = [
             [
                'field' => 'with_agent',
                'label' => 'With Agent',
                'rules' => 'required'
            ]
        ];

         /**
         * Agent Rules
         */
        if($this->post['with_agent'] == 'on') {
            $rules[] = [
                'field' => 'agent_type',
                'label' => 'Agent Type',
                'rules' => 'required'
            ];

            /**
             * If agent code add rule for AGENT CODE
             */
            if($this->post['agent_type'] == 'Agent Code') {
                $rules[] = [
                    'field' => 'agent_code',
                    'label' => 'Agent Code',
                    'rules' => 'required|max_length[50]'
                ];
            }
            /**
             * If not agent code add rule for AGENT'S FIRST NAME and LAST NAME
             */
            elseif ($this->post['agent_type'] == 'Agent Code') {
                $rules[] = [
                    'field' => 'agent_fname',
                    'label' => 'Agent\'s First Name',
                    'rules' => 'required|max_length[50]'
                ];
                $rules[] = [
                    'field' => 'agent_lname',
                    'label' => 'Agent\'s Last Name',
                    'rules' => 'required|max_length[50]'
                ];
            }
            elseif($this->post['agent_type'] == 'Agency Name') {
                $rules[] = [
                    'field' => 'agency_name',
                    'label' => 'Agency Name',
                    'rules' => 'required|max_length[100]'
                ];
            }
        }

        return $rules;
    }

     /**
     * Company Information Rules
     *
     * @return array
     */
    private function _company_information() {
        return [
            [
                'field' => 'name',
                'label' => 'Company Name',
                'rules' => 'required'
            ],
            [
                'field' => 'industry_type',
                'label' => 'Industry Type',
                'rules' => 'required'
            ],
            [
                'field' => 'incorporation_date',
                'label' => 'Date of Incorporation',
                'rules' => 'required'
            ],
            [
                'field' => 'contact_person',
                'label' => 'Contact Person',
                'rules' => 'required'
            ]
        ];
    }

    /**
     * Contact Information Rules
     *
     * @return array
     */
    private function _contact_information() {
        $rules = [
            [
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => ['valid_email', 'required']
            ],
            [
                'field' => 'province',
                'label' => 'Province',
                'rules' => 'required'
            ],
            [
                'field' => 'city',
                'label' => 'City',
                'rules' => 'required'
            ],
            [
                'field' => 'barangay',
                'label' => 'Barangay',
                'rules' => 'required'
            ],
            [
                'field' => 'house_number',
                'label' => 'House Number',
                'rules' => 'required'
            ],
        ];

         /**
         * Phone Number Rules
         */
        if((!isset($this->post['telephone']) || !$this->post['telephone']) && (!isset($this->post['mobile']) || !$this->post['mobile'])) {
            $rules[] = [
                'field' => 'mobile',
                'label' => 'Mobile',
                'rules' => 'required|max_length[50]',
                'errors' => [
                    'required' => 'Either telephone or mobile is required.'
                ]
            ];
        }

        return $rules;
    }

    public function check_agent() {
        if($this->post['with_agent'] == 'on') {
            $this->load->model('agent/agent_model', 'agent');

            /**
             * If agent code add rule for AGENT CODE
             */
            if($this->post['type'] == 'Agent Code') {
                $result = $this->agent->fetch_by_field('code', $this->post['agent_code']);
                if(!$result) {
                    $this->session->set_flashdata('agent_code_error', 'Agent code is invalid');
                    return false;
                }
            } elseif ($this->post['type'] == 'Agency Name') {
                $result = $this->agent->fetch_by_field('agencies.name', $this->post['agency_name']);
                if(!$result) {
                    $this->session->set_flashdata('agent_agency_error', 'Agency name is invalid');
                    return false;
                }
            }
            /**
             * If not agent code add rule for AGENT'S FIRST NAME and LAST NAME
             */
            else {
                $result = $this->agent->fetch_by_name($this->post['agent_fname'], $this->post['agent_lname']);
                if(!$result) {
                    $this->session->set_flashdata('agent_name_error', 'Agent name is invalid');
                    return false;
                }
            }
        }

        return true;
    }
}