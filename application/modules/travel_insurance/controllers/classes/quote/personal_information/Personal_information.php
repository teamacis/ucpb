<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal_information extends Quote implements Quote_interface {
    protected $hasRules = true;
    protected $scripts  = [
        'itp/personal-information.js'
    ];

	public function boot() {
        $this->ci->load->model('travel_insurance/itp_quote_model');
        $this->ci->load->model('travel_insurance/itp_member_model');
        $this->ci->load->model('travel_insurance/itp_itinerary_model');
        $this->ci->load->model('travel_insurance/itp_package_model');
        $this->ci->load->model('travel_insurance/itp_premium_model');
        $this->ci->load->model('travel_insurance/itp_country_model');
        $this->ci->load->model('travel_insurance/itp_quote_agent_model');
        $this->ci->load->model('travel_insurance/itp_emergency_contact_model');
        $this->ci->load->model('travel_insurance/itp_beneficiary_model');
        $this->ci->load->model('travel_insurance/itp_personal_information_model');
        $this->ci->load->model('place/place_model');
        $this->ci->load->model('vehicle/vehicle_model');
        $this->ci->load->model('client/client_model');
        $this->ci->load->model('salutation/salutation_model');
        $this->ci->load->model('suffix/suffix_model');
        $this->ci->load->model('nationality/nationality_model');
        $this->ci->load->model('occupation/occupation_model');
        $this->ci->load->model('coverage_details/coverage_details_model');
        $this->ci->load->model('client/client_rules_model');
        $this->ci->load->model('transaction/transaction_model');
        $this->ci->load->model('agent/agent_model');
        $this->user_pk = $this->ci->session->userdata('pk'); 
    }

    public function get() {
      
        $token = $this->ci->input->get('token');

        if ($token) {
            $itp_quote = $this->ci->itp_quote_model->get_saved_quotes($token);
        }

        if ( !(isset($itp_quote)) OR ! $itp_quote ) {
            redirect(base_url("travel-insurance/quote"),'refresh');
        }

        if ( ! $this->ci->session->userdata('itp_step') OR $this->ci->session->userdata('itp_step') < 2 ) {
            $this->ci->session->set_userdata( 'itp_step', 2 );
        }

        $itp_quote_request      = json_decode($itp_quote->values,TRUE);

        if ( $this->ci->session->userdata('pk') ) {
            $this->ci->load->model('client/client_model');
            $personal_information      = (array) $this->ci->client_model->get_personal_information();        
        } else {
            if ( $itp_quote_request['insurance_cover'] == 'individual' ) {
                $personal_information   = $itp_quote_request['personal_information'];
            } else {
                $personal_information                   =  $itp_quote_request['company_information'];
                $personal_information['email_address']  =  $itp_quote_request['personal_information']['email_address'];
                $personal_information['mobile']         =  $itp_quote_request['personal_information']['mobile'];
                $personal_information['telephone']      =  $itp_quote_request['personal_information']['telephone'];
            }
        }

        $this->ci->load->library('itp_service');
        $this->ci->itp_service->check_user($personal_information['email_address']);

        $this->_show_personal_info(true, $personal_information, $itp_quote_request, $token);
    }

    /**
     * Displays the Personal Information
     */
    private function _show_personal_info($firstLoad = true, $personal_information = false, $itp_quote_request= false, $token = NULL) {
       
        /**
         * Set initial values
         */
        if($firstLoad == true AND $personal_information) {
            $_POST = array_merge($_POST, $personal_information);
        }
        
        $user_type          = $itp_quote_request['travel_information']['coverage_type'];
        $members            = isset($itp_quote_request[strtolower($user_type) . '_member']) ? $itp_quote_request[strtolower($user_type) . '_member'] : array();
        $emergency_contacts = isset($personal_information['emergency_contacts']) ? $personal_information['emergency_contacts'] : array();
        $beneficiaries      = isset($personal_information['beneficiaries']) ? $personal_information['beneficiaries'] : array();
        //echo "<pre>"; print_r($itp_quote_request); exit;

        $this->render('personal_information/personal_information','Personal Information', [
            'suffixes'       => $this->ci->suffix_model->fetch(),
            'salutations'    => $this->ci->salutation_model->fetch(),
            'nationalities'  => $this->ci->nationality_model->fetch(),
            'provinces'      => $this->ci->place_model->provinces(),
            'cities'         => set_value('province') ? $this->ci->place_model->cities(set_value('province')) : false,
            'barangays'      => set_value('city') ? $this->ci->place_model->barangays(set_value('city')) : false,
            'occupations'    => $this->ci->occupation_model->fetch(),
            'itp_quote'    	 => $itp_quote_request,      
            'personal_information' => (object) $personal_information, 
            'first_load'     => $firstLoad,
            'members'        => $members,
            'relations'      => unserialize(RELATIONS),
            'immediate_family' => unserialize(IMMEDIATE_FAMILY),
            'emergency_contacts' => $this->ci->input->post('emergency_contacts') ? $this->ci->input->post('emergency_contacts') : $emergency_contacts,
            'beneficiaries'  => $this->ci->input->post('beneficiaries') ? $this->ci->input->post('beneficiaries') : $beneficiaries,
            'token'          => $token,
            'user_type'      => $user_type,
            'civil_statuses' => unserialize(CIVIL_STATUSES),
            'available_id'   => $this->ci->load->config('id',TRUE),
            'agencies'       => $this->ci->agent_model->get_agencies()
        ]);
    }

    public function post() {
        $this->_do_personal_info();
    }
    /**
     * Personal Information Request
     */
    private function _do_personal_info() {
 
        //$this->ci->load->model('personal_information/personal_information_rules_model');

        //  $validated  = $this->ci->personal_information_rules_model->validate($this->ci->form_validation, 'save', (object) $this->ci->input->post());
        $token      = $this->ci->input->post('token');

        if ( ! $token ) {
            redirect(base_url("travel-insurance/quote"),'refresh');
        }

        $itp_quote              = $this->ci->itp_quote_model->get_saved_quotes($token);
        $itp_quote_request      = json_decode($itp_quote->values,TRUE);
        
        if ( $itp_quote_request['insurance_cover'] == 'individual' ) {
            $personal_information   = $itp_quote_request['personal_information'];
        } else {
            $personal_information   = $itp_quote_request['company_information'];
        }
   
        if($this->rules->validate() OR $this->ci->input->post('submit') == "save") {

            $this->ci->db->trans_start();

            $itp_quote_request['personal_information'] = $this->ci->input->post();

            $insert_fields = [
                'token'   => $token,
                'values'  => json_encode($itp_quote_request)
            ];

            $this->ci->itp_quote_model->save($insert_fields,TRUE, $token, $this->user_pk);

            $this->ci->db->trans_complete();

            $array = array(
                'itp_step' => 3
            );
            
            $this->ci->session->set_userdata( $array );

            if ($this->ci->input->post('submit') == "save") {

                $this->ci->db->trans_start();

                $update_fields = [
                    'token'     => $token,
                    'status'    => STATUS_SAVED
                ];

                $this->ci->itp_quote_model->save($update_fields, TRUE, $token);

                $this->ci->db->trans_complete();

                $link = anchor(base_url("travel-insurance/quote/personal_information?token={$token}"), "here");

                $msg = $this->ci->load->view("email/save_quotes",["link" => $link], TRUE);

                $this->ci->email->clear();
                $this->ci->email->from('itp@ucpbgen.com', 'ITP UCPBGEN');
                $this->ci->email->to($this->ci->input->post('email_address'));
                $this->ci->email->subject('UCPBGEN ITP Quote Session');
                $this->ci->email->message($msg);
                $this->ci->email->send();

                $this->ci->session->set_flashdata('success_message', 'Personal Information successfully saved');
                redirect('travel-insurance/quote/personal_information?token=' . $itp_quote->token);
            } else {
                redirect('travel-insurance/quote/confirmation?token=' . $itp_quote->token);
            }     
        }
        else {
            $this->_show_personal_info(false, $personal_information, $itp_quote_request, $token);
        }
    }
}

/* End of file Personal_information.php */
/* Location: ./application/modules/itp/controllers/classes/quote/personal_information/Personal_information.php */