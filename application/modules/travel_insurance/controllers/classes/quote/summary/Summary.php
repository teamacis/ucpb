<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Summary extends Quote implements Quote_interface {

    public function boot() {
        $this->ci->load->model('travel_insurance/itp_quote_model');
        $this->ci->load->model('travel_insurance/itp_member_model');
        $this->ci->load->model('travel_insurance/itp_itinerary_model');
        $this->ci->load->model('travel_insurance/itp_package_model');
        $this->ci->load->model('travel_insurance/itp_premium_model');
        $this->ci->load->model('travel_insurance/itp_country_model');
        $this->ci->load->model('travel_insurance/itp_computation_model');
        $this->user_pk = $this->ci->session->userdata('pk'); 
    }

    public function get() {
        $token  = $this->ci->input->get('token');
        $action = $this->ci->input->get('action');

        if ($token) {
            $itp_quote = $this->ci->itp_quote_model->get_saved_quotes($token);
            if ( ! $itp_quote OR strlen($itp_quote->values) == 0) {
                redirect(base_url("travel-insurance/quote"),'refresh');
            } else {
                $itp_quote_request = json_decode($itp_quote->values,TRUE);
                $this->ci->load->library('itp_service');
                $this->ci->itp_service->check_user($itp_quote_request['personal_information']['email_address']);
            }
        }else {
            redirect(base_url("travel-insurance/quote"),'refresh');
        }

        if ( ! $this->ci->session->userdata('itp_step') ) {
            $array = array(
                'itp_step' => 1
            );
            
            $this->ci->session->set_userdata( $array );
        }

        if ( $action && $action == 'view_formal_quote' ) {
            $this->ci->load->library('travel_insurance/itp_service');
            $this->ci->itp_service->pdf_formal_quote($token);
            exit;
        }

        $user_type      = $itp_quote_request['travel_information']['coverage_type'];
        $members        = isset($itp_quote_request[strtolower($user_type) . '_member']) ? $itp_quote_request[strtolower($user_type) . '_member'] : array();
        $package_type   = $itp_quote_request[strtolower($user_type).'_package']['same'];
        $itineraries    = $itp_quote_request[strtolower($user_type).'_itinerary']['same'];
    
        foreach ($itineraries AS $index => $itinerary) {
            $itineraries[$index]['country'] = $this->ci->itp_country_model->get_countries($itinerary['destination']);
        }

        $this->ci->load->model('itp_configuration_model');
        $peso_value = $this->ci->itp_configuration_model->get('peso_value');

        $this->ci->load->library("itp_premium", ["itp_quote_pk" => $itp_quote->pk]);

        $coverages = $this->ci->load->config("coverage", TRUE)[strtolower($package_type)];

        $this->render('travel_insurance/summary/summary', 'Quote Summary', [
            'info'        => $itp_quote_request,
            'package_type'=> $package_type,
            'premium'     => $this->ci->itp_premium->compute(),
            'members'     => $members,
            'itineraries' => $itineraries,
            'token'       => $token,
            'peso_value'  => $peso_value,
            'coverages'   => $coverages
        ]);
    }

    public function post() {
        if ($this->ci->input->post('submit') == 'buy') {
            $token = $this->ci->input->post('token');
            $this->ci->session->set_userdata("itp_step",2);
            redirect(base_url("travel-insurance/quote/personal_information?token={$token}"),'refresh');
        }
    }

    private function _coverages() {
        $itp          = $this->ci->session->userdata('itp_quote')['request'];
        $coverageType = $itp['travel_information']['coverage_type'];
        $coverages    = [
            'platinum' => false,
            'gold'     => false,
            'silver'   => false
        ];

        if($coverageType != 'Individual') {
            $packages = $itp[strtolower($coverageType).'_package'];

            if($packages['is_same'] == 'yes') {
                $coverages[strtolower($packages['same'])] = true;
            }
            else {
                foreach($packages['different'] as $package) {
                    $coverages[strtolower($package['package'])] = true;
                }
            }
        } else {
            $coverages[strtolower($itp["individual_package"]['same'])] = true;
        }

        return $coverages;
    }

    private function _premiums() {
        $itp          = $this->ci->session->userdata('itp_quote')['request'];
        $coverageType = $itp['travel_information']['coverage_type'];
        $itineraries  = $itp[strtolower($coverageType).'_itinerary'];
        $packages     = $itp[strtolower($coverageType).'_package'];

        /**
         * Fix Itineraries
         */
        $finalItineraries = [];
        if(isset($itineraries['is_same']) AND $itineraries['is_same'] == 'no') {
            foreach($itineraries['different'] as $key=>$itinerary) {
                $finalItineraries[] = $itinerary;
            }
        }

        /**
         * Fix Packages
         */
        $finalPackages = [];
        if(isset($packages['is_same']) AND $packages['is_same'] == 'no') {
            foreach($packages['different'] as $key=>$package) {
                $finalPackages[] = $package;
            }
        }

        /**
         * Fix Premiums
         */
        $premiums = [];
        foreach($finalItineraries as $key=>$fi) {
            $days = subtract_days($fi['start_date'], $fi['end_date']);
            $country = $this->ci->itp_country_model->get_by_fields(['pk' => $fi['destination']]);

            $result = $this->ci->itp_premium_model->get_by_fields([
                'days'                => $days,
                'user_type'           => $coverageType,
                'premium_type'        => $finalPackages[$key]['package'],
                'itp_country_type_pk' => $country['itp_country_type_pk']
            ]);

            $result['name'] = $fi['first_name'].' '.$fi['last_name'];
            $premiums[] = $result;
        }

        return $premiums;
    }
}