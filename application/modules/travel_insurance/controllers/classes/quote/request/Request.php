<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request extends Quote implements Quote_interface {

    protected $hasRules = true;
    protected $scripts  = [
        'itp/quote-request.js'
    ];
    protected $error_message;
    protected $user_pk;

    public function boot() {
        $this->ci->load->model('travel_insurance/itp_quote_model');
        $this->ci->load->model('travel_insurance/itp_member_model');
        $this->ci->load->model('travel_insurance/itp_itinerary_model');
        $this->ci->load->model('travel_insurance/itp_package_model');
        $this->ci->load->model('travel_insurance/itp_country_model');
        $this->ci->load->model('travel_insurance/itp_premium_model');
        $this->ci->load->model('travel_insurance/itp_configuration_model');
        $this->ci->load->model('travel_insurance/itp_computation_model');
        $this->ci->load->library('travel_insurance/itp_service');
        $this->user_pk = $this->ci->session->userdata('pk'); 
    }

    /**
     * Get Request
     */
    public function get() {
  
        $token = $this->ci->input->get('token');

        if ( ! $this->ci->session->userdata('itp_step') OR ! $token) {
            $array = array(
                'itp_step' => 1
            );
            
            $this->ci->session->set_userdata( $array );
        }

        if ($token) {
            $saved_quote = $this->ci->itp_quote_model->get_saved_quotes($token);
            if ($saved_quote AND strlen($saved_quote->values) > 0) {
                $_POST = json_decode($saved_quote->values,TRUE);
                $this->ci->load->library('itp_service');
                $this->ci->itp_service->check_user($this->ci->input->post('personal_information')['email_address']);
            } else {
                redirect(base_url("travel-insurance/quote/request"),'refresh');
            }
        }

        if ( $this->ci->session->userdata('pk') ) {
            $this->ci->load->model('client/client_model');
            $_POST['personal_information'] = (array) $this->ci->client_model->get_personal_information();        
        }

        $this->_display();
    }

    /**
     * Display View
     */
    private function _display() {

        $token = $this->ci->input->get('token');
  
        $countries = $this->ci->itp_country_model->get();
        $this->render('request/request', 'Quote Request', [
            'countries' => $countries,
            'error_message' => $this->error_message,
            'relations' => unserialize(RELATIONS),
            'token'     => $token,
            'civil_statuses' => unserialize(CIVIL_STATUSES)
        ]);
    }

    /**
     * Post Request
     */
    public function post() {

        //$this->_upload_valid_id();

        if($this->rules->validate()) {

            $this->ci->db->trans_begin();

            $token = $this->ci->input->post('token') ? $this->ci->input->post('token') : $this->_generate_token();

            $insert_fields = [
                'token'     => $token,
                'values'   => json_encode($this->ci->input->post())
            ];
            
            $this->ci->itp_quote_model->save($insert_fields, TRUE, $token, $this->user_pk);

            $this->ci->db->trans_status() === TRUE ? $this->ci->db->trans_commit() : $this->ci->db->trans_rollback();

            if($this->ci->input->post('submit') == "get_quote") {

                $status         = $this->ci->itp_service->check_if_needs_approval($token) ? STATUS_FOR_APPROVAL : STATUS_PENDING;
               
                if($status == STATUS_PENDING) {
                    redirect("travel-insurance/quote/summary?token={$token}");
                } else {
                    redirect('travel-insurance/quote/documents?token=' . $token);
                }
            } else {

                $this->ci->db->trans_begin();

                $update_fields = [
                    'token'     => $token,
                    'status'    => STATUS_SAVED
                ];

                $this->ci->itp_quote_model->save($update_fields, TRUE, $token);

                $this->ci->db->trans_status() === TRUE ? $this->ci->db->trans_commit() : $this->ci->db->trans_rollback();

                $link = anchor(base_url("travel-insurance/quote/request?token={$token}"), "here");

                $msg = $this->ci->load->view("email/save_quotes",["link" => $link], TRUE);
                
                $this->ci->email->clear();
                $this->ci->email->from('itp@ucpbgen.com', 'ITP UCPBGEN');
                $this->ci->email->to($this->ci->input->post('personal_information')['email_address']);
                $this->ci->email->subject('UCPBGEN ITP Quote Session');
                $this->ci->email->message($msg);
                $this->ci->email->send();
  
                $this->ci->session->set_flashdata('success_message', 'Quote Request successfully saved');
                redirect("travel-insurance/quote/request?token={$token}");
            }
        }
        else {
            $this->_display();
        }
    }


    /**
     * Checks if ground for approval
     *
     * @return bool
     */
    private function _check_if_needs_approval() {
        $personalInformation = $this->ci->input->post('personal_information');
        $travelInformation   = $this->ci->input->post('travel_information');

        /**
         * Check Purpose of Travel
         */
        if($travelInformation['purpose_of_travel'] == unserialize(TRAVEL)['others']) {
            return true;
        }

        /**
         * Checks Travellers if age > 69
         */
        $age = calculate_age($personalInformation['birthdate']);
        if($age >= AGE_NEEDS_APPROVAL) {
            return true;
        }

        // Members are only checked if Coverage Type is not Individual
        $type = strtolower($this->ci->input->post('travel_information')['coverage_type']);
        if(ucfirst($type) != COVERAGE_INDIVIDUAL) {
            $members = array_values($this->ci->input->post($type.'_member'));

            // Check Travellers
            foreach($members as $member) {
                $age = calculate_age($member['birth_date']);
                if($age >= AGE_NEEDS_APPROVAL) {
                    return true;
                }
            }
        }

        return false;
    }

    private function _generate_token() {
        $this->ci->load->helper('string');
        return random_string('sha1');
    }

    private function _upload_valid_id() {
      
        if (isset($_FILES)) {
            $config['upload_path']      = './files/valid_id/';
            $config['allowed_types']    = 'gif|jpg|png|pdf|doc';
            $config['encrypt_name']     = TRUE;
            $config['max_width']        = '1024';
            $config['max_height']       = '768';

            $this->ci->load->library('upload', $config);

            if ( $this->ci->upload->do_upload('valid_id') ) {
                $upload_data = $this->ci->upload->data();
                $_POST['personal_information']['valid_id'] = $upload_data['file_name'];
            }

            if($this->ci->upload->do_upload('member_valid_id')) {
                $upload_data = $this->ci->upload->data();
                foreach ($upload_data AS $index => $u) {
                    $user_type = $this->ci->input->post('travel_information')['coverage_type'];
                    $_POST[strtolower($user_type) . '_member'][$index]['valid_id'] = $u['file_name'];
                }
            }

            return TRUE;
        }
    }
}