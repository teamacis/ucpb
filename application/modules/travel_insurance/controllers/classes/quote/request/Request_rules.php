<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_rules extends Quote_rule implements Quote_rule_interface {

    public function rules() {

        if ($this->post['submit'] == 'get_quote') {
            $travelInformation = $this->post['travel_information'];

            $rules = array_merge(
                $this->_contact_information(),
                $this->_travel_information()
            );

            // Include Personal Information Rules
            if ($this->post['insurance_cover'] == 'individual') {
                $rules = array_merge(
                    $rules,
                    $this->_personal_information()
                );
            }
            // Include Company Information Rules
            elseif ($this->post['insurance_cover'] == 'company') {
                 $rules = array_merge(
                    $rules,
                    $this->_company_information()
                );
            }
         
            // Include FAMILY rules
            if($travelInformation['coverage_type'] == COVERAGE_FAMILY) {
                $rules = array_merge(
                    $rules,
                    $this->_members('family'),
                    $this->_itineraries('family'),
                    $this->_packages('family')
                );
            }
            // Include GROUP rules
            else if($travelInformation['coverage_type'] == COVERAGE_GROUP) {
                $rules = array_merge(
                    $rules,
                    $this->_members('group'),
                    $this->_itineraries('group'),
                    $this->_packages('group')
                );
            }
            // Include INDIVIDUAL rules
            else if($travelInformation['coverage_type'] == COVERAGE_INDIVIDUAL) {
                $rules = array_merge(
                    $rules,
                    $this->_itineraries('individual')
                );
            }
        } else {
            $rules = [
                [
                    'field' => 'personal_information[email_address]',
                    'label' => 'Email Address',
                    'rules' => ['valid_email', 'required']
                ],
            ];
        }

        return $rules;
    }

    /**
     * Personal Information Rules
     *
     * @return array
     */
    private function _personal_information() {
        $rules = [
            [
                'field' => 'personal_information[fname]',
                'label' => 'First Name',
                'rules' => 'required'
            ],
            [
                'field' => 'personal_information[lname]',
                'label' => 'Last Name',
                'rules' => 'required'
            ],
            [
                'field' => 'personal_information[birthdate]',
                'label' => 'Date of birth',
                'rules' => 'required'
            ],
             [
                'field' => 'personal_information[civil_status]',
                'label' => 'Civil Status',
                'rules' => 'required'
            ]
        ];

        /*$age = calculate_age($this->post['personal_information']['birthdate']);

        if ( $age >= AGE_NEEDS_APPROVAL) {
            $rules[] = [
                'field' => 'personal_information[valid_id]',
                'label' => 'Valid ID',
                'rules' => 'required'
            ];
        }*/

        return $rules;
    }

     /**
     * Company Information Rules
     *
     * @return array
     */
    private function _company_information() {
        return [
            [
                'field' => 'company_information[name]',
                'label' => 'Company Name',
                'rules' => 'required'
            ],
            [
                'field' => 'company_information[contact_person]',
                'label' => 'Contact Person',
                'rules' => 'required'
            ],
            [
                'field' => 'company_information[designation]',
                'label' => 'Designation',
                'rules' => 'required'
            ]
        ];
    }

    /**
     * Contact Information Rules
     *
     * @return array
     */
    private function _contact_information() {
        $rules = [
            [
                'field' => 'personal_information[email_address]',
                'label' => 'Email Address',
                'rules' => ['valid_email', 'required']
            ],
        ];
        if ( ! $this->post['personal_information']['mobile'])
        {
            $rules[] =  [
                            'field' => 'personal_information[telephone]',
                            'label' => 'Telephone or Mobile',
                            'rules' => 'required'
                        ];
        } 
        elseif ( ! $this->post['personal_information']['telephone'])
        {
            $rules[] = [
                            'field' => 'personal_information[mobile]',
                            'label' => 'Telephone or Mobile',
                            'rules' => 'required'
                        ];
        }

        return $rules;
    }

    /**
     * Travel Information Rules
     *
     * @return array
     */
    private function _travel_information() {
        $travelInformation = $this->post['travel_information'];
        $travelSelect      = unserialize(TRAVEL);

        $rules = [
            [
                'field' => 'travel_information[purpose_of_travel]',
                'label' => 'Purpose of Travel',
                'rules' => 'required'
            ],
            [
                'field' => 'travel_information[coverage_type]',
                'label' => 'Type of Coverage',
                'rules' => 'required'
            ]
        ];

        /**
         * Set Rules on Travel Information when purpose of travel is business
         */
        if($travelInformation['purpose_of_travel'] == $travelSelect['others']) {
            $rules[] = [
                'field' => 'travel_information[purpose_of_travel_others]',
                'label' => 'Others',
                'rules' => 'required'
            ];
        }

        // Set Rules on Travel Information fields when coverage type is group
        if($travelInformation['coverage_type'] == COVERAGE_GROUP) {
            $rules[] = [
                'field' => 'travel_information[group_name]',
                'label' => 'Group Name',
                'rules' => 'required'
            ];
        }

        return $rules;
    }

    /**
     * Family/Group Member Rules
     *
     * @return array
     */
    private function _members($type) {
        return [
            [
                'field' => $type.'_member[]',
                'label' => ucfirst($type).' Member',
                'rules' => 'required'
            ]
        ];
    }

    /**
     * Family/Group Itinerary Rules
     *
     * @return array
     */
    private function _itineraries($type) {
        // There's no $type_itinerary on individual
        $itinerary = isset($this->post[$type.'_itinerary']) ? $this->post[$type.'_itinerary'] : [];

        // !isset($itinerary['is_same'] happens on INDIVIDUAL
        if(!isset($itinerary['is_same']) || $itinerary['is_same'] == 'yes') {
            $rules = [
                [
                    'field' => $type.'_itinerary[same]',
                    'label' => ucfirst($type).' Itinerary',
                    'rules' => 'required'
                ]
            ];
        }
        else {
            $rules = [];

            foreach($itinerary['different'] as $key=>$i) {
                $rules = array_merge($rules, [
                    [
                        'field' => $type."_itinerary[different][$key][first_name]",
                        'label' => 'First Name',
                        'rules' => 'required'
                    ],
                    [
                        'field' => $type."_itinerary[different][$key][last_name]",
                        'label' => 'Last Name',
                        'rules' => 'required'
                    ],
                    [
                        'field' => $type."_itinerary[different][$key][destination]",
                        'label' => 'Destination',
                        'rules' => 'required'
                    ],
                    [
                        'field' => $type."_itinerary[different][$key][start_date]",
                        'label' => 'Start Date',
                        'rules' => 'required'
                    ],
                    [
                        'field' => $type."_itinerary[different][$key][end_date]",
                        'label' => 'End Date',
                        'rules' => 'required'
                    ],
                ]);
            }
        }

        return $rules;
    }

    /**
     * Package Rules
     *
     * @return array
     */
    private function _packages($type) {
        $package = $this->post[$type.'_package'];

        if($package['is_same'] == 'yes') {
            $rules = [
                [
                    'field' => $type.'_package[same]',
                    'label' => ucfirst($type).' Package',
                    'rules' => 'required'
                ]
            ];
        }
        else {
            $rules = [];

            foreach($package['different'] as $key=>$i) {
                $rules = array_merge($rules, [
                    [
                        'field' => $type."_package[different][$key][first_name]",
                        'label' => 'First Name',
                        'rules' => 'required'
                    ],
                    [
                        'field' => $type."_package[different][$key][last_name]",
                        'label' => 'Last Name',
                        'rules' => 'required'
                    ],
                    [
                        'field' => $type."_package[different][$key][package]",
                        'label' => 'Package',
                        'rules' => 'required'
                    ]
                ]);
            }
        }

        return $rules;
    }
}