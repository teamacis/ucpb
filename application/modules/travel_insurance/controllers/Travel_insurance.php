<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'classes/quote/Quote.php';
require_once APPPATH.'classes/quote/Quote_interface.php';

require_once APPPATH.'classes/quote/Quote_rule.php';
require_once APPPATH.'classes/quote/Quote_rule_interface.php';

class Travel_insurance extends MY_Controller {

    private $scripts_array_ie = array();
    private $scriptsArray = array();

    public function __construct() {
        parent::__construct();

        $this->scripts_array_ie = array(
            jsUrl('core/html5shiv.min.js'),
            jsUrl('core/respond.min.js')
        );
        $this->srcript_array = array(
            jsUrl('core/jquery.min.js'),
            jsUrl('core/bootstrap.min.js'),
            jsUrl('core/bootstrap-datepicker.min.js'),
            jsUrl('core/moment.js'),
            jsUrl('common.js')
        );
    }

    public function index() {
        $this->redirectDefault();
    }

    /**
     * FACTORY METHOD
     * initialize tab to be use.
     *
     * @param $type
     */
    public function quote($type = null) {
        // If there are not paremeters redirect to the first tab.
        if(!$type) { $this->redirectDefault(); }
        else {
            // Parse the type ex: quote_request to Quote_request.
            $parsedType = ucfirst($type);

            // Check if class for the given type exists.
            if(file_exists(dirname(__FILE__)."/classes/quote/$type/$parsedType.php")) {
                require_once "classes/quote/$type/$parsedType.php";
                $class = new $parsedType();
            }
            else {
                $this->redirectDefault();
            }
        }
    }

    public function success($itp_quote_pk=NULL) {

        $this->load->library('template');
        $this->load->model('travel_insurance/itp_quote_model');
        $this->load->model('transaction/transaction_model');

        $scriptsIE = jsScripts($this->scripts_array_ie);
        $scripts = jsScripts($this->srcript_array);

        $itp_quote = $this->itp_quote_model->get_by_fields(["itp_quotes.pk" => $itp_quote_pk]);

        $this->load->model('travel_insurance/itp_computation_model');
        $computation = $this->itp_computation_model->get($itp_quote_pk);

        if ( ! $itp_quote ) {
            redirect(base_url("travel-insurance"),'refresh');
        }

        $data = array();

        $data['itp_quote']      = $itp_quote;
        $data['transaction']    = $this->transaction_model->get_transaction(ITP, $itp_quote_pk);
        $data['txnid']          = $this->input->get('txnid');
        $data['country_type']   = $computation->country_type;

        $header_data = array('title' => 'Successful Transactions Page', 'scripts' => $scripts, 'scriptsIE' => $scriptsIE);
        $this->load->view('user/header', $header_data);

        $this->template->load('itp_template', 'success', $data);
        $this->load->view('user/footer');
    }

    public function test() {
        $this->load->library('itp_service');
        echo $this->itp_service->pdf_policy(1);
    }

    private function redirectDefault() {
        redirect('travel-insurance/quote/request');
    }   

    public function dump() {
        $agents = array_map('str_getcsv', file('./agents.csv'));

        if ( is_array($agents) ) {
            foreach ( $agents AS $agent ) {
                $aname  = $agent[0];
                $lname  = $agent[1];
                $fname  = $agent[2];
                $mi     = substr($agent[3], 0, 1);
                $suffix = substr($agent[4], 0, 2);
                $loc    = $agent[5];
                $codes  = explode("/", $agent[6]);

                $agency     = $this->db->get_where("agencies", ["name" => $aname]);
                $location   = $this->db->get_where("rate_locations", ["code" => $loc]);

                $agency_pk = 0;

                if ( $agency->num_rows() > 0 ) {
                    $agency_pk = $agency->row()->pk;
                }

                if ( $location->num_rows() > 0 ) {
                    $data = [
                        'agency_pk'         => $agency_pk,
                        'lname'             => $lname,
                        'fname'             => $fname,
                        'mi'                => $mi,
                        'suffix'            => $suffix,
                        'rate_location_pk'  => $location->row()->pk,
                    ];
                    
                    $this->db->insert('agents', $data);
                    $agent_pk = $this->db->insert_id();

                    if ( is_array($codes) ) {
                        foreach ( $codes AS $code ) {
                            $this->db->insert('agent_codes', ['agent_pk' => $agent_pk, 'code' => $code]);
                        }
                    }
                }
            }
        }
    }
}