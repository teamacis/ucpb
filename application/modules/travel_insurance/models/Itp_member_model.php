<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_member_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save($insertFields) {
        $this->db->insert_batch('itp_members', $insertFields);
    }

    public function get($where) {
    	$query = $this->db->get_where('itp_members',$where);

        if($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

    public function number_of_members($itp_quote_pk) {
        $this->db->where('itp_quote_pk', $itp_quote_pk);
        $this->db->from('itp_members');
        return $this->db->count_all_results() + 1;
    }

    public function insert($insertFields) {
        $this->db->insert('itp_members', $insertFields);
        return $this->db->insert_id();
    }
}
