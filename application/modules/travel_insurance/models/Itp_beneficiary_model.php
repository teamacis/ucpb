<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_beneficiary_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}	

	public function save($insertFields=array()) {
        $this->db->insert_batch('itp_beneficiaries', $insertFields);
        return TRUE;
    }

    public function insert($insertFields=array()) {
        $this->db->insert('itp_beneficiaries', $insertFields);
        return $this->db->insert_id();
    }

    public function destroy($itp_quote_pk=0) {
    	return $this->db->delete('itp_beneficiaries',["itp_quote_pk" => $itp_quote_pk]);
    }

    public function get_by_fields($where=array(),$include_member=FALSE) {
    	
    	$result = array();

    	if (is_array($where) AND count($where) > 0) {
    		$this->db->where($where);
    	}

        $this->db->join("itp_members","itp_members.pk = itp_beneficiaries.itp_member_pk","left");
    	$query = $this->db->get("itp_beneficiaries");

        if ( $include_member == TRUE ) {
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() AS $index => $beneficiary) {
                    $member_id = $beneficiary['itp_member_pk'];
                    $result[$member_id][] = $beneficiary;
                }

                return $result;
            }
        } 

    	return $query->result_array();
    }
}

/* End of file ITP_member_contact_model.php */
/* Location: ./application/modules/itp/models/ITP_member_contact_model.php */