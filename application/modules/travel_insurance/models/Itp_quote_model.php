<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_quote_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save($insertFields,$temp=FALSE,$token=NULL,$user_pk=NULL) {

        $table = $temp == TRUE ? 'quotes' : 'itp_quotes';
        $itp_quote = $this->db->get_where($table,["token" => $token]);

        if ( !is_null($token) AND $itp_quote->num_rows() > 0) {
            $this->db->update($table, $insertFields, ["token" => $token]);
            $itp_quote_pk = isset($itp_quote->row()->pk) ? $itp_quote->row()->pk : $token;
        } else {
            if ( $temp == TRUE ) {
                $insertFields['line'] = "itp";
            }
            
            $this->db->insert($table, $insertFields);
            $itp_quote_pk = $this->db->insert_id();

            $this->load->model('client/client_model');
            $this->client_model->save_user_quote( $user_pk, $itp_quote_pk, 'itp' );         
        }      

        return $itp_quote_pk;
    }

    public function update($itp_quote_pk=0, $data=array()) {
        if ($itp_quote_pk > 0 AND count($data) > 0) {
            $this->db->update('itp_quotes',$data,["pk" => $itp_quote_pk]);
            return TRUE;
        }

        return FALSE;
    }

    public function get_by_fields($where) {

        $query = $this->db->where($where)
                        ->join('itp_packages','itp_packages.itp_quote_pk = itp_quotes.pk','inner')
                        ->get('itp_quotes');

        if($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    public function get_saved_quotes($token) {
        $query = $this->db->get_where('quotes',array('token' => $token));

        if ($query->num_rows() == 1) {
            return $query->row();
        }

        return false;
    }

    public function get_saved_quotes_pk($pk=0) {
        $query = $this->db->get_where('quotes',array('pk' => $pk));

        if ($query->num_rows() == 1) {
            return $query->row();
        }

        return false;
    }

    public function generate_policy_id($pk=0,$coverage_type=NULL) {
        $coverage = [
            "Individual"    => "I",
            "Family"        => "F",
            "Group"         => "G"
        ];

        return "PA-{$coverage[$coverage_type]}TP-HO-" . date("y") . "-" . str_pad($pk,6,"0",STR_PAD_LEFT) . "-01";
    }

    public function generate_transaction_id($pk=0) {
        return "ITP-" . date("y") . "-" . str_pad($pk,6,"0",STR_PAD_LEFT);
    }

    public function generate_invoice_number($policy_pk=NULL) {
        if ( ! is_null($policy_pk) ) {
            return "HO-" . str_pad($policy_pk + 8,10, 0, STR_PAD_LEFT);
        }

        return FALSE;
    }

    public function send_email($email,$data) {

        $msg = $this->load->view('travel_insurance/email/success_quotation', $data, TRUE);

        $this->email->clear();
        $this->email->from('itp@ucpbgen.com', 'ITP UCPBGEN');
        $this->email->to($email);
        $this->email->subject("Transaction for ITP Request Number: " . $data['txnid']);
        $this->email->message($msg);

        /**
         * Attachments
         */
        // Policy
        $this->email->attach(POLICIES_FOLDER . 'policy-schedule-' . $data['policy_id'] . '.pdf');

        $this->email->attach(ITP_QUOTE_FILES . 'terms_and_conditions.pdf');

        //$this->email->attach(ITP_QUOTE_FILES_INVOICE . 'invoice-' . $data['txnid'] . '.pdf');

        $this->email->attach(ITP_QUOTE_FILES_APPLICATION . 'application-' . $data['txnid'] . '.pdf');
        // FORMAL QUOTE
        $this->email->attach(FORMAL_QUOTES_FOLDER . $data['txnid'] . '.pdf');

        //$this->email->attach(ITP_QUOTE_FILES . 'warranties_and_clauses.pdf');   

        if ( $data['country_type'] == 3) {
             $this->email->attach(ITP_QUOTE_FILES_COVER . 'confirmation-cover-' . $data['txnid'] . '.pdf');  
        }

        $this->email->send();
    }
}
