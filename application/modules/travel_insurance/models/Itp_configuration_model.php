<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_configuration_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get($code=NULL) {

		if ( ! is_null($code)) {
			$config = $this->db->get_where("itp_configurations",["code" => $code]);

			if ($config->num_rows() > 0) {
				return $config->row()->value;
			}
		}

		return FALSE;
	}

}

/* End of file ITP_configuration_model.php */
/* Location: ./application/modules/itp/models/ITP_configuration_model.php */