<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_quote_agent_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_by_fields($where) {
        $query = $this->db->where($where)->get('itp_quote_agents');

        if($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    public function save($itp_quote_pk=0,$fields) {
        $insert = [];

        $insert['itp_quote_pk'] = $itp_quote_pk;

        if($fields['agent_type'] == 'Agent Name') {
            $insert['is_name'] = true;
            $insert['fname'] = $fields['agent_fname'];
            $insert['lname'] = $fields['agent_lname'];
        }
        elseif($fields['agent_type'] == 'Agent Code') {
            $insert['is_name'] = false;
            $insert['code'] = $fields['agent_code'];

            $query  = $this->db->join("agent_codes","agent_codes.agent_pk=agents.pk","left")->where(['code' => $fields['agent_code']])->get('agents');
            $result = $query->row_object();
            $insert['fname'] = $result->fname;
            $insert['lname'] = $result->lname;
        } else {
            $insert['is_name'] = false;
            $insert['company'] = $fields['agency_name'];

            $query  = $this->db->join("agencies","agencies.pk=agents.agency_pk","left")->where(['agencies.name' => $fields['agency_name']])->get('agents');
            $result = $query->row_object();
            $insert['fname'] = $result->fname;
            $insert['lname'] = $result->lname;
        }

        return $this->db->insert('itp_quote_agents', $insert);

    }

    public function destroy_by_field($field, $value) {
        return $this->db->delete('itp_quote_agents', [$field => $value]);
    }

    public function fetch_by_field($field, $value) {
        $return = false;

        $this->db->where($field, $value);
        $this->db->from('itp_quote_agents');

        $query = $this->db->get();
        if($query->num_rows()) {
            return $query->row_array();
        }
        return $return;
    }
}

/* End of file ITP_agents_model.php */
/* Location: ./application/modules/itp/models/ITP_agents_model.php */