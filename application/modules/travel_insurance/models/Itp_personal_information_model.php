<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_personal_information_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function fetch_by_quote_pk($itp_quote_pk) {
        $this->db->where('quotes_pk', $itp_quote_pk);
        $this->db->from('users_quotes');

        $query = $this->db->get();
        if($query->num_rows()) {
            $result = $query->row_object();
            return $this->fetch_by_field('users_quotes_pk', $result->pk);
        }

        return false;
    }

    public function fetch_by_field($field, $value) {
        $return = false;

        $this->db->where($field, $value);
        $this->db->from('personal_informations');

        $query = $this->db->get();
        if($query->num_rows()) {
            $return = $query->row_array();
            $province   = $this->db->get_where('places', ["pk" => $return['province']])->row()->place;
            $city       = $this->db->get_where('places', ["pk" => $return['city']])->row()->place;
            $barangay   = $this->db->get_where('places', ["pk" => $return['barangay']])->row()->place;

            $return['barangay']     = $barangay;
            $return['city']         = $city;
            $return['province']     = $province;
            $return['full_name']    = $return['salutation'] . ' ' . $return['lname'] . ', ' . $return['fname'] . ' ' . $return['mname'] . '.';
            $return['full_address'] = $return['house_number'] . ' ' . $barangay . ' ' . $city . ' ' . $province;

        }
        
        return $return;
    }

    public function save($fields, $itp_quote_pk) {
        $insert = [];

        /**
         * Fix Generic Fields
         */
        $requiredFields = ['fname', 'mname', 'lname', 'suffix', 'gender', 'salutation', 'birthdate', 'nationality', 'occupation', 'house_number', 'telephone', 'mobile', 'email_address'];
        foreach($requiredFields as $f) {
            if(isset($fields[$f])) {
                $insert[$f] = $fields[$f];
            }
        }

        /**
         * Fix Tin
         */
        if($fields['with_tin'] == 1) {
            $insert['tin'] = $fields['tin'];
        }
        else {
            $insert['id_type'] = $fields['id_type'];
            $insert['id_number'] = $fields['id_number'];
        }

        /**
         * Fix Places
         */
        if(isset($fields['same_location']) && $fields['same_location'] == 'on') {
            $insert['province'] = $fields['default_province'];
            $insert['city'] = $fields['default_city'];
            $insert['barangay'] = $fields['default_barangay'];
        }
        else {
            $insert['province'] = $fields['province'];
            $insert['city'] = $fields['city'];
            $insert['barangay'] = $fields['barangay'];
        }

        $query = $this->db->select('pk')->where(['quotes_pk' => $itp_quote_pk, 'line' => 'itp'])->get('users_quotes');
        $insert['users_quotes_pk'] = $query->row_object()->pk;

        $this->db->insert('personal_informations', $insert);
    }

    public function destroy($itp_quote_pk) {
        $query = $this->db->select('pk')->where('quotes_pk', $itp_quote_pk)->get('users_quotes');

        /**
         * Delete Personal Information
         */
        $this->db->delete('personal_informations', ['users_quotes_pk' => $query->row_object()->pk]);
    }
}