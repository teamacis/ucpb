<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_premium_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_by_fields($where) {
		$query = $this->db->where($where)
						  ->limit(1)
						  ->order_by("days","asc")
			              ->get('itp_premiums');

		if($query->num_rows() > 0) {
			return $query->row_array();
		}
		else {
			return false;
		}
	}

}
