<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_itinerary_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save($insertFields) {
        $this->db->insert_batch('itp_itineraries', $insertFields);
    }

    public function get($where,$is_array=FALSE) {
    	$query = $this->db->where($where)
    				->get('itp_itineraries ii');

        if($query->num_rows() > 0) {
            if ( $is_array ) {
                return $query->result_array();
            } else {
                return $query->result();
            }         
        }

        return false;
    }

}
