<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_computation_model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    public function save($insertFields) {
        $this->db->insert('itp_computation', $insertFields);
    }

    public function get($itp_quote_pk) {
        return $this->db->get_where("itp_computation", ["itp_quote_pk" => $itp_quote_pk])->row();
    }

    public function get_computation($itp_quote_pk=0) {

        $this->load->model('itp_configuration_model');
        $peso_value = $this->itp_configuration_model->get('peso_value');

    	$breakdown_sql = "SELECT 
    						ia.amount,
    						ip.days,
    						ip.ugic,
    						ip.ibero
    					FROM itp_amounts ia
    					INNER JOIN itp_premiums ip 
    						ON ip.pk = ia.itp_premium_pk
    					WHERE 
    						ia.itp_quote_pk = '{$itp_quote_pk}'
    					";

    	$breakdown = $this->db->query($breakdown_sql)->result();

    	$total_sql 	= "SELECT
    						COUNT(ia.pk) AS count,
    						SUM(ia.amount) * {$peso_value} AS amount
    					FROM itp_amounts ia
    					WHERE 
    						ia.itp_quote_pk = '{$itp_quote_pk}'
    				";

    	$total_query = $this->db->query($total_sql);

    	if ($total_query->num_rows() > 0) {
    		$total = $total_query->row();

    		if ($total->count >= ITP_TOTAL_MEMBERS_WITH_DISCOUNT) {
    			$discount = $total->amount * '.' . ITP_DISCOUNT;
    		} else {
    			$discount = 0;
    		}

            $this->load->model('travel_insurance/itp_quote_agent_model');
            $this->load->model('agent/agent_model', 'agent');
            $this->load->model('rate/rate_model', 'rate');

            // Fetch Quote Agent First
            $itp_agent = $this->itp_quote_agent_model->fetch_by_field('itp_quote_pk', $itp_quote_pk);
            
            // Then Get Agent depending on the NAME or CODE
            $agent           = $itp_agent['is_name'] == 1 ? $this->agent->fetch_by_name($itp_agent['fname'], $itp_agent['lname'])
                                                                : $this->agent->fetch_by_field('code', $itp_agent['code']);
            // Then Fetch the Rate
            $rate            = $this->rate->get_rate_by_rate_location_pk($agent['rate_location_pk']);

            // Get the percentage!
            $percentage      = $rate ? $rate->rate : 0.002;
           
            $vat_percentage = $this->itp_configuration_model->get('vat_percentage');

            $doc_stamps = floor($total->amount / 4) * .50;
            $remainder  = fmod($total->amount, 4);
            $doc_stamps = ! $remainder ? $doc_stamps : $doc_stamps + 0.50;
            $vat        = $total->amount * ($vat_percentage / 100);
            $local_gov_tax = $total->amount * $percentage;

    		$amount = [
    			"net_premium"	=> $total->amount,
    			"discount"	    => $discount,
                "doc_stamps"    => $doc_stamps,
                "local_gov_tax" => $local_gov_tax,
                "vat"           => $vat,
    			"total"		    => array_sum([$total->amount - $discount, $doc_stamps, $local_gov_tax, $vat])
    		];
    	} else {
    		return FALSE;
    	}

    	return [
    		"breakdown"	=> $breakdown,
    		"amount"	=> $amount
    	];
    }

    public function compute($itp_quote=NULL) {
        if ( ! is_null($itp_quote) ) {

            $this->load->model('itp_configuration_model');
            $peso_value = $this->itp_configuration_model->get('peso_value');

            $itp_quote_request      = json_decode($itp_quote->request,TRUE);
            $personal_information   = json_decode($itp_quote->personal_information,TRUE);

            $number_of_days = $this->_compute_number_of_days($itp_quote_request);
            $number_of_consecutive_days = $this->_compute_max_consecutive_days($itp_quote_request);

            $net_amount     = $this->_get_net_amount($itp_quote_request, $number_of_days,$number_of_consecutive_days);

            $user_type      = $itp_quote_request['travel_information']['coverage_type'];
            $person_count   = 1;

            if ($user_type != COVERAGE_INDIVIDUAL) {
                $members      = $itp_quote_request[strtolower($user_type) . '_member'];
                $person_count += count($members);
            }

            if ($person_count >= ITP_TOTAL_MEMBERS_WITH_DISCOUNT) {
                $discount = $net_amount * '.' . ITP_DISCOUNT;
            } else {
                $discount = 0;
            }

            $this->load->model('itp/itp_quote_agent_model');
            $this->load->model('agent/agent_model', 'agent');
            $this->load->model('rate/rate_model', 'rate');
            
            // Then Get Agent depending on the NAME or CODE
            $agent = strlen($personal_information['agent_code']) > 0 ? $this->agent->fetch_by_field('code', $personal_information['agent_code'])
                                                                : $this->agent->fetch_by_name($personal_information['agent_fname'], $personal_information['agent_lname']);
            // Then Fetch the Rate
            $rate            = $this->rate->get_rate_by_rate_location_pk($agent['rate_location_pk']);

            // Get the percentage!
            $percentage      = $rate ? $rate->rate : 0.002;
          
            $vat_percentage = $this->itp_configuration_model->get('vat_percentage');

            $doc_stamps = floor($net_amount / 4) * .50;
            $remainder  = fmod($net_amount, 4);
            $doc_stamps = ! $remainder ? $doc_stamps : $doc_stamps + 0.50;
            $vat        = $net_amount * ($vat_percentage / 100);
            $local_gov_tax = $net_amount * $percentage;



            return $amount = [
                "number_of_days" => $number_of_days,
                "net_premium"   => $net_amount,
                "discount"      => $discount,
                "doc_stamps"    => $doc_stamps,
                "local_gov_tax" => $local_gov_tax,
                "vat"           => $vat,
                "total"         => array_sum([$net_amount - $discount, $doc_stamps, $local_gov_tax, $vat])
            ];
        }

        return FALSE;
    }

    private function _compute_number_of_days($itp_quote=NULL) {

        if ( ! is_null($itp_quote)) {
            $number_of_days = 0;

            $type           = strtolower($itp_quote['travel_information']['coverage_type']);
            $itineraries    = $itp_quote[$type.'_itinerary'];

            $itineraries    = !isset($itineraries['is_same']) || $itineraries['is_same'] == 'yes' ? array_values($itineraries['same']) : array_values($itineraries['different']);
            
            $start_dates    = array_column($itineraries, "start_date");
            $end_dates      = array_column($itineraries, "end_date");

            sort($start_dates);
            rsort($end_dates);

            $start_date = isset($start_dates[0]) ? $start_dates[0] : FALSE;
            $end_date   = isset($end_dates[0]) ? $end_dates[0] : FALSE;

            $date1 = new DateTime($start_date);
            $date2 = new DateTime($end_date);
            $interval = $date1->diff($date2);
            
            $number_of_days = $interval->format('%a') + 1;
           
            return $number_of_days;
        }

        return FALSE;
    }

    private function _compute_max_consecutive_days($itp_quote=NULL) {
        if ( ! is_null($itp_quote)) {

            $number_of_consecutive_days_array = array();
            $number_of_consecutive_days = 0;
            $end_date = false;

            $type         = strtolower($itp_quote['travel_information']['coverage_type']);
            $itineraries  = $itp_quote[$type.'_itinerary'];

            $itineraries = !isset($itineraries['is_same']) || $itineraries['is_same'] == 'yes' ? array_values($itineraries['same']) : array_values($itineraries['different']);
        
            foreach ($itineraries AS $itineary) {
                $num_days_from_last = 0;

                $date_start = new DateTime($itineary['start_date']);
                $date_end = new DateTime($itineary['end_date']);
                $interval = $date_start->diff($date_end);

                if ($end_date) {
                    // get the interval of from the last itinerary end date
                    $date_end_from_last = new DateTime($end_date);
                    $interval_from_last = $date_end_from_last->diff($date_start);
                    $num_days_from_last = $interval_from_last->format('%a');
                }

                if ($num_days_from_last == 0) {
                    $number_of_consecutive_days += $interval->format('%a');
                } else {
                    $number_of_consecutive_days = $interval->format('%a');
                }

                $end_date = $itineary['end_date'];

                $number_of_consecutive_days_array[] = $number_of_consecutive_days;
            }

            return max($number_of_consecutive_days_array);
        }

        return FALSE;
    }

    private function _get_net_amount($itp_quote=NULL,$number_of_days = 0,$number_of_consecutive_days=0) {

        $net_amount = 0;

        if ( ! is_null($itp_quote)) {
            $country_type = $this->get_country_type($itp_quote);
            $age    = calculate_age($itp_quote['personal_information']['birth_date']);
            $double = $age >= AGE_DOUBLE_AMOUNT ? TRUE : FALSE;
            $net_amount += $this->_compute_amount($itp_quote, $country_type, $number_of_days, $number_of_consecutive_days, $double);

            $user_type      = $itp_quote['travel_information']['coverage_type'];
            if ($user_type != COVERAGE_INDIVIDUAL) {
                $members        = $itp_quote[strtolower($user_type) . '_member'];

                if ($members) {
                    foreach ($members AS $member) {

                        $member_age = calculate_age($member['birth_date']);
                        $is_double  = $member_age >= AGE_DOUBLE_AMOUNT ? TRUE : FALSE;

                        $net_amount += $this->_compute_amount($itp_quote, $country_type, $number_of_days, $number_of_consecutive_days, $double);
                    }
                }
            }
        }

        return $net_amount;
    }

    private function _compute_amount($itp_quote=NULL,$country_type=0,$number_of_days = 0, $number_of_consecutive_days=0, $double = FALSE) {
        if ( ! is_null($itp_quote)) {

            $user_type      = $itp_quote['travel_information']['coverage_type'];
            $premium_type   = $itp_quote[strtolower($user_type) . '_package'];

            $where = [
                'user_type'             => $user_type,
                'days >= '              => $number_of_days,
                'premium_type'          => isset($premium_type['same']) ? $premium_type['same'] : $premium_type['different'],
                'itp_country_type_pk'   => $country_type
            ];
            
            if ($number_of_days >= 180 AND $number_of_days < 365) {
                if ($number_of_consecutive_days > 90 AND $number_of_consecutive_days <= 180) {
                    $where["is_max"] = 1;
                } elseif ($number_of_consecutive_days <= 90) {
                    $where["is_max"] = 0;
                }
            } elseif ($number_of_days >= 365) {
                if ($number_of_consecutive_days > 180 AND $number_of_consecutive_days <= 365) {
                    $where["is_max"] = 1;
                } elseif ($number_of_consecutive_days <= 180) {
                    $where["is_max"] = 0;
                }
            }
           
            $premium  = $this->itp_premium_model->get_by_fields($where); 
       
            if ($premium) {
                $amount = $double == TRUE ? (floatval($premium['ugic']) * 2) : floatval($premium['ugic']);
                return $amount;
            }
        }

        return FALSE;
    }

    public function get_country_type($itp_quote=NULL) {

        if ( ! is_null($itp_quote) ) {
            $type         = strtolower($itp_quote['travel_information']['coverage_type']);
            $itineraries  = $itp_quote[$type.'_itinerary'];

            $itineraries = !isset($itineraries['is_same']) || $itineraries['is_same'] == 'yes' ? array_values($itineraries['same']) : array_values($itineraries['different']);
        
            $destinations = array_column($itineraries, "destination");

            return $this->itp_country_model->get_highest_type_by_ids($destinations);
        }

        return FALSE;
    }
}

/* End of file ITP_amount_model.php */
/* Location: ./application/modules/itp/models/ITP_amount_model.php */