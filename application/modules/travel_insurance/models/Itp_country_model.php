<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_country_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get() {
        $query = $this->db->where('is_blacklisted = false')
                          ->order_by('country ASC')
                          ->get('itp_countries');

        if($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

    public function get_by_fields($where) {
        $query = $this->db->where($where)->get('itp_countries');

        if($query->num_rows() > 0) {
            return $query->row_array();
        }

        return false;
    }

    public function get_highest_type_by_ids($country_ids=array()) {
        $query = $this->db->where_in("pk",$country_ids)
                            ->order_by("itp_country_type_pk","desc")
                            ->get('itp_countries');

        if($query->num_rows() > 0) {
            return $query->row()->itp_country_type_pk;
        }

        return false;
    }

    public function get_countries($countries=NULL) {
        if ( ! is_null($countries) ) {

            $countries = ltrim($countries,',');

            $sql = "SELECT
                        GROUP_CONCAT(TRIM(country)) AS countries
                    FROM itp_countries
                    WHERE pk IN({$countries})
                    ";

            $query = $this->db->query($sql);

            if ( $query->num_rows() > 0 ) {
                return $query->row()->countries;
            }
        }

        return FALSE;
    }
}