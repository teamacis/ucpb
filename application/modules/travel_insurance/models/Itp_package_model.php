<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itp_package_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save($insertFields) {
        $this->db->insert_batch('itp_packages', $insertFields);
    }

}
