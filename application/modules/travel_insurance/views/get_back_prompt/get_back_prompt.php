<div class="container motor_content box success_page">
    <div class="row no_gutter"><div class="s_content col-md-6">
            <h4>Thank you for requesting a quote from us!</h4>
            <div class="final_message">
                <p>A representative of UCPB GEN will get back to you within 2 working days.</p>
            </div>
            <p class="form_button">
                <button type="button" class="btn btn-success btn-lg" onclick="window.location.href = '<?php echo base_url("travel-insurance"); ?>'">Home</button>
            </p>
        </div>    </div>
</div>