<?php $this->load->view('user/quote_sidebar', array('tab' => 'travel-insurance'));?>

<div class="col-md-9 col-sm-9">
    <div class="col-md-12 title_bar">
        <h3>Quote Request</h3>
        <p></p>
    </div>
    <?php if ($this->session->flashdata('success_message')): ?>
        <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success_message'); ?></div>
    <?php endif; ?>
    <?php if (isset($error_message) AND strlen($error_message) > 0): ?>
        <div class="alert alert-danger" role="alert"><?php echo $error_message; ?></div>
    <?php endif; ?>
    <div class="col-md-12 content_field">
        <form class="form_style" name="quote-request-form" method="POST" accept-charset="utf-8" enctype="multipart/form-data" action="<?= site_url('travel-insurance/quote/request'); ?>">
            <?php echo form_hidden('token', $token); ?>
            <?php $this->load->view('travel_insurance/request/parts/all/insurance_type'); ?>

            <?php $this->load->view('travel_insurance/request/parts/all/personal_information'); ?>
            <?php $this->load->view('travel_insurance/request/parts/all/company_information'); ?>
            <?php $this->load->view('travel_insurance/request/parts/all/contact_information'); ?>
            <?php $this->load->view('travel_insurance/request/parts/all/travel_information'); ?>

            <!-- Family Members !-->
            <div id="family-fields" class="family_group" <?php if($this->input->post('travel_information')['coverage_type'] != COVERAGE_FAMILY): ?>style="display: none;"<?php endif; ?>>
                <?php $this->load->view('travel_insurance/request/parts/family/itinerary'); ?>
                <?php $this->load->view('travel_insurance/request/parts/family/members'); ?>            
                <?php $this->load->view('travel_insurance/request/parts/family/package'); ?>
            </div>

            <!-- Group Members !-->
            <div id="group-fields" class="group_group" <?php if($this->input->post('travel_information')['coverage_type'] != COVERAGE_GROUP): ?>style="display: none;"<?php endif; ?>>
                <?php $this->load->view('travel_insurance/request/parts/group/itinerary'); ?>
                <?php $this->load->view('travel_insurance/request/parts/group/members'); ?>               
                <?php $this->load->view('travel_insurance/request/parts/group/package'); ?>
            </div>

            <!-- Individual -->
            <div id="individual-fields" class="form_content content_bg" <?php if($this->input->post('travel_information')['coverage_type'] != COVERAGE_INDIVIDUAL): ?>style="display: none;"<?php endif; ?>>
                <?php $this->load->view('travel_insurance/request/parts/individual/itinerary'); ?>
                <?php $this->load->view('travel_insurance/request/parts/individual/package'); ?>
            </div>

            <?php //$this->load->view('travel_insurance/request/parts/all/documents'); ?>
            
            <p class="form_button">
                <button type="submit" name="submit" value="save" class="btn btn-primary btn-lg">Save</button>
                <button type="submit" name="submit" value="get_quote" class="btn btn-success btn-lg">Get Quote</button>
            </p>
        </form>
    </div>
    <!-- END OF CONTENT FIELD -->


    <!-- Delivery Date Modal -->
    <div class="modal fade terms_condition_modal" id="work-warning-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 500px;">
            <div class="modal-content">
                <button type="button" class="modal_close_btn" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                        <img src="<?=base_url('assets/images/site_logo2.png')?>" class="logo" />
                    </h4>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger">
                        Work-related accidents during the trip are not covered.
                    </div>
                    <div align="center">
                        <a href="" data-dismiss="modal" onclick="acceptTerms()"><b>Accept Disclaimer</b></a><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="countries-list" style="display: none">
        <span></span>
        <?php foreach($countries as $country): ?>
            <span data-pk="<?= $country->pk; ?>" data-country-type-pk="<?= $country->itp_country_type_pk; ?>" data-region="<?= $country->region; ?>">
                <?= $country->country; ?>
            </span>
        <?php endforeach; ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".modal_close_btn").click(function(e){
            alert("Please read and accept disclaimer.");
            return false;
        });
    });
</script>