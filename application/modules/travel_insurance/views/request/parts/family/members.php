<div id="family-members" class="form_content" data-property="members_box">
    <div class="line_title"><p>Family Members</p></div>
    <div class="form_line">
        <div id="family-members-list" class="col-md-8 form_input" data-property="members_list">
            <!-- Javascript Appends the Family Members Here !-->

            <!-- This is the initial members when you submit a form and redirected back because of validations !-->
            <?php if($this->input->post('family_member')): ?>
                <?php foreach($this->input->post('family_member') as $key=>$member): ?>
                <div class="gray-box" data-dependency-control="$dependencyControl">
                    <div class="item_choice">
                        <span class="value_name"><?= $member['first_name']; ?> <?= $member['last_name']; ?></span>
                        <span class="value_bday"><?= $member['birth_date']; ?></span>
                        <span class="value_relations"><?= $member['relationship']; ?></span>
                        <span class="value_civil_status"><?= $member['civil_status']; ?></span>
                        <input type="hidden" name="family_member[<?= $key; ?>][first_name]" value="<?= $member['first_name']; ?>">
                        <input type="hidden" name="family_member[<?= $key; ?>][last_name]" value="<?= $member['last_name']; ?>">
                        <input type="hidden" name="family_member[<?= $key; ?>][birth_date]" value="<?= $member['birth_date']; ?>">
                        <input type="hidden" name="family_member[<?= $key; ?>][relationship]" value="<?= $member['relationship']; ?>">
                        <input type="hidden" name="family_member[<?= $key; ?>][civil_status]" value="<?= $member['civil_status']; ?>">
                        <a href="javascript:void(0)" class="btn_close member-remove"></a>
                    </div>
                </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="form_line">
        <div class="col-md-4 form_input">
            <input type="text" class="form-control required" placeholder="Last Name" name="last_name" />
        </div>
        <div class="col-md-4 form_input">
            <input type="text" class="form-control required" placeholder="First Name" name="first_name" />
        </div>
    </div>
    <div class="form_line">
        <div class="col-md-4 form_input">
            <input type="text" class="form-control calendar_input required member_birth_date" placeholder="Date of Birth" name="birth_date" />
        </div>
        <div class="col-md-4 form_input">
            <select class="form-control  chosen-select required" data-placeholder="Relations" name="relationship">
                <option></option>
                <?php foreach ($relations AS $value => $title): ?>
                    <option value="<?php echo $value; ?>"><?php echo $title; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-4 form_input">
            <?php echo form_dropdown('civil_status', $civil_statuses, '',' class="form-control chosen-select required" data-placeholder="Civil Status"'); ?>
        </div>
        <div class="col-md-12 form_input" style="margin: 0;">
            <?= form_error('family_member[]'); ?>
        </div>
    </div>
    <div class="form_line">
        <button type="button" class="btn btn_add" name="family-member-add-btn"></button>
    </div>
</div>