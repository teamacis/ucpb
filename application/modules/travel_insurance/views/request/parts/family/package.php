<div class="form_content">
    <!-- <div class="form_line">
        <div class="radio_type">
            <p class="radio_type_title">Do you want the same package for all the members?</p>
            <div class="radio_group">
                <label class="radio-inline">
                    <input type="radio" name="family_package[is_same]" id="inlineRadio5" value="yes" <?php if(in_array($this->input->post('family_package')['is_same'], ['yes', ''])): ?> checked <?php endif; ?>> Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" name="family_package[is_same]" id="inlineRadio6" value="no" <?php if($this->input->post('family_package')['is_same'] == 'no'): ?> checked <?php endif; ?>> No
                </label>
            </div>
        </div>
    </div> -->
    <input type="hidden" name="family_package[is_same]" value="yes">

    <?php $packageSelect = unserialize(PACKAGE); ?>

    <!-- Family Same Package !-->
    <div id="family-same-package" class="form_line" <?php // if($this->input->post('family_package')['is_same'] == 'no'): echo 'style="display: none;"'; endif; ?>>
        <div class="line_title"><p>Package</p></div>
        <div class="col-md-4 form_input">
            <select name="family_package[same]" id="family_package" class="form-control chosen-select required package" data-placeholder="Package">
                <option></option>
                <?php foreach($packageSelect as $key=>$label): ?>
                    <option value="<?= $label; ?>" <?php if($this->input->post('family_package')['same'] == $label): ?> selected <?php endif; ?>>
                        <?= $label; ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <?= form_error('family_package[same]'); ?>
        </div>
    </div>

    <!-- Family Different Package !-->
    <div id="family-different-package" class="group_line" <?php // if($this->input->post('family_package')['is_same'] != 'no'): ?> style="display: none;" <?php // endif; ?>>

        <?php if(!$this->input->post('family_itinerary')['different']): ?>
            <div class="gray-box-field item_field">
                <div class="col-md-4 form_input al_ver">
                    <div class="check_box">
                        <input type="checkbox" />
                    </div>
                    <span class="value_field name">
                        <span data-dependency="personal_information_first_name"></span>
                        <span data-dependency="personal_information_last_name"></span>
                    </span>
                    <input type="hidden" data-dependency="personal_information_first_name" class="form-control required first_name" name="family_package[different][100000][first_name]" />
                    <input type="hidden" data-dependency="personal_information_last_name" class="form-control required last_name" name="family_package[different][100000][last_name]" />
                </div>
                <div class="col-md-8 input_group">
                    <div class="col-md-12 form_input">
                        <select class="form-control chosen-select required package" data-placeholder="Package" name="family_package[different][100000][package]" data-type="package">
                            <option></option>
                            <?php foreach($packageSelect as $key=>$label): ?>
                                <option <?php if(isset($package['package']) && $package['package'] == $label): ?> selected <?php endif; ?>><?= $label; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <?php foreach($this->input->post('family_package')['different'] as $key=>$package): ?>
                <div class="gray-box-field item_field">
                    <div class="col-md-4 form_input al_ver">
                        <div class="check_box">
                            <input type="checkbox" />
                        </div>
                    <span class="value_field name">
                        <span <?php if($key == 100000): ?>data-dependency="personal_information_first_name"<?php endif; ?>>
                            <?= $package['first_name']; ?>
                        </span>
                        <span <?php if($key == 100000): ?>data-dependency="personal_information_last_name"<?php endif; ?>>
                            <?= $package['last_name']; ?>
                        </span>
                    </span>
                        <input type="hidden" class="form-control required first_name" name="family_package[different][<?= $key; ?>][first_name]"
                               <?php if($key == 100000): ?>data-dependency="personal_information_first_name"<?php endif; ?>
                               value="<?= $package['first_name']; ?>" />
                        <input type="hidden" class="form-control required last_name" name="family_package[different][<?= $key; ?>][last_name]"
                               <?php if($key == 100000): ?>data-dependency="personal_information_last_name"<?php endif; ?>
                               value="<?= $package['last_name']; ?>" />
                    </div>
                    <div class="col-md-8 input_group">
                        <div class="col-md-12 form_input">
                            <select class="form-control chosen-select required package" data-placeholder="Package" name="family_package[different][<?= $key; ?>][package]" data-type="package">
                                <option></option>
                                <?php foreach($packageSelect as $key=>$label): ?>
                                    <option <?php if($package['package'] == $label): ?> selected <?php endif; ?>><?= $label; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?= form_error("family_package[different][$key][package]"); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>