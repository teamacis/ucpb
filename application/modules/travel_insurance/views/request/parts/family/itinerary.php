<div class="form_content">
    <!-- <div class="form_line">
        <div class="radio_type">
            <p class="radio_type_title">Do all members have the same itinerary</p>
            <div class="radio_group">
                <label class="radio-inline">
                    <input type="radio" name="family_itinerary[is_same]" id="inlineRadio3" value="yes" <?php if(in_array($this->input->post('family_itinerary')['is_same'], ['yes', ''])): ?> checked <?php endif; ?>> Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" name="family_itinerary[is_same]" id="inlineRadio4" value="no" <?php if($this->input->post('family_itinerary')['is_same'] == 'no'): ?> checked <?php endif; ?>> No
                </label>
            </div>
        </div>
    </div> -->
    <input type="hidden" name="family_itinerary[is_same]" value="yes">

    <!-- Same Itinerary !-->
    <div id="family-same-itinerary" class="group_line gray-box-field" data-country-type="family" <?php if($this->input->post('family_itinerary')['is_same'] == 'no'): ?> style="display: none;" <?php endif; ?>>
        <div class="line_title"><p>Itinerary</p></div>
        <div class="form_line">
            <div class="col-md-8 form_input itinerary-box" id="family-same-itinerary-list">
                <!-- Append Itinerary here !-->

                <?php if(isset($this->input->post('family_itinerary')['same']) && $this->input->post('family_itinerary')['same']): ?>
                    <?php foreach($this->input->post('family_itinerary')['same'] as $key=>$itinerary): ?>
                        <div class="gray-box">
                            <div class="item_choice">
                                <span class="value_destination"><?= $this->itp_country_model->get_countries($itinerary['destination']); ?></span>
                                <span class="value_sday"><?= $itinerary['start_date']; ?></span>
                                <span class="value_eday"><?= $itinerary['end_date']; ?></span>
                                <input type="hidden" name="family_itinerary[same][<?= $key; ?>][destination]" value="<?= $itinerary['destination']; ?>">
                                <input type="hidden" class="start_date" name="family_itinerary[same][<?= $key; ?>][start_date]" value="<?= $itinerary['start_date']; ?>">
                                <input type="hidden" class="end_date" name="family_itinerary[same][<?= $key; ?>][end_date]" value="<?= $itinerary['end_date']; ?>">
                                <a href="javascript:void(0)" class="btn_close same-itinerary-remove"></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form_line">
            <div class="col-md-4 form_input">
                <select class="form-control required destination chosen-select" multiple data-placeholder="Destination" name="destination">

                </select>
            </div>
        </div>
        <div class="form_line">
            <div class="col-md-4 form_input">
                <input type="text" class="form-control calendar_input required date start_date" placeholder="Date of Departure from the Philippines" name="start_date" />
            </div>
            <div class="col-md-4 form_input">
                <input type="text" class="form-control calendar_input required date end_date" placeholder="Date of Arrival to the Philippines" name="end_date" />
            </div>
            <div class="col-md-8 form_input">
                <?= form_error('family_itinerary[same]'); ?>
            </div>
        </div>
        <div class="form_line">
            <button class="btn btn_add" name="family-member-same-itinerary-add-btn" type="button"></button>
        </div>
        <p class="small_text">Note: Please don't include stopovers</p>
    </div>

    <!-- Different Itinerary !-->
    <div id="family-different-itinerary" class="group_line" <?php if($this->input->post('family_itinerary')['is_same'] != 'no'): ?> style="display: none;" <?php endif; ?>>
        <!-- Append Itinerary here !-->

        <?php if(!$this->input->post('family_itinerary')['different']): ?>
            <div class="gray-box-field item_field" data-type="family">
                <div class="col-md-4 form_input al_ver">
                    <div class="check_box"><input type="checkbox" /></div>
                    <a href="javascript:void(0)" class="btn_plus active different-itinerary-add"></a>
                    <span class="value_field name">
                        <span data-dependency="personal_information_first_name"></span>
                        <span data-dependency="personal_information_last_name"></span>
                    </span>
                    <input type="hidden" data-dependency="personal_information_first_name" class="form-control required first_name" name="family_itinerary[different][100000][first_name]" />
                    <input type="hidden" data-dependency="personal_information_last_name" class="form-control required last_name" name="family_itinerary[different][100000][last_name]" />
                    <input type="hidden" class="form-control required" name="family_itinerary[different][100000][is_me]" value="true" />
                </div>
                <div class="col-md-8 input_group">
                    <div class="col-md-5 form_input">
                        <select class="form-control required destination chosen-select" multiple data-placeholder="Destination" name="family_itinerary[different][100000][destination]" data-type="destination">

                        </select>
                    </div>
                    <div class="col-md-3 form_input">
                        <input type="text" class="form-control calendar_input required date start_date" placeholder="Start Date" name="family_itinerary[different][100000][start_date]" data-type="start_date" />
                    </div>
                    <div class="col-md-3 form_input">
                        <input type="text" class="form-control calendar_input required date end_date" placeholder="End Date" name="family_itinerary[different][100000][end_date]" data-type="end_date"/>
                    </div>
                    <div class="col-md-1 form_input btn_type"><a href="javascript:void(0)" class="btn_remove active different-itinerary-remove"></a></div>
                </div>
            </div>
        <?php else: ?>
            <?php foreach($this->input->post('family_itinerary')['different'] as $key=>$itinerary): ?>
                <div class="gray-box-field item_field" data-type="family">
                    <div class="col-md-4 form_input al_ver">
                        <div class="check_box"><input type="checkbox" /></div>
                        <a href="javascript:void(0)" class="btn_plus active different-itinerary-add"></a>
                            <span class="value_field name">
                                <span <?php if($itinerary['is_me'] == 'true'): ?>data-dependency="personal_information_first_name"<?php endif; ?>>
                                    <?= $itinerary['first_name']; ?>
                                </span>
                                <span <?php if($itinerary['is_me'] == 'true'): ?>data-dependency="personal_information_last_name"<?php endif; ?>>
                                    <?= $itinerary['last_name']; ?>
                                </span>
                            </span>
                        <input type="hidden" class="form-control required first_name" name="family_itinerary[different][<?= $key; ?>][first_name]"
                               <?php if($itinerary['is_me'] == 'true'): ?>data-dependency="personal_information_first_name"<?php endif; ?>
                               value="<?= $itinerary['first_name']; ?>" />
                        <input type="hidden" class="form-control required last_name" name="family_itinerary[different][<?= $key; ?>][last_name]"
                               <?php if($itinerary['is_me'] == 'true'): ?>data-dependency="personal_information_last_name"<?php endif; ?>
                               value="<?= $itinerary['last_name']; ?>" />
                        <input type="hidden" class="form-control required" name="family_itinerary[different][<?= $key; ?>][is_me]"
                               value="<?= $itinerary['is_me']; ?>" />
                    </div>
                    <div class="col-md-8 input_group">
                            <div class="col-md-5 form_input">
                                <select class="form-control required chosen-select destination" multiple data-placeholder="Destination" name="family_itinerary[different][<?= $key; ?>][destination]" data-type="destination">
                                    <?php foreach($countries as $country): ?>
                                        <option value=""></option>
                                        <option value="<?= $country->pk; ?>" <?php if($itinerary['destination'] == $country->pk): ?> selected <?php endif; ?>>
                                            <?= $country->country; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>

                                <?= form_error("family_itinerary[different][$key][destination]"); ?>
                            </div>
                            <div class="col-md-3 form_input">
                                <input type="text" class="form-control calendar_input required date start_date" placeholder="Start Date" name="family_itinerary[different][<?= $key; ?>][start_date]" data-type="start_date"
                                       value="<?= $itinerary['start_date']; ?>"/>

                                <?= form_error("family_itinerary[different][$key][start_date]"); ?>
                            </div>
                            <div class="col-md-3 form_input">
                                <input type="text" class="form-control calendar_input required date end_date" placeholder="End Date" name="family_itinerary[different][<?= $key; ?>][end_date]" data-type="end_date"
                                       value="<?= $itinerary['end_date']; ?>"/>

                                <?= form_error("family_itinerary[different][$key][end_date]"); ?>
                            </div>
                            <div class="col-md-1 form_input btn_type"><a href="javascript:void(0)" class="btn_remove active different-itinerary-remove"></a></div>
                    </div>
                 </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>