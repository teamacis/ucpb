<div class="line_title"><p>Itinerary</p></div>
<div class="form_content">
    <!-- Same Itinerary !-->
    <div id="individual-itinerary" class="group_line gray-box-field parent-box" data-country-type="individual">
        <div class="form_line">
            <div class="col-md-8 form_input itinerary-box" id="individual-itinerary-list">
                <!-- Append Itinerary here !-->

                <?php if($this->input->post('individual_itinerary')['same']): ?>
                    <?php foreach($this->input->post('individual_itinerary')['same'] as $key=>$itinerary): ?>
                        <div class="gray-box">
                            <div class="item_choice">
                                <span class="value_destination"><?= $this->itp_country_model->get_countries($itinerary['destination']); ?></span>
                                <span class="value_sday"><?= $itinerary['start_date']; ?></span>
                                <span class="value_eday"><?= $itinerary['end_date']; ?></span>
                                <input type="hidden" name="individual_itinerary[same][<?= $key; ?>][destination]" value="<?= $itinerary['destination']; ?>">
                                <input type="hidden" class="start_date" name="individual_itinerary[same][<?= $key; ?>][start_date]" value="<?= $itinerary['start_date']; ?>">
                                <input type="hidden" class="end_date" name="individual_itinerary[same][<?= $key; ?>][end_date]" value="<?= $itinerary['end_date']; ?>">
                                <a href="javascript:void(0)" class="btn_close same-itinerary-remove"></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form_line">
            <div class="col-md-4 form_input">
                <select class="form-control required destination chosen-select" multiple data-placeholder="Destination" name="destination">
                    
                </select>
            </div>
        </div>
        <div class="form_line">
            <div class="col-md-4 form_input">
                <input type="text" class="form-control calendar_input required date start_date" placeholder="Date of Departure from the Philippines" name="start_date" />
            </div>
            <div class="col-md-4 form_input">
                <input type="text" class="form-control calendar_input required date end_date" placeholder="Date of Arrival to the Philippines" name="end_date" />
            </div>
            <div class="col-md-8 form_input">
                <?= form_error('individual_itinerary[same]'); ?>
            </div>
        </div>
        <div class="form_line">
            <button class="btn btn_add" name="individual-itinerary-add-btn" type="button"></button>
        </div>
        <p class="small_text">Note: Please don't include stopovers</p>
    </div>
</div>