<div class="line_title"><p>Package</p></div>
<div class="form_content">
	<div id="individual-package" class="group_line gray-box-field">
		<?php $packageSelect = unserialize(PACKAGE); ?>
		<div class="col-md-4 form_input">
            <select name="individual_package[same]" id="individual_package" class="form-control chosen-select required package" data-placeholder="Package">
                <option></option>
                <?php foreach($packageSelect as $key=>$label): ?>
                    <option value="<?= $label; ?>" <?php if($this->input->post('individual_package')['same'] == $label): ?> selected <?php endif; ?>>
                        <?= $label; ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <?= form_error('individual_package[same]'); ?>
        </div>
	</div>
</div>