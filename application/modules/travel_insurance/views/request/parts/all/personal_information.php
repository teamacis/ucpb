<div class="form_content<?php echo $this->input->post('insurance_cover') == 'company' ? ' hidden' : ''; ?>" data-property="personal_info">
    <div class="line_title">
        <p>Personal Information</p>
    </div>
    <div class="form_line">
        <div class="col-md-4 form_input" style="display: none;">
            <input type="text" class="form-control required" placeholder="Company Name" name="company_name" />
        </div>
        <div class="col-md-4 form_input">
            <input type="text" class="form-control required" placeholder="Last Name" name="personal_information[lname]" id="personal_information_last_name" <?php echo $this->session->userdata('pk') ? "readonly" : ""; ?>
                   value="<?= $this->input->post('personal_information')['lname']; ?>" />
            <?= form_error('personal_information[lname]'); ?>
        </div>
        <div class="col-md-4 form_input">
            <input type="text" class="form-control required" placeholder="First Name" name="personal_information[fname]" id="personal_information_first_name" <?php echo $this->session->userdata('pk') ? "readonly" : ""; ?>
                   value="<?= $this->input->post('personal_information')['fname']; ?>" />
            <?= form_error('personal_information[fname]'); ?>
        </div>
    </div>
    <div class="form_line">
        <div class="col-md-4 form_input">
            <input type="text" class="form-control calendar_input required birth_date" placeholder="Date of Birth" name="personal_information[birthdate]"
                   value="<?= $this->input->post('personal_information')['birthdate']; ?>" />
            <?= form_error('personal_information[birthdate]'); ?>
        </div>
        <div class="col-md-4 form_input">
            <?php echo form_dropdown('personal_information[civil_status]', $civil_statuses, $this->input->post('personal_information')['civil_status'],' class="form-control chosen-select required" data-placeholder="Civil Status"'); ?>
            <?= form_error('personal_information[civil_status]'); ?>
        </div>
    </div>
</div>