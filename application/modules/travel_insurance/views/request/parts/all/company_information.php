<div class="form_content<?php echo $this->input->post('insurance_cover') != 'company' ? ' hidden' : '' ?>" data-property="company_info">
    <div class="line_title">
        <p>Company Information</p>
    </div>
    <div class="form_line">
        <div class="col-md-4 form_input">
            <input type="text" class="form-control required" placeholder="Company Name" name="company_information[name]" id="company_information_name"
            value="<?= $this->input->post('company_information')['name']; ?>" />
            <?= form_error('company_information[name]'); ?>
        </div>
    </div>
    <div class="form_line">
        <div class="col-md-4 form_input">
            <input type="text" class="form-control required" placeholder="Contact Person" name="company_information[contact_person]" id="company_information_contact_person"
                   value="<?= $this->input->post('company_information')['contact_person']; ?>" />
             <?= form_error('company_information[contact_person]'); ?>
        </div>
        <div class="col-md-4 form_input">
           <input type="text" class="form-control required" placeholder="Designation" name="company_information[designation]" id="company_information_designation"
            value="<?= $this->input->post('company_information')['designation']; ?>" />
            <?= form_error('company_information[designation]'); ?>
        </div>
    </div>
</div>