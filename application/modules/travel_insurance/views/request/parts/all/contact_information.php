<div class="form_content">
    <div class="line_title"><p>Contact Information</p></div>
    <div class="form_line">
        <div class="col-md-4 form_input">
            <input type="text" class="form-control required" placeholder="Email Address" name="personal_information[email_address]" <?php echo isset($user_info['email_address']) ? "readonly" : ""; ?>
                   value="<?= isset($user_info['email_address']) ? $user_info['email_address'] : $this->input->post('personal_information')['email_address']; ?>" />
            <?= form_error('personal_information[email_address]'); ?>
        </div>
    </div>
    <div class="form_line">
        <div class="col-md-4 form_input">
            <input type="text" class="form-control required number" placeholder="Telephone Number" name="personal_information[telephone]"
                   value="<?= $this->input->post('personal_information')['telephone']; ?>" />
        </div>
        <div class="col-md-4 form_input">
            <input type="text" class="form-control required number" placeholder="Mobile Number" name="personal_information[mobile]"
                   value="<?= $this->input->post('personal_information')['mobile']; ?>" />
        </div>
        <div class="col-md-8 form_input">
            <?= form_error('personal_information[telephone]'); ?>
            <?= form_error('personal_information[mobile]'); ?>
        </div>
    </div>
    <p class="small_text">Note: Please provide either Telephone Number or Mobile Number</p>
    
</div>