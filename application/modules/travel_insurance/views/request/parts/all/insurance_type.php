<div class="form_content">
    <div class="radio_type">
        <p class="radio_type_title">Please select cover of Insurance</p>
        <div class="radio_group">
            <label class="radio-inline">
                <input type="radio" name="insurance_cover" value="individual"<?php echo $this->input->post('insurance_cover') != 'company' ? ' checked' : ''; ?>> Individual
            </label>
            <label class="radio-inline">
                <input type="radio" name="insurance_cover" value="company"<?php echo $this->input->post('insurance_cover') == 'company' ? ' checked' : ''; ?>> Company
            </label>
        </div>
    </div>
</div>