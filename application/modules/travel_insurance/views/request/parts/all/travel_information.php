<div class="form_content">
    <div class="line_title"><p>Travel Information</p></div>
    <div class="form_line">
        <div class="col-md-4 form_input">
            <?php $travelSelect = unserialize(TRAVEL); ?>

            <select name="travel_information[purpose_of_travel]" class="form-control chosen-select required" data-placeholder="Purpose of Travel">
                <option></option>
                <?php foreach($travelSelect as $key=>$label): ?>
                    <option <?php if($this->input->post('travel_information')['purpose_of_travel'] == $label): ?> selected <?php endif; ?>>
                        <?= $label; ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <?= form_error('travel_information[purpose_of_travel]'); ?>
        </div>

        <div id="purpose-of-travel-specify" class="col-md-4 form_input" <?php if(!isset($this->input->post('travel_information')['purpose_of_travel']) || $this->input->post('travel_information')['purpose_of_travel'] != $travelSelect['others']): ?>style="display: none;"<?php endif; ?>>
            <input type="text" name="travel_information[purpose_of_travel_others]" class="form-control required" placeholder="Please specify"
                   value="<?= $this->input->post('travel_information')['purpose_of_travel_others']; ?>" />
            <?= form_error('travel_information[purpose_of_travel_others]'); ?>
        </div>
    </div>

    <div class="form_line">
        <div class="col-md-4 form_input">
            <select class="form-control chosen-select required" name="travel_information[coverage_type]" data-placeholder="Type of Coverage">
                <option></option>
                <option <?php if($this->input->post('travel_information')['coverage_type'] == COVERAGE_FAMILY): ?> selected <?php endif; ?>>
                    <?= COVERAGE_FAMILY; ?>
                </option>
                <option <?php if($this->input->post('travel_information')['coverage_type'] == COVERAGE_GROUP): ?> selected <?php endif; ?>>
                    <?= COVERAGE_GROUP; ?>
                </option>
                <option <?php if($this->input->post('travel_information')['coverage_type'] == COVERAGE_INDIVIDUAL): ?> selected <?php endif; ?>>
                    <?= COVERAGE_INDIVIDUAL; ?>
                </option>
            </select>
            <?= form_error('travel_information[coverage_type]'); ?>
        </div>
    </div>

    <!-- Coverage Type is GROUP !-->
    <div id="group-name" class="form_line" <?php if($this->input->post('travel_information')['coverage_type'] != COVERAGE_GROUP): ?> style="display: none;" <?php endif; ?>>
        <div class="col-md-4 form_input">
            <input type="text" name="travel_information[group_name]" class="form-control required" placeholder="Group Name"
                   value="<?= $this->input->post('travel_information')['group_name']; ?>" />
            <?= form_error('travel_information[group_name]'); ?>
        </div>
    </div>
</div>