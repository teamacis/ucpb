<div data-property="documents" class="<?php echo ($this->input->post('personal_information')['birthdate'] AND calculate_age($this->input->post('personal_information')['birthdate']) >= AGE_NEEDS_APPROVAL) ? '' : ' hidden'; ?>">
	<div class="line_title"><p>Required Documents</p></div>
	<div class="form_content">
		<div class="col-md-12">
			<p class="n_title">Please Upload Certificate of Good Health</p>
		</div>
		<div class="form_line content_bg main_member">
			<div class="col-md-4 form_input">
		        <div class="upload_box">
		            <input type="file" class="nice" name="valid_id" /><a href="#" class="btn_close"></a>
		            <?= form_error('personal_information[valid_id]'); ?>
		        </div>
			</div>
		</div>
	</div>
</div>