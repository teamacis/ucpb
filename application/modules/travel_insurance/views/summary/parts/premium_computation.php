<div class="col-md-8 top_head">
    <div class="col-md-6 info_field"><strong class="label_item">Net Premium</strong></div>
    <div class="col-md-6 info_field text-right">
        <span class="value_item">
            USD <?php echo number_format($premium->net_premium,2); ?>
        </span>
    </div>
</div>
<div class="col-md-8 summary_group">
    <div class="summary_line">
        <div class="col-md-12 info_field"><strong class="label_item">Plus:</strong></div>
    </div>
    <div class="summary_line">
        <div class="col-md-6 info_field"><strong class="label_item">Documentary Stamps</strong></div>
        <div class="col-md-6 info_field text-right">
            <span class="value_item">
                <?php echo number_format($premium->dst,2); ?>
            </span>
        </div>
    </div>
    <div class="summary_line">
        <div class="col-md-6 info_field"><strong class="label_item">Premium TAX</strong></div>
        <div class="col-md-6 info_field text-right">
            <span class="value_item">
                <?php echo number_format($premium->premium_tax,2); ?>
            </span>
        </div>
    </div>
    <div class="summary_line">
        <div class="col-md-6 info_field"><strong class="label_item">Local Government Tax</strong></div>
        <div class="col-md-6 info_field text-right">
            <span class="value_item">
               <?php echo number_format($premium->lgt,2); ?>
            </span>
        </div>
    </div>
</div>
<div class="col-md-8 bottom_foot">
    <div class="col-md-6 info_field"><strong class="label_item">Total Amount Due</strong></div>
    <div class="col-md-6 info_field text-right">
        <span class="value_item">
            USD <?php echo number_format($premium->net_total_usd,2); ?>
        </span>
        <br />
        <span class="value_item">
            PHP <?php echo number_format($premium->net_total_php,2); ?>
        </span>
    </div>
</div>