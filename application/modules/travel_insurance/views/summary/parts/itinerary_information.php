<div class="summary_info insurance_info">
    <div class="summary_title title-2"><h4>Itineraries</h4></div>
    <div class="summary_content">
        <?php $ctr = 1; ?>
        <?php if (isset($itineraries) AND is_array($itineraries) AND count($itineraries) > 0): ?>
            <?php foreach ($itineraries AS $itinerary): ?>
                <?php $itinerary = (object) $itinerary;?>
                <div class="col-md-8 summary_group">
                    <div class="line_title"><p>Itinerary #<?php echo $ctr; ?></p></div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Country:</strong>
                            <span class="value_item">
                                <?php echo $itinerary->country; ?>
                            </span>
                        </div>
                    </div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Date of Departure from the Philippines:</strong>
                            <span class="value_item">
                                <?php echo date("m-d-Y",strtotime($itinerary->start_date)); ?>
                            </span>
                        </div>
                    </div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Date of Arrival to the Philippines:</strong>
                            <span class="value_item">
                                <?php echo date("m-d-Y",strtotime($itinerary->end_date)); ?>
                            </span>
                        </div>
                    </div>
                </div>
            <?php $ctr++; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>