<?php foreach($premiums as $premium): ?>
	<div class="col-md-8 top_head">
		<div class="col-md-6 info_field"><strong class="label_item"><?= $premium['name']; ?></strong></div>
		<div class="col-md-6 info_field text-right">
        <span class="value_item">

        </span>
		</div>
	</div>
	<div class="col-md-8 summary_group" style="margin-bottom: 30px;">
		<div class="summary_line">
			<div class="col-md-12 info_field"><strong class="label_item">Plus:</strong></div>
		</div>
		<div class="summary_line">
			<div class="col-md-6 info_field"><strong class="label_item">Ugit</strong></div>
			<div class="col-md-6 info_field text-right">
            <span class="value_item">
                <?= $premium['currency']; ?> <?= $premium['ugit']; ?>
            </span>
			</div>
		</div>
		<div class="summary_line">
			<div class="col-md-6 info_field"><strong class="label_item">Ibero</strong></div>
			<div class="col-md-6 info_field text-right">
            <span class="value_item">
                <?= $premium['currency']; ?> <?= $premium['ibero']; ?>
            </span>
			</div>
		</div>
	</div>
<?php endforeach; ?>