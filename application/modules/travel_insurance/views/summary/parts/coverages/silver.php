<div class="summary_content">
    <div class="col-md-10 summary_group">
        <div class="summary_line">
            <h4>Silver Quotation</h4>
        </div>

        <div class="summary_line">
            <div class="col-md-8 info_field">
                <span class="order">I.</span>
                <strong class="label_item">Accidental Death / Disablement </strong>
            </div>
            <div class="col-md-4 info_field text-right">
                <span class="value_item">US $ 10,000.00 </span>
            </div>
        </div>

        <div class="summary_line">
            <div class="col-md-8 info_field">
                <span class="order">II.</span>
                <strong class="label_item">Accidental Brutal Benefit</strong>
            </div>
            <div class="col-md-4 info_field text-right">
                <span class="value_item">US $ 250.00 </span>
            </div>
        </div>

        <div class="summary_line">
            <div class="col-md-8 info_field">
                <span class="order">III.</span>
                <strong class="label_item">Personal Liability</strong>
            </div>
            <div class="col-md-4 info_field text-right">
                <span class="value_item">US $ 1000.00 </span>
            </div>
        </div>

        <div class="summary_line">
            <div class="col-md-8 info_field">
                <span class="order">IV.</span>
                <strong class="label_item">Travel Assistance Services</strong>
            </div>
        </div>

        <div style="margin-left: 32px;">
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">1.</span>
                    <strong class="label_item">Medical expense (including Sabotage and Terrorism coverage) and hospitalization abroad *** </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">US $ 20000.00 </span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">2.</span>
                    <strong class="label_item">Transport or repatriation in case of illness or accident </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">3.</span>
                    <strong class="label_item">Emergency dental care</strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">US $ 200.00</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">4.</span>
                    <strong class="label_item">Repatriation of a family member traveling with the insured </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">5.</span>
                    <strong class="label_item">Repatriation of mortal remains </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">6.</span>
                    <strong class="label_item">Escort of dependent child </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">7.</span>
                    <strong class="label_item">Travel of one immediate family member </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Travel cost plus up to 100 US$/day maximum 1,000 $ </span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">8.</span>
                    <strong class="label_item">Emergency return home following death of a close family member </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">9.</span>
                    <strong class="label_item">Delivery of Medicines</strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">10.</span>
                    <strong class="label_item">Relay of urgent messages </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">11.</span>
                    <strong class="label_item">Long distance medical information service </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">12.</span>
                    <strong class="label_item">Medical referral/appointment of local medical specialist </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">13.</span>
                    <strong class="label_item">Connection services </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">14.</span>
                    <strong class="label_item">Advance of bail bond </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">US $ 1000.00</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">15.</span>
                    <strong class="label_item">Trip Cancellation</strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">US $ 3000.00</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">16.</span>
                    <strong class="label_item">Reimbursement of Forfeited Holidays/Trip Curtailment </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Up to US $ 650.00</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">17.</span>
                    <strong class="label_item">Delayed Departure </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">US $ 200.00</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">18.</span>
                    <strong class="label_item">Flight Misconnection </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">US $ 100.00</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">19.</span>
                    <strong class="label_item">Baggage Delay </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">US $ 40.00</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">20.</span>
                    <strong class="label_item">Compensation for in-flight loss of checked-in baggage</strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">US $ 40.00 Up to US $ 1,200 subject to limit 150 US$ for any item </span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">21.</span>
                    <strong class="label_item">Location and forwarding of baggage and personal effects </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">Actual Expense</span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-8 info_field">
                    <span class="order">22.</span>
                    <strong class="label_item">Loss of Passport, Driving License, National Identity Card Abroad </strong>
                </div>
                <div class="col-md-4 info_field text-right">
                    <span class="value_item">$ US 250.00</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="summary_note">
    <div class="col-md-8">
        <p>
            <span>
                *** For FAMILY PLAN, coverage for Spouse is equal to the Principal, children 18 years
                old and below are covered up to 50% of the Medical Expenses benefit.
                <br><br>
                Please feel free to call us at tel. No. 811-1788 should there anything you wish to clarify.
            </span>
        </p>
    </div>
</div>
