<?php if (isset($members) AND is_array($members) AND count($members) > 0): ?>
    <div class="summary_info insurance_info">
        <div class="summary_title title-2"><h4>Members</h4></div>
        <div class="summary_content">
            <?php $ctr = 1; ?>
            <?php foreach ($members AS $member): ?>
                <?php $member = (object) $member; ?>
                <div class="col-md-8 summary_group">
                    <div class="line_title"><p>Member #<?php echo $ctr; ?></p></div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Name:</strong>
                            <span class="value_item">
                                <?php echo $member->first_name; ?> <?php echo $member->last_name; ?>
                            </span>
                        </div>
                    </div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Birthday:</strong>
                            <span class="value_item">
                                <?php echo date("m-d-Y",strtotime($member->birth_date)); ?>
                            </span>
                        </div>
                    </div>
                    <div class="summary_line">
                        <div class="col-md-12 info_field">
                            <strong class="label_item">Relation:</strong>
                            <span class="value_item">
                                <?php echo $member->relationship; ?>
                            </span>
                        </div>
                    </div>
                </div>
             <?php $ctr++; ?>
            <?php endforeach; ?>         
        </div>
    </div>
<?php endif; ?>