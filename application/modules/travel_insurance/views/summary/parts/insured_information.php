<div class="summary_info insurance_info">
    <div class="summary_title title-2"><h4>Insured Information</h4></div>
    <div class="summary_content">
        <div class="col-md-8 summary_group">
            <?php if ($info['insurance_cover'] == 'individual'): ?>
                <?php $personal_information = (object) $info['personal_information']; ?>
                <div class="line_title"><p>Personal Information</p></div>     
                <div class="summary_line">
                    <div class="col-md-12 info_field">
                        <strong class="label_item">Name:</strong>
                        <span class="value_item">
                            <?php echo $personal_information->fname; ?> <?php echo $personal_information->lname; ?>
                        </span>
                    </div>
                </div>
                <div class="summary_line">
                    <div class="col-md-12 info_field">
                        <strong class="label_item">Birthday:</strong>
                        <span class="value_item">
                            <?php echo date("M d, Y", strtotime($personal_information->birthdate)); ?>
                        </span>
                    </div>
                </div>
            <?php else: ?>
                <div class="line_title"><p>Company Information</p></div> 
                <?php $company_information = (object) $info['company_information']; ?>    
                <div class="summary_line">
                    <div class="col-md-12 info_field">
                        <strong class="label_item">Name:</strong>
                        <span class="value_item">
                            <?php echo $company_information->name; ?>
                        </span>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-md-8 summary_group">
            <div class="line_title"><p>Contact Information</p></div>
            <?php $personal_information = (object) $info['personal_information']; ?>
            <?php if ($info['insurance_cover'] == 'company'): ?>
                <div class="summary_line">
                    <div class="col-md-12 info_field">
                        <strong class="label_item">Contact Person:</strong>
                        <span class="value_item">
                            <?php echo $company_information->contact_person; ?>
                        </span>
                    </div>
                </div>
            <?php endif; ?>
            <div class="summary_line">
                <div class="col-md-12 info_field">
                    <strong class="label_item">Telephone Number:</strong>
                    <span class="value_item">
                        <?php echo $personal_information->telephone; ?>
                    </span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-12 info_field">
                    <strong class="label_item">Mobile Number:</strong>
                    <span class="value_item">
                        <?php echo $personal_information->mobile; ?>
                    </span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-12 info_field">
                    <strong class="label_item">Email:</strong>
                    <span class="value_item">
                        <?php echo $personal_information->email_address; ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>