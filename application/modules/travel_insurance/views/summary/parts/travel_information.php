<div class="summary_info insurance_info">
    <div class="summary_title title-2"><h4>Travel Information</h4></div>
    <?php $travel_information = (object) $info['travel_information']; ?>
    <div class="summary_content">
    	<div class="col-md-8 summary_group">
            <div class="summary_line">
                <div class="col-md-12 info_field">
                    <strong class="label_item">Purpose of Travel:</strong>
                    <span class="value_item">
                        <?php echo strlen($travel_information->purpose_of_travel_others) > 0 ? $travel_information->purpose_of_travel_others : $travel_information->purpose_of_travel; ?>
                    </span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-12 info_field">
                    <strong class="label_item">Type of Coverage:</strong>
                    <span class="value_item">
                        <?php echo $travel_information->coverage_type; ?>
                    </span>
                </div>
            </div>
            <div class="summary_line">
                <div class="col-md-12 info_field">
                    <strong class="label_item">Start Date:</strong>
                    <span class="value_item">
                        <?php echo date("m-d-Y",strtotime($premium->departure_date)); ?>
                    </span>
                </div>
            </div>
             <div class="summary_line">
                <div class="col-md-12 info_field">
                    <strong class="label_item">End Date:</strong>
                    <span class="value_item">
                        <?php echo date("m-d-Y",strtotime($premium->arrival_date)); ?> (Both dates inclusive)
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>