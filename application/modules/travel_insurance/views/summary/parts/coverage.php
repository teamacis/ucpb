<div class="summary_content">
    <div class="col-md-10 summary_group">
        <div class="summary_line">
            <h4><?php echo $package_type; ?> Quotation</h4>
        </div>

        <div class="summary_line">
            <div class="col-md-8 info_field">
                <span class="order">I.</span>
                <strong class="label_item"><?php echo $coverages['accidental_death']['label']; ?></strong>
            </div>
            <div class="col-md-4 info_field text-right">
                <span class="value_item"><?php echo $coverages['accidental_death']['value']; ?></span>
            </div>
        </div>

        <div class="summary_line">
            <div class="col-md-8 info_field">
                <span class="order">II.</span>
                <strong class="label_item"><?php echo $coverages['accidental_burial_benefit']['label']; ?></strong>
            </div>
            <div class="col-md-4 info_field text-right">
                <span class="value_item"><?php echo $coverages['accidental_burial_benefit']['value']; ?></span>
            </div>
        </div>

        <div class="summary_line">
            <div class="col-md-8 info_field">
                <span class="order">III.</span>
                <strong class="label_item"><?php echo $coverages['personal_liability']['label']; ?></strong>
            </div>
            <div class="col-md-4 info_field text-right">
                <span class="value_item"><?php echo $coverages['personal_liability']['value']; ?></span>
            </div>
        </div>

        <div class="summary_line">
            <div class="col-md-8 info_field">
                <span class="order">IV.</span>
                <strong class="label_item">Travel Assistance Services</strong>
            </div>
        </div>

        <div style="margin-left: 32px;">
            <?php if ( isset($coverages['travel_assistant_services']) AND is_array($coverages['travel_assistant_services']) ): ?>
                <?php foreach ( $coverages['travel_assistant_services'] AS $index => $row ): ?>
                    <?php $number = $index + 1; ?>
                    <div class="summary_line">
                        <div class="col-md-8 info_field">
                            <span class="order"><?php echo $number; ?>.</span>
                            <strong class="label_item"><?php echo $row['label']; ?></strong>
                        </div>
                        <div class="col-md-4 info_field text-right">
                            <span class="value_item"><?php echo $row['value']; ?> </span>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="summary_note">
    <div class="col-md-8">
        <p>
            <span>
                *** For FAMILY PLAN, coverage for Spouse is equal to the Principal, children 18 years
                old and below are covered up to 50% of the Medical Expenses benefit.
                <br><br>
                Please feel free to call us at tel. No. 811-1788 should there anything you wish to clarify.
            </span>
        </p>
    </div>
</div>
