<?php $this->load->view('user/quote_sidebar', array('tab' => 'travel-insurance'));?>
<div class="col-md-9 col-sm-9">
    <div class="col-md-12 col-xs-12 title_bar">
        <h3>QUOTE SUMMARY</h3>
        <p></p>
    </div>
    <!-- CONTENT FIELD -->
    <div class="col-md-12 content_field summary_field">
        <!-- PREMIUM COMPUTATION -->
        <div class="summary_info computation">
            <div class="summary_title title-1"><h4>Premium Computation</h4></div>
            <div class="summary_content">
                <?php $this->load->view('travel_insurance/summary/parts/premium_computation'); ?>
            </div>
            <div class="summary_note">
                <div class="col-md-8">
                    <strong>Documentary Stamps Tax</strong>
                    <p>Due to BIR implementation of EDST(Electronic Documentary Stamp Tax) system effective July 1, 2010, policy holders are mandated to pay the DST portion of the premium once the policy is issued. Refund on DST for cancelled policies is not allowed.</p>
                </div>
            </div>
        </div>
        <!-- END OF PREMIUM COMPUTATION -->
        <!-- COVERAGE -->
        <div class="summary_info coverage">
            <div class="summary_title title-1"><h4>Coverage</h4></div>
            <?php $this->load->view('travel_insurance/summary/parts/coverage'); ?>
        </div>
        <!-- END OF COVERAGE -->

        <?php $this->load->view('travel_insurance/summary/parts/travel_information'); ?>
        <?php $this->load->view('travel_insurance/summary/parts/itinerary_information'); ?>
        <?php $this->load->view('travel_insurance/summary/parts/member_information'); ?>
        <?php $this->load->view('travel_insurance/summary/parts/insured_information'); ?>

        <?php echo form_open(); ?>
            <p class="form_button">
                <?php echo form_hidden('token', $token);?>
                <a target="_blank" class="btn btn-primary btn-lg" href="<?php echo base_url("travel-insurance/quote/summary?token={$token}&action=view_formal_quote") ?>">View Formal Quote</a>
                <button type="submit" class="btn btn-success btn-lg" name="submit" value="buy">Buy</button>
            </p>
        <?php echo form_close(); ?>
    </div>
</div>
<!-- END OF INFORMATION -->