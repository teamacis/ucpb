<?php $this->load->view('user/quote_sidebar', array('tab' => 'travel-insurance'));?>

<div class="col-md-9 col-sm-9">
    <div class="col-md-12 title_bar">
        <h3>Required Documents</h3>
        <p></p>
    </div>
    <?php if ($this->session->flashdata('success_message')): ?>
        <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success_message'); ?></div>
    <?php endif; ?>
    <?php if (isset($error_message) AND strlen($error_message) > 0): ?>
        <div class="alert alert-danger" role="alert"><?php echo $error_message; ?></div>
    <?php endif; ?>
    <div class="col-md-12 content_field">
        <p>Please upload a Certification of Good Health for the following persons.</p>
        <ol>
            <?php if ( is_array($persons) ): ?>
                <?php foreach ( $persons AS $person ): ?>
                    <li><?php echo $person; ?></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ol>
        <input name="documents[]" type="file" multiple class="file-loading">
        <div class="clearfix">&nbsp;</div>
        <?php echo form_open('','class="form_style"', ["token" => $token]); ?>
            <p class="form_button">
                <button type="submit" name="submit" value="save" class="btn btn-primary btn-lg">Submit Requirements</button>
            </p>
        <?php echo form_close(); ?>
        <div class="clearfix">&nbsp;</div>
    </div>
</div>
<script>
    $("input[type=file]").fileinput({
        uploadUrl: "<?php echo $upload_url; ?>",
        minFileCount: <?php echo $total; ?>,
        allowedFileExtensions: ["jpg", "png", "doc", "pdf"],
        previewFileIcon: '<i class="fa fa-file"></i>',
        allowedPreviewTypes: null, // set to empty, null or false to disable preview for all types
        previewFileIconSettings: {
            'doc': '<i class="fa fa-file-word-o text-primary"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
        },
        previewFileExtSettings: {
            'doc': function(ext) {
                return ext.match(/(doc|docx)$/i);
            },
            'jpg': function(ext) {
                return ext.match(/(jpg|jpeg)$/i);
            },
            'pdf': function(ext) {
                return ext.match(/(pdf)$/i);
            },
            'png': function(ext) {
                return ext.match(/(png)$/i);
            },
        },
        deleteUrl : '<?php echo $delete_url; ?>',
        showUploadedThumbs : false,
        overwriteInitial : false,
        uploadExtraData: function(previewId, index) {
            return {key: index};
        },
        <?php /*if ( is_array($preview_config) AND count($preview_config) > 0 ): ?>
            initialPreview          : '<?php echo $preview_config["initial_preview"] ?>',
            initialPreviewConfig    : '<?php echo $preview_config["initial_config"] ?>',
            append : '<?php echo $preview_config["append"] ?>'
        <?php endif;*/ ?>
        <?php if ( $documents['count'] > 0 ): ?>
            initialPreview : [
                <?php foreach ( $documents['data'] AS $index => $document ): ?>
                    <?php
                        $icon = "fa-file";

                        if ( $document->file_type == "image/jpg" OR $document->file_type == "image/jpeg" OR $document->file_type == "image/jpe" OR $document->file_type == "image/png" ) {
                            $icon = "fa-file-photo-o text-warning";
                        } elseif ( $document->file_type == "application/pdf" ) {
                            $icon = "fa-file-pdf-o text-danger";
                        } elseif ( $document->file_type == "application/msword" ) {
                            $icon = "fa-file-word-o text-primary";
                        }

                        $initialPreview = '<div class="file-preview-other-frame">' . 
                                                '<div class="file-preview-other">' .
                                                    '<span class="file-icon-4x"><i class="fa '.$icon.'"></i></span>' .
                                                '</div>' .
                                            '</div>';
                        echo "'".$initialPreview."'";
                        if ( $index != ($documents['count'] -1) ) {
                            echo ',';
                        }                
                    ?>
                <?php endforeach; ?>
            ],
            initialPreviewConfig : [
                <?php foreach ( $documents['data'] AS $index => $document ): ?>
                <?php
                    $initial_config =[
                        "url"   => base_url("quotes/documents/delete/{$api_key}/{$token}"),
                        "key"   => $document->pk,
                        "caption" => $document->file_name,
                        "width" => "120px"
                    ];
                    echo json_encode($initial_config);
                    if ( $index != $documents['count'] ) {
                        echo ',';
                    }
                ?>
                <?php endforeach; ?>
            ],
            append : true
        <?php endif; ?>
    });
</script>