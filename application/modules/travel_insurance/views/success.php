<div class="s_content col-md-6">
    <h4>Thank you for trusting <strong>UCPB GEN</strong>!</h4>
    <div class="transaction_details">
        <p>Your purchase from <strong>UCPB GEN</strong> is <strong>COMPLETE</strong></p>
        <p>Your transaction ID for this payment is : <span class="trans_id"><?php echo $txnid; ?></span></p>
    </div>
    <div class="documents_section">
        <ul class="doc_list">
            <li>
                <a href="<?=base_url(POLICIES_FOLDER . 'policy-schedule-' . $transaction['policy_id'] . '.pdf')?>" target="_blank" class="type_pdf">Policy Schedule</a>
            </li>
            <li>
                <a href="<?=base_url(ITP_QUOTE_FILES . 'terms_and_conditions.pdf')?>" target="_blank" class="type_pdf">Terms and Conditions</a>
            </li>
            <?php /*<li>
                <a href="<?=base_url(ITP_QUOTE_FILES_INVOICE . 'invoice-' . $txnid . '.pdf')?>" target="_blank" class="type_pdf">Invoice</a>
            </li>*/?>
            <li>
                <a href="<?=base_url(ITP_QUOTE_FILES_APPLICATION . 'application-' . $txnid . '.pdf')?>" target="_blank" class="type_pdf">Application Form</a>
            </li>
            <li>
                <a href="<?=base_url(FORMAL_QUOTES_FOLDER . $txnid . '.pdf')?>" target="_blank" class="type_pdf">Formal Quote</a>
            </li>
            <?php if ( $country_type == 3 ): ?>
                <li>
                    <a href="<?=base_url(ITP_QUOTE_FILES_COVER . 'confirmation-cover-' . $txnid . '.pdf')?>" target="_blank" class="type_pdf">Confirmation Cover</a>
                </li>
            <?php endif; ?>
            <?php /*<li>
                <a href="<?=base_url(ITP_QUOTE_FILES . 'warranties_and_clauses.pdf')?>" target="_blank" class="type_pdf">Warranties and Clauses</a>
            </li>*/ ?>
        </ul>
    </div>
    <div class="final_message">
        <p>Keep this receipt, in case you need customer support from Dragonpay or UCPB GEN. We will send a copy of this receipt and confirmation email to <span class="email_id"><?php echo $itp_quote->email_address; ?></span>.</p>
    </div>
    <p class="form_button">
        <a href="<?php echo site_url('travel-insurance'); ?>" type="button" class="btn btn-success btn-lg" style="line-height: 25px;">Home</a>
    </p>
</div>