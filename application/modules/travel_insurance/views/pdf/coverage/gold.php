<p align="center"><strong>TABLE OF COVERAGE AND BENEFITS</strong></p>
<table>
	<tr>
		<td align="center"><strong>Coverage</strong></td>
		<td align="center"><strong>Platinum</strong></td>
	</tr>
	<tr>
		<td>I.   Accidental Death / Disablement</td>
		<td align="center">US $ 50,000.00</td>
	</tr>
	<tr>
		<td>II.  Accidental Burial Benefit</td>
		<td align="center">US $ 1,000.00</td>
	</tr>
	<tr>
		<td>III. Personal Liability</td>
		<td align="center">US $ 20,000.00</td>
	</tr>
	<tr>
		<td><strong>IV.  Travel Assistance Services</strong></td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">
			<table>
				<tr>
					<td>1</td>
					<td>Medical expense (<strong>including Sabotage and Terrorism coverage</strong>) and hospitalization abroad ***</td>
					<td>50,000 US$</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Transport or repatriation in case of illness or accident</td>
					<td>Actual Expense</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Emergency dental care</td>
					<td>200 US$</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Repatriation of a family member traveling with the insured</td>
					<td>Actual Expense</td>
				</tr>
				<tr>
					<td>5</td>
					<td>Repatriation of mortal remains</td>
					<td>Actual Expense</td>
				</tr>
				<tr>
					<td>6</td>
					<td>Escort of dependent child</td>
					<td>Actual Expense</td>
				</tr>
				<tr>
					<td>7</td>
					<td>Travel of one immediate family member</td>
					<td>Travel cost plus up to 100 US$/day maximum 1,000 $</td>
				</tr>
				<tr>
					<td>8</td>
					<td>Emergency return home following death of a close family member</td>
					<td>Actual Expense</td>
				</tr>
				<tr>
					<td>9</td>
					<td>Delivery of Medicines</td>
					<td>Actual Expense</td>
				</tr>
				<tr>
					<td>10</td>
					<td>Relay of urgent messages</td>
					<td>Actual Expense</td>
				</tr>
				<tr>
					<td>11</td>
					<td>Long distance medical information service</td>
					<td>Actual Expense</td>
				</tr>
				<tr>
					<td>12</td>
					<td>Medical referral/appointment of local medical specialist</td>
					<td>Actual Expense</td>
				</tr>
				<tr>
					<td>13</td>
					<td>Connection services</td>
					<td>Actual Expense </td>
				</tr>
				<tr>
					<td>14</td>
					<td>Advance of bail bond</td>
					<td>1,000 $</td>
				</tr>
				<tr>
					<td>15</td>
					<td>Trip Cancellation</td>
					<td>3,000 US$ </td>
				</tr>
				<tr>
					<td>16</td>
					<td>Reimbursement of Forfeited Holidays/Trip Curtailment</td>
					<td>Up to 750 US$</td>
				</tr>
				<tr>
					<td>17</td>
					<td>Delayed Departure</td>
					<td>200 $</td>
				</tr>
				<tr>
					<td>18</td>
					<td>Flight Misconnection</td>
					<td>150 $</td>
				</tr>
				<tr>
					<td>19</td>
					<td>Baggage Delay</td>
					<td>100 $</td>
				</tr>
				<tr>
					<td>20</td>
					<td>Compensation for in-flight loss of checked-in baggage</td>
					<td>Up to 1,200 US$ subject to limit 150 US$ for any item</td>
				</tr>
				<tr>
					<td>21</td>
					<td>Location and forwarding of baggage and personal effects</td>
					<td>Actual Expense</td>
				</tr>
				<tr>
					<td>22</td>
					<td>Loss of Passport, Driving License, National Identity Card Abroad</td>
					<td>250 $</td>
				</tr>
			</table>
		</td>
	</tr>
</table>