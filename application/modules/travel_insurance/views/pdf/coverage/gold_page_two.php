<table>
	<tr>
		<td>8</td>
		<td>Emergency return home following death of a close family member</td>
		<td>Actual Expense</td>
	</tr>
	<tr>
		<td>9</td>
		<td>Delivery of Medicines</td>
		<td>Actual Expense</td>
	</tr>
	<tr>
		<td>10</td>
		<td>Relay of urgent messages</td>
		<td>Actual Expense</td>
	</tr>
	<tr>
		<td>11</td>
		<td>Long distance medical information service</td>
		<td>Actual Expense</td>
	</tr>
	<tr>
		<td>12</td>
		<td>Medical referral/appointment of local medical specialist</td>
		<td>Actual Expense</td>
	</tr>
	<tr>
		<td>13</td>
		<td>Connection services</td>
		<td>Actual Expense </td>
	</tr>
	<tr>
		<td>14</td>
		<td>Advance of bail bond</td>
		<td>1,000 $</td>
	</tr>
	<tr>
		<td>15</td>
		<td>Trip Cancellation</td>
		<td>3,000 US$ </td>
	</tr>
	<tr>
		<td>16</td>
		<td>Reimbursement of Forfeited Holidays/Trip Curtailment</td>
		<td>Up to 750 US$</td>
	</tr>
	<tr>
		<td>17</td>
		<td>Delayed Departure</td>
		<td>200 $</td>
	</tr>
	<tr>
		<td>18</td>
		<td>Flight Misconnection</td>
		<td>150 $</td>
	</tr>
	<tr>
		<td>19</td>
		<td>Baggage Delay</td>
		<td>100 $</td>
	</tr>
	<tr>
		<td>20</td>
		<td>Compensation for in-flight loss of checked-in baggage</td>
		<td>Up to 1,200 US$ subject to limit 150 US$ for any item</td>
	</tr>
	<tr>
		<td>21</td>
		<td>Location and forwarding of baggage and personal effects</td>
		<td>Actual Expense</td>
	</tr>
	<tr>
		<td>22</td>
		<td>Loss of Passport, Driving License, National Identity Card Abroad</td>
		<td>250 $</td>
	</tr>
</table>