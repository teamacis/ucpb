<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8"/>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<title></title>
		<style type="text/css">
			body {
				font-size: 10px;
				font-family:Arial;
			}
			h5 {
				font-size: 12px;
			}
		</style>
	</head>
	<body>
		<div style="position:relative;top:0;">
			<img src="<?php echo base_url("assets/images/itp_formal_quote_header.png") ?>">
		</div>
		<h1 align="center">Application Form</h2>
		<hr>

		<h2>Quote Request</h2>
		<table cellspacing="5">
			<tr>
				<td><strong>Please select cover of Insurance :</strong></td>
				<td><?php echo ucwords($request->insurance_cover); ?></td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><h5>Personal Information</h5></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>First Name :</strong></td>
				<td><?php echo $request->personal_information->fname; ?></td>
				<td><strong>Last Name :</strong></td>
				<td><?php echo $request->personal_information->lname; ?></td>
			</tr>
			<tr>
				<td><strong>Date of Birth :</strong></td>
				<td><?php echo date('d-M-Y',strtotime($request->personal_information->birthdate)); ?></td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><h5>Contact Information</h5></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Email Address :</strong></td>
				<td><?php echo $request->personal_information->email_address; ?></td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Telephone Number :</strong></td>
				<td><?php echo $request->personal_information->telephone; ?></td>
				<td><strong>Mobile Number :</strong></td>
				<td><?php echo $request->personal_information->mobile; ?></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><h5>Travel Information</h5></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Purpose of Travel :</strong></td>
				<td><?php echo $request->travel_information->purpose_of_travel; ?></td>
				<td><strong>Type of Coverage :</strong></td>
				<td><?php echo $request->travel_information->coverage_type; ?></td>
				<?php if ($request->travel_information->coverage_type == COVERAGE_GROUP): ?>
					<td><strong>Group Name :</strong></td>
					<td><?php echo $request->travel_information->group_name; ?></td>
				<?php else: ?>
					<td colspan="2">&nbsp;</td>
				<?php endif; ?>
				
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><h5>Itineraries</h5></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<?php $this->load->model('itp_country_model'); ?>
			<?php foreach ($itineraries AS $i): ?>
				<tr>
					<td><strong>Destination :</strong></td>
					<td><?php echo $this->itp_country_model->get_countries($i['destination']); ?></td>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td><strong>Start Date :</strong></td>
					<td><?php echo date('d-M-Y', strtotime($i['start_date'])); ?></td>
					<td><strong>End Date :</strong></td>
					<td><?php echo date('d-M-Y', strtotime($i['end_date'])); ?></td>
					<td colspan="2">&nbsp;</td>
				</tr>
			<?php endforeach; ?>
			<?php if ($request->travel_information->coverage_type != COVERAGE_INDIVIDUAL): ?>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="6"><h5>Members</h5></td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<?php
					$coverage_type 	= strtolower($request->travel_information->coverage_type).'_member'; 
					$members 		= $request->$coverage_type;
					if ( count($members) > 0):
						foreach ($members AS $member):
				?>
							<tr>
								<td><strong>First Name :</strong></td>
								<td><?php echo $member->first_name; ?></td>
								<td><strong>Last Name :</strong></td>
								<td><?php echo $member->last_name; ?></td>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td><strong>Date of Birth :</strong></td>
								<td><?php echo date('d-M-Y', strtotime($member->birth_date)); ?></td>
								<td><strong>Relationship :</strong></td>
								<td><?php echo $member->relationship; ?></td>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="6">&nbsp;</td>
							</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			<?php endif; ?>
		</table>
		<hr>
		<h2>Personal Information</h2>
		<table cellspacing="5">
			<tr>
				<td colspan="6"><h5>Agent Information</h5></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td>Do you have an agent with UCPB Gen?</td>
				<td><?php echo $personal_info->with_agent == 'on' ? "Yes" : "No"; ?></td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<?php if ($personal_info->with_agent == 'on'): ?>
				<?php if ($personal_info->agent_type == 'Agent Code'): ?>
					<tr>
						<td><strong>Agent Code :</strong></td>
						<td><?php echo $personal_info->agent_code; ?></td>
						<td colspan="4">&nbsp;</td>
					</tr>
				<?php else: ?>
					<tr>
						<td><strong>Agent Name :</strong></td>
						<td><?php echo $personal_info->agent_fname . ' ' . $personal_info->agent_lname; ?></td>
						<td colspan="4">&nbsp;</td>
					</tr>
				<?php endif; ?>
			<?php endif; ?>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><h5>Personal Information</h5></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Salutation :</strong></td>
				<td><?php echo $personal_info->salutation; ?></td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Last Name :</strong></td>
				<td><?php echo $personal_info->lname; ?></td>
				<td><strong>First Name :</strong></td>
				<td><?php echo $personal_info->fname; ?></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Middle Initial :</strong></td>
				<td><?php echo $personal_info->mname; ?></td>
				<td><strong>Suffix :</strong></td>
				<td><?php echo $personal_info->suffix; ?></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Gender :</strong></td>
				<td><?php echo $personal_info->gender; ?></td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Date of Birth :</strong></td>
				<td><?php echo date('d-M-Y', strtotime($personal_info->birthdate)); ?></td>
				<td><strong>Nationality :</strong></td>
				<td><?php echo $personal_info->nationality; ?></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Occupation :</strong></td>
				<td><?php echo $personal_info->occupation; ?></td>
				<td><strong>Tax Identifiation Number :</strong></td>
				<td><?php echo $personal_info->tin; ?></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><h5>Mailing Address</h5></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>House Number :</strong></td>
				<td><?php echo $personal_information['house_number']; ?></td>
				<td><strong>Barangay :</strong></td>
				<td><?php echo $personal_information['barangay']; ?></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Province :</strong></td>
				<td><?php echo $personal_information['province']; ?></td>
				<td><strong>City :</strong></td>
				<td><?php echo $personal_information['city']; ?></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><h5>Contact Information</h5></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Email Address :</strong></td>
				<td><?php echo $personal_info->email_address; ?></td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td><strong>Mobile :</strong></td>
				<td><?php echo $personal_info->mobile; ?></td>
				<td><strong>Telephone :</strong></td>
				<td><?php echo $personal_info->telephone; ?></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<?php if ( isset($emergency_contacts) AND is_array($emergency_contacts) AND count($emergency_contacts) > 0 ): ?>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="6"><h5>Emergency Contacts</h5></td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<?php foreach ( $emergency_contacts AS $member_id => $contacts ): ?>
					<?php 
						if ($member_id == 0) {
							$name = $personal_info->fname . ' ' . $personal_info->lname;
						} else {
							$name = (isset($contacts[0]['first_name']) AND isset($contacts[0]['last_name'])) ? $contacts[0]['first_name'] . ' ' . $contacts[0]['last_name'] : $personal_info->fname . ' ' . $personal_info->fname;
						}
					?>
					<tr>
						<td colspan="6"><strong><?php echo $name; ?></strong></td>
					</tr>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
					<?php foreach ($contacts AS $c): ?>
						<tr>
							<td><strong>Name :</strong></td>
							<td><?php echo $c['name']; ?></td>
							<td><strong>Contact Number :</strong></td>
							<td><?php echo $c['contact_number']; ?></td>
							<td><strong>Relationship :</strong></td>
							<td><?php echo $c['relation'] ?></td>
						</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
			<?php if ( isset($beneficiaries) AND is_array($beneficiaries) AND count($beneficiaries) > 0 ): ?>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="6"><h5>Beneficiaries</h5></td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<?php foreach ( $beneficiaries AS $member_id => $beneficiary ): ?>
					<?php 
						if ($member_id == 0) {
							$name = $personal_info->fname . ' ' . $personal_info->lname;
						} else {
							$name = (isset($beneficiary[0]['first_name']) AND isset($beneficiary[0]['last_name'])) ? $beneficiary[0]['first_name'] . ' ' . $beneficiary[0]['last_name'] : $personal_info->fname . ' ' . $personal_info->fname;
						}
					?>
					<tr>
						<td colspan="6"><strong><?php echo $name; ?></strong></td>
					</tr>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
					<?php foreach ($beneficiary AS $b): ?>
						<tr>
							<td><strong>Name :</strong></td>
							<td><?php echo $b['name']; ?></td>
							<td><strong>Relationship :</strong></td>
							<td><?php echo $b['relation']; ?></td>
							<td colspan="2">&nbsp;</td>
						</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</table>
		<img src="<?php echo base_url("assets/images/itp_formal_quote_footer.png") ?>">
	</body>
</html>