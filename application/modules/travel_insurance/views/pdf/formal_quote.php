<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8"/>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<title></title>
		<style type="text/css">
			body {
				font-size: 11px;
				font-family: Arial;
			}
		</style>
	</head>
	<body>
		<div style="position:relative;top:0;">
			<img src="<?php echo base_url("assets/images/itp_formal_quote_header.png") ?>">
		</div>
		<p><?php echo date("F d, Y"); ?></p>
		<p>&nbsp;</p>
		<p>
			<?php echo $personal_information['fname'] . ' ' . $personal_information['lname']; ?>
		</p>
		<p>Subject: International Travel Protect Insurance Proposal</p>
		<p>Dear Sir/Ma'am:</p>
		<p>We are pleased to submit our <strong>International Travel Protect</strong> insurance quotation as follows.</p>
		<br>
		<table border="0" style="width:100%;">
			<tr>
				<td width="13%"></td>
				<td align="left">Insured's Name</td>
				<td align="center">:</td>
				<td align="left"><strong><?php echo $personal_information['fname'] . ' ' . $personal_information['lname']; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td align="left">Destination</td>
				<td align="center">:</td>
				<td align="left"><strong><?php echo $destinations; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td align="left">Plan Type</td>
				<td align="center">:</td>
				<td align="left"><strong><?php echo $plan_type; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td align="left">Date of Departure</td>
				<td align="center">:</td>
				<td align="left"><strong><?php echo $departure_date; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td align="left">Date of Arrival</td>
				<td align="center">:</td>
				<td align="left"><strong><?php echo $arrival_date; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td align="left">Premium wth Taxes</td>
				<td align="center">:</td>
				<td align="left">
					<strong>USD <?php echo number_format($premium->net_premium, 2); ?></strong>
					<br />
					<strong>PHP <?php echo number_format($premium->net_premium_php, 2); ?></strong>
				</td>
			</tr>
		</table>
		<br>
		<p align="center"><strong>TABLE OF COVERAGE AND BENEFITS</strong></p>
		<table border="1" cellspacing="0" cellpadding="5">
			<tr>
				<td width="50%" colspan="2" align="center"><strong>Coverage</strong></td>
				<td width="50%" align="center"><strong><?php echo $plan_type; ?></strong></td>
			</tr>
			<tr>
				<td colspan="2" >I.   <?php echo $coverages['accidental_death']['label']; ?></td>
				<td align="center"><?php echo $coverages['accidental_death']['value']; ?></td>
			</tr>
			<tr>
				<td colspan="2" >II.  <?php echo $coverages['accidental_burial_benefit']['label']; ?></td>
				<td align="center"><?php echo $coverages['accidental_burial_benefit']['value']; ?></td>
			</tr>
			<tr>
				<td colspan="2" >III. <?php echo $coverages['personal_liability']['label']; ?></td>
				<td align="center"><?php echo $coverages['personal_liability']['value']; ?></td>
			</tr>
			<tr>
				<td colspan="2" ><strong>IV.  Travel Assistance Services</strong></td>
				<td></td>
			</tr>
			<?php if ( isset($coverages['travel_assistant_services']) AND is_array($coverages['travel_assistant_services']) ): ?>
                <?php foreach ( $coverages['travel_assistant_services'] AS $index => $row ): ?>
                	<?php $number = $index + 1; ?>
					<tr>
						<td width="5%" align="center"><?php echo $number; ?></td>
						<td width="45%"><?php echo $row['label']; ?></td>
						<td align="center"><?php echo $row['value']; ?></td>
					</tr>
				<?php endforeach; ?>
            <?php endif; ?>
		</table>
		<p align="center">*** For FAMILY PLAN, coverage for Spouse is equal to the Principal, children 18 years 
         old and below are covered up to 50% of the Medical Expenses benefit.
     	</p>
     	<br>
     	<p>The above quotation is subject to standard policy terms and conditions. Please feel free to call us at tel. No. 811-8329 should there be anything you wish to clarify.</p>
     	<br>
     	<p>Thank you very much.</p>
     	<br>
     	<p>Sincerely yours <br>UCPB General Insurance Company Inc.</p>
     	<p>&nbsp;</p>
		<img src="<?php echo base_url("assets/images/itp_formal_quote_footer.png") ?>">

	</body>
</html>