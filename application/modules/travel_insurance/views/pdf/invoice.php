<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8"/>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<title></title>
		<style type="text/css">
			body {
				font-size: 11px;
				font-family:Courier;
			}
		</style>
	</head>

	<body>
		<div style="position:relative;top:0;">
			<img src="<?php echo base_url("assets/images/itp_formal_quote_header.png") ?>">
		</div>
		<h2 style="text-align:center">INVOICE</h2>
		<hr>
		<table cellspacing="5">
			<tr>
				<td width="60%">
					<table>
						<tr>
							<td width="40%"><strong>VAT. Reg. T.I.N</strong></td>
							<td width="60%"><?php echo TIN; ?></td>
						</tr>
						<tr>
							<td width="40%"><strong>BIR Permit No.</strong></td>
							<td width="60%"><?php echo BIR_PERMIT_NO; ?></td>
						</tr>
						<tr>
							<td width="40%"><strong>Date Issued</strong></td>
							<td width="60%"><?php echo DATE_ISSUED; ?></td>
						</tr>
						<tr>
							<td width="40%"><strong>Invoice Series</strong></td>
							<td width="60%"><?php echo INVOICE_SERIES; ?></td>
						</tr>
						<tr>
							<td width="40%"><strong>Insured</strong></td>
							<td width="60%"><?php echo $personal_info['full_name']; ?></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td width="40%"><strong>Address</strong></td>
							<td width="60%"><?php echo $personal_info['full_address']; ?></td>
						</tr>
						<tr>
							<td width="40%"><strong>TIN</strong></td>
							<td width="60%"><?php echo $personal_info['tin']; ?></td>
						</tr>
					</table>
				</td>
				<td width="40%">
					<table>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td width="45%"><strong>Date of Issuance</strong></td>
							<td width="55%"><?php echo date('d F Y'); ?></td>
						</tr>
						<tr>
							<td width="45%"><strong>Invoice No.</strong></td>
							<td width="55%"><?php echo $invoice_number; ?></td>
						</tr>
						<tr>
							<td width="45%"><strong>Producer</strong></td>
							<td width="55%">UCPB - DEL MONTE BONI</td>
						</tr>
						<tr>
							<td width="45%"><strong>Code</strong></td>
							<td width="55%">5463</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<hr>
		<table cellspacing="5">
			<tr>
				<td width="60%">
					<table>
						<tr>
							<td width="40%"><strong>Class</strong></td>
							<td width="60%">PA TRAVEL INTL (IND)</td>
						</tr>
						<tr>
							<td width="40%"><strong>Policy No.</strong></td>
							<td width="60%"><?php echo $policy_id; ?></td>
						</tr>
						<tr>
							<td width="40%"><strong>Sum Insured</strong></td>
							<td width="60%">50,000.00 </td>
						</tr>
						<tr>
							<td width="40%"><strong>Period of Insurance</strong></td>
							<td width="60%"><strong>FROM</strong> 12:00 NOON of <?php echo date("d F Y",strtotime($itinerary_info['departure_date'])); ?></td>
						</tr>
						<tr>
							<td width="40%">&nbsp;</td>
							<td width="60%"><strong>TO</strong> 12:00 NOON of <?php echo date("d F Y",strtotime($itinerary_info['arrival_date'])); ?></td>
						</tr>
					</table>
				</td>
				<td width="40%">
					<table>
						<tr>
							<td width="60%"><strong>CURRENCY</strong></td>
							<td width="40%" align="right">US DOLLAR</td>
						</tr>
						<tr>
							<td width="60%"><strong>PREMIUM (VAT EXEMPT)</strong></td>
							<td width="40%" align="right"><?php echo number_format($computation->net_premium, 2); ?></td>
						</tr>
						<tr>
							<td width="60%"><strong>DOCUMENTARY</strong></td>
							<td width="40%" align="right"><?php echo number_format($computation->dst, 2); ?></td>
						</tr>
						<tr>
							<td width="60%"><strong>LOCAL GOVERNMENT</strong></td>
							<td width="40%" align="right"><?php echo number_format($computation->lgt, 2); ?></td>
						</tr>
						<tr>
							<td width="60%"><strong>PREMIUM TAX</strong></td>
							<td width="40%" align="right"><?php echo number_format($computation->premium_tax, 2); ?></td>
						</tr>
						<tr>
							<td width="60%"><strong>TOTAL AMOUNT DUE</strong></td>
							<td width="40%" align="right"><?php echo number_format($computation->net_total_usd, 2); ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- <table width="35%">
			<tr>
				<td>
					Payment should be made in favor of
					<br>
					<strong>UCPB GENERAL INSURANCE CO., INC. </strong>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #000"></td>
			</tr>
			<tr>
				<td align="center">Authorized Signature</td>
			</tr>
		</table> -->
		<div class="clearfix">&nbsp;</div>
		<br><br>
		<p align="center"><strong>“THIS DOCUMENT IS NOT VALID FOR CLAIM OF INPUT TAX”</strong></p>
		<table>
			<tr>
				<td colspan="2"><strong><i>Important</i></strong></td>
			</tr>
			<tr>
				<td width="3%"></td>
				<td width="97%"><p style="font-size:8px"><i>Should the policy be cancelled or endorsed to a lower value, the insured is still liable to pay the full amount of the documentary stamps as stipulated in the policy prior to cancellation/endorsement.</i></p></td>
			</tr>
		</table>
		<p>&nbsp;</p>
		<img src="<?php echo base_url("assets/images/itp_formal_quote_footer.png") ?>">
	</body>
</html>