<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8"/>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<title></title>
		<style type="text/css">
			body {
				font-size: 11px;
				font-family: Arial;
			}
		</style>
	</head>
	<body>
		<table border="1" cellspacing="0" cellpadding="5">
			<tr>
				<td align="center" width="5%">8</td>
				<td width="45%">Emergency return home following death of a close family member</td>
				<td align="center" width="50%">Actual Expense</td>
			</tr>
			<tr>
				<td align="center">9</td>
				<td>Delivery of Medicines</td>
				<td align="center">Actual Expense</td>
			</tr>
			<tr>
				<td align="center">10</td>
				<td>Relay of urgent messages</td>
				<td align="center">Actual Expense</td>
			</tr>
			<tr>
				<td>11</td>
				<td>Long distance medical information service</td>
				<td align="center">Actual Expense</td>
			</tr>
			<tr>
				<td align="center">12</td>
				<td>Medical referral/appointment of local medical specialist</td>
				<td align="center">Actual Expense</td>
			</tr>
			<tr>
				<td align="center">13</td>
				<td>Connection services</td>
				<td align="center">Actual Expense </td>
			</tr>
			<tr>
				<td align="center">14</td>
				<td>Advance of bail bond</td>
				<td align="center">1,000 $</td>
			</tr>
			<tr>
				<td align="center">15</td>
				<td>Trip Cancellation</td>
				<td align="center">3,000 US$ </td>
			</tr>
			<tr>
				<td align="center">16</td>
				<td>Reimbursement of Forfeited Holidays/Trip Curtailment</td>
				<td align="center">Up to 650 US$</td>
			</tr>
			<tr>
				<td align="center">17</td>
				<td>Delayed Departure</td>
				<td align="center">200 $</td>
			</tr>
			<tr>
				<td align="center">18</td>
				<td>Flight Misconnection</td>
				<td align="center">100 $</td>
			</tr>
			<tr>
				<td align="center">19</td>
				<td>Baggage Delay</td>
				<td align="center">40 $</td>
			</tr>
			<tr>
				<td align="center">20</td>
				<td>Compensation for in-flight loss of checked-in baggage</td>
				<td align="center">Up to 1,200 US$ subject to limit 150 US$ for any item</td>
			</tr>
			<tr>
				<td align="center">21</td>
				<td>Location and forwarding of baggage and personal effects</td>
				<td align="center">Actual Expense</td>
			</tr>
			<tr>
				<td align="center">22</td>
				<td>Loss of Passport, Driving License, National Identity Card Abroad</td>
				<td align="center">250 $</td>
			</tr>
		</table>
		<p align="center">*** For FAMILY PLAN, coverage for Spouse is equal to the Principal, children 18 years 
         old and below are covered up to 50% of the Medical Expenses benefit.
     	</p>
     	<br>
     	<p>Please feel free to call us at tel. No. 811-1788 should there anything you wish to clarify.</p>
     	<br>
     	<p>Thank you very much.</p>
     	<br>
     	<p>Sincerely yours <br>UCPB General Insurance Corporation Inc<br>Account Associate I<br>(Department)</p>
     	<p>&nbsp;</p>
		<img src="<?php echo base_url("assets/images/itp_formal_quote_footer.png") ?>">
	</body>
</html>