<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8"/>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<title></title>
		<style type="text/css">
			body {
				font-size: 11px;
				font-family: Arial;
			}
		</style>
	</head>
	<body>
		<div style="position:relative;top:0;">
			<img src="<?php echo base_url("assets/images/itp_formal_quote_header.png") ?>">
		</div>
		<p><?php echo date("F d, Y"); ?></p>
		<p>
			<?php echo $personal_information['fname'] . ' ' . $personal_information['lname']; ?>
		</p>
		<p>&nbsp;</p>
		<p><strong>RE: INTERNATIONAL TRAVEL PROTECT INSURANCE PROPOSAL</strong></p>
		<p>Dear Sir/Ma'am:</p>
		<p>We are pleased to submit our quotation on International Travel Protect insurance for the captioned account. Details as follows.</p>
		<br>
		<table border="0" style="width:100%;">
			<tr>
				<td width="13%"></td>
				<td align="left">ASSURED NAME</td>
				<td align="center">:</td>
				<td align="left"><strong><?php echo $personal_information['fname'] . ' ' . $personal_information['lname']; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td align="left">DESTINATION</td>
				<td align="center">:</td>
				<td align="left"><strong><?php echo $destinations; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td align="left">PLAN TYPE</td>
				<td align="center">:</td>
				<td align="left"><strong><?php echo $plan_type; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td align="left">DATE OF DEPARTURE</td>
				<td align="center">:</td>
				<td align="left"><strong><?php echo $departure_date; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td align="left">DATE OF ARRIVAL</td>
				<td align="center">:</td>
				<td align="left"><strong><?php echo $arrival_date; ?></strong></td>
			</tr>
			<tr>
				<td></td>
				<td align="left">PREMIUM WITH TAXES</td>
				<td align="center">:</td>
				<td align="left"><strong>USD</strong></td>
			</tr>
		</table>
		<br>
		<p align="center"><strong>TABLE OF COVERAGE AND BENEFITS</strong></p>
		<table border="1" cellspacing="0" cellpadding="5">
			<tr>
				<td width="50%" colspan="2" align="center"><strong>Coverage</strong></td>
				<td width="50%" align="center"><strong>Platinum</strong></td>
			</tr>
			<tr>
				<td colspan="2" >I.   Accidental Death / Disablement</td>
				<td align="center">US $ 10,000.00</td>
			</tr>
			<tr>
				<td colspan="2" >II.  Accidental Burial Benefit</td>
				<td align="center">US $   250.00</td>
			</tr>
			<tr>
				<td colspan="2" >III. Personal Liability</td>
				<td align="center">US $ 1,000.00</td>
			</tr>
			<tr>
				<td colspan="2" ><strong>IV.  Travel Assistance Services</strong></td>
				<td></td>
			</tr>
			<tr>
				<td width="5%" align="center">1</td>
				<td width="45%">Medical expense (<strong>including Sabotage and Terrorism coverage</strong>) and hospitalization abroad ***</td>
				<td align="center">20,000 US$</td>
			</tr>
			<tr>
				<td align="center">2</td>
				<td>Transport or repatriation in case of illness or accident</td>
				<td align="center">Actual Expense</td>
			</tr>
			<tr>
				<td align="center">3</td>
				<td>Emergency dental care</td>
				<td align="center">200 US$</td>
			</tr>
			<tr>
				<td align="center">4</td>
				<td>Repatriation of a family member traveling with the insured</td>
				<td align="center">Actual Expense</td>
			</tr>
			<tr>
				<td align="center">5</td>
				<td>Repatriation of mortal remains</td>
				<td align="center">Actual Expense</td>
			</tr>
			<tr>
				<td align="center">6</td>
				<td>Escort of dependent child</td>
				<td align="center">Actual Expense</td>
			</tr>
			<tr>
				<td align="center">7</td>
				<td>Travel of one immediate family member</td>
				<td align="center">Travel cost plus up to 100 US$/day maximum 1,000 $</td>
			</tr>
		</table>
		<br>
		<br>
		<br>
		<br>
	</body>
</html>