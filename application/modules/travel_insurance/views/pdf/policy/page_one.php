<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8"/>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<title></title>
		<style type="text/css">
			body {
				font-size: 11px;
				font-family:Courier;
			}
		</style>
	</head>

	<body>
		<div style="position:relative;top:0;">
			<img src="<?php echo base_url("assets/images/itp_formal_quote_header.png") ?>">
		</div>
		<p align="center">
			<strong>ACCIDENT POLICY SCHEDULE</strong><br>
			-------------------------
		</p>
		<table>
			<tr>
				<td width="10%">Line</td>
				<td width="40%">: PERSONAL ACCIDENT</td>
				<td colspan="2">Policy Period</td>
			</tr>
			<tr>
				<td>Subline</td>
				<td>: PA TRAVEL INTL (IND)</td>
				<td width="10%">From :</td>
				<td width="40%"><?php echo $itinerary_info['departure_date']; ?> 12:00:00 NOON</td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td>To :</td>
				<td><?php echo $itinerary_info['arrival_date']; ?> 12:00:00 NOON</td>
			</tr>
			<tr>
				<td>Issue Date</td>
				<td>: <?php echo date('F d, Y', strtotime($itp_quote->issue_date)); ?></td>
				<td colspan="2"></td>
			</tr>
		</table>
		<p>&nbsp;</p>
		<table>
			<tr>
				<td width="40%">
					<table>
						<tr>
							<td width="25%">Policy No.</td>
							<td width="75%">: <?php echo $policy_id; ?></td>
						</tr>
						<tr>
							<td>Insured</td>
							<td>: 
								<?php if ($itp_quote->coverage_type == COVERAGE_GROUP): ?>
									<?php echo $itp_quote->group_name; ?>
								<?php else: ?>
									<?php echo $personal_info['lname']; ?>, <?php echo $personal_info['fname']; ?> <?php echo $personal_info['mname']; ?>.
									<?php if ($itp_quote->coverage_type == COVERAGE_FAMILY): ?>
										 AND FAMILY
									<?php endif; ?>
								<?php endif; ?>
								
							</td>
						</tr>
						<tr>
							<td>Address</td>
							<td>: <?php echo $personal_info['full_address']; ?></td>
						</tr>
					</table>
				</td>
				<td width="5%">&nbsp;</td>
				<td width="55%">
					<table>
						<tr>
							<td width="50%">PREMIUM</td>
							<td width="20%">: USD</td>
							<td align="right" width="30%"><?php echo number_format($computation->net_premium,2); ?></td>
						</tr>
						<tr>
							<td>DOCUMENTARY STAMPS</td>
							<td>:</td>
							<td align="right"><?php echo number_format($computation->dst,2); ?></td>
						</tr>
						<tr>
							<td>LOCAL GOVERNMENT TAX</td>
							<td>:</td>
							<td align="right"><?php echo number_format($computation->lgt,2); ?></td>
						</tr>
						<tr>
							<td>PREMIUM TAX</td>
							<td>:</td>
							<td align="right"><?php echo number_format($computation->premium_tax,2); ?></td>
						</tr>
						<tr>
							<td colspan="2"></td>
							<td align="right">----------------</td>
						</tr>
						<tr>
							<td>AMOUNT DUE</td>
							<td>: USD</td>
							<td align="right"><?php echo number_format($computation->net_total_usd,2); ?></td>
						</tr>
						<tr>
							<td colspan="2"></td>
							<td align="right">----------------</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>			
		<table>
			<tr>
				<td colspan="2">Ref.Po No.: 136484</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td width="20%">Total Sum Insured</td>
				<td width="80%">: <strong><?php echo strtoupper(convert_number_to_words(intval(str_replace(',','',$coverage['accidental_death']['value'])))); ?> IN UNITED STATES DOLLAR (<?php echo $coverage['accidental_death']['value']; ?>)</strong></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>POLICY PERIOD</td>
				<td>: <?php echo strtoupper($itinerary_info['departure_date']); ?> TO <?php echo strtoupper($itinerary_info['arrival_date']); ?> BOTH DATES INCLUSIVE</td>
			</tr>
		</table>
		<br>
		<div align="left" style="line-height:24px;border-top:1px dashed #000;border-bottom:1px dashed #000;">
			<strong>CLIENT'S INFORMATION</strong>
		</div>
		<br>
		<table>
			<tr>
				<td width="25%">Name</td>
				<td width="40%">: <?php echo $personal_info['lname']; ?>, <?php echo $personal_info['fname']; ?> <?php echo $personal_info['mname']; ?>.</td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td>Age</td>
				<td>: <?php echo calculate_age($personal_info['birthdate']); ?></td>
				<td width="5%">Sex</td>
				<td>: <?php echo $personal_info['gender']; ?></td>
			</tr>
			<tr>
				<td>Date of Birth</td>
				<td>: <?php echo date("d-M-Y",strtotime($personal_info['birthdate'])); ?></td>
				<td colspan="2"></td>
			</tr>
			<!-- <tr>
				<td>Status</td>
				<td>: MARRIED</td>
				<td colspan="2"></td>
			</tr> -->
			<tr>
				<td>Position</td>
				<td>: <?php echo $personal_info['occupation'] == 'Others' ? $personal_info['other_occupation'] : $personal_info['occupation']; ?></td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td>No. of Persons</td>
				<td>: <?php echo $number_of_persons; ?></td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td>Itinerary</td>
				<td>: <?php echo $itinerary_info['destinations']; ?></td>
				<td colspan="2"></td>
			</tr>
		</table>
		<?php if ($itp_quote->coverage_type != COVERAGE_INDIVIDUAL): ?>
			<div class="clearfix"></div>
			<table cellpadding="5px">
				<tr>
					<th align="center" style="border-bottom:1px dashed #000">INSURED</th>
					<th align="center" style="border-bottom:1px dashed #000">Eff Date</th>
					<th align="center" style="border-bottom:1px dashed #000" style="border-bottom:1px dashed #000">Exp Date</th>
					<th align="center" style="border-bottom:1px dashed #000">Birth Date</th>
					<th align="center" style="border-bottom:1px dashed #000">Age</th>
					<th align="center" style="border-bottom:1px dashed #000">Amount Coverage</th>
				</tr>
				<?php ?>
				<tr>
					<td>1 <?php echo $personal_info['fname']; ?>, <?php echo $personal_info['lname']; ?></td>
					<td><?php echo date("m/d/Y",strtotime($itinerary_info['departure_date'])); ?></td>
					<td><?php echo date("m/d/Y",strtotime($itinerary_info['arrival_date'])); ?></td>
					<td><?php echo date("m/d/Y",strtotime($personal_info['birthdate'])); ?></td>
					<td><?php echo calculate_age($personal_info['birthdate']); ?></td>
					<td>10,000.00</td>
				</tr>
				<?php $ctr = 2; ?>
				<?php foreach ($members AS $member): ?>
					<tr>
						<td><?php echo $ctr; ?> <?php echo $member->first_name; ?>, <?php echo $member->last_name; ?></td>
						<td><?php echo date("m/d/Y",strtotime($itinerary_info['departure_date'])); ?></td>
						<td><?php echo date("m/d/Y",strtotime($itinerary_info['arrival_date'])); ?></td>
						<td><?php echo date("m/d/Y",strtotime($member->birth_date)); ?></td>
						<td><?php echo calculate_age($member->birth_date); ?></td>
						<td>10,000.00</td>
					</tr>
					<?php $ctr++; ?>
				<?php endforeach; ?>
			</table>
		<?php endif; ?>
		<br>
		<p><strong>BENEFICIARY INFORMATION</strong></p>
		<br>
		<table cellpadding="5px">
			<tr>
				<td align="center" width="55%" colspan="3" style="border-bottom:1px dashed #000">Beneficiary</td>
				<td width="3%">&nbsp;</td>
				<td align="center" width="40%" style="border-bottom:1px dashed #000">Relation</td>
			</tr>
		</table>
		<br><br>
		<table>
			<?php if (isset($beneficiaries) AND is_array($beneficiaries) AND count($beneficiaries) > 0): ?>
				<?php foreach ($beneficiaries AS $beneficiary): ?>
					<tr>
						<td width="60%" colspan="3"><?php echo $beneficiary['name']; ?></td>
						<td width="40%"><?php echo $beneficiary['relation']; ?></td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
			<tr>
				<td>Item TSI</td>
				<td>: USD</td>
				<td><?php echo $coverage['accidental_death']['value']; ?></td>
				<td></td>
			</tr>
			<tr>
				<td>Item Premium</td>
				<td>: USD</td>
				<td><?php echo number_format($computation->net_premium,2); ?></td>
				<td></td>
			</tr>
		</table>
		<br><br>
		<table cellpadding="5px">
			<tr>
				<td align="center" width="26%" style="border-bottom:1px dashed #000">Benefits</td>
				<td width="3%">&nbsp;</td>
				<td align="center" width="26%" colspan="2" style="border-bottom:1px dashed #000">Sum Insured</td>
				<td width="3%">&nbsp;</td>
				<td align="center" width="27%" colspan="2" style="border-bottom:1px dashed #000">Premium</td>
			</tr>
		</table>
		<br><br>
		<table>	
			<tr>
				<td width="29%">ACCIDENTAL DEATH/DISABLEMENT</td>
				<td width="6%">USD</td>
				<td width="20%" align="right"><?php echo $coverage['accidental_death']['value']; ?></td>
				<td width="3%">&nbsp;</td>
				<td width="7%">USD</td>
				<td width="20%" align="right"><?php echo number_format($computation->net_premium,2); ?></td>
			</tr>
			<tr>
				<td>ACCIDENTAL BURIAL BENEFIT</td>
				<td>USD</td>
				<td align="right"><?php echo $coverage['accidental_burial_benefit']['value']; ?></td>
				<td width="3%">&nbsp;</td>
				<td>USD</td>
				<td align="right">0.00</td>
			</tr>
			<tr>
				<td>PERSONAL LIABILITY</td>
				<td>USD</td>
				<td align="right"><?php echo $coverage['personal_liability']['value']; ?></td>
				<td width="3%">&nbsp;</td>
				<td>USD</td>
				<td align="right">0.00</td>
			</tr>
		</table>
		<br>
		<p><strong>DEDUCTIBLES</strong></p>
		<br>
		<table cellpadding="5px">
			<tr>
				<td width="23%" align="center" style="border-bottom:1px dashed #000">Deductible/s</td>
				<td width="3%">&nbsp;</td>
				<td width="23%" align="center" style="border-bottom:1px dashed #000">Amount</td>
				<td width="3%">&nbsp;</td>
				<td width="23%" align="center" style="border-bottom:1px dashed #000">Rate (%)</td>
				<td width="3%">&nbsp;</td>
				<td width="22%" align="center" style="border-bottom:1px dashed #000">Benefits</td>
			</tr>
		</table>
		<br><br>
		<table>
			<tr>
				<td>REFER TO BOOKLET</td>
				<td align="right">0.00</td>
				<td></td>
				<td></td>
			</tr>
		</table>
		<br>
		<div align="left" style="line-height:24px;border-top:1px dashed #000;border-bottom:1px dashed #000;">
			<strong>WARRANTIES AND CLAUSES</strong>
		</div>
		<br />
		<p><strong>SABOTAGE AND TERRORISM INCLUSION ENDORSEMENT </strong></p>
		<p>
			Subject to the exclusions, limits and conditions hereinafter contained, this Insurance insures bodily injury sustained by the Insured occurring during the period of this Policy caused by an Act of Terrorism or Sabotage, as herein defined. 
			<br />
			For the purpose of this Insurance: <br />
			- an Act of Terrorism means an act or series of acts, including the use of force or violence, of any person or group(s) of persons, whether acting alone or on behalf of or in connection with any organisation(s), committed for political, religious or ideological purposes including the intention to influence any government and/or to put the public in fear for such purposes. 
			<br />
			- an act of sabotage means a subversive act or series of such acts committed for political, religious or ideological purposes including the intention to influence any government and/or to put the public in fear for such purposes. This insurance excludes any bodily injury sustained by the Insured
			<br />
			a) acts of terrorism or sabotage occurring in the Provinces of Basilan, Lanao del Sur, Maguindanao, Sulu, Tawi-tawi and/or Marawi. 
			<br />
			b) caused by measures taken to prevent, suppress or control actual or potential terrorism or sabotage unless agreed by the Company in writing prior to such measures being taken. 
			<br />
			c) arising directly or indirectly from nuclear detonation, nuclear reaction, nuclear radiation or radioactive contamination, however such nuclear detonation, nuclear reaction, nuclear radiation or radioactive contamination may have been caused. directly or indirectly by war, invasion or warlike operations (whether war be declared or not), hostile acts of sovereign or local government entities, civil war, rebellion, revolution, insurrection, martial law, usurpation of power, or civil commotion assuming the proportions of or amounting to an uprising. 
			<br />
			d) arising directly or indirectly from or in consequence of chemical or biological emission, release, discharge, dispersal or escape or chemical or biological exposure of any kind.
		</p>
		<p>&nbsp;</p>
		<div align="left" style="line-height:24px;border-top:1px dashed #000;border-bottom:1px dashed #000;">
			<strong>GENERAL INFORMATION</strong>
		</div>
		<p>PURPOSE OF TRAVEL : TRAINING</p>
		<p>COVERAGE <br>========</p>
		<p>LIMIT OF LIABILITY</p>
		<table cellspacing="5px">
			<tr>
				<td width="70%" align="right">CURRENCY</td>
				<td width="25%" align="right">US Dollar ($)</td>
			</tr>
			<tr>
				<td colspan="2">TRAVEL ASSISTANCE SERIVES</td>
			</tr>
			<?php if (isset($coverage['travel_assistant_services']) AND is_array($coverage['travel_assistant_services']) AND count($coverage['travel_assistant_services']) > 0): ?>
				<?php foreach ($coverage['travel_assistant_services'] AS $row): ?>
					<tr>
						<td><?php echo $row['label']; ?></td>
						<td align="right"><?php echo $row['value']; ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
			<!-- <tr>
				<td>Medical expense (including Sabotage and Terrorism coverage)</td>
				<td align="right">50,000.00 US$ and hospitalization abroad</td>	
			</tr>
			<tr>
				<td>Transport or repatriation in case of illness or accident</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Emergency dental care</td>
				<td align="right">200.00 US$</td>	
			</tr>
			<tr>
				<td>Repatriation of family member travelling with the Insured</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Repatriation of mortal remains</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Escort of dependent child</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Travel of one immediate family member</td>
				<td align="right">Travel cost plus up to US $100.00/day maximum of US $1,000.0</td>	
			</tr>
			<tr>
				<td>Emergency return home following death of a close family member</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Delivery of Medicines</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Relay of urgent messages</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Long distance medical information service</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Medical referral/appointment of local medical specialist</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Connection services</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Advance of bail bond</td>
				<td align="right">1,000.00 US$</td>	
			</tr>
			<tr>
				<td>Trip cancellation</td>
				<td align="right">3,000.00 US$</td>	
			</tr>
			<tr>
				<td>Reimbursement ofForfeited Holidays/Trip Curtailment</td>
				<td align="right">Up to 750.00 US$</td>	
			</tr>
			<tr>
				<td>Delayed Departure</td>
				<td align="right">200.00 US$</td>	
			</tr>
			<tr>
				<td>Flight Misconnection</td>
				<td align="right">150.00 US$</td>	
			</tr>
			<tr>
				<td>Baggage Delay</td>
				<td align="right">100.00 US$</td>	
			</tr>
			<tr>
				<td>Compensation for in-flight loss of checked-in baggage</td>
				<td align="right"> Up to US $1,200.00, subject to limit US $150.00 for any item</td>	
			</tr>
			<tr>
				<td>Location and forwarding of baggage and personal effects</td>
				<td align="right">Actual Expense</td>	
			</tr>
			<tr>
				<td>Loss of Passport, Driving License, National Identity Card Abroad</td>
				<td align="right">250.00 US$</td>	
			</tr> -->
		</table>
		<p>
			For FAMILY PLAN, coverage for Spouse is equal to the Principal, children 18 years old and below are covered up to 50% of the Medical Expenses benefit.
		</p>
		<p>
			CASHLESS SETTLEMENTS OF CLAIM <br>
			Assistance Hotline: <br>
			MAPFRE ASISTENCIA, S.A. COMPANIA INTERNACIONAL DE SEGUROS Y REASEGUROS <br>
			C/Sor Angela de la Cruz 6, 28020 Madrid Spain or IBERO ASISTENCIA (Administrative Arm in the Philippines) <br>
			Call (24/7) for assistance : HOTLINE: 0063-24594736 / Fax No.: 0063-28922465 <br>
			E-mail: ficosiam@iberoasistencia.ph
		</p>
		<p>
			IN WITNESS WHEREOF, the company has caused this policy to be signed by its duly authorized officer / representative as of the date of issue at
		</p>
		<div>
			<div style="width:50%">
				MPARALE <br>
				<?php echo date("d-M-Y H:i:s"); ?> <br>
				4691-300051 / 339-203148 <br>
				INTERTRADE INSURANCE BROKERS / <br>
				ITIB/EMPLOYEES <br>
				PAR No. <?php echo $policy_id; ?> <br>
				Cred. Br.   : HC <br>
				Policy ID   : <?php echo $policy_id; ?>
			</div>
			<div style="width:50%">

			</div>
		</div>
		<img src="<?php echo base_url("assets/images/itp_formal_quote_footer.png") ?>">
	</body>
</html>