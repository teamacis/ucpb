<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8"/>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<title></title>
		<style type="text/css">
			body {
				font-size: 9px;
				font-family:Arial;
			}
		</style>
	</head>

	<body>
		<div style="position:relative;top:0;">
			<img src="<?php echo base_url("assets/images/itp_formal_quote_header.png") ?>">
		</div>
		<p align="center" style="font-size:14px"><strong>INTERNATIONAL TRAVEL PERSONAL ACCIDENT</strong></h2>
		<p align="center" style="font-size:14px">CONFIRMATION OF COVER</p>
		<p align="center" style="font-size:10px"><strong>Valid for all the 25 Schengen Member States</strong></p>

		<table>
			<tr>
				<td widh="15%">&nbsp;</td>
				<td align="left" width="30%" colspan="2">INSURED</td>
				<td align="center" width="5%">:</td>
				<td align="left"  width="50%"><?php echo $personal_info['full_name']; ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2">POLICY NO</td>
				<td align="center">:</td>
				<td><?php echo $policy_id; ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td width="25%">PERIOD OF INSURANCE</td>
				<td width="5%" align="left" >FROM</td>
				<td align="center">:</td>
				<td><?php echo $itinerary_info['departure_date']; ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td width="25%">(Both dates inclusive)</td>
				<td width="5%">TO</td>
				<td align="center">:</td>
				<td><?php echo $itinerary_info['arrival_date']; ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2">DESTINATION</td>
				<td align="center">:</td>
				<td><?php echo $itinerary_info['destinations']; ?></td>
			</tr>
		</table>
		<h4 style="font-size: 8px;" align="center">TABLE OF COVERAGE AND BENEFITS</h4>
		<table border="1" cellspacing="0" cellpadding="3">
			<tr>
				<th colspan="2" width="70%" align="center">Coverage</th>
				<th width="30%" align="center">Limit</th>
			</tr>
			<tr>
				<td colspan="2" width="70%">I. <?php echo $coverage['accidental_death']['label']; ?></td>
				<td width="30%"><?php echo $coverage['accidental_death']['value']; ?></td>
			</tr>
			<tr>
				<td colspan="2" width="70%">II. <?php echo $coverage['accidental_burial_benefit']['label']; ?></td>
				<td width="30%"><?php echo $coverage['accidental_burial_benefit']['value']; ?></td>
			</tr>
			<tr>
				<td colspan="2" width="70%">III. <?php echo $coverage['personal_liability']['label']; ?></td>
				<td width="30%"><?php echo $coverage['personal_liability']['value']; ?></td>
			</tr>
			<tr>
				<td colspan="2" width="70%"><strong>IV. Travel Assistance Services</strong></td>
				<td width="30%">&nbsp;</td>
			</tr>
			<?php if (isset($coverage['travel_assistant_services']) AND is_array($coverage['travel_assistant_services']) AND count($coverage['travel_assistant_services']) > 0): ?>
				<?php $ctr = 1; ?>
				<?php foreach ($coverage['travel_assistant_services'] AS $row): ?>
					<tr>
						<td width="5%"><?php echo $ctr; ?></td>
						<td width="65%"><?php echo $row['label']; ?></td>
						<td width="30%"><?php echo $row['value']; ?></td>
					</tr>
					<?php $ctr++; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</table>
		<p align="center"><strong>*** For FAMILY PLAN, coverage for Spouse is equal to the Principal, children 18 years old and below are covered up to 50% of the Medical Expenses benefit.</strong></p>
		<br><br>
		<p style="font-size:6px;">
			The 25 Schengen Member States: <br>
			Austria, Finland, Iceland, Malta, Slovakia, Belgium, France, Italy, Netherlands, Slovenia, Czech Republic, Germany, Latvia, Norway, Spain, Denmark, Greece, Lithunia, Poland, Sweden, Estonia, Hungary, Luxembourg, Portugal and Switzerland.<br>
			<span style="font-size:7px;"><strong>CASHLESS SETTLEMENTS OF CLAIM</strong></span>
		</p>
		<p style="font-size:7px;">
			<strong>Assistance Hotline:</strong><br>
			<strong>MAPFRE ASISTENCIA, S.A. COMPANIA INTERNACIONAL DE SEGUROS Y REASEGUROS</strong> <br>
			C/Sor Angela de la Cruz 6, 28020 Madrid Spain or IBERO ASISTENCIA (Administrative Arm in the Philippines) <br>
			Call (24/7) for assistance ▪ Hotline Number  0063-27518272 ▪ Fax No.: 0063-28922465<br>
			E-mail: ficosiam@iberoasistencia.ph
		</p>
		<br>
		<br>
		<p align="right">
			________________________ <br>
			Authorized Signature&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</p>
		<img src="<?php echo base_url("assets/images/itp_formal_quote_footer.png") ?>">
	</body>
</html>