Dear <?php echo $fname . ' ' . $lname; ?>,<br>
<br>
Thank you for choosing UCPB GEN as your insurance partner!<br>
<br>
We are pleased to inform you that your transaction has been completed. Attached in this email are the following documents for your policy:<br>
1.  Policy Schedule<br>
2.  Policy Terms and Conditions<br>
3.  Completed Application Form<br>
4.  Formal Quotation<br>
<?php if ( $country_type == 3 ): ?>
5.  Confirmation of Cover<br>
<?php endif; ?>
<br>
Your Official Receipt <?php echo $country_type == 3 ? "[and Certificate of Cover] " : ""; ?>will be delivered to your mailing address within 48 (sample only) hours.<br>
To access electronic copies of these documents anytime, please activate your online UCPB GEN account at<br>
<a href='<?php echo site_url("client/verify/" . $token); ?>'><?php echo site_url("client/verify/" . $token); ?></a>.<br>
<br>
For further assistance and additional concerns, you may reach us at contact numbers;