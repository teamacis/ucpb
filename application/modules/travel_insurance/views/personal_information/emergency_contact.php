<div class="form_content content_bg">
	<div class="line_title"><p>Person to contact in case of emergency</p></div>
	<div class="form_line">
		<?php if ($emergency_contacts AND isset($emergency_contacts[0])): ?>		
			<?php foreach ($emergency_contacts[0] AS $index => $ec): ?>
				<div class="item_field" data-model="emergency_contact" data-member-id="0">
					<?php //echo form_hidden("emergency_contacts[0][{$index}][itp_quote_pk]", $itp_quote->pk); ?>
					<?php echo form_hidden("emergency_contacts[0][{$index}][itp_member_pk]", 0); ?>
					<div class="col-md-4 form_input al_ver">
						<a href="#" class="btn_plus active" data-button="add_emergency_contact" data-clone-template="primary_contact"></a>	
						<span class="value_field">
							<?php echo $itp_quote['insurance_cover'] == 'individual' ? $personal_information->fname . ' ' . $personal_information->lname : $personal_information->contact_person; ?>
						</span>
					</div>
					<div class="col-md-8 input_group">
						<div class="col-md-4 form_input">
							<input type="text" name="emergency_contacts[0][<?php echo $index; ?>][name]" class="form-control required" placeholder="Name" value="<?php echo $ec['name']; ?>" />
							<?php echo form_error("emergency_contacts[0][{$index}][name]"); ?>
						</div>
						<div class="col-md-4 form_input">
							<input type="text" name="emergency_contacts[0][<?php echo $index; ?>][contact_number]" class="form-control required" placeholder="Contact Number" value="<?php echo $ec['contact_number']; ?>" />
							<?php echo form_error("emergency_contacts[0][{$index}][contact_number]"); ?>
						</div>
						<div class="col-md-3 form_input">
							<!-- select name="emergency_contacts[0][<?php echo $index; ?>][relation]" data-placeholder="Relations" class="form-control chosen-select required" data-placeholder="Relationship">
								<option></option>
								<?php foreach ($relations AS $value => $title): ?>
									<option value="<?php echo $value; ?>"<?php echo $value == $ec['relation'] ? " selected='selected'" : ""; ?>><?php echo $title; ?></option>
								<?php endforeach; ?>
							</select> -->
							<input type="text" name="emergency_contacts[0][<?php echo $index; ?>][relation]" class="form-control required" placeholder="Relationship" value="<?php echo $ec['relation']; ?>" />
							<?php echo form_error("emergency_contacts[0][{$index}][relation]"); ?>
						</div>
						<div class="col-md-1 form_input btn_type"><a href="#" class="btn_remove<?php echo $index > 0 ? ' active' : ''; ?>" data-button="remove_emergency_contact"></a></div>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<div class="item_field" data-model="emergency_contact" data-member-id="0">
				<?php //echo form_hidden("emergency_contacts[0][0][itp_quote_pk]", $itp_quote->pk); ?>
				<?php echo form_hidden('emergency_contacts[0][0][itp_member_pk]', 0); ?>
				<div class="col-md-4 form_input al_ver">
					<a href="#" class="btn_plus active" data-button="add_emergency_contact" data-clone-template="primary_contact"></a>	
					<span class="value_field">
						<?php echo $itp_quote['insurance_cover'] == 'individual' ? $personal_information->fname . ' ' . $personal_information->lname : $personal_information->contact_person; ?>
					</span>
				</div>
				<div class="col-md-8 input_group">
					<div class="col-md-4 form_input"><input type="text" name="emergency_contacts[0][0][name]" class="form-control required" placeholder="Name" /></div>
					<div class="col-md-4 form_input"><input type="text" name="emergency_contacts[0][0][contact_number]" class="form-control required" placeholder="Contact Number" /></div>
					<div class="col-md-3 form_input">
						<?php /*<select name="emergency_contacts[0][0][relation]" data-placeholder="Relations" class="form-control chosen-select required" data-placeholder="Relationship">
							<option></option>
							<?php foreach ($relations AS $value => $title): ?>
								<option value="<?php echo $value; ?>"><?php echo $title; ?></option>
							<?php endforeach; ?>
						</select>*/ ?>
						<input type="text" name="emergency_contacts[0][0][relation]" class="form-control required" placeholder="Relationship" />
					</div>
					<div class="col-md-1 form_input btn_type"><a href="#" class="btn_remove" data-button="remove_emergency_contact"></a></div>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($members) AND is_array($members) AND count($members) > 0): ?>
			<?php foreach ($members AS $member_index => $member): ?>
				<?php $member = (object) $member; ?>
				<?php if (isset($emergency_contacts[$member_index])): ?>
					<?php foreach ($emergency_contacts[$member_index] AS $index => $mec): ?>
						<div class="item_field" data-model="emergency_contact" data-member-id="<?php echo $member_index; ?>">
							<?php //echo form_hidden("emergency_contacts[{$member_index}][{$index}][itp_quote_pk]", $itp_quote->pk); ?>
							<?php //echo form_hidden("emergency_contacts[{$member_index}][{$index}][itp_member_pk]", $member_index); ?>
							<div class="col-md-4 form_input al_ver">
								<a href="#" class="btn_plus active" data-button="add_emergency_contact" data-clone-template="member_contact_<?php echo $member_index; ?>"></a>	
								<span class="value_field">
									<?php echo $member->first_name . ' ' . $member->last_name; ?>
								</span>
							</div>
							<div class="col-md-8 input_group">
								<div class="col-md-4 form_input">
									<input type="text" name="emergency_contacts[<?php echo $member_index; ?>][<?php echo $index; ?>][name]" class="form-control required" placeholder="Name" value="<?php echo $mec['name']; ?>" />
									<?php echo form_error("emergency_contacts[{$member_index}][{$index}][name]"); ?>
								</div>
								<div class="col-md-4 form_input">
									<input type="text" name="emergency_contacts[<?php echo $member_index; ?>][<?php echo $index; ?>][contact_number]" class="form-control required" placeholder="Contact Number" value="<?php echo $mec['contact_number']; ?>" />
									<?php echo form_error("emergency_contacts[{$member_index}][{$index}][contact_number]"); ?>
								</div>
								<div class="col-md-3 form_input">
									<?php /*<select name="emergency_contacts[<?php echo $member_index; ?>][<?php echo $index; ?>][relation]" data-placeholder="Relations" class="form-control chosen-select required" data-placeholder="Relationship">
										<option></option>
										<?php foreach ($relations AS $value => $title): ?>
											<option value="<?php echo $value; ?>"<?php echo $value == $mec['relation'] ? " selected='selected'" : ""; ?>><?php echo $title; ?></option>
										<?php endforeach; ?>
									</select>*/ ?>
									<input type="text" name="emergency_contacts[<?php echo $member_index; ?>][<?php echo $index; ?>][relation]" class="form-control required" placeholder="Relationship" value="<?php echo $mec['relation']; ?>" />
									<?php echo form_error("emergency_contacts[{$member_index}][{$index}][relation]"); ?>
								</div>
								<div class="col-md-1 form_input btn_type"><a href="#" class="btn_remove active" data-button="remove_emergency_contact"></a></div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php else: ?>
					<div class="item_field" data-model="emergency_contact" data-member-id="<?php echo $member_index; ?>">
						<?php //echo form_hidden("emergency_contacts[{$member_index}][0][itp_quote_pk]", $itp_quote->pk); ?>
						<?php //echo form_hidden("emergency_contacts[{$member_index}][0][itp_member_pk]", $member_index); ?>
						<div class="col-md-4 form_input al_ver">
							<a href="#" class="btn_plus active" data-button="add_emergency_contact" data-clone-template="member_contact_<?php echo $member_index; ?>"></a>	
							<span class="value_field"><?php echo $member->first_name . ' ' . $member->last_name; ?></span>
						</div>
						<div class="col-md-8 input_group">
							<div class="col-md-4 form_input"><input type="text" name="emergency_contacts[<?php echo $member_index; ?>][0][name]" class="form-control required" placeholder="Name" /></div>
							<div class="col-md-4 form_input"><input type="text" name="emergency_contacts[<?php echo $member_index; ?>][0][contact_number]" class="form-control required" placeholder="Contact Number" /></div>
							<div class="col-md-3 form_input">
								<?php /*<select name="emergency_contacts[<?php echo $member_index; ?>][0][relation]" data-placeholder="Relations" class="form-control chosen-select required" data-placeholder="Relationship">
									<option></option>
									<?php foreach ($relations AS $value => $title): ?>
										<option value="<?php echo $value; ?>"><?php echo $title; ?></option>
									<?php endforeach; ?>
								</select>*/ ?>
								<input type="text" name="emergency_contacts[<?php echo $member_index; ?>][0][relation]" class="form-control required" placeholder="Relationship" />
							</div>
							<div class="col-md-1 form_input btn_type"><a href="#" class="btn_remove active" data-button="remove_emergency_contact"></a></div>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>