<div class="form_content content_bg">
	<div class="line_title"><p>Beneficiaries</p></div>
	<div class="form_line">
		<?php if ($beneficiaries AND isset($beneficiaries[0])): ?>		
			<?php foreach ($beneficiaries[0] AS $index => $b): ?>
				<div class="item_field" data-model="beneficiary" data-member-id="0">
					<?php //echo form_hidden("beneficiaries[0][{$index}][itp_quote_pk]", $itp_quote->pk); ?>
					<?php echo form_hidden("beneficiaries[0][{$index}][itp_member_pk]", 0); ?>
					<div class="col-md-4 form_input al_ver">
						<?php if ($user_type != COVERAGE_GROUP): ?>
							<a href="#" class="btn_plus active" data-button="add_beneficiary" data-clone-template="primary_beneficiary"></a>
						<?php endif; ?>
						<span class="value_field">
							<?php echo $itp_quote['insurance_cover'] == 'individual' ? $personal_information->fname . ' ' . $personal_information->lname : $personal_information->contact_person; ?>
						</span>
					</div>
					<div class="col-md-8 input_group">
						<div class="col-md-6 form_input">
							<input type="text" name="beneficiaries[0][<?php echo $index; ?>][name]" value="<?php echo $b['name']; ?>" class="form-control required" placeholder="Name" />
							<?php echo form_error("beneficiaries[0][{$index}][name]"); ?>
						</div>
						<div class="col-md-5 form_input">
							<select name="beneficiaries[0][<?php echo $index; ?>][relation]" class="form-control chosen-select" data-placeholder="Relationship">
								<option></option>
								<?php $relationship = $personal_information->civil_status == 'Married' ? $immediate_family : $relations; ?>
								<?php foreach ($relationship AS $value => $title): ?>
									<option value="<?php echo $value; ?>"<?php echo $value == $b['relation'] ? " selected='selected'" : ""; ?>><?php echo $title; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo form_error("beneficiaries[0][{$index}][relation]"); ?>
						</div>
						<div class="col-md-1 form_input btn_type"><a href="#" class="btn_remove<?php echo $index > 0 ? ' active' : ''; ?>" data-button="remove_beneficiary"></a></div>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<div class="item_field" data-model="beneficiary" data-member-id="0">
				<?php //echo form_hidden("beneficiaries[0][0][itp_quote_pk]", $itp_quote->pk); ?>
				<?php echo form_hidden('beneficiaries[0][0][itp_member_pk]', 0); ?>
				<div class="col-md-4 form_input al_ver">
					<?php if ($user_type != COVERAGE_GROUP): ?>
						<a href="#" class="btn_plus active" data-button="add_beneficiary" data-clone-template="primary_beneficiary"></a>
					<?php endif; ?>
					<span class="value_field">
						<?php echo $itp_quote['insurance_cover'] == 'individual' ? $personal_information->fname . ' ' . $personal_information->lname : $personal_information->contact_person; ?>
					</span>
				</div>
				<div class="col-md-8 input_group">
					<div class="col-md-6 form_input">
						<input type="text" name="beneficiaries[0][0][name]" class="form-control required" placeholder="Name" />
					</div>
					<div class="col-md-5 form_input">
						<select name="beneficiaries[0][0][relation]" class="form-control chosen-select" data-placeholder="Relationship">
							<option></option>
							<?php $relationship = $personal_information->civil_status == 'Married' ? $immediate_family : $relations; ?>
							<?php foreach ($relationship AS $value => $title): ?>
								<option value="<?php echo $value; ?>"><?php echo $title; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-md-1 form_input btn_type"><a href="#" class="btn_remove"data-button="remove_beneficiary"></a></div>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($members) AND is_array($members) AND count($members) > 0): ?>
			<?php foreach ($members AS $member_index => $member): ?>
				<?php $member =(object) $member; ?>
				<?php if (isset($beneficiaries[$member_index])): ?>
					<?php foreach ($beneficiaries[$member_index] AS $index => $b): ?>
						<div class="item_field" data-model="beneficiary" data-member-id="<?php echo $member_index; ?>">
							<?php //echo form_hidden("beneficiaries[{$member_index}][{$index}][itp_quote_pk]", $itp_quote->pk); ?>
							<?php echo form_hidden("beneficiaries[{$member_index}][{$index}][itp_member_pk]", $member_index); ?>
							<div class="col-md-4 form_input al_ver">
								<?php if ($user_type != COVERAGE_GROUP): ?>
									<a href="#" class="btn_plus active" data-button="add_beneficiary" data-clone-template="beneficiary_<?php echo $member_index; ?>"></a>	
								<?php endif; ?>
								<span class="value_field">
									<?php echo $member->first_name . ' ' . $member->last_name; ?>
								</span>
							</div>
							<div class="col-md-8 input_group">
								<div class="col-md-6 form_input">
									<input type="text" name="beneficiaries[<?php echo $member_index; ?>][<?php echo $index; ?>][name]" class="form-control required" placeholder="Name" value="<?php echo $b['name']; ?>" />
									<?php echo form_error("beneficiaries[{$member_index}][{$index}][name]"); ?>
								</div>
								<div class="col-md-5 form_input">
									<select name="beneficiaries[<?php echo $member_index; ?>][<?php echo $index; ?>][relation]" data-placeholder="Relations" class="form-control chosen-select required" data-placeholder="Relationship">
										<option></option>
										<?php $relationship = $member->civil_status == 'Married' ? $immediate_family : $relations; ?>
										<?php foreach ($relationship AS $value => $title): ?>
											<option value="<?php echo $value; ?>"<?php echo $value == $b['relation'] ? " selected='selected'" : ""; ?>><?php echo $title; ?></option>
										<?php endforeach; ?>
									</select>
									<?php echo form_error("beneficiaries[{$member_index}][{$index}][relation]"); ?>
								</div>
								<div class="col-md-1 form_input btn_type"><a href="#" class="btn_remove<?php echo $index > 0 ? ' active' : ''; ?>" data-button="remove_beneficiary"></a></div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php else: ?>
					<div class="item_field" data-model="beneficiary" data-member-id="<?php echo $member_index; ?>">
						<?php //echo form_hidden("beneficiaries[{$member_index}][0][itp_quote_pk]", $itp_quote->pk); ?>
						<?php echo form_hidden("beneficiaries[{$member_index}][0][itp_member_pk]", $member_index); ?>
						<div class="col-md-4 form_input al_ver">
							<?php if ($user_type != COVERAGE_GROUP): ?>
								<a href="#" class="btn_plus active" data-button="add_beneficiary" data-clone-template="beneficiary_<?php echo $member_index; ?>"></a>	
							<?php endif; ?>
							<span class="value_field">
								<?php echo $member->first_name . ' ' . $member->last_name; ?>
							</span>
						</div>
						<div class="col-md-8 input_group">
							<div class="col-md-6 form_input"><input type="text" name="beneficiaries[<?php echo $member_index; ?>][0][name]" class="form-control required" placeholder="Name" /></div>
							<div class="col-md-5 form_input">
								<select name="beneficiaries[<?php echo $member_index; ?>][0][relation]" data-placeholder="Relations" class="form-control chosen-select required" data-placeholder="Relationship">
									<option></option>
									<?php $relationship = $member->civil_status == 'Married' ? $immediate_family : $relations; ?>
									<?php foreach ($relationship AS $value => $title): ?>
										<option value="<?php echo $value; ?>"><?php echo $title; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-md-1 form_input btn_type"><a href="#" class="btn_remove active" data-button="remove_beneficiary"></a></div>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>