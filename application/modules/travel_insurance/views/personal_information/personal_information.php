<?php $this->load->view('user/quote_sidebar', array('tab' => 'travel-insurance'));?>

<div class="col-md-9 col-sm-9">
    <div class="col-md-12 col-xs-12 title_bar">
        <h3>Agent Information</h3>
        <p></p>
    </div>
    <?php if ($this->session->flashdata('success_message')): ?>
        <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success_message'); ?></div>
    <?php endif; ?>
    <form name="personalform" id="personalform" class="form_style" method="post" action="<?php echo base_url("travel-insurance/quote/personal_information?token={$token}")?>">
        <!-- CONTENT FIELD -->
        <?php echo form_hidden('insurance_cover', $itp_quote['insurance_cover']); ?>
        <div class="col-md-12 content_field">
            <!-- AGENT TYPE -->
            <div class="form_content">
                <div class="radio_type form_input">
                    <p class="radio_type_title">Do you have an agent with <strong>UCPB Gen</strong>?</p>
                    <div class="radio_group">
                        <label class="radio-inline">
                            <input type="radio" name="with_agent" value="on" <?php if(set_value('with_agent') == 'on') { ?> checked <?php } ?>> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="with_agent" value="off" <?php if(set_value('with_agent') != 'on') { ?> checked <?php } ?>> No
                        </label>
                    </div>
                </div>
            </div>
            <!-- END OF AGENT TYPE -->
            <!-- AGENT INFO -->
            <div class="form_content agent-info" <?php if(set_value('with_agent') != 'on') { ?> style="display: none;" <?php } ?>>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <select class="form-control chosen-select" data-placeholder="Agent Type" name="agent_type">
                            <option></option>
                            <option <?php if($this->input->post('agent_type') == 'Agent Code'): ?> selected <?php endif; ?>>Agent Code</option>
                            <option <?php if($this->input->post('agent_type') == 'Agent Name'): ?> selected <?php endif; ?>>Agent Name</option>
                            <option <?php if($this->input->post('agent_type') == 'Agency Name'): ?> selected <?php endif; ?>>Agency Name</option>
                        </select>
                        <?= form_error('agent_type'); ?>
                    </div>
                    <div class="col-md-4 form_input agent-name" <?php if(!set_value('agent_type') || set_value('agent_type') != 'Agent Name') { ?>style="display: none; <?php } ?>">
                        <input type="text" class="form-control required" name="agent_fname" placeholder="First Name" value="<?= set_value('agent_fname'); ?>" />
                        <?= form_error('agent_fname'); ?>
                        <?php if($this->session->flashdata('agent_name_error')): ?>
                            <div class="error_msg"><p><?= $this->session->flashdata('agent_name_error'); ?></p></div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4 form_input agent-name" <?php if(!set_value('agent_type') || set_value('agent_type') != 'Agent Name') { ?>style="display: none; <?php } ?>">
                        <input type="text" class="form-control required" name="agent_lname" placeholder="Last Name" value="<?= set_value('agent_lname'); ?>" />
                        <?= form_error('agent_lname'); ?>
                        <?php if($this->session->flashdata('agent_name_error')): ?>
                            <div class="error_msg"><p><?= $this->session->flashdata('agent_name_error'); ?></p></div>
                        <?php endif; ?>
                    </div>
                    <!-- ====== HIDDEN AGENT CODE ====== -->
                    <div class="col-md-4 form_input agent-code" <?php if(!set_value('agent_type') || set_value('agent_type') != 'Agent Code') { ?>style="display: none; <?php } ?>">
                        <input type="text" class="form-control required" name="agent_code" placeholder="Agent's Code" value="<?= set_value('agent_code'); ?>" />
                        <?= form_error('agent_code'); ?>
                        <?php if($this->session->flashdata('agent_code_error')): ?>
                            <div class="error_msg"><p><?= $this->session->flashdata('agent_code_error'); ?></p></div>
                        <?php endif; ?>
                    </div>
                    <!-- ====== END OF HIDDEN AGENT CODE ====== -->
                    <div class="col-md-4 form_input agent-agency" <?php if(!set_value('agent_type') || set_value('agent_type') != 'Agency Name') { ?>style="display: none; <?php } ?>">
                        <input type="text" class="form-control required" name="agency_name" placeholder="Agency Name" value="<?= set_value('agency_name'); ?>" />
                        <?= form_error('agency_name'); ?>
                        <?php if($this->session->flashdata('agency_name_error')): ?>
                            <div class="error_msg"><p><?= $this->session->flashdata('agency_name_error'); ?></p></div>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- <div class="form_line">
                     <div class="col-md-4 form_input">
                         <input type="text" class="form-control" name="agent_company" placeholder="Agency Company Name" value="<?= set_value('agent_company'); ?>" />
                    </div>
                </div> -->
            </div>
            <!-- /END OF AGENT INFO -->
        </div>
        <!-- END OF CONTENT FIELD -->
        <div class="col-md-12 title_bar">
            <?php if ($itp_quote['insurance_cover'] == 'individual'): ?>
                <h3>Personal Information</h3>
            <?php else: ?>
                <h3>Company Information</h3>
            <?php endif; ?>
            <!-- <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor.</p> -->
        </div>
        <div class="col-md-12 content_field">
            <!-- PERSONAL INFO -->
            <?php if ($itp_quote['insurance_cover'] == 'individual'): ?>
                <div class="form_content" data-property="personal_info">
                    <div class="line_title"><p>Personal Information</p></div>
                    <div class="form_line">
                        <div class="col-md-4 form_input required">
                            <select class="form-control chosen-select" data-placeholder="Salutation" name="salutation">
                                <option></option>
                                <?php foreach ($salutations as $salutation) { ?>
                                    <option value="<?= $salutation->salutation; ?>" <?= $this->input->post('salutation') ? set_select('salutation', $salutation->salutation, TRUE) : ''; ?>><?= $salutation->salutation; ?></option>';
                                <?php } ?>
                                <option value="">NONE</option>
                            </select>
                            <?= form_error('salutation'); ?>
                        </div>
                    </div>
                    <div class="form_line">
                        <div class="col-md-4 form_input">
                            <input type="text" class="form-control required" placeholder="Last Name" name="lname" <?php echo $this->session->userdata('pk') ? "readonly" : ""; ?> value="<?= set_value('lname'); ?>" />
                            <?= form_error('lname'); ?>
                        </div>
                        <div class="col-md-4 form_input">
                            <input type="text" class="form-control required" placeholder="First Name" name="fname" <?php echo $this->session->userdata('pk') ? "readonly" : ""; ?> value="<?= set_value('fname'); ?>" />
                            <?= form_error('fname'); ?>
                        </div>
                    </div>
                    <div class="form_line">
                        <div class="col-md-4 form_input">
                            <input type="text" class="form-control" placeholder="Middle Initial" name="mname" <?php echo $this->session->userdata('pk') ? "readonly" : ""; ?> value="<?= set_value('mname'); ?>" maxlength="1"/>
                            <?= form_error('mname'); ?>
                        </div>
                        <div class="col-md-4 form_input">
                            <select class="form-control chosen-select required" name="suffix" data-placeholder="Suffix">
                                <option></option>
                                <?php foreach ($suffixes as $suffix) { ?>
                                    <option value="<?= $suffix->suffix; ?>" <?= $this->input->post('suffix') ? set_select('suffix', $suffix->suffix, TRUE) : ''; ?>><?= $suffix->suffix; ?></option>;
                                <?php } ?>
                            </select>
                            <?= form_error('suffix'); ?>
                        </div>
                    </div>
                    <div class="form_inline">
                         <div class="col-md-4 form_input">
                            <?php echo form_dropdown('civil_status', $civil_statuses,  set_value('civil_status'),' class="form-control chosen-select required" data-placeholder="Civil Status"'); ?>
                            <?= form_error('civil_status'); ?>
                        </div>
                    </div>
                    <div class="form_line">
                        <div class="radio_type form_input">
                            <p class="radio_type_title">Gender</p>
                            <div class="radio_group" style="width: 150px;">
                                <label class="radio-inline">
                                    <input type="radio" value="Male" name="gender" <?php if(set_value('gender') == 'Male') { ?> checked <?php } ?>> Male
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" value="Female" name="gender" <?php if(set_value('gender') == 'Female') { ?> checked <?php } ?>> Female
                                </label>
                                <?= form_error('gender'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form_line">
                        <div class="col-md-4 form_input">
                            <input type="text" class="form-control calendar_input required birth_date" placeholder="Date of Birth" name="birthdate" value="<?= set_value('birthdate') ? set_value('birthdate') : $personal_information->birth_date; ?>" readonly />
                            <?= form_error('birthdate'); ?>
                        </div>
                        <div class="col-md-4 form_input required">
                            <select class="form-control chosen-select required" name="nationality" data-placeholder="Nationality">
                                <option></option>
                                <?php foreach ($nationalities as $nationality) { ?>
                                    <option value="<?= $nationality->nationality; ?>" <?= $this->input->post('nationality') ? set_select('nationality', $nationality->nationality, TRUE) : $nationality->nationality == 'Filipino' ? 'selected="selected"' : ''; ?>><?= $nationality->nationality; ?></option>;
                                <?php } ?>
                            </select>
                            <?= form_error('nationality'); ?>
                        </div>
                    </div>
                    <div class="form_line">
                        <div class="col-md-4 form_input required">
                            <input type="hidden" name="with_tin" value="<?= set_value('with_tin'); ?>">
                            <select class="form-control chosen-select required" name="occupation" data-placeholder="Occupation">
                                <option></option>
                                <?php foreach ($occupations as $occupation) { ?>
                                    <option data-with-tin='<?= $occupation->with_tin; ?>' value='<?= $occupation->occupation; ?>' <?= $this->input->post('occupation') ? set_select('occupation', $occupation->occupation, TRUE) : ''; ?>>
                                        <?= $occupation->occupation; ?>
                                    </option>
                                <?php } ?>
                                <option value="Others" data-with-tin="0" <?= $this->input->post('occupation') ? set_select('occupation', 'Others', TRUE) : ''; ?>>Others</option>
                            </select>
                            <?= form_error('occupation'); ?>
                        </div>
                        <div class="col-md-4 form_input other-occupation" <?php if(set_value('occupation') != 'Others'): ?>style="display: none;"<?php endif; ?>>
                            <input type="text" class="form-control required" placeholder="Enter you occupation here" name="other_occupation" value="<?= set_value('other_occupation'); ?>"/>
                            <?= form_error('other_occupation'); ?>
                        </div>
                        <div class="col-md-4 form_input with-tin" <?php if(set_value('with_tin') != 1): ?>style="display: none;"<?php endif; ?>>
                            <input type="text" class="form-control required" placeholder="Tax Identification Number" name="tin" value="<?= set_value('tin'); ?>"/>
                            <?= form_error('tin'); ?>
                        </div>
                    </div>
                    <div class="form_line without-tin" <?php if(set_value('with_tin') != 0): ?>style="display: none;"<?php endif; ?>>
                        <div class="col-md-4 form_input">
                            <?php echo form_dropdown('id_type', array("" => "") + $available_id, set_value('id_type'),'class="form-control chosen-select required" data-placeholder="Select Type of ID"'); ?>
                            <?= form_error('id_type'); ?>
                        </div>
                        <div class="col-md-4 form_input">
                            <input type="text" class="form-control required" placeholder="ID Number" name="id_number" value="<?php echo set_value('id_number'); ?>" />
                            <?= form_error('id_number'); ?>
                        </div>
                    </div>
                    <div class="form_line">
                        <div class="col-md-4 form_input">
                            <input type="text" class="form-control required" placeholder="Employer" name="employer" id="personal_information_employer"
                                   value="<?php echo set_value('employer'); ?>" />
                            <?= form_error('employer'); ?>
                        </div>
                    </div>                   
                </div>
            <?php else: ?>
                <div class="form_content" data-property="company_info">
                    <div class="form_line">
                        <div class="col-md-4 form_input">
                            <input type="text" class="form-control required" placeholder="Company Name" name="name" value="<?= set_value('name'); ?>" />
                            <?= form_error('name'); ?>
                        </div>
                        <div class="col-md-4 form_input">
                            <input type="text" class="form-control calendar_input required date" placeholder="Date of Incorporation" name="incorporation_date" value="<?= set_value('incorporation_date'); ?>" />
                            <?= form_error('incorporation_date'); ?>
                        </div>
                    </div>
                    <div class="form_line">
                        <div class="col-md-4 form_input">
                            <input type="text" class="form-control required" placeholder="Industry Type" name="industry_type" value="<?= set_value('industry_type'); ?>" />
                            <?= form_error('industry_type'); ?>
                        </div>
                        <div class="col-md-4 form_input">
                            <input type="text" class="form-control required" placeholder="Contact Person" name="contact_person" value="<?= set_value('contact_person'); ?>" />
                            <?= form_error('contact_person'); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <!-- /END OF PERSONAL INFO -->
            <!-- MAILING INFO -->
            <div class="form_content">
                <div class="line_title"><p>Mailing Address</p></div>
                <?php echo form_hidden('same_location', 'off'); ?>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <?php
                            $placeholder = 'Province';
                            $disabled    = '';
                        ?>
                        <select class="form-control chosen-select location required" name="province" onchange="getCities()"
                                data-placeholder="<?= $placeholder; ?>" <?= $disabled; ?>>
                            <option></option>
                            <?php foreach ($provinces as $province) { ?>
                                <option value="<?= $province['pk']; ?>" <?= $this->input->post('province') ? set_select('province', $province['pk'], TRUE) : ''; ?>>
                                    <?= $province['place']; ?>
                                </option>
                            <?php } ?>
                        </select>
                        <?= form_error('province'); ?>
                        <input type="hidden" name="default_province" value="">
                    </div>
                    <div class="col-md-4 form_input required">
                        <?php
                            $placeholder = 'City';
                            $disabled    = '';                         
                        ?>
                        <select class="form-control chosen-select location required" name="city" onchange="getBarangays()"
                                data-placeholder="<?= $placeholder; ?>" <?= $disabled; ?>>
                            <option></option>
                            <?php if($cities) { ?>
                                <?php foreach ($cities as $city) { ?>
                                    <option value="<?= $city['pk']; ?>" <?= set_select('city', $city['pk'], TRUE); ?>>
                                        <?= $city['place']; ?>
                                    </option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?= form_error('city'); ?>
                        <input type="hidden" name="default_city" value="">
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input required">
                        <?php
                            $placeholder = 'Barangay';
                            $disabled    = '';
                        ?>
                        <select class="form-control chosen-select location required" name="barangay"
                                data-placeholder="<?= $placeholder; ?>" <?= $disabled; ?>>
                            <option></option>
                            <?php if($barangays) { ?>
                                <?php foreach ($barangays as $barangay) { ?>
                                    <option value="<?= $barangay['pk']; ?>" <?= set_select('barangay', $barangay['pk'], TRUE); ?>>
                                        <?= $barangay['place']; ?>
                                    </option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?= form_error('barangay'); ?>
                        <input type="hidden" name="default_barangay" value="">
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-8 form_input">
                        <input type="text" class="form-control required" placeholder="House No., Street Name, Subdivision Name" name="house_number" value="<?= set_value('house_number'); ?>"/>
                        <?= form_error('house_number'); ?>
                    </div>
                </div>
            </div>
            <!-- END OF MAILING INFO -->
            <!-- CONTACT INFO -->
            <div class="form_content">
                <div class="line_title"><p>Contact Information</p></div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="email" class="form-control required" placeholder="Email Address" name="email_address" maxlength="50" data-ng-model="formdata.email_address" data-ng-disabled="true"
                               value="<?= set_value('email_address') ? set_value('email_address') : $personal_information->email_address; ?>"/>
                        <?= form_error('email_address'); ?>
                    </div>
                </div>
                <div class="form_line">
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Mobile" name="mobile" value="<?= set_value('mobile') ? set_value('mobile') : $personal_information->mobile; ?>" />      
                    </div>
                    <div class="col-md-4 form_input">
                        <input type="text" class="form-control required" placeholder="Telephone" name="telephone" value="<?= set_value('telephone') ? set_value('telephone') : $personal_information->telephone; ?>" />
                        
                    </div>
                    <div class="col-md-8 form_input">
                        <?= form_error('mobile'); ?>
                        <?= form_error('telephone'); ?>
                    </div>
                </div>
                <p class="small_text">Note: Please provide either Telephone Number or Mobile Number</p>
            </div>
            <!-- END OF CONTACT INFO -->
            <!-- EMERGENCY CONTACT -->
                <?php $this->load->view('personal_information/emergency_contact'); ?>
            <!-- END OF EMERGENCY CONTACT  -->
            <!-- BENEFICIARIES -->
                 <?php $this->load->view('personal_information/beneficiaries'); ?>
            <!-- END OF BENEFICIARIES  -->
            <div class="form_content">
                <div class="col-md-8 form_input">
                    <div class="checkbox">
                        <label><input type="checkbox" name="delivery_policy" <?= $this->input->post('delivery_policy') ? 'checked' : ''; ?> /> Deliver hardcopy of my policy</label>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OF CONTENT FIELD -->
        <p class="form_button">
            <?php echo form_hidden('token', $token); ?>
            <a class="btn btn-success btn-lg back" href="<?php echo base_url("travel-insurance/quote/summary?token={$token}") ?>">Back</a>
            <button type="submit" name="submit" value="save" class="btn btn-primary btn-lg">Save</button>
            <button type="submit" name="submit" value="next" class="btn btn-success btn-lg">Next</button>
        </p>
    </form>
    <?php $this->load->view('personal_information/emergency_contact_template'); ?>
    <?php $this->load->view('personal_information/beneficiary_template'); ?>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('[name=birthdate]').datepicker({ autoclose: true, format: 'yyyy-mm-dd', endDate: '-18y' });
        $('.chosen-select').chosen({width:"100%", disable_search_threshold: 10, search_contains: true});
        $('.back').click(function() {
            window.location.href = '<?=site_url('motor_insurance/coverage_details')?>';
        });

        /**
         * Hides the Agent Info
         */
        $('[name=with_agent').change(function() {
            if($(this).val() == 'off') {
                $('.agent-info').hide();
            }
            else {
                $('.agent-info').show();
            }
        });

        /**
         * Toggles Agent Code and Agent Name
         */
        $('[name=agent_type]').change(function() {
            $('.agent-code, .agent-name, .agent-agency').hide();
            if($(this).val() == 'Agent Code') {
                 $('.agent-code').show();
            }
            else if($(this).val() == 'Agent Name') {
                 $('.agent-name').show();
            }
            else if($(this).val() == 'Agency Name') {
                $('.agent-agency').show();
            }
        });

        /**
         * Toggles TIN and ID information
         */
        $('[name=occupation').change(function() {
            tinControl();
            otherOccupation();
        });

        function tinControl() {
            var tin = parseInt($('[name=occupation').find(":selected").attr('data-with-tin'));
            $('[name=with_tin]').val(tin);

            $('.with-tin, .without-tin').hide();
            if(tin) {
                $('.with-tin').show();
            }
            else if(tin == '0' || tin == 0){
                $('.without-tin').show();
            }
        }

        function otherOccupation() {
            var bIsOthers = $("select[name=occupation]").val() == 'Others';

            if ( bIsOthers ) {
                $(".other-occupation").show();
            } else {
                $(".other-occupation").hide();
            }
        }

        // Must run on first load to control tin.
        tinControl();
        otherOccupation();

        /**
         * Mailing Address
         */
        var locationUserInput = {};

        $('[name=same_location]').click(function() {
            checkSameLocation();
        });

        function checkSameLocation() {
            var location = $('.location');
            if($('[name=same_location]').prop('checked')) {
                location.attr('disabled', 'disabled');
                updateLocationUserInput(true);
            }
            else {
                location.removeAttr('disabled');
                updateLocationUserInput();
            }

            location.trigger('chosen:updated');
        }

        function updateLocationUserInput(setDefault) {
            var province = $('[name=province]');
            var city     = $('[name=city]');
            var barangay = $('[name=barangay]');

            if(setDefault) {
                locationUserInput.province = province.val();
                locationUserInput.city     = city.val();
                locationUserInput.barangay = barangay.val();

                province.val('').attr('data-placeholder', defaultLabels.province);
                city.val('').attr('data-placeholder', defaultLabels.city);
                barangay.val('').attr('data-placeholder', defaultLabels.barangay);
            }
            else {
                province.val(locationUserInput.province).attr('data-placeholder', 'Province');
                city.val(locationUserInput.city).attr('data-placeholder', 'City');
                barangay.val(locationUserInput.barangay).attr('data-placeholder', 'Barangay');
            }
        }

    });

    /**
     * LOCATION FETCH
     */
    function getCities() {
        common.ajax('<?=site_url('place/cities')?>', {province: $("[name=province]").val()},
            function(response) {
                if (response.length > 0) {
                    var options = '<option></option>';
                    $.each(response, function(i, v) {
                        options += '<option value="' + v.pk + '">' + v.place + '</option>';
                    });
                    $("[name=city]").html(options).prop("disabled", false).trigger("chosen:updated");
                    $("[name=barangay]").val(null).prop("disabled", true).trigger("chosen:updated");
                }
            });
    }
    function getBarangays() {
        common.ajax('<?=site_url('place/barangays')?>', {city: $("[name=city]").val()},
            function(response) {
                if (response.length > 0) {
                    var options = '<option></option>';
                    $.each(response, function(i, v) {
                        options += '<option value="' + v.pk + '">' + v.place + '</option>';
                    });
                    $("[name=barangay]").html(options).prop("disabled", false).trigger("chosen:updated");
                }
            });
    }
</script>