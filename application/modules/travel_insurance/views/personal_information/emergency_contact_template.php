<div class="hidden">
	<div class="item_field" data-model="emergency_contact" data-template="primary_contact" data-member-id="0">
		<?php //echo form_hidden("emergency_contacts[0][{counter}][itp_quote_pk]", $itp_quote->pk); ?>
		<?php echo form_hidden("emergency_contacts[0][{counter}][itp_member_pk]", 0); ?>
		<div class="col-md-4 form_input al_ver">
			<a href="#" class="btn_plus active" data-button="add_emergency_contact" data-clone-template="primary_contact"></a>	
			<span class="value_field">
				<?php echo $itp_quote['insurance_cover'] == 'individual' ? $personal_information->fname . ' ' . $personal_information->lname : $personal_information->contact_person; ?>
			</span>
		</div>
		<div class="col-md-8 input_group">
			<div class="col-md-4 form_input"><input type="text" name="emergency_contacts[0][{counter}][name]" class="form-control required" placeholder="Name" /></div>
			<div class="col-md-4 form_input"><input type="text" name="emergency_contacts[0][{counter}][contact_number]" class="form-control required" placeholder="Contact Number" /></div>
			<div class="col-md-3 form_input">
				<select data-chosen-select="true" name="emergency_contacts[0][{counter}][relation]" data-placeholder="Relations" class="form-control required" data-placeholder="Relationship">
					<option></option>
					<?php foreach ($relations AS $value => $title): ?>
						<option value="<?php echo $value; ?>"><?php echo $title; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="col-md-1 form_input btn_type"><a href="#" class="btn_remove active" data-button="remove_emergency_contact"></a></div>
		</div>
	</div>
	<?php if (isset($members) AND is_array($members) AND count($members) > 0): ?>
		<?php foreach ($members AS $member_index => $member): ?>
			<?php $member = (object) $member; ?>
			<div class="item_field" data-model="emergency_contact" data-member-id="<?php echo $member_index; ?>" data-template="member_contact_<?php echo $member_index; ?>">
				<?php //echo form_hidden("emergency_contacts[{$member_index}][{counter}][itp_quote_pk]", $itp_quote->pk); ?>
				<?php echo form_hidden("emergency_contacts[{$member_index}][{counter}][itp_member_pk]", $member_index); ?>
				<div class="col-md-4 form_input al_ver">
					<a href="#" class="btn_plus active" data-button="add_emergency_contact" data-clone-template="member_contact_<?php echo $member_index; ?>"></a>	
					<span class="value_field"><?php echo $member->first_name . ' ' . $member->last_name; ?></span>
				</div>
				<div class="col-md-8 input_group">
					<div class="col-md-4 form_input"><input type="text" name="emergency_contacts[<?php echo $member_index; ?>][{counter}][name]" class="form-control required" placeholder="Name" /></div>
					<div class="col-md-4 form_input"><input type="text" name="emergency_contacts[<?php echo $member_index; ?>][{counter}][contact_number]" class="form-control required" placeholder="Contact Number" /></div>
					<div class="col-md-3 form_input">
						<select data-chosen-select="true" name="emergency_contacts[<?php echo $member_index; ?>][{counter}][relation]" data-placeholder="Relations" class="form-control required" data-placeholder="Relationship">
							<option></option>
							<?php foreach ($relations AS $value => $title): ?>
								<option value="<?php echo $value; ?>"><?php echo $title; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-md-1 form_input btn_type"><a href="#" class="btn_remove active" data-button="remove_emergency_contact"></a></div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>