<div class="col-md-12 no_gutter">
	<table class="table summary_table table_2">
		<colgroup>
			<col width="50%" />
			<col width="50%" />
		</colgroup>
		<thead>
			<tr>
				<th colspan="2">Gold Quotation</th>
			</tr>	
		</thead>
		<tbody>
			<tr>
				<th><span class="pre_ltr">I.</span><strong>Accidental Death / Disablement</strong></th>
				<td><span>US $ 25,000.00</span></td>
			</tr>
			<tr>
				<th><span class="pre_ltr">II.</span><strong>Accidental Brutal Benefit</th>
				<td><span>US $ 500.00</span></td>
			</tr>
			<tr>
				<th><span class="pre_ltr">III.</span><strong>Personal Liability</strong></th>
				<td><span>US $ 10,000.00</span></td>
			</tr>
			<tr>
				<th>
					<span class="pre_ltr">IV.</span><strong>Travel Assistance Services</strong>
					<ul class="list">
						<li>Medical expense (including Sabotage and Terrorism coverage) and hospitalization abroad ***</li>
						<li>Transport or repatriation in case of illness or accident</li>
						<li>Emergency dental care</li>
						<li>Repatriation of a family member traveling with the insured</li>
						<li>Repatriation of mortal remains</li>
						<li>Escort of dependent child</li>
						<li>Travel of one immediate family member</li>
						<li>Emergency return home following death of a close family member</li>
						<li>Delivery of Medicines</li>
						<li>Relay of urgent messages</li>
						<li>Long distance medical information service</li>
						<li>Medical referral/appointment of local medical specialist</li>
						<li>Connection services</li>
						<li>Advance of bail bond</li>
						<li>Trip Cancellation</li>
						<li>Reimbursement of Forfeited Holidays/Trip Curtailment</li>
						<li>Delayed Departure</li>
						<li>Flight Misconnection</li>
						<li>Baggage Delay</li>
						<li>Compensation for in-flight loss of checked-in baggage</li>
						<li>Location and forwarding of baggage and personal effects</li>
						<li>Loss of Passport, Driving License, National Identity Card Abroad</li>
					</ul>
				</th>
				<td>
					<span></span><br />
					<span>US $ 45,000.00</span><br><br><br><br><br>
					<span>Actual Expense</span><br><br><br>
					<span>US $ 200.00</span><br />
					<span>Actual Expense</span><br><br><br>
					<span>Actual Expense</span><br><br>
					<span>Actual Expense</span><br><br>
					<span>Travel cost plus up to 100 US$/day maximum 1,000 $</span><br>
					<span>Actual Expense</span><br><br><br>
					<span>Actual Expense</span><br>
					<span>Actual Expense</span><br><br>
					<span>Actual Expense</span><br><br>
					<span>Actual Expense</span><br><br><br>
					<span>Actual Expense</span><br>
					<span>US $ 1000.00</span><br>
					<span>US $ 3000.00</span><br>
					<span>Up to US $ 650.00</span><br><br><br>
					<span>US $ 200.00</span><br>
					<span>US $ 150.00</span><br>
					<span>US $ 90.00</span><br>
					<span>Up to US $ 1,200.00 subject to limit US $ 150.00 for any item</span><br><br>
					<span>Actual Expense</span><br><br><br>
					<span>$ US 250.00</span>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="disclaimer_section">
		<p>
            <span>
                *** For FAMILY PLAN, coverage for Spouse is equal to the Principal, children 18 years
                old and below are covered up to 50% of the Medical Expenses benefit.
                <br><br>
                Please feel free to call us at tel. No. 811-1788 should there anything you wish to clarify.
            </span>
        </p>
	</div>
</div>