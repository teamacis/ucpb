<div class="col-md-12 no_gutter">
	<table class="table summary_table table_1">
		<colgroup>
			<col width="50%" />
			<col width="50%" />
		</colgroup>
		<thead>
			<tr>
				<th>Net Premium</th>
				<td><span>USD <?php echo number_format($premium->net_premium,2); ?></span></td>
			</tr>	
		</thead>
		<tbody>
			<tr>
				<th colspan="2">Plus:</th>
			</tr>
			<tr>
				<th>Documentary Stamps</th>
				<td><span><?php echo number_format($premium->dst,2); ?></span></td>
			</tr>
			<tr>
				<th>Premium Tax</th>
				<td><span><?php echo number_format($premium->premium_tax,2); ?></span></td>
			</tr>
			<tr>
				<th>Local Government Tax</th>
				<td><span><?php echo number_format($premium->lgt,2); ?></span></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<th>Total Premium</th>
				<td>
					<span>USD <?php echo number_format($premium->net_total_usd,2); ?></span>
					<br>
					<span>PHP <?php echo number_format($premium->net_total_php,2); ?></span>
				</td>
			</tr>
			<?php if ( $premium->for_delivery ): ?>
				<tr>
					<th>Delivery Charge</th>
					<td><span>PHP <?php echo number_format($premium->delivery_charge,2); ?></span></td>
				</tr>
				<tr>
					<th>Total Amount Due</th>
					<td><span>PHP <?php echo number_format($premium->total_amount,2); ?></span></td>
				</tr>
			<?php endif; ?>	
		</tfoot>
	</table>
	<!-- <div class="disclaimer_section">
		<h6>Documentary Stamps Tax</h6>
		<p>
			This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit. 
		</p>
	</div> -->
</div>