<!-- /process steps -->
<div class="container motor_content box">
	<div class="row no_gutter">
		<div class="col-md-12 col-sm-12 full_length">
			<div class="col-md-12 title_bar">
				<h3>Summary &amp; Confirmation</h3>
			</div>
		<!-- quote summary -->
			<div class="new_row">
				<div class="col-md-6">
					<?php if ($itp_quote['insurance_cover'] == 'individual'): ?>
						<table class="table summary_table table_3">
							<thead>
								<tr>
									<th colspan="2">Personal Information</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><strong>Name:</strong><span class="value_2"><?php echo $personal_information->fname . ' ' . $personal_information->lname; ?></span></td>
								</tr>
							</tbody>
						</table>
					<?php else: ?>
						<table class="table summary_table table_3">
							<thead>
								<tr>
									<th colspan="2">Company Information</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><strong>Name:</strong><span class="value_2"><?php echo $personal_information->name; ?></span></td>
								</tr>
							</tbody>
						</table>
					<?php endif ; ?>
					<table class="table summary_table table_3">
						<thead>
							<tr>
								<th colspan="2">Contact Information</th>
							</tr>
						</thead>
						<tbody>
							<?php if ($itp_quote['insurance_cover'] == 'company'): ?>
								<tr>
									<td><strong>Contact Person:</strong><span class="value_1"><?php echo $personal_information->contact_person; ?></span></td>
								</tr>
							<?php endif; ?>
							<tr>
								<td><strong>Telephone Number:</strong><span class="value_1"><?php echo $personal_information->telephone; ?></span></td>
							</tr>
							<tr>
								<td><strong>Mobile Number:</strong><span class="value_1"><?php echo $personal_information->mobile; ?></span></td>
							</tr>
							<tr>
								<td><strong>Email:</strong><span class="value_1"><?php echo $personal_information->email_address; ?></span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-6">
					<table class="table summary_table table_3">
						<thead>
							<tr>
								<th colspan="4">Travel Information</th>
							</tr>
						</thead>
						<tbody>	
							<tr>
								<td colspan="2"><strong>Purpose of Travel:</strong><span class="value_2"><?php echo $travel_information->purpose_of_travel; ?></span></td>							
							</tr>
							<tr>
								<td colspan="2"><strong>Type of Coverage:</strong><span class="value_2"><?php echo $travel_information->coverage_type; ?></span></td>
							</tr>
							<tr>
								<td colspan="2"><strong>Start Date:</strong><span class="value_2"><?php echo date("m-d-Y",strtotime($premium->departure_date)); ?></span></td>
							</tr>
							<tr>
								<td colspan="2"><strong>End Date:</strong><span class="value_2"><?php echo date("m-d-Y",strtotime($premium->arrival_date)); ?>  (Both dates inclusive)</span></td>
							</tr>
						</tbody>
					</table>
					<table class="table summary_table table_3">
						<thead>
							<tr>
								<th colspan="4">Itineraries</th>
							</tr>
						</thead>
						<tbody>
							<?php if (isset($itineraries) AND is_array($itineraries) AND count($itineraries) > 0): ?>
								<?php foreach ($itineraries AS $itinerary): ?>
									<?php $itinerary = (object) $itinerary; ?>
									<tr>
										<td><strong>Destination:</strong><span class="value_2"><?php echo $itinerary->country; ?></span></td>
										<td><strong>Date:</strong><span class="value_2"><?php echo date("M d, Y",strtotime($itinerary->start_date)) . ' - ' . date("M d, Y",strtotime($itinerary->end_date)); ?></span></td>
									</tr>
								<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					<?php /*if (isset($members) AND is_array($members) AND count($members) > 0): ?>
						<table class="table summary_table table_3">
							<thead>
								<tr>
									<th colspan="4">Member Information</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($members AS $member): ?>
									<tr>
										<td><strong>Name:</strong><span class="value_1"><?php echo $member->first_name . ' ' . $member->last_name; ?></span></td>
										<td><strong>Age:</strong><span class="value_1"><?php echo calculate_age($member->birth_date); ?></span></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php endif;*/ ?>
				</div>
			</div>
			<div class="new_row row_pad">
				<div class="col-md-6 summary_section table_type_2">
					<div class="section_title title_1">
						<h5>Coverage</h5>
					</div>	
					<?php $package_type == "Silver" ? $this->load->view('travel_insurance/confirmation/coverage/silver') : ''; ?>
            		<?php $package_type == "Gold" ? $this->load->view('travel_insurance/confirmation/coverage/gold') : ''; ?>
            		<?php $package_type == "Platinum" ? $this->load->view('travel_insurance/confirmation/coverage/platinum') : ''; ?>
				</div>
				<div class="col-md-6 summary_section table_type_1">
					<div class="section_title title_1">
						<h5>Premium Computation</h5>
					</div>	
					<?php $this->load->view('travel_insurance/confirmation/premium_computation'); ?>
				</div>
			</div>
			<div class="new_row row_pad terms_conditions">
				
				<form method="post" class="form_style" action="<?php echo base_url("travel-insurance/quote/confirmation?token={$token}"); ?>">
					<?php echo form_hidden('token', $token); ?>
					<div class="checkbox">
		                <label>
		                    <a href="#" data-toggle="modal" data-target="#myModal">Terms and Conditions</a>
		                    <!-- <input type="checkbox"> Accept <a href="#" data-toggle="modal" data-target="#myModal">Terms and Conditions</a> -->
		                </label>
		            </div>
					<p class="form_button">
						<button type="submit" class="btn btn-success btn-lg" disabled="disabled" id="buy_button">Buy</button>	
						<button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal2">Cancel</button>
					</p>
				</form>
			</div>
			
		<!-- /quote summary -->	
		</div>
	</div>
</div>
<!-- Terns and Conditions modal -->
<div class="modal fade terms_condition_modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<button type="button" class="modal_close_btn" data-dismiss="modal" aria-label="Close"></button>
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">
					<img src="<?php echo base_url() ?>assets/images/site_logo2.png" class="logo" />
					<span>Terms and Conditions</span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="tc_content" style="overflow: hidden;">
                    <iframe src="<?=base_url('files/itp_quote/terms/index.html')?>" border="0" width="100%" height="100%"></iframe>
                </div>
			</div>
			<div align="center">
                <a href="" data-dismiss="modal" onclick="acceptTerms()"><b>Accept Terms and Conditions</b></a><br /><br />
            </div>
		</div>
	</div>
</div>
<!-- cancel modal -->
<div class="modal fade cancel_modal bs-example-modal-sm" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<button type="button" class="modal_close_btn" data-dismiss="modal" aria-label="Close"></button>
			<div class="modal-body">
				<p class="message">Are you sure you want to cancel your transaction?</p>
				<p class="form_button">
					<button type="button" onClick="cancelTransaction()" class="btn btn-primary btn-lg">Yes</button>
					<br><br>
					<button type="button" class="btn btn-success btn-lg"  data-dismiss="modal" aria-label="Close">No</button>	
				</p>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function acceptTerms(){
        $('#buy_button').removeAttr('disabled');
    }

    function cancelTransaction(){
        window.location.href = '<?=site_url("travel-insurance")?>';
    }

    $(document).ready(function(){
    	$(".modal_close_btn").click(function(e){
    		alert("Please read and accept the terms and conditions.");
    		return false;
    	});
    });
</script>