<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function post_callback($params) {
        $data = [];
        $data['transaction_id']   = $params['txnid'];
        $data['reference_number'] = $params['refno'];
        $data['status']           = $params['status'];
        $data['message']          = $params['message'];
        $data['digest']           = $params['digest'];
        $data['amount']           = $params['amount'];

        $this->db->where([
            'transaction_id'   => $data['transaction_id'],
            'reference_number' => $data['reference_number'],
            'digest'           => $data['digest']
        ]);

        $this->db->from('dragonpay_transactions');
        $query = $this->db->get();

        if ($query->num_rows() == 0) {
            $this->db->insert('dragonpay_transactions', $data);
            $dragonPayId = $this->db->insert_id();
            return [
                'line' => substr($params['txnid'], 0, 3),
                'pk'   => $dragonPayId,
                'action' => 'insert'
            ];
        }
        else {
            $this->db->where(array('transaction_id' => $params['txnid']));
            $this->db->update('dragonpay_transactions', $data);

            return [
                'line' => substr($params['txnid'], 0, 3),
                'pk'   => $query->row()->pk,
                'action' => 'update'
            ];
        }
    }

    public function get_transaction($line, $quoteId) {
        $return = false;
        $this->db->where(array("t.quotes_pk" => $quoteId));
        $this->db->from('dragonpay_transactions as t');
        $this->db->join($line . '_quotes as q', 't.quotes_pk = q.pk');
        $this->db->join('policies p','t.pk = p.dragonpay_transaction_pk','left');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return $return;
    }

    public function update($data=array(),$where=array()) {
        if ( count($data) > 0 AND count($where) > 0 ) {
            return $this->db->update("dragonpay_transactions", $data, $where);
        }
        
        return FALSE;
    }

    public function get($where=array()) {
        return $this->db->get_where("dragonpay_transactions", $where)->row();
    }
}
