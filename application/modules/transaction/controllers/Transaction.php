<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends MY_Controller {
    public function __construct() {
        parent::__construct();

        $this->scripts_array_ie = array(
            jsUrl('core/html5shiv.min.js'),
            jsUrl('core/respond.min.js')
        );
        $this->srcript_array = array(
            jsUrl('core/jquery.min.js'),
            jsUrl('core/bootstrap.min.js'),
            jsUrl('common.js')
        );

        $this->load->model('transaction_model');
        $this->load->model('motor_insurance/motor_model');
        $this->load->model('motor_insurance/motor_quote_model');
        $this->load->model('vehicle/vehicle_model');
        $this->load->model('client/client_model');
        $this->load->model('policy/policy_model');
        $this->load->model('travel_insurance/itp_quote_model');
    }

    /**
     * Dragon Pay Post Callback
     */
    public function post_callback() {
        $data = TEST ? $this->input->get() : $this->input->post();

//        $this->db->trans_begin();
        if ($data['status'] == 'S') {


            $email_address = "";
            $quote_pk = "";

            if (substr($data['txnid'], 0, 3) == 'MTR') {
                $motor_saved_quote_id = explode("-", $data['txnid']);
                $motor_saved_quote_id = (int) end($motor_saved_quote_id);
                $saved_quote = $this->motor_model->get_motor_saved_quote_by_id($motor_saved_quote_id);
                $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($saved_quote['token']), TRUE);

                $motor_info = $this->vehicle_model->get_motor_for_computation($motor_saved_quotes['motors_pk']);
                $quote_premiums = $this->motor_model->quote_premiums($motor_info, $motor_saved_quotes);

                $data['amount'] = number_format($quote_premiums['premium'], 2, '.', '');

                $dragonPay = $this->transaction_model->post_callback($data);

                $personal_information = $motor_saved_quotes['personal_information'];
                $vehicle_information = $motor_saved_quotes['vehicle_information'];
                $contact_information = $motor_saved_quotes['contact_information'];
                $coverage_details = $motor_saved_quotes['coverage_details'];

                $motor_quotes_pk = NULL;
                $motor_quotes_information_id = NULL;
                if ($dragonPay['action'] == 'insert') {

                    $personal_informations = array_merge($motor_saved_quotes['personal_information'], $motor_saved_quotes['mailing_address'], $motor_saved_quotes['contact_information']);
                    unset($personal_informations['with_tin']);
                    unset($personal_informations['email_address']);
                    $personal_informations['date_created'] = date("Y-m-d H:i:s");
                    $this->db->insert('personal_informations', $personal_informations);
                    $personal_information_id = $this->db->insert_id();

                    /**
                     * insert motor_insurance quotes
                     */
                    $motor_quotes = array(
                        "lname" => $personal_information['lname'],
                        "fname" => $personal_information['fname'],
                        "email_address" => $contact_information['email_address'],
                        "telephone" => $contact_information['telephone'],
                        "mobile" => $contact_information['mobile'],
                        "type" => $motor_saved_quotes['type'],
                        "province" => $vehicle_information['address']['province'],
                        "city" => $vehicle_information['address']['city'],
                        "barangay" => $vehicle_information['address']['province'],
                        "insurance_type" => $vehicle_information['insurance_type'],
                        "is_new" => $vehicle_information['is_new'],
                        "motors_pk" => $motor_saved_quotes['motors_pk'],
                        "year" => $vehicle_information['year'],
                        "brand" => $vehicle_information['brand'],
                        "model_name" => $motor_saved_quotes['motors_pk'] != "" ? $vehicle_information['model_name'] : $vehicle_information['other_model'],
                        "market_value" => $vehicle_information['market_value'],
                        "original_market_value" => $vehicle_information['original_market_value'],
                        "status" => 'APPROVED',
                        "token" => $motor_saved_quotes['token'],
                        "date_created" => date("Y-m-d H:i:s")
                    );
                    $this->db->insert('motor_quotes', $motor_quotes);
                    $motor_quotes_pk = $this->db->insert_id();

                    /**
                     * insert motor_insurance quotes informations
                     */
                    $motor_quotes_informations = array(
                        "motor_quotes_pk" => $motor_quotes_pk,
                        "mortgagees_pk" => $vehicle_information['mortgagees_pk'],
                        "insurance_providers_pk" => $vehicle_information['insurance_providers_pk'],
                        "overnight_parking" => $vehicle_information['overnight_parking'],
                        "plate_number" => $vehicle_information['plate_number'],
                        "engine_number" => $vehicle_information['engine_number'],
                        "chassis_number" => $vehicle_information['chassis_number'],
                        "mv_file_number" => $vehicle_information['mv_file_number'],
                        "color" => $vehicle_information['color'],
                        "or_cr" => $vehicle_information['or_cr']
                    );
                    $this->db->insert('motor_quotes_informations', $motor_quotes_informations);
                    $motor_quotes_information_id = $this->db->insert_id();

                    /**
                     * insert non standard accessories
                     */
                    foreach ($vehicle_information['non_standard'] as $value) {
                        $non_standard = array(
                            "motor_quotes_pk" => $motor_quotes_pk,
                            "motor_accessories" => $value['non_standard_accessories'],
                            "value" => $value['non_standard_values']
                        );

                        $this->db->insert('motor_quotes_non_standards', $non_standard);
                        $motor_quotes_non_standards = $this->db->insert_id();
                    }
                    /**
                     * insert standard accessories
                     */
                    foreach ($vehicle_information['other_accessories_selected'] as $value) {
                        $motor_accessory = array(
                            "motor_quotes_pk" => $motor_quotes_pk,
                            "motor_accessories" => $value
                        );

                        $this->db->insert('motor_quotes_other_standards', $motor_accessory);
                        $motor_quotes_other_standards = $this->db->insert_id();
                    }

                    if(isset($coverage_details['comprehensive_start_date'])) {
                        $this->db->insert('motor_quotes_coverages', [
                            "motor_quotes_pk" => $motor_quotes_pk,
                            "is_ctpl" => 0,
                            "start_date" => $coverage_details['comprehensive_start_date'],
                            "end_date" => $coverage_details['comprehensive_end_date'],
                        ]);
                    }
                    if(isset($coverage_details['ctpl_start_date'])) {
                        $this->db->insert('motor_quotes_coverages', [
                            "motor_quotes_pk" => $motor_quotes_pk,
                            "is_ctpl" => 1,
                            "start_date" => $coverage_details['ctpl_start_date'],
                            "end_date" => $coverage_details['ctpl_end_date'],
                        ]);
                    }

                }

                $quote_pk = $motor_quotes_pk;
                $email_address = $contact_information['email_address'];

                $this->transaction_model->update(["quotes_pk" => $motor_quotes_pk, "line" => "motor_insurance"], ["transaction_id" => $data['txnid']]);

                $user_id = $this->client_model->process_policy_to_user($email_address, $quote_pk, $dragonPay);
                $user       = $this->client_model->get_by_field('pk', $user_id);

//                $this->db->trans_status() === TRUE ? $this->db->trans_commit() : $this->db->trans_rollback();

                $this->_create_pdfs($motor_quotes_pk, $vehicle_information['insurance_type']);

                /**
                 * Send Mail
                 */
                $this->_send_mail($contact_information['email_address'], "Transaction for Motor Policy Number: " . $data['txnid'], [
                    'fname'          => $personal_information['fname'],
                    'lname'          => $personal_information['lname'],
                    'token'          => $user['token'],
                    'insurance_type' => $vehicle_information['insurance_type'],
                    'txnid'          => $data['txnid']
                ]);

            } elseif (substr($data['txnid'], 0, 3) == 'ITP') {

                $this->load->model('travel_insurance/itp_computation_model');

                $quote_pk = ltrim(explode("-", $data['txnid'])[2], 0);

                $saved_quote = $this->itp_quote_model->get_saved_quotes_pk($quote_pk);
                $itp_quote_request = json_decode($saved_quote->values, TRUE);
                $personal_information = $itp_quote_request['personal_information'];
                $computations = $itp_quote_request['computation'];

                $data['amount'] = $computations['total_amount'];
                $dragonPay      = $this->transaction_model->post_callback($data);

                $itp_quote_pk = $this->_insert_itp_quote($itp_quote_request, $saved_quote->token);

                $update_fields = [
                    'status'    => STATUS_COMPLETED
                ];

                $this->itp_quote_model->save($update_fields, TRUE, $saved_quote->token);

                $this->_insert_members($itp_quote_pk, $itp_quote_request, $personal_information);
                $this->_insert_itineraries($itp_quote_pk, $itp_quote_request);
                $this->_insert_packages($itp_quote_pk, $itp_quote_request);
                $this->_insert_personal_information($itp_quote_pk, $personal_information, $itp_quote_request['insurance_cover']);

                $computations['itp_quote_pk'] = $itp_quote_pk;

                $this->itp_computation_model->save($computations);

                $itp_quote = $this->itp_quote_model->get_by_fields(["itp_quotes.pk" => $itp_quote_pk]);
                $email_address = $itp_quote->email_address;

                $this->transaction_model->update(["quotes_pk" => $itp_quote_pk, "line" => "itp"], ["transaction_id" => $data['txnid']]);

                $user_id    = $this->client_model->process_policy_to_user($email_address, $itp_quote_pk, $dragonPay, 'itp', $itp_quote_request['travel_information']['coverage_type'],$computations['departure_date'],$computations['arrival_date']);
                $user       = $this->client_model->get_by_field('pk', $user_id);

                $this->db->trans_status() === TRUE ? $this->db->trans_commit() : $this->db->trans_rollback();
  
                $this->load->library('travel_insurance/itp_service');
                $this->itp_service->pdf_policy($itp_quote_pk, TRUE);
                $this->itp_service->pdf_formal_quote($data['param2'], TRUE);  
                $this->itp_service->pdf_invoice($itp_quote_pk);
                $this->itp_service->pdf_application_form($itp_quote_pk);

                log_message('debug', 'Quote PK ' . $computations['country_type']);

                if ( $computations['country_type'] == 3 ) {
                    $this->itp_service->pdf_confirmation_cover($itp_quote_pk);
                }

                $this->load->model('policy/policy_model');
                $policy = $this->policy_model->get_policy(["dt.line" => "itp", "quotes_pk" => $itp_quote_pk]);
               
                $email_data = [
                    'fname'          => $personal_information['fname'],
                    'lname'          => $personal_information['lname'],
                    'token'          => $user['token'],
                    'txnid'          => $data['txnid'],
                    'quote_id'       => $itp_quote->quote_id,
                    'policy_id'      => $policy->policy_id,
                    'country_type'   => $computations['country_type']
                ];

                $email_address = $itp_quote->email_address;

                $this->itp_quote_model->send_email($email_address,$email_data);
            }
        }
    }

    /**
     * Dragon Pay Get Callback
     */
    public function return_callback() {
        $data  = $this->input->get();

        $lines = array("MTR" => MOTOR, "HEP" => HEP, "ITP" => ITP);
        $quote_pk = NULL;
        if($data['status'] == 'S') {
            if(substr($data['txnid'], 0, 3) == 'MTR') {

                $motor_saved_quote_id = explode("-", $data['txnid']);
                $motor_saved_quote_id = (int) end($motor_saved_quote_id);
                $saved_quote = $this->motor_model->get_motor_saved_quote_by_id($motor_saved_quote_id);
                $motor_saved_quotes = json_decode($this->motor_model->get_motor_saved_quote_values_by_token($saved_quote['token']), TRUE);

                $motor_info = $this->vehicle_model->get_motor_for_computation($motor_saved_quotes['motors_pk']);
                $quote_premiums = $this->motor_model->quote_premiums($motor_info, $motor_saved_quotes);

                $data['amount'] = number_format($quote_premiums['premium'], 2, '.', '');

                $dragonPay  = $this->transaction_model->post_callback($data);

                $motorQuote = $this->motor_model->get_motor_quote_by_field('token', $saved_quote['token']);

                $quote_pk = $motorQuote['pk'];
            } elseif(substr($data['txnid'], 0, 3) == 'ITP') {

                $itp_quote  = $this->itp_quote_model->get_by_fields(["token" => $data['param2']]);
                $quote_pk   = $itp_quote->pk;

                $this->load->model('travel_insurance/itp_computation_model');
                $computations = $this->itp_computation_model->get($quote_pk);

                $data['amount'] = $computations->net_total_php;

                $dragonPay  = $this->transaction_model->post_callback($data);

                $array = array(
                    'itp_step' => 'finish'
                );

                $this->session->set_userdata( $array );
            }

            $page = $lines[$dragonPay['line']] == 'itp' ? 'travel-insurance' : 'motor-insurance';

            redirect($page."/success/".$quote_pk . "?txnid=" . $data['txnid']);
        }
        else {
            redirect("access/failed");
        }
    }

    /**
     * Generate PDF'S to be attached
     *
     * @param $quoteId
     */
    private function _create_pdfs($motor_quotes_pk, $insuranceType) {
//        $insuranceType = $this->session->userdata('motor_get_quote')['insurance_type'];

        // Confirmation of Cover
        if(in_array($insuranceType, [CTPL, COMPREHENSIVE_CTPL, COMPREHENSIVE_CTPL_AON])) {
            $this->motor_quote_model->generate_confirmation_of_cover($motor_quotes_pk, true);
        }

        // Bank Certificate
        $this->motor_quote_model->generate_bank_certificate($motor_quotes_pk, TRUE);

        // Motor Quote Request
        $this->motor_quote_model->generate_motor_quote_request($motor_quotes_pk, TRUE);

        /**
         * Policy Schedule
         */
        if($insuranceType == CTPL) {
            $this->motor_quote_model->generate_policy_schedule($motor_quotes_pk, TRUE, TRUE);
            $this->motor_quote_model->generate_invoice($motor_quotes_pk, TRUE, TRUE);
        }
        else if ($insuranceType == COMPREHENSIVE_AON || $insuranceType == COMPREHENSIVE) {
            $this->motor_quote_model->generate_policy_schedule($motor_quotes_pk, FALSE, TRUE);
            $this->motor_quote_model->generate_invoice($motor_quotes_pk, FALSE, TRUE);
        }
        else {
            $this->motor_quote_model->generate_policy_schedule($motor_quotes_pk, TRUE, TRUE);
            $this->motor_quote_model->generate_policy_schedule($motor_quotes_pk, FALSE, TRUE);
            $this->motor_quote_model->generate_invoice($motor_quotes_pk, TRUE, TRUE);
            $this->motor_quote_model->generate_invoice($motor_quotes_pk, FALSE, TRUE);
        }
    }

    /**
     * Send Mail after dragonpay success
     *
     * @param $email
     * @param $subject
     * @param $data
     */
    private function _send_mail($email, $subject, $data) {
        $msg = "Dear " . $data['fname'] . ' ' . $data['lname'] . ",<br>
<br>
Thank you for choosing UCPB GEN as your insurance partner!<br>
<br>
We are pleased to inform you that your transaction has been completed. Attached in this email are the following documents for your policy:<br>
1.	Policy Schedule<br>
2.	Policy Terms and Conditions<br>
3.	Invoice<br>
4.	Completed Application Form<br>
5.	Bank Certificate<br>
6.	Acknowledgement Receipt of payment<br>
<br>
Your Official Receipt [and Certificate of Cover]  will be delivered to your mailing address within 48 (sample only) hours.<br>
To access electronic copies of these documents anytime, please activate your online UCPB GEN account at<br>
<a href='" . site_url("motor_insurance/verify/" . $data['token']) . "'>" . site_url("motor-insurance/verify/" . $data['token']) . "</a>.<br>
<br>
For further assistance and additional concerns, you may reach us at <contact numbers>";

        $this->email->initialize([
            'mailtype' => 'html'
        ]);
        $this->email->clear();
        $this->email->from('motor_insurance@ucpbgen.com', 'MOTOR UCPBGEN');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($msg);

        /**
         * Attachments
         */

        // QUOTE REQUEST
        $this->email->attach(MOTOR_QUOTE_FILES_QUOTE_REQUEST . "quote-request-".$data['token'].".pdf");

        // CONFIRMATION OF COVER
        if(in_array($data['insurance_type'], [CTPL, COMPREHENSIVE_CTPL, COMPREHENSIVE_CTPL_AON])) {
            $this->email->attach(MOTOR_QUOTE_CONFIRMATION_OF_COVER."confirmation-of-cover-".$data['token'].".pdf");
        }

        // BANK CERTIFICATE
        $this->email->attach(MOTOR_QUOTE_FILES_BANK_CERTIFICATE . "bank-certificate-".$data['token'].".pdf");

        // POLICY SCHEDULE
        if ($data['insurance_type'] == CTPL) {
            $this->email->attach(POLICIES_FOLDER . 'policy-schedule-' . $data['token']   . '-ctpl.pdf');
        }
        else if ($data['insurance_type'] == COMPREHENSIVE_AON || $data['insurance_type'] == COMPREHENSIVE) {
            $this->email->attach(POLICIES_FOLDER . 'policy-schedule-' . $data['token']   . '-comprehensive.pdf');
        } else {
            $this->email->attach(POLICIES_FOLDER . 'policy-schedule-' . $data['token']   . '-ctpl.pdf');
            $this->email->attach(POLICIES_FOLDER . 'policy-schedule-' . $data['token']   . '-comprehensive.pdf');
        }

        // FORMAL QUOTE
        $this->email->attach(FORMAL_QUOTES_FOLDER . $data['token'] . '.pdf');

        $this->email->send();
    }

    /**
     * Inserts the ITP Quote
     *
     * @return mixed
     */
    private function _insert_itp_quote($itp_quote=NULL, $token=NULL) {

        if ( ! is_null($itp_quote) ) {
            $personal_information = $itp_quote['personal_information'];
            $travel_information   = $itp_quote['travel_information'];

            $insert_fields              = array_merge(["email_address" => $personal_information['email_address']],$travel_information);
            $insert_fields['status']    = STATUS_ACTIVE;
            $insert_fields['token']     = $token;

            // OVERRIDE purpose_of_travel with the Others field then UNSET the purpose_travel_others
            if($insert_fields['purpose_of_travel'] == unserialize(TRAVEL)['others']) {
                $insert_fields['purpose_of_travel'] = $insert_fields['purpose_of_travel_others'];
            }
            unset($insert_fields['purpose_of_travel_others']);

            // When coverage type is not GROUP we just set the group_name to empty string because it may have a value in the front end when they switch to Group and switch back to Family or Individual.
            if($insert_fields['coverage_type'] != unserialize(COVERAGE)['group']) {
                $insert_fields['group_name'] = '';
            }

            /**
             * Fix if it's the same itinerary or package.
             * In the front end value is toggled between 'yes' and 'no'
             */
            $type = strtolower($insert_fields['coverage_type']);

            // If is_same is not set meaning INDIVIDUAL is chosen.
            $itinerary = $itp_quote[$type.'_itinerary'];
            $insert_fields['same_itinerary'] = !isset($itinerary['is_same']) || $itinerary['is_same'] == 'yes' ? true : false;

            // If is_same is not set meaning INDIVIDUAL is chosen.
            $package = $itp_quote[$type.'_package'];
            $insert_fields['same_package']   = !isset($package['is_same']) || $package['is_same'] == 'yes' ? true : false;
            
            return $this->itp_quote_model->save($insert_fields);
        }
        
        return FALSE;
    }

    /**
     * Inserts Members
     *
     * @param $itp_quote_pk
     */
    public function _insert_members($itp_quote_pk, $itp_quote=NULL,$personal_information=NULL) {
        $this->load->model('travel_insurance/itp_member_model');
        $this->load->model('travel_insurance/itp_emergency_contact_model');
        $this->load->model('travel_insurance/itp_beneficiary_model');

        $type = strtolower($itp_quote['travel_information']['coverage_type']);

        if(ucfirst($type) != COVERAGE_INDIVIDUAL) {
            //$insertFields = array_values($itp_quote[$type.'_member']);
            //$insertFields = $this->_add_itp_quote_on_array($insertFields, $itpQuotePk);
            $members = $itp_quote[$type.'_member'];
            if (is_array($members) AND count($members) > 0) {
                foreach ($members AS $index => $member) {
                    $member['itp_quote_pk'] = $itp_quote_pk;
                    $member_id = $this->itp_member_model->insert($member);

                    /* Insert Emergency Contacts */
                    if (!is_null($personal_information) AND isset($personal_information['emergency_contacts'])) {
                        $emergency_contacts = $personal_information['emergency_contacts'];

                        // Emergency Contacts of Members
                        if (isset($emergency_contacts[$index]) AND is_array($emergency_contacts[$index])) {
                            foreach ($emergency_contacts[$index] AS $contact) {
                                $contact['itp_quote_pk']    = $itp_quote_pk;
                                $contact['itp_member_pk']   = $member_id;
                                $this->itp_emergency_contact_model->insert($contact);
                            }
                        }
                    }

                    /* Insert Beneficiaries */
                    if (!is_null($personal_information) AND isset($personal_information['beneficiaries'])) {
                        $beneficiaries = $personal_information['beneficiaries'];

                        // Beneficiaries of Members
                        if (isset($beneficiaries[$index]) AND is_array($beneficiaries[$index])) {
                            foreach ($beneficiaries[$index] AS $beneficiary) {
                                $beneficiary['itp_quote_pk']    = $itp_quote_pk;
                                $beneficiary['itp_member_pk']   = $member_id;
                                $this->itp_beneficiary_model->insert($beneficiary);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Inserts Itineraries
     *
     * @param $itpQuotePk
     */
    public function _insert_itineraries($itpQuotePk, $itp_quote=NULL) {
        $this->load->model('travel_insurance/itp_itinerary_model');

        $type         = strtolower($itp_quote['travel_information']['coverage_type']);
        $itineraries  = $itp_quote[$type.'_itinerary'];

        /**
         * If is_same is not set meaning INDIVIDUAL is chosen.
         * array_values is used because the key needs to be messed up in the front end
         */
        $insertFields = !isset($itineraries['is_same']) || $itineraries['is_same'] == 'yes' ? array_values($itineraries['same']) : array_values($itineraries['different']);
        $insertFields = $this->_add_itp_quote_on_array($insertFields, $itpQuotePk, true);

        $this->itp_itinerary_model->save($insertFields);
    }

    /**
     * Inserts Packages
     *
     * @param $itpQuotePk
     */
    public function _insert_packages($itpQuotePk, $itp_quote=NULL) {
        $this->load->model('travel_insurance/itp_package_model');

        $type         = strtolower($itp_quote['travel_information']['coverage_type']);
        $packages     = $itp_quote[$type.'_package'];

        /**
         * If is_same is not set meaning INDIVIDUAL is chosen.
         * array_values is used because the key needs to be messed up in the front end
         */
        if(!isset($packages['is_same']) || $packages['is_same'] == 'yes') {
            $insertFields = [
                [
                    'package'      => $packages['same'],
                    'itp_quote_pk' => $itpQuotePk
                ]
            ];
        }
        else {
            $insertFields = array_values($packages['different']);
            $insertFields = $this->_add_itp_quote_on_array($insertFields, $itpQuotePk, true);
        }

        $this->itp_package_model->save($insertFields);
    }

    private function _insert_personal_information($itp_quote_pk=0, $personal_information=array(), $insurance_cover='individual') {
        $this->load->model('travel_insurance/itp_emergency_contact_model');
        $this->load->model('travel_insurance/itp_beneficiary_model');
        $this->load->model('personal_information/personal_information_model');
        $this->load->model('company/company_information_model');
        $this->load->model('travel_insurance/itp_quote_agent_model');
        /**
         * Save ITP Agent
         */
        //$this->itp_quote_agent_model->destroy_by_field('itp_quotes_pk', $this->session->userdata('itp_quote')['pk']);
        if($personal_information['with_agent'] == 'on') {
            $this->load->model('itp/itp_quote_agent_model');
            $this->itp_quote_agent_model->save($itp_quote_pk,$personal_information);
        }

        if ($insurance_cover == 'individual') {
            /**
             * Save Personal Information!
             */
            $this->personal_information_model->save($personal_information, $itp_quote_pk);
        } else {
            /**
             * Save Company Information!
             */
            $this->company_information_model->save($personal_information, $itp_quote_pk);
        }

        /* Insert Emergency Contacts */
        $emergency_contacts = $personal_information['emergency_contacts'];

        if (isset($emergency_contacts[0]) AND is_array($emergency_contacts[0])) {
            foreach ($emergency_contacts[0] AS $contact) {
                $contact['itp_quote_pk']    = $itp_quote_pk;
                $contact['itp_member_pk']   = 0;
                $this->itp_emergency_contact_model->insert($contact);
            }
        }

        /* Insert Beneficiaries */
        $beneficiaries = $personal_information['beneficiaries'];

        if (isset($beneficiaries[0]) AND is_array($beneficiaries[0])) {
            foreach ($beneficiaries[0] AS $beneficiary) {
                $beneficiary['itp_quote_pk']    = $itp_quote_pk;
                $beneficiary['itp_member_pk']   = 0;
                $this->itp_beneficiary_model->insert($beneficiary);
            }
        }
    }

    /**
     * Adds an ITP Quote Pk field on the array.
     *
     * @param $array
     * @param $itpQuotePk
     * @return array
     */
    private function _add_itp_quote_on_array($array, $itpQuotePk, $isMe = false) {
        foreach($array as $key => $field) {
            $array[$key]['itp_quote_pk'] = $itpQuotePk;

            if($isMe) {
                $array[$key]['is_me'] = !isset($array[$key]['is_me']) ? '' : true;
            }
        }

        return $array;
    }
}
