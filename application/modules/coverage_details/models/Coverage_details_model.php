<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coverage_details_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function fetch_by_field($field, $value) {
        $return = false;

        $this->db->where($field, $value);
        $this->db->from('motor_quotes_coverages');

        $query = $this->db->get();
        if($query->num_rows()) {
            return $query->result();
        }
        return $return;
    }

    public function save($data, $motor_saved_quotes) {
//        $data = array();
//        if(isset($fields['comprehensive_start_date'])) {
//            $data['coverage_details']['comprehensive_start_date'] = $fields['comprehensive_start_date'];
//            $data['coverage_details']['comprehensive_end_date'] = $fields['comprehensive_end_date'];
//        }
//        if(isset($fields['ctpl_start_date'])) {
//            $data['coverage_details']['ctpl_start_date'] = $fields['ctpl_start_date'];
//            $data['coverage_details']['ctpl_end_date'] = $fields['ctpl_end_date'];
//        }
        $data = array_replace_recursive($motor_saved_quotes, $data);
        $this->motor_model->save_quote($data);
    }

}