<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coverage_details_rules_model extends CI_Model {

    protected $fields;

    /**
     * Rule Factory Setups
     *
     * @param $formValidation
     * @param $type
     * @return bool
     */
    public function validate($formValidation, $type, $fields = []) {
        $this->fields = $fields;

        if(method_exists($this, $type)) {
            $formValidation->set_rules($this->$type());
            $formValidation->set_error_delimiters('<div class="error_msg"><p>', '</p></div>');
            $formValidation->set_message('require\d', '{field} is required.');

            return $formValidation->run();
        }

        return false;
    }

    /**
     * Save Personal Information Validation
     * @return array
     */
    private function save() {
        $rules = [];

        if($this->fields->vehicle_information['insurance_type'] != CTPL) {
            $rules[] = [
                'field' => 'coverage_details[comprehensive_start_date]',
                'label' => 'Start Date',
                'rules' => 'required'
            ];

            $rules[] = [
                'field' => 'coverage_details[comprehensive_end_date]',
                'label' => 'End Date',
                'rules' => 'required'
            ];
        }
        if($this->fields->vehicle_information['insurance_type'] != COMPREHENSIVE && $this->fields->vehicle_information['insurance_type'] != COMPREHENSIVE_AON) {
            $rules[] = [
                'field' => 'coverage_details[ctpl_start_date]',
                'label' => 'Start Date',
                'rules' => 'required'
            ];

            $rules[] = [
                'field' => 'coverage_details[ctpl_end_date]',
                'label' => 'Start Date',
                'rules' => 'required'
            ];
        }

        return $rules;
    }

}