<style type="text/css">
    .error {
        color: red;
        font-size: 11px;
    }
    .kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
        margin: 0;
        padding: 0;
        border: none;
        box-shadow: none;
        text-align: center;
    }
    .kv-avatar .file-input {
        display: table-cell;
        max-width: 132px;
    }

    .kv-avatar .file-drop-disabled {
        overflow: hidden;
    }
    .file-preview-frame, .file-preview-image, .file-preview-other {
        height: auto;
    }
</style>
<div class="client_page">
    <div class="container box client_box">
        <?php if ( $this->session->flashdata('success_message') ): ?>
            <div class="alert alert-success" role="alert">
                <?php echo $this->session->flashdata('success_message'); ?>
            </div>
        <?php endif; ?>
         <?php if ( $this->session->flashdata('error_message') ): ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $this->session->flashdata('error_message'); ?>
            </div>
        <?php endif; ?>
        <div class="col-md-8 npl">
            <div class="round_box">
                <h4 class="box_title">Personal Information</h4>
                <div class="client_personal_info">
                    <div class="col-md-3 profile_img">
                        <!-- <div class="client_img">
                            <a href="#" class="img"><img width="118" src="<?php echo $profile_picture; ?>" alt="Michael Angelo Bautista" /></a>
                        </div>  -->   
                        <div id="kv-avatar-errors" class="center-block" style="display:none"></div>
                        <form class="text-center" method="post" enctype="multipart/form-data">
                            <?php echo form_hidden('pk', $pi->pk); ?>
                            <div class="kv-avatar center-block" style="width:120px">
                                <input id="avatar" name="photo" type="file" class="file-loading">
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <button type="submit" class="btn btn-primary btn-sm">Upload</button>
                        </form>                                   
                    </div>    
                    <div class="col-md-9">
                        <div class="info_grp">
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Full Name </div>
                                <div class="col-md-8 info_val">
                                    <?= $pi->salutation; ?> <?= $pi->fname; ?> <?= $pi->mname; ?> <?= $pi->lname; ?>
                                </div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Gender</div>
                                <div class="col-md-8 info_val"><?= $pi->gender; ?></div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Date of Birth</div>
                                <div class="col-md-8 info_val"><?= $pi->birthdate; ?></div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Nationality</div>
                                <div class="col-md-8 info_val"><?= $pi->nationality; ?></div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Occupation</div>
                                <div class="col-md-8 info_val" data-detail="occupation"><?= $pi->occupation == 'Others' ? $pi->other_occupation : $pi->occupation; ?></div>
                            </div>
                        </div>
                        <div class="title_break"><h5>Mailing Address</h5></div>
                        <div class="info_grp">
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Province</div>
                                <div class="col-md-8 info_val" data-detail="province"><?= $pi->places[0]; ?></div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4 info_label">City</div>
                                <div class="col-md-8 info_val" data-detail="city"><?= $pi->places[1]; ?></div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Barangay</div>
                                <div class="col-md-8 info_val" data-detail="barangay"><?= $pi->places[2]; ?></div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4  info_label">House number</div>
                                <div class="col-md-8 info_val" data-detail="house_number"><?= $pi->house_number; ?></div>
                            </div>
                        </div>
                        <div class="title_break"><h5>Contact Information</h5></div>
                        <div class="info_grp">
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Telephone</div>
                                <div class="col-md-8 info_val" data-detail="telephone"><?= $pi->telephone; ?></div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Mobile Number</div>
                                <div class="col-md-8 info_val" data-detail="mobile"><?= $pi->mobile; ?></div>
                            </div>
                            <div class="grp_line">
                                <div class="col-md-4  info_label">Email Address</div>
                                <div class="col-md-8 info_val" data-detail="email_address"><?= $pi->email_address; ?></div>
                            </div>
                        </div>
                        <p class="form_button a-right">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#edit_profile">Edit</button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 npr">
            <div class="round_box rb_mb login_info">
                <h4 class="box_title">Login Information</h4>
                <div class="info_grp">
                    <div class="col-md-12 info_label">Email Address</div>
                    <div class="col-md-12 info_val"><?php echo $pi->user_email_address; ?> <span class="count">1st</span></div>
                </div>
                <p class="form_button a-right">
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#change_password">Change Password</button>
                </p>
            </div>

            <?php $this->load->view('client/blocks/contact_us'); ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="edit_profile" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
            </div>
            <div class="modal-body">
                 <?php echo form_open('', 'id="update_profile" role="form" data-validate class="form-horizontal form-validate"', ["pk" => $pi->pk]); ?>     
                    <div class="form-group">
                        <label for="occupation" class="col-sm-3 control-label">Occupation</label>
                        <div class="col-sm-9">
                            <select id="occupation" class="form-control chosen-select required" name="occupation" data-placeholder="Select Occupation">
                                <option></option>
                                <?php foreach ( $occupations AS $o ): ?>
                                    <option value="<?php echo $o->occupation; ?>"<?php echo strtolower($pi->occupation) == strtolower($o->occupation) ? " selected='selected'" : ""; ?>>
                                        <?php echo $o->occupation; ?>
                                    </option>
                                <?php endforeach ;?>
                                <option value="Others"<?php echo $pi->occupation == 'Others' ? " selected='selected'" : ""; ?>>Others</option>
                            </select>
                            <input style="margin-top:10px" type="text" placeholder="Enter your occupation here" name="other_occupation" id="other_occupation" class="form-control<?php echo $pi->occupation == 'Others' ? ' required' : ' hidden'; ?>" value="<?php echo $pi->other_occupation; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <strong>Mailing Address</strong>
                    </div>
                    <div class="form-group">
                        <label for="province" class="col-sm-3 control-label">Province</label>
                        <div class="col-sm-9">
                            <select id="province" class="form-control chosen-select required" name="province" data-placeholder="Select Province" onChange="getCities()">
                                <option></option>
                                 <?php foreach ( $provinces['data'] AS $p ): ?>
                                    <option value="<?php echo $p->pk; ?>"<?php echo $pi->province == $p->pk ? " selected='selected'" : ""; ?>>
                                        <?php echo $p->place; ?>
                                    </option>
                                <?php endforeach ;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-sm-3 control-label">City</label>
                        <div class="col-sm-9">
                            <select id="city" class="form-control chosen-select required" name="city" data-placeholder="Select City" onChange="getBarangays()">
                                <option></option>
                                 <?php foreach ( $cities['data'] AS $c ): ?>
                                    <option value="<?php echo $c->pk; ?>"<?php echo $pi->city == $c->pk ? " selected='selected'" : ""; ?>>
                                        <?php echo $c->place; ?>
                                    </option>
                                <?php endforeach ;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="barangay" class="col-sm-3 control-label">Barangay</label>
                        <div class="col-sm-9">
                            <select id="barangay" class="form-control chosen-select required" name="barangay" data-placeholder="Select Barangay">
                                <option></option>
                                 <?php foreach ( $barangays['data'] AS $b ): ?>
                                    <option value="<?php echo $b->pk; ?>"<?php echo $pi->barangay == $b->pk ? " selected='selected'" : ""; ?>>
                                        <?php echo $b->place; ?>
                                    </option>
                                <?php endforeach ;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="house_number" class="col-sm-3 control-label">House number</label>
                        <div class="col-sm-9">
                            <?php echo form_input('house_number', $pi->house_number, "class='form-control' id='house_number' placeholder='Enter your house number here'"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <strong>Contact Information</strong>
                    </div>
                    <div class="form-group">
                        <label for="telephone" class="col-sm-3 control-label">Telephone</label>
                        <div class="col-sm-9">
                            <?php echo form_input('telephone', $pi->telephone, "class='form-control number' id='telephone' placeholder='Enter your telephone number here'"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mobile" class="col-sm-3 control-label">Mobile</label>
                        <div class="col-sm-9">
                            <?php echo form_input('mobile', $pi->mobile, "class='form-control number' id='mobile' placeholder='Enter your mobile number here'"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email_address" class="col-sm-3 control-label">Email address</label>
                        <div class="col-sm-9">
                            <input type="email" name="email_address" class="form-control required email" placeholder='Enter your email address here' id="email_address" value="<?php echo $pi->email_address; ?>" />
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                <?php echo form_close(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-button="save_profile">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="change_password" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('', 'id="update_password" role="form" class="form-horizontal" data-validate', ["user_pk" => $pi->user_pk]); ?>                 
                    <div class="form-group">
                        <label for="current_password" class="col-sm-4 control-label">Current password</label>
                        <div class="col-sm-8">
                            <?php echo form_password('current_password', '', "class='form-control required' id='current_password' placeholder='Enter your current password here'"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-4 control-label">New password</label>
                        <div class="col-sm-8">
                            <?php echo form_password('password', '', "class='form-control required' id='password' placeholder='Enter your new password here'"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confim_password" class="col-sm-4 control-label">Confirm password</label>
                        <div class="col-sm-8">
                            <?php echo form_password('confirm_password', '', "class='form-control required' id='confirm_password' placeholder='Confirm your new password'"); ?>
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-button="update_password">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
/**
     * LOCATION FETCH
     */
    function getCities() {
        common.ajax('<?=site_url('place/cities')?>', {province: $("[name=province]").val()},
            function(response) {
                if (response.length > 0) {
                    var options = '<option></option>';
                    $.each(response, function(i, v) {
                        options += '<option value="' + v.pk + '">' + v.place + '</option>';
                    });
                    $("[name=city]").html(options).prop("disabled", false).trigger("chosen:updated");
                    $("[name=barangay]").val(null).prop("disabled", true).trigger("chosen:updated");
                }
            });
    }
    function getBarangays() {
        common.ajax('<?=site_url('place/barangays')?>', {city: $("[name=city]").val()},
            function(response) {
                if (response.length > 0) {
                    var options = '<option></option>';
                    $.each(response, function(i, v) {
                        options += '<option value="' + v.pk + '">' + v.place + '</option>';
                    });
                    $("[name=barangay]").html(options).prop("disabled", false).trigger("chosen:updated");
                }
            });
    }

    $(document).ready(function(){
        $(".chosen-select").chosen({width:"100%", disable_search_threshold: 10, search_contains: true});
    
        $("[data-button=save_profile]").click(function(){
            $("#update_profile").submit();
        });
        $("#update_profile").validate();
        $("#update_profile").submit(function(){
            var oForm = $("#update_profile");
            var oData = {
                pk                  : oForm.find("[name=pk]").val(),
                occupation          : oForm.find("#occupation").val(),
                other_occupation    : oForm.find("#other_occupation").val(),
                province            : oForm.find("#province").val(),
                city                : oForm.find("#city").val(),
                barangay            : oForm.find("#barangay").val(),
                house_number        : oForm.find("#house_number").val(),
                telephone           : oForm.find("#telephone").val(),
                mobile              : oForm.find("#mobile").val(),
                email_address       : oForm.find("#email_address").val()
            };

            oForm.find("#form_errors").remove();

            $.ajax({
                type : 'POST',
                url  : baseUrl + 'client/ajax_update_profile',
                data : oData,
                beforeSend : function() {
                    $(".p-loader").removeClass('hidden');
                },
                success : function(sData) {
                    var oReturn = $.parseJSON(sData);
                    if ( oReturn.status == true ) {
                        if ( oData.occupation == 'Others' ) {
                            $("[data-detail=occupation]").html(oData.other_occupation);
                        } else {
                            $("[data-detail=occupation]").html(oData.occupation);
                        }

                        $("[data-detail=province]").html(oForm.find("#province").find("option:selected").text());
                        $("[data-detail=city]").html(oForm.find("#city").find("option:selected").text());
                        $("[data-detail=barangay]").html(oForm.find("#barangay").find("option:selected").text());
                        $("[data-detail=house_number]").html(oData.house_number);
                        $("[data-detail=telephone]").html(oData.telephone);
                        $("[data-detail=mobile]").html(oData.mobile);
                        $("[data-detail=email_address]").html(oData.email_address);

                        $('#edit_profile').modal('hide');
                    } else {
                       $('<div class="alert alert-danger" role="alert" id="form_errors">').html(oReturn.message).prependTo(oForm);
                    }
                }
            }).complete(function() {
                $(".p-loader").addClass('hidden');
            });

            return false;
        });
    
        $("[data-button=update_password]").click(function(){
            $("#update_password").submit();
        });
        $("#update_password").validate();
        $("#update_password").submit(function(){
            var oForm = $("#update_password");
            var oData = {
                user_pk             : oForm.find("[name=user_pk]").val(),
                current_password    : oForm.find("#current_password").val(),
                password            : oForm.find("#password").val(),
                confirm_password    : oForm.find("#confirm_password").val()
            };

            oForm.find("#form_errors").remove();

            $.ajax({
                type : 'POST',
                url  : baseUrl + 'client/ajax_change_password',
                data : oData,
                beforeSend : function() {
                    $(".p-loader").removeClass('hidden');
                },
                success : function (sData) {
                    $(".p-loader").addClass('hidden');
                    var oReturn = $.parseJSON(sData);

                    if ( oReturn.status == true ) {
                        $("#update_password")[0].reset();
                        $('#change_password').modal('hide');
                    } else {
                        $('<div class="alert alert-danger" role="alert" id="form_errors">').html(oReturn.message).prependTo(oForm);
                    }
                }
            }).complete(function() {
                $(".p-loader").addClass('hidden');
            });

            return false;
        });

        $("#avatar").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="<?php echo imgUrl("default-user-image.png"); ?>" alt="Your Avatar" style="width:120px">',
            layoutTemplates: {main2: '{preview} {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"],
            previewSettings : {
                image: {width: "120px", height: "120px"},
            },
            initialPreview: "<img src='<?php echo $profile_picture; ?>' alt='Your Avatar' style='width:120px'>",
            initialPreviewConfig : {
                caption : ""
            }
        });
    });
</script>