<div class="container box client_box">
	<div class="col-md-12 rb_mb npr npl ">
		<div class="client_tabs">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" <?php echo $active == "motor" ? 'class="active"' : ''; ?>><a href="#<?php //echo base_url("client/policies/motor") ?>">Motor</a></li>
				<li role="presentation" <?php echo $active == "fire" ? 'class="active"' : ''; ?>><a href="#<?php //echo base_url("client/policies/fire") ?>">Fire</a></li>
				<li role="presentation" <?php echo $active == "itp" ? 'class="active"' : ''; ?>><a href="<?php echo base_url("client/policies/itp") ?>">ITP</a></li>
			</ul>
			<!-- Tab panels -->
			<div class="tab-content round_box">
				<div class="col-md-12">
					<?php echo $policy_table; ?>
				</div>
			</div>
		</div>
	</div>
</div>