<div class="round_box ">
    <h4 class="box_title">Contact Us</h4>
    <div class="text_list">
        <h5 class="title">Car, Home, and Travel Insurance</h5>
        <ul>
            <li>Hotline: +65 6827 9966 <a href="#" class="btn_call"></a></li>
            <li>Fax: +65 6827 7571</li>
        </ul>
    </div>
    <div class="text_list">
        <h5 class="title">Car Insurance Claims & Emergency Breakdown</h5>
        <ul>
            <li>Assistance Helpline: +65 6333 2222 <a href="#" class="btn_call"></a></li>
            <li>
                Home Insurance Claims & Emergency Assistance<br/>
                Helpline: +65 6322 2022 <a href="#" class="btn_call"></a>
            </li>
            <li>
                Travel Insurance Claims & Emergency Assistance<br/>
                Helpline: +65 6322 2022 <a href="#" class="btn_call"></a>
            </li>
        </ul>
    </div>
    <div class="text_list"><p>For legal firms & workshops making a Third Party Motor Vehicle claim against UCPB Gen, please click <a href="#">here</a> for instructions.</p></div>
    <div class="text_list">
        <h5 class="title">Main Solutions</h5>
        <ul>
            <li>Hotline:  +65 6827 9929 <a href="#" class="btn_call"></a></li>
            <li> Fax: +65 6827 7900</li>
        </ul>
    </div>
</div>