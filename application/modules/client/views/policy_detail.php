<div class="container box client_box">
	<div class="col-md-12 rb_mb npr npl ">
		<div class="round_box full_box">
			<div class="col-md-12 title_bar">
				<p class="form_button pull-right">
					<a href="<?php echo base_url("client/policies/{$policy->line}") ?>" class="btn btn-success btn-lg">Back to My List</a>	
				</p>
				<a target="_blank" href="<?php echo base_url("files/policies/policy-schedule-{$policy->policy_id}.pdf") ?>" class="print_btn">Print</a>
				<h3><?php echo $policy->policy_id; ?></h3>
				<span class="status active"><?php echo $policy->status; ?></span>
			</div>
			<?php echo $detail; ?>
		</div>
	</div>
</div>