<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_rules_model extends CI_Model {

    /**
     * Client Rule Factory Setups
     *
     * @param $formValidation
     * @param $type
     * @return bool
     */
    public function validate($formValidation, $type) {

        if(method_exists($this, $type)) {
            $formValidation->set_rules($this->$type());
            $formValidation->set_error_delimiters('<div class="error_msg"><p>', '</p></div>');
            $formValidation->set_message('required', '{field} is required.');

            return $formValidation->run();
        }

        return false;
    }

    /**
     * Login Validation
     * @return array
     */
    private function login() {
        return array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|max_length[50]'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|max_length[50]'
            )
        );
    }

    /**
     * Login Validation
     * @return array
     */
    private function verify() {
        return array(
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|max_length[20]|min_length[8]'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'required|max_length[20]|min_length[8]|matches[password]'
            )
        );
    }

    public function profile() {
        $rules = [
            [
                "field"     => "occupation",
                "label"     => "Occupation",
                "rules"     => "trim|required|xss_clean"
            ],
            [
                "field"     => "province",
                "label"     => "Province",
                "rules"     => "trim|required|xss_clean"
            ],
            [
                "field"     => "city",
                "label"     => "City",
                "rules"     => "trim|required|xss_clean"
            ],
            [
                "field"     => "barangay",
                "label"     => "Barangay",
                "rules"     => "trim|required|xss_clean"
            ],
            [
                "field"     => "house_number",
                "label"     => "House number",
                "rules"     => "trim|required|xss_clean"
            ],
            [
                "field"     => "email_address",
                "label"     => "Email address",
                "rules"     => "trim|required|valid_email|xss_clean"
            ]
        ];

        if ( $this->input->post('occupation') == 'Others' ) {
            $rules[] = [
                "field"     => "other_occupation",
                "label"     => "Occupation",
                "rules"     => "trim|required|xss_clean"
            ];
        }

        return $rules;
    }

    public function change_password() {
        return array(
            array(
                'field' => 'current_password',
                'label' => 'Current Password',
                'rules' => 'required'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|max_length[20]|min_length[8]|alpha_dash'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'required|max_length[20]|min_length[8]|matches[password]'
            )
        );
    }
}
