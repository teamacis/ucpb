<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_model extends CI_Model {

    public function __construct() {
        parent::__construct();

//        $this->load->model('client_view_model', 'view');
    }

    /**
     * Redirects control of Client Module
     *
     * @param $logged_in
     *   defines if the page must be or must not be logged in.
     */
    public function logged_in($logged_in,$return_uri=NULL) {
        if($logged_in) {
            if(!$this->session->userdata('pk') || $this->session->userdata('user_type') != USER_CLIENT) {
                $return_uri = !is_null($return_uri) ? $return_uri : current_full_url();
                redirect('client/login?return_uri=' . urlencode($return_uri));
            }
        }
        else {
            if($this->session->userdata('pk') && $this->session->userdata('user_type') == USER_CLIENT) {
                redirect('client');
            }
        }
    }

    /**
     * Login Function
     *
     * @param $username
     * @param $password
     */
    public function login($username, $password) {
        $query = $this->db->select(
                              array(
                                  'users.pk',
                                  'users.password',
                                  'users.type',
                                  'users.user_type',
                                  'users.email_address',
                                  'personal_informations.fname',
                                  'personal_informations.mname',
                                  'personal_informations.lname',
                                  'personal_informations.mobile'
                              )
                          )
                          // Left join to get user's full name
                          ->join('users_quotes', 'users_quotes.users_pk = users.pk', 'left')
                          ->join('personal_informations', 'personal_informations.users_quotes_pk = users_quotes.pk', 'left')
                          ->where('users.email_address', $username)
                          ->where('status', USER_ACTIVE)
                          // Must be client
                          ->where('user_type', USER_CLIENT)
                          // Limit is defined because we are doing left join on quotes which it's greater than 1 column.
                          ->limit(1)
                          ->get('users');

        if($query->num_rows()) {
            $user = $query->row_object();

            if(password_verify($password, $user->password)) {
                $this->session->set_userdata(array(
                    'pk'            => $user->pk,
                    'email_address' => $user->email_address,
                    'type'          => $user->type,
                    'name'          => "$user->fname $user->mname $user->lname",
                    'mobile'        => $user->mobile,
                    'first_name'    => $user->fname,
                    'initial'       => $user->mname,
                    'last_name'     => $user->lname,
                    'user_type'     => $user->user_type
                ));

                if ($this->input->get('return_uri') == NULL || $this->input->get('return_uri') == ""){
                    redirect('client');
                } else {
                    redirect(urldecode($this->input->get('return_uri')));
                }
            } else {
              redirect(uri_string());
            }
        } else {
            redirect(uri_string());
        }
    }

    /**
     * Fetch personal information.
     * @return mixed
     */
    public function get_personal_information() {
        $userId = $this->session->userdata('pk');

        $query = $this->db->query("SELECT users.email_address AS user_email_address,
                                          users.pk AS user_pk,
                                          personal_informations.*,
                                          (SELECT GROUP_CONCAT(place SEPARATOR '*')
                                           FROM places
                                           WHERE pk IN (personal_informations.province,
                                                        personal_informations.city,
                                                        personal_informations.barangay)
                                            AND is_blacklisted = 0) as places
                                   FROM users
                                   LEFT JOIN users_quotes
                                          ON users_quotes.users_pk = users.pk
                                   LEFT JOIN personal_informations
                                          ON personal_informations.users_quotes_pk = users_quotes.pk
                                   WHERE users.pk = '$userId' AND personal_informations.is_main = 1
                                   LIMIT 1");

        if($query->num_rows()) {
            $personalInformation = $query->row_object();
            $personalInformation->places = explode('*', $personalInformation->places);
            return $personalInformation;
        }
        else {
            redirect('client/login');
        }
    }

    /**
     * Get User By Field
     */
    public function get_by_field($field, $value) {
        $query = $this->db->where($field, $value)
                          ->get('users');

        if($query->num_rows() > 0) {
            return $query->row_array();
        }
        else {
            return false;
        }
    }

    /**
     * Fetches list of mortgagees
     * @return bool
     * @return object
     */
    public function insurance_providers() {
        return $this->db->order_by("provider")
                ->select('provider')
                ->select('pk')
                ->get('insurance_providers')
                ->result_array();
    }

    /**
     * Fetches list of mortgagees
     * @return bool
     * @return object
     */
    public function mortgagees() {
        return $this->db->order_by("mortgagee")
                        ->select('mortgagee')
                        ->select('pk')
                        ->get('mortgagees')
                        ->result_array();
    }

    /**
     * Fetches list of nationalities
     * @return bool
     * @return array
     */
    public function nationalities() {
        return $this->db->order_by("nationality")
                        ->select('nationality')
                        ->get('nationalities')
                        ->result_array();
    }

    public function insert_client($data) {
        if ($this->db->insert('users', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    public function update_user_quote($data, $motor_quote_pk) {
        return $this->db->where(
                            array(
                                'quotes_pk' => $motor_quote_pk
                            ))
                        ->update('users_quotes', $data);
    }

    public function get_user_quote_by_field($field, $value) {
        $query = $this->db->where([
                    $field => $value
                 ])
                 ->get('users_quotes');

        if($query->num_rows() > 0) {
            return $query->row_array();
        }
        else {
            return false;
        }
    }

    public function check_active_user($email_address) {
        $query = $this->db->limit(1)
                            ->where(array(
                                'u.email_address' => $email_address
                            ))
                            ->join('users as u', 'p.user_pk = u.pk')
                            ->get('policies as p');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @param $user
     * @param $password
     * @return mixed
     */
    public function change_password($pk, $password) {
        return $this->db->where(array(
                            'pk' => $pk
                        ))
                        ->update('users', array(
                            'password' => password_hash($password, PASSWORD_BCRYPT),
                            'status' => 1
                        )
                    );
    }

    /**
     * Verifies the token if valid
     * @param string $token
     * @return bool
     */
    public function get_user_by_token($token = '') {
        return $this->db->where('token', $token)
            ->where('status', 0)
            ->from('users')
            ->get()
            ->row_array();
    }

    /**
     * Proccess policy to user
     * @param $email_address
     * @param $quote_pk
     */
    public function process_policy_to_user($email_address, $quote_pk, $dragonPay, $line='motor_insurance',$extra=NULL, $start_date=NULL,$end_date=NULL) {
        $user = $this->client_model->get_by_field('email_address', $email_address);

        if ( ! $user ) { // check if the user is exist

            $token = generateRandomString(50);
            /**
             * Insert Client
             */
            $user_id = $this->client_model->insert_client([
                "email_address" => $email_address,
                "token"         => $token,
                "type"          => 1,
                "status"        => 0,
                "date_created"  => date("Y-m-d H:i:s"),
                "user_type"     => 1
            ]);

        } else {
            $user_id = $user['pk'];
        }

        /**
         * Update User Quote's users pk
         */
        $user_quote_pk = $this->client_model->save_user_quote($user_id, $quote_pk, $line);

        if ( $line == "itp" ) {
          $this->db->update("itp_quotes", ["users_quotes_pk" => $user_quote_pk], ["pk" => $quote_pk]);
        }

        /**
         * Insert Policy
         */
        $policy_id = $this->policy_model->insert_policy([
            'dragonpay_transaction_pk' => $dragonPay['pk'],
            'user_pk'                  => $user_id,
            'start_date'               => $start_date,
            'end_date'                 => $end_date
        ], $line, $extra, $start_date, $end_date);
  
        /*$this->db->insert('users_quotes', [
            'users_pk' => $user_id,
            'policy_pk' => $policy_id
        ]);*/

        return $user_id;
    }

    public function save_user_quote($user_pk=NULL,$quote_pk=NULL,$line=NULL)
    {
        if ( ! is_null( $quote_pk ) AND ! is_null( $line ) ) {
            $where = $data = [
                "quotes_pk"  => $quote_pk,
                "line"       => $line
            ];

            if ( ! is_null( $user_pk ) ) {
                $data['users_pk']   = $user_pk;
            }

            $user_quote = $this->db->get_where( 'users_quotes', $where );

            if ( $user_quote->num_rows() == 0 ) { // not exist
                // insert user quotes
                $this->db->insert( 'users_quotes', $data );
                $user_quote_pk = $this->db->insert_id();
            } else {
                // update user quotes
                $this->db->update( 'users_quotes', $data, $where );
                $user_quote_pk = $user_quote->row()->pk;
            }

            return $user_quote_pk;
        }

        return FALSE;
    }

    public function get_user_by_email($email=NULL) {
        if ( !is_null($email) ) {
            $query = $this->db->get_where("users",["email_address" => $email]);
            if ( $query->num_rows() > 0 ) {
                return $query->row();
            }
        }

        return FALSE;
    }

    public function add_main_personal_information($user_pk=NULL)
    {
      if ( ! is_null($user_pk) ) {


        $user_quotes = $this->db->get_where("users_quotes", ["users_pk" => $user_pk]);

        if ( $user_quotes->num_rows() > 0 ) {
          $user_quote_pk = $user_quotes->row()->pk;
          $where = [
            "users_quotes_pk" => $user_quote_pk
          ];

          $this->db->update("personal_informations", ["is_main" => 1], $where);
          return $this->db->affected_rows();
        }
      }

      return FALSE;
    }

    public function update_personal_information($pk=NULL,$data=NULL)
    {
      if ( ! is_null($pk) AND ! is_null($data) ) {
        $this->db->update('personal_informations', $data, ["pk" => $pk]);
        return TRUE;
      }

      return FALSE;
    }
}
