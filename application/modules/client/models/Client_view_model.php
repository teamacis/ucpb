<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_view_model extends CI_Model {
    private $header_data;

    public function __construct() {
        parent::__construct();

        $scriptsIE = jsScripts(array(
            jsUrl('core/html5shiv.min.js'),
            jsUrl('core/respond.min.js')
        ));

        $scripts = jsScripts(array(
            jsUrl('core/jquery.min.js'),
            jsUrl('core/bootstrap.min.js'),
            jsUrl('core/chosen.jquery.min.js'),
            base_url('assets/jquery-validation/dist/jquery.validate.min.js'),
            base_url('assets/jquery-validation/dist/additional-methods.min.js'),
            base_url('assets/bootstrap-fileinput/js/fileinput.min.js'),
            jsUrl('common.js')
        ));

        $this->header_data = array(
            'scripts'   => $scripts,
            'scriptsIE' => $scriptsIE
        );
    }

    /**
     * Renders the login page
     */
    public function login() {
        $this->header_data['title'] = 'Client Login';

        $this->load->view('user/header', $this->header_data);
        $this->load->view('login');
        $this->load->view('user/footer');
    }

    /**
     * Renders the My Account Page
     * @param $personalInformation
     */
    public function my_account($personalInformation) {
        $this->header_data['title'] = 'Client - My Account';

        $this->load->model('occupation/occupation_model');
        $this->load->model('place/place_model');
        $occupations = $this->occupation_model->fetch();    

        if(!is_null($personalInformation->photo) 
            AND file_exists(PHOTOS_DIRECTORY . $personalInformation->photo)) {
            $profile_picture = base_url("files/photos/{$personalInformation->photo}");
        } else {
            $profile_picture = imgUrl('default-user-image.png');
        }

        if ( $this->input->post() ) {
           
            $pk = $this->input->post('pk');

            $config['upload_path']      = PHOTOS_DIRECTORY;
            $config['allowed_types']    = 'jpg|png|gif';
            $config['encrypt_name']     = TRUE;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('photo')) {
                $upload_data    = $this->upload->data();
                $file_name      = $upload_data['file_name'];
                $post['photo']  = $file_name;

                if(!is_null($personalInformation->photo)) {
                    unlink(PHOTOS_DIRECTORY . $personalInformation->photo);
                }

                if ($this->model->update_personal_information($pk,$post)) {
                    $this->session->set_flashdata('success_message','Profile picture successfully changed');
                } else {
                    $this->session->set_flashdata('error_message','Unable to update profile picture');
                }
            } else {
                $this->session->set_flashdata('error_message',$this->upload->display_errors());            
            }

            redirect(base_url('client'),'refresh');
        }

        $data = [
            'pi'            => $personalInformation, 
            'occupations'   => $occupations,
            'provinces'     => $this->place_model->get('province'),
            'cities'        => $this->place_model->get('city', $personalInformation->province),
            'barangays'     => $this->place_model->get('barangay', $personalInformation->city),
            'profile_picture' => $profile_picture
        ];

        $this->load->view('client/header', $this->header_data);
        $this->load->view('preload');
        $this->load->view('index', $data);
        $this->load->view('client/footer');
    }

    /**
     * Render the Policies page
     * list of policies
     */
    public function policies($policies) {
        $this->header_data['title'] = 'Client - Policies';

        $this->load->view('client/header', $this->header_data);
        $this->load->view('client/policies', ['policies' => $policies]);
        $this->load->view('client/footer');
    }

     /**
     * Renders the verify page
     */
    public function verify() {
        
        $data['token'] = ($this->uri->segment(3) != FALSE) ? $this->uri->segment(3) : '';
        $user = $this->model->get_user_by_token($data['token']);

        if($this->input->post()) {
            if ($this->rules->validate($this->form_validation, 'verify') != FALSE) {
                $this->model->add_main_personal_information($user['pk']);
                if ($this->model->change_password($user['pk'], $this->input->post('password'))) {
                    $this->model->login($user['email_address'],$this->input->post('password'));
                }
            }
        }

        if ($user != false) {
            $data['email_address'] = $user['email_address'];

            $this->header_data['title'] = 'Verify Account Page';
            $this->load->view('user/header', $this->header_data);
            $this->load->view('verify', $data);
            $this->load->view('user/footer');
        } else {
            $this->header_data['title'] = 'Verify Nothing Page';
            $this->load->view('user/header', $this->header_data);
            $this->load->view('verify_nothing');
            $this->load->view('user/footer');
        }
    }
}