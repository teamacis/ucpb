<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('client_model', 'model');
        $this->load->model('client_view_model', 'view');
        $this->load->model('client_rules_model', 'rules');

        $this->load->model('motor_insurance/client/motor_quote_client_model', 'motor_quote');
    }

    /**
     * Login Page
     */
    public function login() {
        $this->model->logged_in(false);

        $this->view->login();
        $this->session->set_flashdata('form_error', 'Invalid username or password.');
        $this->view->login();
    }

    public function auto_login () {

    }

    /**
     * Logout Function
     */
    public function logout() {
        session_destroy();

        redirect('client/login');
    }

    /**
     * Personal Information Page
     */
    public function index() {
        $this->model->logged_in(true);

        $personalInformation = $this->model->get_personal_information();

        $this->view->my_account($personalInformation);
    }

    public function ajax_update_profile()
    {
        if ( $this->input->is_ajax_request() ) {
            $this->form_validation->set_rules($this->rules->profile());
            if ( $this->form_validation->run() == TRUE ) {
                $pk = $this->input->post('pk');
                $post = $this->input->post();
                unset($post['pk']);

                if ($this->model->update_personal_information($pk,$post)) {
                    echo json_encode(["status" => TRUE]);
                } else {
                    echo json_encode(["status" => FALSE, "message" => "Unable to update profile"]);
                }
            } else {
                echo json_encode(["status" => FALSE, "message" => validation_errors()]);
            }
        }
    }

    public function ajax_change_password()
    {
        if ( $this->input->is_ajax_request() ) {
            $this->form_validation->set_rules($this->rules->change_password());
            $password = $this->input->post('password');
            $current_password = $this->input->post('current_password');
            if ( $this->form_validation->run() == TRUE ) {
                $user = $this->model->get_by_field('pk',$this->input->post('user_pk'));
                if ( password_verify($current_password, $user['password']) ) {
                    $this->model->change_password($user['pk'], $password);
                    echo json_encode(["status" => TRUE]);
                } else {
                    echo json_encode(["status" => FALSE, "message" => "Current password is incorrect"]);
                }
            } else {
                echo json_encode(["status" => FALSE, "message" => validation_errors()]);
            }
        }
    }

    /**
     * My Policies Page
     */
   /* public function policies() {
        $this->model->logged_in(true);

        $motorQuotes = $this->motor_quote->fetch();

        $this->view->policies($motorQuotes);
    }*/

    /* Verify User Page */
    public function verify() {
        $this->model->logged_in(false);

        $this->view->verify();
    }
}