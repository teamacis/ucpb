<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Policies extends Client_Controller {

	public function index()
	{
		$this->itp();
	}

	public function itp()
	{
		$policy_table = Modules::run("policy/itp", API_KEY, $this->user_pk);
		$this->render("policies",["policy_table" => $policy_table,"active" => "itp"], "ITP", "Policies");
	}

	public function motor()
	{
		$policy_table = Modules::run("policy/motor", API_KEY, $this->user_pk);
		$this->render("policies",["policy_table" => $policy_table,"active" => "motor"], "Motor", "Policies");
	}

	public function fire()
	{
		$policy_table = Modules::run("policy/fire", API_KEY, $this->user_pk);
		$this->render("policies",["policy_table" => $policy_table,"active" => "fire"], "Fire", "Policies");
	}

	public function detail($policy_id=NULL) {
		$policy =  Modules::run("policy/detail", API_KEY, $this->user_pk, $policy_id);
		$this->render("policy_detail", $policy, "Detail", "Policies");
	}
}

/* End of file Policies.php */
/* Location: ./application/modules/client/controllers/Policies.php */