<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClientRequest extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('client_model', 'database');
        $this->load->model('client_view_model', 'view');
        $this->load->model('client_rules_model', 'rule');
    }

    public function login() {
        $validated = $this->rule->validate($this->form_validation, 'login');
        
        if($validated) {
            if ($this->database->login($this->input->post('username'), $this->input->post('password'))) {

            } else {
                 $this->view->login();
            }
        }
        else {
            $this->view->login();
        }
    }

}