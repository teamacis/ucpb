<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$title?></title>
    <link href="<?=cssUrl("bootstrap.min.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("style.css")?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome-4.5.0/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-fileinput/css/fileinput.min.css'); ?>">
    <link href="<?=cssUrl("jquery-ui.min.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("custom.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("font-face.css")?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <?php if(isset($scriptsIE)) echo $scriptsIE;?>
    <![endif]-->
    <?php if(isset($scripts)) echo $scripts;?>
</head>
<body>
<header>
    <div class="header_top">
        <div class="container">
            <div class="col-xs-1 visible-xs-inline">
                <button type="button" class="navbar-toggle collapsed mnb_toggle m_m" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </button>
            </div>
            <div class="col-xs-10 visible-xs-inline"><h1 class="mobile_title"><?php
                    if ($this->uri->segment(1) == 'travel-insurance' || $this->uri->segment(1) == 'travel_insurance') echo 'Travel';
                    else if ($this->uri->segment(1) == 'motor-insurance' || $this->uri->segment(1) == 'motor_insurance') echo 'Motor';?> Insurance</h1></div>
            <div class="col-xs-1 visible-xs-inline"><a href="#"><span class="search_btn">Search</span></a></div>
            <ul class="gnb hidden-xs">
                <li><a href="<?= site_url('client/login')?>">Login</a></li>
                <li><a href="#"><span class="search_btn">Search</span></a></li>
            </ul>
        </div>
    </div>
    <?php if ($this->uri->segment(2) != 'login' && $this->uri->segment(3) != 'login') { ?>
    <!-- navigation -->
    <div class="navbar navbar-default navbar-static-top header_mnb">
        <div class="container">
            <div class="navbar-header hidden-xs">
                <a class="navbar-brand logo" href="#"><img src="<?=imgUrl('site_logo.png')?>" alt="UCPB Gen" /></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse" aria-expanded="false">
                <ul class="nav navbar-nav navbar-right mnb">
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Customer Care</a></li>
                    <li><a href="#">Claims</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li class="visible-xs-inline"><a href="#">Login To</a>"</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /navigation -->
    <?php } ?>
</header>
<div id="content">
