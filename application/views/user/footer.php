</div>
<footer>
    <div class="container">
        <div class="row visible-xs-inline visible-sm-inline">
            <strong class="col-xs-3 footer_logo"><a href=""><img src="<?=imgUrl("footer_logo.png")?>" alt="UCPB Gen"></a></strong>
            <ul class="col-xs-9 mob_footnav">
                <li><a href="#"><span class="ico1"></span></a></li>
                <li><a href="#"><span class="ico2"></span></a></li>
                <li><a href="#"><span class="ico3"></span></a></li>
            </ul>
        </div>
        <div class="row nav_links hidden-xs hidden-sm">
            <div class="col-md-2">
                <dl>
                    <dt><a href="#" class="link_top">Products</a></dt>
                    <dd><a href="#">Lorem Ipsum</a></dd>
                    <dd><a href="#">Dolor Sit Amet</a></dd>
                    <dd><a href="#">Proin Venice Tineh</a></dd>
                    <dd><a href="#">Dolor Lorem Site Amet</a></dd>
                    <dd><a href="#">Proin Goinhhe Kontienort</a></dd>
                    <dd><a href="#">Morem Iplum</a></dd>
                    <dd><a href="#">Sorempi Un Meliste</a></dd>
                </dl>
            </div>
            <div class="col-md-2">
                <dl>
                    <dt><a href="#" class="link_top">Services</a></dt>
                    <dd><a href="#">Proin Tospe</a></dd>
                    <dd><a href="#">Lorem Ipsum Dolor</a></dd>
                    <dd><a href="#">Dolor Lorem Site Amet</a></dd>
                    <dd><a href="#">Proin Venice Tineh</a></dd>
                </dl>
            </div>
            <div class="col-md-2">
                <a href="#" class="link_top">Customer Care</a>
            </div>
            <div class="col-md-2">
                <a href="#" class="link_top">Claims</a>
            </div>
            <div class="col-md-2">
                <a href="#" class="link_top">Careers</a>
            </div>
        </div>
        <div class="row hidden-xs hidden-sm">
            <div class="col-md-6">
                <ul class="list-inline footer_nav">
                    <li><a href="#">Legal</a></li>
                    <li><a href="#">Privacy Policies</a></li>
                    <li><a href="#">Copyright</a></li>
                    <li><a href="#">Terms and Conditions</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-md-offset-3">
                <p class="copyright">
                    © 2015 UCPB Gen<br/>
                    Company Reg No: 196900499K<br/>
                    GST Reg No: MR-8500166-8
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- ClickDesk Live Chat Service for websites -->
<!--<script type='text/javascript'>-->
<!--    var _glc =_glc || []; _glc.push('all_ag9zfmNsaWNrZGVza2NoYXRyDwsSBXVzZXJzGJqanq4MDA');-->
<!--    var glcpath = (('https:' == document.location.protocol) ? 'https://my.clickdesk.com/clickdesk-ui/browser/' :-->
<!--        'http://my.clickdesk.com/clickdesk-ui/browser/');-->
<!--    var glcp = (('https:' == document.location.protocol) ? 'https://' : 'http://');-->
<!--    var glcspt = document.createElement('script'); glcspt.type = 'text/javascript';-->
<!--    glcspt.async = true; glcspt.src = glcpath + 'livechat-new.js';-->
<!--    var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(glcspt, s);-->
<!--</script>-->
<!-- End of ClickDesk -->
</body>
</html>
<!--
Developed by: Power House Team
Project Manager: Francis Cargullo a.k.a. Kiko
Lead Developer: John Alexander Virtucio a.k.a. Alex
Designer: Julius Sabile a.k.a. Juls
Quality Assurance: Laissa Miralles a.k.a. Lai
Frontend Developer: Kits Zarate
