</div>
<footer>
    <div class="container">
        <div class="row nav_links">
            <div class="col-md-2">
                <dl>
                    <dt><a href="#" class="link_top">Products</a></dt>
                    <dd><a href="#">Lorem Ipsum</a></dd>
                    <dd><a href="#">Dolor Sit Amet</a></dd>
                    <dd><a href="#">Proin Venice Tineh</a></dd>
                    <dd><a href="#">Dolor Lorem Site Amet</a></dd>
                    <dd><a href="#">Proin Goinhhe Kontienort</a></dd>
                    <dd><a href="#">Morem Iplum</a></dd>
                    <dd><a href="#">Sorempi Un Meliste</a></dd>
                </dl>
            </div>
            <div class="col-md-2">
                <dl>
                    <dt><a href="#" class="link_top">Services</a></dt>
                    <dd><a href="#">Proin Tospe</a></dd>
                    <dd><a href="#">Lorem Ipsum Dolor</a></dd>
                    <dd><a href="#">Dolor Lorem Site Amet</a></dd>
                    <dd><a href="#">Proin Venice Tineh</a></dd>
                </dl>
            </div>
            <div class="col-md-2">
                <a href="#" class="link_top">Customer Care</a>
            </div>
            <div class="col-md-2">
                <a href="#" class="link_top">Claims</a>
            </div>
            <div class="col-md-2">
                <a href="#" class="link_top">Careers</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <ul class="list-inline footer_nav">
                    <li><a href="#">Legal</a></li>
                    <li><a href="#">Privacy Policies</a></li>
                    <li><a href="#">Copyright</a></li>
                    <li><a href="#">Terms and Conditions</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-md-offset-3">
                <p class="copyright">
                    © 2015 UCPB Gen<br/>
                    Company Reg No: 196900499K<br/>
                    GST Reg No: MR-8500166-8
                </p>
            </div>
        </div>
    </div>
</footer>
</body>
</html>