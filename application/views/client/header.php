<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$title?></title>
    <link href="<?=cssUrl("bootstrap.min.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("style.css")?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome-4.5.0/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-fileinput/css/fileinput.min.css'); ?>">
    <link href="<?=cssUrl("custom.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("font-face.css")?>" rel="stylesheet">
    <script type="text/javascript">
        var siteUrl = "<?php echo site_url()?>/";
        var baseUrl = "<?php echo base_url()?>";
    </script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <?php if(isset($scriptsIE)) echo $scriptsIE;?>
    <![endif]-->
    <?php if(isset($scripts)) echo $scripts;?>
</head>
<body>
<header>
    <div class="header_top">
        <div class="container">
            <ul class="gnb">
                <?php if(!$this->session->userdata('pk')) { ?>
                    <li><a href="#">Login to</a></li>
                    <li><a href="#"><span class="search_btn">Search</span></a></li>
                <?php } else { ?>
                    <li><a href="#">Hi <?= $this->session->userdata('name'); ?></a></li>
                    <li><a href="<?= site_url('client/logout'); ?>">Logout</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <!-- navigation -->
    <div class="navbar navbar-default navbar-static-top header_mnb">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed mnb_toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </button>
                <a class="navbar-brand logo" href="#"><img src="<?=imgUrl('site_logo.png')?>" alt="UCPB Gen" /></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse" aria-expanded="false">
                <ul class="nav navbar-nav navbar-right mnb client_mnb">
                    <li <?php if(!$this->uri->segment(2)){ ?> class="on" <?php } ?>>
                        <a href="<?= site_url('client'); ?>" class="icon01">My Account</a>
                    </li>
                    <li <?php if($this->uri->segment(2) == 'policies'){ ?> class="on" <?php } ?>>
                        <a href="<?= site_url('client/policies'); ?>" class="icon01">Policies</a>
                    </li>
                    <li><a href="#" class="icon03">Get Quote</a></li>
                    <li><a href="#" class="icon04">Best Deals</a></li>
                    <li><a href="#" class="icon05">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /navigation -->
</header>

<div id="content">