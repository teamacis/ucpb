<div id="content" class="admin_dashboard">
	<!-- page top -->
	<div class="admin_page_top box ">
		<div class="container">
			<h4 class="page_title_top col-md-6"><?php echo $title; ?></h4>
			<div class="site_date col-md-6">
				<div class="date_container">
					<a href="#" class="btn_info"></a>
					<div class="date_content"><span class="today_is">Today is:</span><strong class="today_date"><?php echo date('l, F d, Y'); ?></strong></div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page top-->
	<!-- additional title -->
	<div class="container additional_title">
		<h5><?php echo $page_title; ?></h5>
	</div>
	<!-- / additional title -->
	<div class="container dashboard_content box">
		<div class="p-loader hidden">
		    <div class="load-con">
		        <p class="loader"></p>
		        <span>Loading...</span>
		    </div>
		</div>
		<div class="row">
			<div class="col-md-1 side_menu">
				<ul class="dash_menu text-hide">
                    <li><a href="<?=site_url(L_DASHBOARD)?>" class="menu_link link_dashboard" title="Dashboard">Dashboard</a></li>
                    <li><a href="<?=site_url(L_TRANSACTIONS_MOTOR)?>" class="menu_link link_transaction" title="Transactions">Transactions</a></li>
                    <li><a href="#" class="menu_link link_reports" title="Reports">Reports</a></li>
                    <li><a href="#" class="menu_link link_resourcecenter" title="Resource Center">Resource Center</a></li>
                    <li><a href="#" class="menu_link link_search" title="Research">Research</a></li>
                </ul>
                <ul class="dash_menu text-hide">
                    <li><a href="#" class="menu_link link_profile" title="Dashboard">My Profile</a></li>
                    <li><a href="#" class="menu_link link_settings" title="Settings">Settings</a></li>
                </ul>
			</div>
			<div class="col-md-11">
				<?php echo $content; ?>
			</div>
		</div>
	</div>
</div>