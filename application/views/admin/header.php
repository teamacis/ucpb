<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$title?></title>
    <link href="<?=cssUrl("bootstrap.min.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("bootstrap-switch.min.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("style.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("custom.css")?>" rel="stylesheet">
    <link href="<?=cssUrl("font-face.css")?>" rel="stylesheet">
    <script type="text/javascript">
        var siteUrl = "<?php echo site_url()?>/";
        var baseUrl = "<?php echo base_url()?>";
        var apiKey  = "<?php echo API_KEY; ?>";
    </script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <?php if(isset($scriptsIE)) echo $scriptsIE;?>
    <![endif]-->
    <?php if(isset($scripts)) echo $scripts;?>
</head>
<body>
<header>
    <div class="header_top">
        <div class="container">
            <ul class="gnb">
                <?php if(!$this->session->userdata('pk')) { ?>
                    <li><a href="#">Login to</a></li>
                    <li><a href="#"><span class="search_btn">Search</span></a></li>
                <?php } else { ?>
                    <li><a href="#">Hi! <?= $this->session->userdata('email_address'); ?></a></li>
                    <li><a href="<?= site_url('admin/logout'); ?>">Logout</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <!-- navigation -->
    <!-- ====== ADD HIDDEN CLASS TO HIDE NAVBAR DURING LOGIN PAGE ====== -->
    <?php if($this->session->userdata('pk')) { ?>
    <div class="navbar navbar-default navbar-static-top header_mnb">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed mnb_toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </button>
                <a class="navbar-brand logo" href="#"><img src="<?= imgUrl('site_logo.png'); ?>" alt="UCPB Gen" /></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse" aria-expanded="false">
                <ul class="nav navbar-nav navbar-right mnb">
                    <li><a href="<?= site_url('admin'); ?>" class="link_dashboard">Dashboard</a></li>
                    <li><a href="<?= site_url('admin/delivery'); ?>" class="link_transaction">Delivery</a></li>
                    <li><a href="<?= site_url('admin/transactions'); ?>" class="link_transaction">Transactions</a></li>
                    <li><a href="#" class="link_reports">Reports</a></li>
                    <li><a href="#" class="link_resourcecenter">Resource Center</a></li>
                    <li><a href="#" class="link_search">Research</a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php } ?>
    <!-- ====== END OF ADD HIDDEN CLASS TO HIDE NAVBAR DURING LOGIN PAGE ====== -->
    <!-- /navigation -->
</header>