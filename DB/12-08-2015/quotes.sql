ALTER TABLE `quotes`
	ADD COLUMN `remarks` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;

ALTER TABLE `itp_quote_agents`
	ALTER `fname` DROP DEFAULT,
	ALTER `lname` DROP DEFAULT,
	ALTER `code` DROP DEFAULT;
ALTER TABLE `itp_quote_agents`
	CHANGE COLUMN `fname` `fname` VARCHAR(50) NULL AFTER `is_name`,
	CHANGE COLUMN `lname` `lname` VARCHAR(50) NULL AFTER `fname`,
	CHANGE COLUMN `code` `code` VARCHAR(50) NULL AFTER `lname`;