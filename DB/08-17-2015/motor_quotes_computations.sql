-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2015 at 01:53 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ucpbgen`
--

-- --------------------------------------------------------

--
-- Table structure for table `motor_quotes_computations`
--

CREATE TABLE IF NOT EXISTS `motor_quotes_computations` (
`pk` int(11) NOT NULL,
  `motor_quotes_pk` int(11) NOT NULL,
  `net_premium` varchar(20) NOT NULL,
  `ctpl_net_premium` varchar(20) NOT NULL,
  `gross_premium` varchar(20) NOT NULL,
  `ctpl_gross_premium` varchar(20) NOT NULL,
  `doc_stamps` varchar(20) NOT NULL,
  `ctpl_doc_stamps` varchar(20) NOT NULL,
  `vat` varchar(20) NOT NULL,
  `ctpl_vat` varchar(20) NOT NULL,
  `local_government_tax` varchar(20) NOT NULL,
  `ctpl_local_government_tax` varchar(20) NOT NULL,
  `own_damage` varchar(20) NOT NULL,
  `bodily_injury` varchar(20) NOT NULL,
  `property_damage` varchar(20) NOT NULL,
  `auto_personal_accident` varchar(20) NOT NULL,
  `aon` varchar(20) NOT NULL,
  `deductible` varchar(20) NOT NULL,
  `ctpl_Interconnectivity` varchar(20) NOT NULL,
  `ctpl` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motor_quotes_computations`
--

INSERT INTO `motor_quotes_computations` (`pk`, `motor_quotes_pk`, `net_premium`, `ctpl_net_premium`, `gross_premium`, `ctpl_gross_premium`, `doc_stamps`, `ctpl_doc_stamps`, `vat`, `ctpl_vat`, `local_government_tax`, `ctpl_local_government_tax`, `own_damage`, `bodily_injury`, `property_damage`, `auto_personal_accident`, `aon`, `deductible`, `ctpl_Interconnectivity`, `ctpl`) VALUES
(7, 4, '56490', '448.75', '71069.18', '625.4', '7062', '56.5', '6778.8', '53.85', '112.98', '0.9', '43875', '780', '1635', '450', '9750', '19500', '65.4', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `motor_quotes_computations`
--
ALTER TABLE `motor_quotes_computations`
 ADD PRIMARY KEY (`pk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `motor_quotes_computations`
--
ALTER TABLE `motor_quotes_computations`
MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
