ALTER TABLE `itp_computation`
	ADD COLUMN `country_type` TINYINT NOT NULL DEFAULT '1' AFTER `arrival_date`;

ALTER TABLE `itp_computation`
	ADD COLUMN `net_premium_php` DECIMAL(10,2) NOT NULL AFTER `net_premium`;