ALTER TABLE `itp_quotes`
	ADD COLUMN `number_of_days` MEDIUMINT NULL DEFAULT NULL AFTER `same_package`;

ALTER TABLE `itp_premiums`
	CHANGE COLUMN `ugit` `ugic` VARCHAR(10) NULL DEFAULT NULL AFTER `itp_country_type_pk`;

CREATE TABLE `itp_amounts` (
	`pk` INT(11) NOT NULL AUTO_INCREMENT,
	`itp_quote_pk` INT(11) NOT NULL DEFAULT '0',
	`itp_member_pk` INT(11) NOT NULL DEFAULT '0',
	`amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
	PRIMARY KEY (`pk`),
	INDEX `itp_quote_pk` (`itp_quote_pk`),
	INDEX `itp_member_pk` (`itp_member_pk`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `itp_saved_quotes` (
	`token` VARCHAR(32) NOT NULL,
	`values` TEXT NOT NULL,
	PRIMARY KEY (`token`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
