ALTER TABLE `ucpbgen`.`users_quotes` 
CHANGE COLUMN `quotes_pk` `policy_pk` INT(11) NOT NULL COMMENT 'quote pk of 3 lines' ;


ALTER TABLE `ucpbgen`.`users_quotes` 
DROP COLUMN `type`;

ALTER TABLE `motor_quotes_files` DROP FOREIGN KEY `motor_quotes_files_ibfk_1` ;

ALTER TABLE `motor_quotes_files` CHANGE `motor_quotes_pk` `motor_saved_quotes_token` INT( 11 ) NULL DEFAULT NULL ;