CREATE TABLE `quotes` (
	`pk` INT(11) NOT NULL AUTO_INCREMENT,
	`token` VARCHAR(50) NOT NULL,
	`values` TEXT NOT NULL,
	`line` ENUM('itp','motor','hep') NOT NULL,
	`time_stamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`pk`)
)
ENGINE=InnoDB
;

DROP TABLE `itp_computation`;
CREATE TABLE `itp_computation` (
	`pk` INT(11) NOT NULL AUTO_INCREMENT,
	`itp_quote_pk` INT(11) NOT NULL,
	`number_of_days` INT(11) NOT NULL,
	`net_premium` DECIMAL(10,2) NOT NULL,
	`discount` DECIMAL(10,2) NOT NULL,
	`dst` DECIMAL(10,2) NOT NULL,
	`lgt` DECIMAL(10,2) NOT NULL,
	`premium_tax` DECIMAL(10,2) NOT NULL,
	`total_amount_usd` DECIMAL(10,2) NOT NULL,
	`total_amount_php` DECIMAL(10,2) NOT NULL,
	`peso_value` DECIMAL(10,2) NOT NULL,
	PRIMARY KEY (`pk`),
	INDEX `itp_quote_pk` (`itp_quote_pk`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;