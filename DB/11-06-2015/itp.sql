ALTER TABLE `users_quotes`
	ADD COLUMN `line` ENUM('itp','motor','hep') NOT NULL AFTER `type`;
ALTER TABLE `itp_premiums`
	CHANGE COLUMN `days` `days` INT NULL DEFAULT NULL AFTER `premium_type`;