CREATE TABLE `itp_beneficiaries` (
	`itp_quote_pk` INT(11) NOT NULL DEFAULT '0',
	`itp_member_pk` INT(11) NOT NULL DEFAULT '0',
	`name` VARCHAR(100) NOT NULL,
	`relation` VARCHAR(50) NOT NULL,
	INDEX `itp_quote_pk` (`itp_quote_pk`),
	INDEX `itp_member_pk` (`itp_member_pk`)
)
ENGINE=InnoDB
;

ALTER TABLE `itp_quote_agents`
	CHANGE COLUMN `itp_quotes_pk` `itp_quote_pk` INT(11) NOT NULL AFTER `pk`;