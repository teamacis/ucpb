ALTER TABLE `itp_computation`
	ADD COLUMN `departure_date` DATE NOT NULL AFTER `for_delivery`,
	ADD COLUMN `arrival_date` DATE NOT NULL AFTER `departure_date`;

ALTER TABLE `company_informations`
	ADD COLUMN `designation` VARCHAR(250) NOT NULL AFTER `industry_type`;