BEGIN;

CREATE TABLE rate_locations(
		pk INT PRIMARY KEY AUTO_INCREMENT,
		location VARCHAR(100)
);
INSERT INTO rate_locations(location)
       VALUES('ALABANG'), 
             ('BACOLOD'), 
             ('BATANGAS'), 
             ('BINAN'),
             ('BULACAN'),
             ('CABANATUAN'),
             ('CAGAYAN'),
             ('CEBU'),
             ('DAGUPAN'),
             ('DAU'),
             ('DAVAO'),
             ('GEN SAN'),
             ('HOF/CADM/DLS/PWS/HAA'),
             ('ILOILO'),
             ('LEGAZPI'),
             ('LUCENA'),
             ('MAKATI'),
             ('MANILA'),
             ('NAGA'),
             ('OLONGAPO'),
             ('QUEZON CITY'),
             ('VIGAN');

CREATE TABLE rates(
		pk INT PRIMARY KEY AUTO_INCREMENT,
  	rate VARCHAR(100),
		year INT,
		rate_location_pk INT,
		FOREIGN KEY(rate_location_pk) REFERENCES rate_locations(pk),
		UNIQUE(year, rate_location_pk)
);
INSERT INTO rates(rate, year, rate_location_pk)
			VALUES('0.500', 2014, 1),
			      ('0.750', 2014, 2),
			      ('0.484', 2014, 3),
			      ('0.550', 2014, 4),
			      ('0.20',  2015, 5),
			      ('0.750', 2014, 6),
			      ('0.750', 2014, 7),
			      ('0.500', 2014, 8),
			      ('0.750', 2014, 9),
			      ('0.550', 2014, 10),
			      ('0.550', 2014, 11),
			      ('0.750', 2015, 12),
			      ('0.200', 2015, 13),
			      ('0.750', 2014, 14),
			      ('0.825', 2014, 15),
			      ('0.750', 2014, 16),
			      ('0.200', 2014, 17),
			      ('0.201', 2014, 18),
			      ('0.600', 2014, 19),
			      ('0.750', 2014, 20),
			      ('0.600', 2015, 21),
			      ('0.05',  2015, 22);


CREATE TABLE agents(
		pk INT PRIMARY KEY AUTO_INCREMENT,
		code VARCHAR(100) UNIQUE,
		fname VARCHAR(100),
		lname VARCHAR(100),
		rate_location_pk INT,
		FOREIGN KEY(rate_location_pk) REFERENCES rate_locations(pk),
		UNIQUE(fname, lname)
);
INSERT INTO agents(code, fname, lname, rate_location_pk)
						VALUES('12345', 'KEVIN', 'COTONGCO', 21),
						      ('23456', 'MIRAJANE', 'STRAUSS', 15),
						      ('91011', 'LISANNA', 'STRAUSS', 18);
COMMIT;