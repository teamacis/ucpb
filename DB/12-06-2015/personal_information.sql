ALTER TABLE `personal_informations`
	ADD COLUMN `other_occupation` VARCHAR(100) NULL AFTER `occupation`;

ALTER TABLE `personal_informations`
	ADD COLUMN `is_main` TINYINT NOT NULL DEFAULT '0' AFTER `civil_status`;

ALTER TABLE `personal_informations`
	ADD COLUMN `photo` VARCHAR(255) NULL DEFAULT NULL AFTER `is_main`;

ALTER TABLE `personal_informations`
	ADD COLUMN `email_address` VARCHAR(255) NULL DEFAULT NULL AFTER `photo`;