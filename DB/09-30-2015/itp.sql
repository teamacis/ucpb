CREATE TABLE `itp_emergency_contacts` (
	`pk` INT(11) NOT NULL AUTO_INCREMENT,
	`itp_quote_pk` INT(11) NOT NULL DEFAULT '0',
	`itp_member_pk` INT(11) NOT NULL DEFAULT '0',
	`name` VARCHAR(100) NOT NULL DEFAULT '0',
	`contact_number` VARCHAR(50) NOT NULL DEFAULT '0',
	`relation` VARCHAR(50) NOT NULL DEFAULT '0',
	PRIMARY KEY (`pk`),
	INDEX `itp_quote_pk` (`itp_quote_pk`),
	INDEX `itp_member_pk` (`itp_member_pk`)
)
ENGINE=InnoDB
;
