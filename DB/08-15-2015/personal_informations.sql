-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2015 at 12:11 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ucpbgen`
--

-- --------------------------------------------------------

--
-- Table structure for table `personal_informations`
--

DROP TABLE IF EXISTS `personal_informations`;
CREATE TABLE IF NOT EXISTS `personal_informations` (
`pk` int(11) NOT NULL,
  `users_quotes_pk` int(11) DEFAULT NULL,
  `salutation` text,
  `fname` text NOT NULL,
  `mname` text,
  `lname` text NOT NULL,
  `suffix` text,
  `gender` text,
  `birthdate` text,
  `nationality` text,
  `occupation` text,
  `tin` text,
  `employer` text,
  `business_nature` text,
  `province` text,
  `city` text,
  `barangay` text,
  `house_number` text,
  `telephone` varchar(20) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_type` varchar(50) DEFAULT NULL,
  `id_number` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personal_informations`
--

INSERT INTO `personal_informations` (`pk`, `users_quotes_pk`, `salutation`, `fname`, `mname`, `lname`, `suffix`, `gender`, `birthdate`, `nationality`, `occupation`, `tin`, `employer`, `business_nature`, `province`, `city`, `barangay`, `house_number`, `telephone`, `mobile`, `updated_by`, `date_created`, `date_updated`, `id_type`, `id_number`) VALUES
(1, 1, 'Mr.', 'John Alexander', 'R', 'Virtucio', '', 'Male', '1997-01-01', 'Filipino', ' BILLING & COLL. CLERK', '1234567890', NULL, NULL, '50500000', '50517000', '50517003', '21 Ewan St', 'n/a', 'n/a', 0, '0000-00-00 00:00:00', '2015-08-15 09:03:54', NULL, NULL),
(2, NULL, 'Mr.', 'John Alexander', 'R', 'Virtucio', '', 'Male', '1997-01-01', 'Filipino', ' ADMIN SALES STAFF/BUYER', '12345678', NULL, NULL, '', '', '', '21 Ewan St', 'n/a', 'n/a', 0, '0000-00-00 00:00:00', '2015-08-11 12:36:33', NULL, NULL),
(3, NULL, 'Mr.', 'John Alexander', 'R', 'Virtucio', '', 'Male', '1997-01-01', 'Filipino', ' 3L OPERATION ASSISTANT', '1234567890', NULL, NULL, '', '', '', '21 Ewan St', 'n/a', 'n/a', 0, '0000-00-00 00:00:00', '2015-08-11 13:39:49', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `personal_informations`
--
ALTER TABLE `personal_informations`
 ADD PRIMARY KEY (`pk`), ADD UNIQUE KEY `users_pk` (`users_quotes_pk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `personal_informations`
--
ALTER TABLE `personal_informations`
MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
