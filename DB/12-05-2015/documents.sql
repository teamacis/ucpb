CREATE TABLE `quote_documents` (
	`pk` INT(11) NOT NULL AUTO_INCREMENT,
	`quote_pk` INT(11) NOT NULL DEFAULT '0',
	`file_name` VARCHAR(255) NOT NULL,
	`file_type` VARCHAR(50) NOT NULL,
	`data` TEXT NOT NULL,
	`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`pk`),
	INDEX `quote_pk` (`quote_pk`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;