ALTER TABLE `quotes`
	CHANGE COLUMN `status` `status` ENUM('APPROVED','DECLINED','EXPIRED','FOR APPROVAL','PENDING','ACTIVE') NOT NULL DEFAULT 'PENDING' AFTER `time_stamp`;