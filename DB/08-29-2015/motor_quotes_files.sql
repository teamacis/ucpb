-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2015 at 06:59 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ucpbgen`
--

-- --------------------------------------------------------

--
-- Table structure for table `motor_quotes_files`
--

CREATE TABLE IF NOT EXISTS `motor_quotes_files` (
`pk` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(111) NOT NULL,
  `motor_quotes_pk` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `motor_quotes_files`
--
ALTER TABLE `motor_quotes_files`
 ADD PRIMARY KEY (`pk`), ADD KEY `motor_quotes_pk` (`motor_quotes_pk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `motor_quotes_files`
--
ALTER TABLE `motor_quotes_files`
MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `motor_quotes_files`
--
ALTER TABLE `motor_quotes_files`
ADD CONSTRAINT `motor_quotes_files_ibfk_1` FOREIGN KEY (`motor_quotes_pk`) REFERENCES `motor_quotes` (`pk`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
