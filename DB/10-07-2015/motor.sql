-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2015 at 01:10 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ucpbgen`
--

-- --------------------------------------------------------

--
-- Table structure for table `motor_configurations`
--

CREATE TABLE IF NOT EXISTS `motor_configurations` (
`pk` int(11) NOT NULL,
  `ctpl_amount` varchar(20) NOT NULL,
  `pc_own_damage_percentage` varchar(10) NOT NULL,
  `cv_own_damage_percentage` varchar(10) NOT NULL,
  `pc_bodily_injury` varchar(20) NOT NULL,
  `cv_bodily_injury` varchar(20) NOT NULL,
  `pc_property_damage` varchar(20) NOT NULL,
  `cv_property_damage` varchar(20) NOT NULL,
  `pc_auto_personal_accident_multiplier` int(11) NOT NULL,
  `cv_auto_personal_accident_multiplier` int(11) NOT NULL,
  `auto_personal_accident` varchar(20) NOT NULL,
  `aon_percentage` varchar(10) NOT NULL,
  `bodily_injury_max` varchar(20) NOT NULL,
  `property_damage_max` varchar(20) NOT NULL,
  `tax` int(11) NOT NULL,
  `deductible_percentage` varchar(20) NOT NULL,
  `pc_deductible_min` varchar(20) NOT NULL,
  `cv_deductible_min` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motor_configurations`
--

INSERT INTO `motor_configurations` (`pk`, `ctpl_amount`, `pc_own_damage_percentage`, `cv_own_damage_percentage`, `pc_bodily_injury`, `cv_bodily_injury`, `pc_property_damage`, `cv_property_damage`, `pc_auto_personal_accident_multiplier`, `cv_auto_personal_accident_multiplier`, `auto_personal_accident`, `aon_percentage`, `bodily_injury_max`, `property_damage_max`, `tax`, `deductible_percentage`, `pc_deductible_min`, `cv_deductible_min`) VALUES
(1, '100000', '2.25', '1.25', '780', '855', '1635', '1680', 90, 105, '55000', '0.5', '500000', '500000', 12, '10', '2000', '3000');

-- --------------------------------------------------------

--
-- Table structure for table `motor_saved_quotes`
--

CREATE TABLE IF NOT EXISTS `motor_saved_quotes` (
  `token` varchar(50) NOT NULL,
  `values` text NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motor_saved_quotes`
--

INSERT INTO `motor_saved_quotes` (`token`, `values`, `time_stamp`) VALUES
('2U7pB04C9kleL6Od13N23n8GqPcBfANM7ojE6tyeOxSXlN8NiY', '{"lname":"Virtucio","fname":"John Alexander","email_address":"","telephone":"2345678","mobile":"2345678","province":"140100000","city":"140102000","barangay":"140102003","insurance_type":"5","is_new":"1","year":"2015","brand":"AUDI","model_name":"A3 Turbo FSI 1.8L Gas 7Spd S-Tronic","other_model":"","market_value":"2418896","original_market_value":"2350000","token":"2U7pB04C9kleL6Od13N23n8GqPcBfANM7ojE6tyeOxSXlN8NiY","submit":"Get Quote","type":1,"motors_pk":"57399","status":"PENDING","mortgagees_pk":"2","insurance_providers_pk":"4","overnight_parking":"Street parking","plate_number":"56789","engine_number":"567890","chassis_number":"567890","mv_file_number":"67890","color":"ACRYLIC RED","or_cr":"MTR20152U7pB04C9kleL6Od13N23n8GqPcBfANM7ojE6tyeOxSXlN8NiY39.gif","non_standard":[],"other_accessories_selected":[],"coverage_details":{"comprehensive_start_date":"2015-10-29","comprehensive_end_date":"2016-10-29","ctpl_start_date":"2015-10-29","ctpl_end_date":"2018-10-29"},"agent":{"type":"","code":"","fname":"","lname":"","with_agent":""},"delivery":0,"salutation":"","suffix":"","gender":"","birthdate":"","nationality":"","with_tin":"","id_type":"","id_number":"","same_location":"","default_province":"","default_city":"","default_barangay":"","house_number":"","non_standard_accessory":"","non_standard_accessory_value":"","other_color":"","loaded":"true"}', '2015-10-06 09:36:20'),
('7WaczCc9wrlEz7WbORQHQppHKYr78gks5vSFcqsdzdT4NDxIXR', '{"personal_information":{"lname":"asdsa","fname":""},"contact_information":{"email_address":"lkjasd@asd.com","telephone":"234567","mobile":"67890"},"vehicle_information":{"address":{"province":"50500000"},"insurance_type":"5","is_new":"1","year":"2015","other_model":"","market_value":"0","original_market_value":"20000000"},"token":"7WaczCc9wrlEz7WbORQHQppHKYr78gks5vSFcqsdzdT4NDxIXR"}', '2015-10-06 12:58:25'),
('BD5YOp7ynhRpou9wrJllCfhQvKiverfim3sF0ONCJqAzF7FxnB', '{"personal_information":{"lname":"asdsa","fname":""},"contact_information":{"email_address":"lkjasd@asd.com","telephone":"234567","mobile":"67890"},"vehicle_information":{"address":{"province":"50500000"},"insurance_type":"5","is_new":"1","year":"2015","other_model":"","market_value":"0","original_market_value":"20000000"},"token":"BD5YOp7ynhRpou9wrJllCfhQvKiverfim3sF0ONCJqAzF7FxnB"}', '2015-10-06 13:00:19'),
('bxVSSO533nxocjvIieHnMdlcERazlgnhR1GIFdkHHr1AsOYcUT', '{"lname":"vasdsa","fname":"jkhjkhjsad","email_address":"kjhasjkd@fadsa.com","telephone":"32423423","mobile":"34534543","province":"160200000","city":"160205000","barangay":"160205001","insurance_type":"5","is_new":"1","year":"2015","brand":"BENTLEY","model_name":"Continental GT Gas V8 A\\/T","other_model":"","market_value":"20650602","original_market_value":"20000000","token":"bxVSSO533nxocjvIieHnMdlcERazlgnhR1GIFdkHHr1AsOYcUT","submit":"Get Quote","type":1,"motors_pk":"57459","status":"FOR APPROVAL","mortgagees_pk":"","insurance_providers_pk":"","overnight_parking":"","plate_number":"","engine_number":"","chassis_number":"","mv_file_number":"","color":"","or_cr":"","non_standard":[],"other_accessories_selected":[],"coverage_details":{"comprehensive_start_date":"","comprehensive_end_date":"","ctpl_start_date":"","ctpl_end_date":""},"agent":{"type":"","code":"","fname":"","lname":"","with_agent":""},"delivery":0,"salutation":"","mname":"","suffix":"","gender":"","birthdate":"","nationality":"","occupation":"","tin":"","employer":"","business_nature":"","id_type":"","id_number":"","same_location":"","house_number":""}', '2015-10-06 09:53:19'),
('dcWjuvi4UuGbBlWB8PpOB7VLCXPVgzZh8EIhkrSUqEODwvmpeI', '{"personal_information":{"lname":"asdsa","fname":""},"contact_information":{"email_address":"lkjasd@asd.com","telephone":"234567","mobile":"67890"},"vehicle_information":{"address":{"province":"50500000","city":"50517000","barangay":"50517003"},"insurance_type":"5","is_new":"1","year":"2015","brand":"BENTLEY","model_name":"Continental GT Gas V8 A\\/T","other_model":"","market_value":"20811245","original_market_value":"20000000"},"token":"dcWjuvi4UuGbBlWB8PpOB7VLCXPVgzZh8EIhkrSUqEODwvmpeI"}', '2015-10-06 12:56:54'),
('dD2FABGZvQlnkM90SqGDwc1s67MbWC24TGoOjMXC1RieoqwQfp', '{"personal_information":{"lname":"asdasd","fname":""},"contact_information":{"email_address":"dasd@asdas.com","telephone":"2345678","mobile":""},"vehicle_information":{"address":{"province":"160200000","city":"160203000","barangay":"160203006"},"insurance_type":"5","is_new":"1","year":"2015","brand":"AUDI","model_name":"A1 Turbo FSI Sportsback 1.4L Gas S-Tronic","other_model":"","market_value":"1985241","original_market_value":"1950000"},"token":"dD2FABGZvQlnkM90SqGDwc1s67MbWC24TGoOjMXC1RieoqwQfp"}', '2015-10-06 12:55:58'),
('DL7UQntX8zbYWtsMfF2Kwe8GJLsYmneqlEapaGvLO3WOJSIqTJ', '{"lname":"Virtucio","fname":"John Alexander","email_address":"ala@yahoo.com","telephone":"876543","mobile":"3456789","province":"160200000","city":"160204000","barangay":"160204009","insurance_type":"2","is_new":"1","year":"2015","brand":"AUDI","model_name":"A1 Turbo FSI Sportsback 1.4L Gas S-Tronic","other_model":"","market_value":"1755000","original_market_value":"","token":"DL7UQntX8zbYWtsMfF2Kwe8GJLsYmneqlEapaGvLO3WOJSIqTJ","submit":"Save"}', '2015-10-03 09:39:07'),
('EK0sCgNNVThoGQkfuQgPp6X53mIuwNp2pan9NFsQh20zXtQhqH', '{"personal_information":{"lname":"sdsadhjk","fname":"jkhjsakhdj"},"contact_information":{"email_address":"sadsad@asdsa.com","telephone":"gjbkjhkj","mobile":"kjhkjhjk"},"vehicle_information":{"address":{"province":"160300000","city":"160303000","barangay":"160303032"},"insurance_type":"5","is_new":"1","year":"2015","brand":"AUDI","model_name":"A3 Turbo FSI 1.8L Gas 7Spd S-Tronic","other_model":"","market_value":"2432108","original_market_value":"2350000"},"token":"EK0sCgNNVThoGQkfuQgPp6X53mIuwNp2pan9NFsQh20zXtQhqH","submit":"Save"}', '2015-10-06 12:25:02'),
('f6FElHZbGuydBWqWX0d55a0SOnp1v8uK0nRKEAChiJl2gh8fpd', '{"personal_information":{"lname":"asdsa","fname":""},"contact_information":{"email_address":"lkjasd@asd.com","telephone":"234567","mobile":"67890"},"vehicle_information":{"address":{"province":"50500000"},"insurance_type":"5","is_new":"1","year":"2015","other_model":"","market_value":"0","original_market_value":"20000000"},"token":"f6FElHZbGuydBWqWX0d55a0SOnp1v8uK0nRKEAChiJl2gh8fpd"}', '2015-10-06 12:58:50'),
('gFDP9hOcUcQaSUPXTmlji1upXSR0bnxiQv80hJKlk0X3lOQl97', '{"lname":"","fname":"","email_address":"testing@yopmail.com","telephone":"","mobile":"","province":"","insurance_type":"","year":"","other_model":"","market_value":"0","original_market_value":"","token":"gFDP9hOcUcQaSUPXTmlji1upXSR0bnxiQv80hJKlk0X3lOQl97","submit":"Save"}', '2015-10-05 11:07:13'),
('jsbRG1UqXaBF6tWVA1b3nr2oXMHMhGVzPcUByWndV0xfWY732X', '{"lname":"Virtucio","fname":"John Alexander","email_address":"xelakz@gmail.com","telephone":"1234567890","mobile":"n\\/a","province":"","city":"","barangay":"","insurance_type":"5","is_new":"1","year":"2015","brand":"BMW","model_name":"118D  Sport Package 2.0L Diesel 8Spd A\\/T","other_model":"","market_value":"2790470","original_market_value":"2690000","token":"jsbRG1UqXaBF6tWVA1b3nr2oXMHMhGVzPcUByWndV0xfWY732X","submit":"Get Quote","type":1,"motors_pk":"57470","status":"PENDING","mortgagees_pk":"","insurance_providers_pk":"","overnight_parking":"","plate_number":"","engine_number":"","chassis_number":"","mv_file_number":"","color":"","or_cr":"","non_standard":[],"other_accessories_selected":[],"coverage_details":{"comprehensive_start_date":"","comprehensive_end_date":"","ctpl_start_date":"","ctpl_end_date":""},"agent":{"type":"","code":"","fname":"","lname":"","with_agent":""},"delivery":0,"salutation":"","mname":"","suffix":"","gender":"","birthdate":"","nationality":"","occupation":"","tin":"","employer":"","business_nature":"","id_type":"","id_number":"","same_location":"","house_number":""}', '2015-10-06 09:49:49'),
('pYZ5EQ3kICHvsgvmdfy5QxtrGCjCzrHkRtJOnIYMuAxr250wlG', '{"coverage_details":{"comprehensive_start_date":"","comprehensive_end_date":"","ctpl_start_date":"","ctpl_end_date":""},"contact_information":{"email_address":"lkjasd@asd.com","telephone":"234567","mobile":"67890"},"agent":{"type":"","code":"","fname":"","lname":"","with_agent":""},"personal_information":{"lname":"asdsa","fname":"dfgdfgdf"},"mailing_address":{"province":"","city":"","barangay":""},"vehicle_information":{"address":{"province":"50500000","city":"50502000","barangay":"50502002"},"insurance_type":"5","is_new":"1","year":"2015","brand":"AUDI","model_name":"A1 Turbo FSI Sportsback 1.4L Gas S-Tronic","other_model":"","market_value":"2025964","original_market_value":"1950000"},"type":1,"delivery":0,"employer":"","business_nature":"","same_location":"","token":"pYZ5EQ3kICHvsgvmdfy5QxtrGCjCzrHkRtJOnIYMuAxr250wlG","motors_pk":"57396","status":"PENDING"}', '2015-10-07 10:22:57'),
('SJDubxjzEjoa61qZS8xgQCjHjXQxa5ItIv2KPbo3QzSDAD5bpR', '{"personal_information":{"lname":"asdsa","fname":""},"contact_information":{"email_address":"lkjasd@asd.com","telephone":"234567","mobile":"67890"},"vehicle_information":{"address":{"province":"50500000"},"insurance_type":"5","is_new":"1","year":"2015","other_model":"","market_value":"0","original_market_value":"20000000"},"token":"SJDubxjzEjoa61qZS8xgQCjHjXQxa5ItIv2KPbo3QzSDAD5bpR"}', '2015-10-06 12:57:13'),
('TUvO2xe75rxWhsqSkWkZsIeTn9BGj3oWYO6LAfGLSHC030Q6Qx', '{"personal_information":{"lname":"hjhaskj","fname":"jkhjkasd "},"contact_information":{"email_address":"kjhsajk@asd.com","telephone":"234234","mobile":"23456789"},"vehicle_information":{"address":{"province":"160200000","city":"160206000","barangay":"160206004"},"insurance_type":"5","is_new":"1","year":"2015","brand":"BMW","model_name":"118D  Urban Essential 2.0L Diesel 8Spd A\\/T","other_model":"","market_value":"1969688","original_market_value":"1910000"},"token":"TUvO2xe75rxWhsqSkWkZsIeTn9BGj3oWYO6LAfGLSHC030Q6Qx","submit":"Save"}', '2015-10-06 12:23:36'),
('wVMJBbmrEv5YULVU5immpWmUA2oEPrqbSuUBW2QtJpGWttoyTx', '{"lname":"Virtucio","fname":"John Alexander","email_address":"ala@yahoo.com","telephone":"876543","mobile":"3456789","province":"160200000","city":"160204000","barangay":"160204009","insurance_type":"2","is_new":"1","year":"2015","brand":"AUDI","model_name":"A1 Turbo FSI Sportsback 1.4L Gas S-Tronic","other_model":"","market_value":"1755000","original_market_value":"1950000","token":"wVMJBbmrEv5YULVU5immpWmUA2oEPrqbSuUBW2QtJpGWttoyTx","submit":"Get Quote","type":1,"motors_pk":"57396","status":"PENDING","motor_quotes_pk":"","mortgagees_pk":"","insurance_providers_pk":"","overnight_parking":"","plate_number":"","engine_number":"","chassis_number":"","mv_file_number":"","color":"","or_cr":""}', '2015-10-05 12:14:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `motor_configurations`
--
ALTER TABLE `motor_configurations`
 ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `motor_saved_quotes`
--
ALTER TABLE `motor_saved_quotes`
 ADD PRIMARY KEY (`token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `motor_configurations`
--
ALTER TABLE `motor_configurations`
MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
