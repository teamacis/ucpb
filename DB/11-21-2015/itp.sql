ALTER TABLE `itp_itineraries`
	CHANGE COLUMN `destination` `destination` TEXT NULL DEFAULT NULL AFTER `last_name`,
	DROP INDEX `destination`;