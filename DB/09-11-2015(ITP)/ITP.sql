CREATE TABLE itp_country_types(
    pk           INT(100) PRIMARY KEY AUTO_INCREMENT,
    country_type ENUM('ASIA', 'ROW', 'SCHENGEN')
);
INSERT INTO itp_country_types(country_type) VALUES ('ASIA'), ('ROW'), ('SCHENGEN');

CREATE TABLE itp_premiums(
    pk                   INT PRIMARY KEY AUTO_INCREMENT,
    user_type            ENUM('Family', 'Group', 'Individual'),
    premium_type         ENUM('Silver', 'Gold', 'Platinum'),
    days                 VARCHAR(20),
    itp_country_type_pk  INT(100),
    ugit                 VARCHAR(10),
    ibero                VARCHAR(10),
    currency             VARCHAR(5),
    FOREIGN KEY(itp_country_type_pk) REFERENCES itp_country_types(pk)
);

CREATE TABLE itp_countries(
    pk                   INT PRIMARY KEY AUTO_INCREMENT,
    country              VARCHAR(100),
    region               VARCHAR(100),
    itp_country_type_pk  INT(100),
    is_blacklisted       BOOLEAN DEFAULT false,
    FOREIGN KEY(itp_country_type_pk) REFERENCES itp_country_types(pk)
);

CREATE TABLE itp_quotes(
    pk                INT PRIMARY KEY AUTO_INCREMENT,
    fname             VARCHAR(100),
    lname             VARCHAR(100),
    birth_date        DATE,
    email_address     VARCHAR(100),
    telephone         VARCHAR(100),
    mobile            VARCHAR(100),
    purpose_of_travel VARCHAR(100),
    coverage_type     ENUM('Family', 'Group', 'Individual'),
    status            VARCHAR(100),
    group_name        VARCHAR(100) NULL,
    same_itinerary    BOOLEAN,
    same_package      BOOLEAN
);

CREATE TABLE itp_members(
    pk               INT PRIMARY KEY AUTO_INCREMENT,
    first_name       VARCHAR(100),
    last_name        VARCHAR(100),
    birth_date       date,
    relationship     VARCHAR(100),
    itp_quote_pk     INT(100),
    FOREIGN KEY(itp_quote_pk) REFERENCES itp_quotes(pk)
);

CREATE TABLE itp_itineraries(
    pk               INT PRIMARY KEY AUTO_INCREMENT,
    first_name       VARCHAR(100),
    last_name        VARCHAR(100),
    destination      INT(100),
    start_date       DATE,
    end_date         DATE,
    is_me            BOOLEAN,
    itp_quote_pk     INT(100),
    FOREIGN KEY(destination) REFERENCES itp_countries(pk),
    FOREIGN KEY(itp_quote_pk) REFERENCES itp_quotes(pk)
);

CREATE TABLE itp_packages(
    pk               INT PRIMARY KEY AUTO_INCREMENT,
    first_name       VARCHAR(100),
    last_name        VARCHAR(100),
    package          ENUM('Platinum', 'Gold', 'Silver'),
    is_me            BOOLEAN,
    itp_quote_pk     INT(100),
    FOREIGN KEY(itp_quote_pk) REFERENCES itp_quotes(pk)
);