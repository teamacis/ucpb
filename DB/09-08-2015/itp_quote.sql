CREATE TABLE itp_quotes(
    pk                INT PRIMARY KEY AUTO_INCREMENT,
    fname             VARCHAR(100),
    lname             VARCHAR(100),
    birth_date        DATE,
    email_address     VARCHAR(100),
    telephone         VARCHAR(100),
    mobile            VARCHAR(100),
    purpose_of_travel VARCHAR(100),
    coverage_type     ENUM('Family', 'Group', 'Individual'),
    status            VARCHAR(100),
    group_name        VARCHAR(100) NULL,
    same_itinerary    BOOLEAN,
    same_package      BOOLEAN
);

CREATE TABLE itp_members(
    pk               INT PRIMARY KEY AUTO_INCREMENT,
    first_name       VARCHAR(100),
    last_name        VARCHAR(100),
    birth_date       date,
    relationship     VARCHAR(100),
    itp_quote_pk     INT(100),
    FOREIGN KEY(itp_quote_pk) REFERENCES itp_quotes(pk)
);

CREATE TABLE itp_itineraries(
    pk               INT PRIMARY KEY AUTO_INCREMENT,
    first_name       VARCHAR(100),
    last_name        VARCHAR(100),
    destination      VARCHAR(100),
    start_date       DATE,
    end_date         DATE,
    is_me            BOOLEAN,
    itp_quote_pk     INT(100),
    FOREIGN KEY(itp_quote_pk) REFERENCES itp_quotes(pk)
);

CREATE TABLE itp_packages(
    pk               INT PRIMARY KEY AUTO_INCREMENT,
    first_name       VARCHAR(100),
    last_name        VARCHAR(100),
    package          ENUM('Platinum', 'Gold', 'Silver'),
    is_me            BOOLEAN,
    itp_quote_pk     INT(100),
    FOREIGN KEY(itp_quote_pk) REFERENCES itp_quotes(pk)
);