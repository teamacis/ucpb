INSERT INTO `ucpbgen`.`itp_configurations` (`code`, `value`) VALUES ('premium_tax', '0.02');

ALTER TABLE `itp_computation`
	ALTER `doc_stamps` DROP DEFAULT,
	ALTER `local_gov_tax` DROP DEFAULT,
	ALTER `vat` DROP DEFAULT,
	ALTER `total` DROP DEFAULT;
ALTER TABLE `itp_computation`
	CHANGE COLUMN `doc_stamps` `dst` DECIMAL(10,2) NOT NULL AFTER `discount`,
	CHANGE COLUMN `local_gov_tax` `lgt` DECIMAL(10,2) NOT NULL AFTER `dst`,
	CHANGE COLUMN `vat` `premium_tax` DECIMAL(10,2) NOT NULL AFTER `lgt`,
	CHANGE COLUMN `total` `net_total_usd` DECIMAL(10,2) NOT NULL AFTER `premium_tax`,
	ADD COLUMN `net_total_php` DECIMAL(10,2) NOT NULL AFTER `net_total_usd`,
	ADD COLUMN `peso_value` DECIMAL(10,2) NOT NULL AFTER `net_total_php`;

