ALTER TABLE `itp_quotes`
	ADD COLUMN `valid_id` VARCHAR(255) NULL DEFAULT NULL AFTER `birth_date`;

ALTER TABLE `itp_members`
	ADD COLUMN `valid_id` VARCHAR(255) NULL DEFAULT NULL AFTER `relationship`;

ALTER TABLE `company_informations`
	ADD COLUMN `province` VARCHAR(250) NOT NULL AFTER `contact_person`,
	ADD COLUMN `city` VARCHAR(250) NOT NULL AFTER `province`,
	ADD COLUMN `barangay` VARCHAR(250) NOT NULL AFTER `city`,
	ADD COLUMN `house_number` VARCHAR(250) NOT NULL AFTER `barangay`;

ALTER TABLE `company_informations`
	ADD COLUMN `telephone` VARCHAR(50) NOT NULL AFTER `contact_person`,
	ADD COLUMN `mobile` VARCHAR(50) NOT NULL AFTER `telephone`;