ALTER TABLE `itp_members`
	ADD COLUMN `civil_status` ENUM('Single','Married') NULL AFTER `itp_quote_pk`;

ALTER TABLE `itp_quotes`
	ADD COLUMN `civil_status` ENUM('Single','Married') NULL DEFAULT NULL AFTER `email_address`;

ALTER TABLE `personal_informations`
	ADD COLUMN `civil_status` ENUM('Single','Married') NULL AFTER `id_number`;

INSERT INTO `ucpbgen`.`itp_countries` (`country`, `region`, `itp_country_type_pk`) VALUES ('Schengen', 'Schengen', 3);

ALTER TABLE `itp_quotes`
	ADD COLUMN `employer` VARCHAR(100) NOT NULL AFTER `issue_date`;

ALTER TABLE `itp_quote_agents`
	ADD COLUMN `company` VARCHAR(100) NULL AFTER `code`;