-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2015 at 03:24 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ucpbgen`
--

-- --------------------------------------------------------

--
-- Table structure for table `motor_configurations`
--

CREATE TABLE IF NOT EXISTS `motor_configurations` (
`pk` int(11) NOT NULL,
  `ctpl_amount` varchar(20) NOT NULL,
  `own_damage_percentage` varchar(10) NOT NULL,
  `bodily_injury` varchar(20) NOT NULL,
  `property_damage` varchar(20) NOT NULL,
  `auto_personal_accident_multiplier` int(11) NOT NULL,
  `aon_percentage` varchar(10) NOT NULL,
  `bodily_injury_max` varchar(20) NOT NULL,
  `property_damage_max` varchar(20) NOT NULL,
  `tax` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motor_configurations`
--

INSERT INTO `motor_configurations` (`pk`, `ctpl_amount`, `own_damage_percentage`, `bodily_injury`, `property_damage`, `auto_personal_accident_multiplier`, `aon_percentage`, `bodily_injury_max`, `property_damage_max`, `tax`) VALUES
(1, '100000', '2.25', '780', '1635', 90, '0.5', '500000', '500000', 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `motor_configurations`
--
ALTER TABLE `motor_configurations`
 ADD PRIMARY KEY (`pk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `motor_configurations`
--
ALTER TABLE `motor_configurations`
MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
