DROP TABLE `itp_computation`;
CREATE TABLE `itp_computation` (
	`pk` INT(11) NOT NULL AUTO_INCREMENT,
	`itp_quote_pk` INT(11) NOT NULL,
	`number_of_days` INT(11) NOT NULL,
	`net_premium` DECIMAL(10,2) NOT NULL,
	`discount` DECIMAL(10,2) NOT NULL,
	`dst` DECIMAL(10,2) NOT NULL,
	`lgt` DECIMAL(10,2) NOT NULL,
	`premium_tax` DECIMAL(10,2) NOT NULL,
	`net_total_usd` DECIMAL(10,2) NOT NULL,
	`net_total_php` DECIMAL(10,2) NOT NULL,
	`total_amount` DECIMAL(10,2) NOT NULL,
	`peso_value` DECIMAL(10,2) NOT NULL,
	`delivery_charge` DECIMAL(10,2) NOT NULL,
	`for_delivery` TINYINT(1) NOT NULL DEFAULT '0',
	PRIMARY KEY (`pk`),
	INDEX `itp_quote_pk` (`itp_quote_pk`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;