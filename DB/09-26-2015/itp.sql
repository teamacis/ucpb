ALTER TABLE `itp_quotes`
	ADD COLUMN `users_quotes_pk` INT(11) NOT NULL DEFAULT '0' AFTER `pk`,
	ADD INDEX `users_quotes_pk` (`users_quotes_pk`);

CREATE TABLE `itp_quote_agents` (
	`pk` INT(11) NOT NULL AUTO_INCREMENT,
	`itp_quotes_pk` INT(11) NOT NULL,
	`is_name` TINYINT(1) NOT NULL DEFAULT '1',
	`fname` VARCHAR(50) NOT NULL,
	`lname` VARCHAR(50) NOT NULL,
	`code` VARCHAR(50) NOT NULL,
	PRIMARY KEY (`pk`),
	UNIQUE INDEX `itp_quotes_pk` (`itp_quotes_pk`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

ALTER TABLE `itp_quotes`
	ADD COLUMN `token` VARCHAR(50) NULL AFTER `number_of_days`;

ALTER TABLE `itp_amounts`
	ADD COLUMN `itp_premium_pk` INT NOT NULL DEFAULT '0' AFTER `itp_member_pk`,
	ADD INDEX `itp_premium_pk` (`itp_premium_pk`);

ALTER TABLE `itp_saved_quotes`
	CHANGE COLUMN `token` `token` VARCHAR(40) NOT NULL FIRST;