CREATE TABLE policies(
  pk INT PRIMARY KEY AUTO_INCREMENT,
  policy_id VARCHAR(100),
  is_delivered BOOLEAN DEFAULT false,
  date_created TIMESTAMP DEFAULT NOW(),
  date_delivered TIMESTAMP,
  dragonpay_transaction_pk INT,
  user_pk INT,
  FOREIGN KEY(dragonpay_transaction_pk) references dragonpay_transactions(pk),
  FOREIGN KEY(user_pk) REFERENCES users(pk)
);