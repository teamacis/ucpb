$(document).ready(function() {

    var QuoteRequest = {

        /**
         * List of elements to be used.
         */
        elements: {
            form:                       '[name=quote-request-form]',
            chosenSelect:               '.chosen-select',
            grayBox:                    '.gray-box',
            grayBoxField:               '.gray-box-field',
            birthDate:                  '.birth_date',
            memberBdate:                '.member_birth_date',
            date:                       '.date',
            insurance_cover:            '[name=insurance_cover]',
            personal_info:              '[data-property=personal_info]',
            company_info:               '[data-property=company_info]',
            file:                       '.nice',

            // Business
            businessFields:             '#business-fields',
            purposeOfTravelSpecify:     '#purpose-of-travel-specify',

            // Group
            groupName:                  '#group-name',
            groupFields:                '#group-fields',
            groupSameItinerary:         '#group-same-itinerary',
            groupDifferentItinerary:    '#group-different-itinerary',
            groupSamePackage:           '#group-same-package',
            groupDifferentPackage:      '#group-different-package',
            groupMemberAddBtn:          '[name=group-member-add-btn]',
            groupSameItineraryAddBtn:   '[name=group-member-same-itinerary-add-btn]',

            // Family
            familyFields:                '#family-fields',
            familySameItinerary:         '#family-same-itinerary',
            familyDifferentItinerary:    '#family-different-itinerary',
            familySamePackage:           '#family-same-package',
            familyDifferentPackage:      '#family-different-package',
            familyMemberAddBtn:          '[name=family-member-add-btn]',
            familySameItineraryAddBtn:   '[name=family-member-same-itinerary-add-btn]',

            memberRemoveBtn:             '.member-remove',
            sameItineraryRemoveBtn:      '.same-itinerary-remove',
            differentItineraryRemoveBtn: '.different-itinerary-remove',

            differentItineraryAddBtn:    '.different-itinerary-add',

            // Individual
            individualFields:            '#individual-fields',
            individualItineraryAddBtn:   '[name=individual-itinerary-add-btn]',

            /**
             * This is to return the jQuery element.
             * Because some of the elements we only need their classes like the one we use on .on() events.
             */
            get: function(selector) {
                return $(QuoteRequest.elements[selector]);
            }
        },

        templates: {
            member: '<div class="gray-box" data-dependency-control="$dependencyControl">\
                         <div class="item_choice">\
                            <span class="value_name">$first_name $last_name</span>\
                            <span class="value_bday">$birth_date</span>\
                            <span class="value_relations">$relationship</span>\
                            <span class="value_civil_status">$civil_status</span>\
                            <input type="hidden" name="$type_member[$index][first_name]" value="$first_name">\
                            <input type="hidden" name="$type_member[$index][last_name]" value="$last_name">\
                            <input type="hidden" name="$type_member[$index][birth_date]" value="$birth_date">\
                            <input type="hidden" name="$type_member[$index][relationship]" value="$relationship">\
                            <input type="hidden" name="$type_member[$index][civil_status]" value="$civil_status">\
                            <a href="javascript:void(0)" class="btn_close member-remove"></a>\
                         </div>\
                     </div>',

            sameItinerary: '<div class="gray-box">\
                                <div class="item_choice">\
                                    <span class="value_destination">$destiname</span>\
                                    <span class="value_sday">$start_date</span>\
                                    <span class="value_eday">$end_date</span>\
                                    <span class="value_country_type hidden">$country_type</span>\
                                    <input type="hidden" name="$type_itinerary[same][$index][destination]" value="$destination">\
                                    <input type="hidden" class="start_date" name="$type_itinerary[same][$index][start_date]" value="$start_date">\
                                    <input type="hidden" class="end_date" name="$type_itinerary[same][$index][end_date]" value="$end_date">\
                                    <a href="javascript:void(0)" class="btn_close same-itinerary-remove"></a>\
                                </div>\
                            </div>',

            differentItinerary: '<div class="gray-box-field item_field" data-dependency="$dependency" data-type="$type">\
                                     <div class="col-md-4 form_input al_ver">\
                                         <div class="check_box"><input type="checkbox" /></div>\
                                         <a href="javascript:void(0)" class="btn_plus active different-itinerary-add"></a>\
                                         <span class="value_field name">\
                                             <span data-dependency="$dependencyFirstName">$first_name</span> \
                                             <span data-dependency="$dependencyLastName">$last_name</span>\
                                         </span>\
                                         <input type="hidden" data-dependency="$dependencyFirstName" class="form-control required first_name" name="$type_itinerary[different][$index][first_name]" value="$first_name" />\
                                         <input type="hidden" data-dependency="$dependencyLastName" class="form-control required last_name" name="$type_itinerary[different][$index][last_name]" value="$last_name" />\
                                         <input type="hidden" class="form-control required last_name" name="$type_itinerary[different][$index][is_me]" value="$isMe" />\
                                     </div>\
                                     <div class="col-md-8 input_group">\
                                        <div class="col-md-5 form_input">\
                                            <select class="form-control required destination chosen-select" data-placeholder="Destination" name="$type_itinerary[different][$index][destination]" data-type="destination"></select>\
                                        </div>\
                                        <div class="col-md-3 form_input">\
                                            <input type="text" class="form-control calendar_input required date start_date" placeholder="Start Date" name="$type_itinerary[different][$index][start_date]" data-type="start_date" />\
                                        </div>\
                                        <div class="col-md-3 form_input">\
                                            <input type="text" class="form-control calendar_input required date end_date" placeholder="End Date" name="$type_itinerary[different][$index][end_date]" data-type="end_date" />\
                                        </div>\
                                        <div class="col-md-1 form_input btn_type"><a href="javascript:void(0)" class="btn_remove active different-itinerary-remove"></a></div>\
                                    </div>\
                                 </div>',

            differentPackage: '<div class="gray-box-field item_field" data-dependency="$dependency">\
                                   <div class="col-md-4 form_input al_ver">\
                                       <div class="check_box">\
                                           <input type="checkbox" />\
                                       </div>\
                                       <span class="value_field name">\
                                           <span>$first_name</span> \
                                           <span>$last_name</span>\
                                       </span>\
                                       <input type="hidden" class="form-control required first_name" name="$type_package[different][$index][first_name]" value="$first_name" />\
                                       <input type="hidden" class="form-control required last_name" name="$type_package[different][$index][last_name]" value="$last_name" />\
                                       <input type="hidden" class="form-control required" name="$type_package[different][$index][is_me]" value="$isMe" />\
                                   </div>\
                                   <div class="col-md-8 input_group">\
                                       <div class="col-md-12 form_input">\
                                           <select class="form-control chosen-select required" data-placeholder="Package" name="$type_package[different][$index][package]" data-type="package">\
                                               <option></option>\
                                               <option>Platinum</option>\
                                               <option>Gold</option>\
                                               <option>Silver</option>\
                                           </select>\
                                       </div>\
                                   </div>\
                               </div>\
                            </div>',
            memberValidID:  '<div class="form_line" data-property="member_valid_id">\
                                <div class="col-md-4 form_input">\
                                    <p class="n_title">Upload Certification of Good Health</p>\
                                    <div class="upload_box">\
                                        <input type="file" class="nice member_valid_id" name="member_valid_id[$index]" /><a href="#" class="btn_close"></a>\
                                    </div>\
                                </div>\
                            </div>'
        },

        initialize: function() {

            this.loadCountries();
            this.loadDestinations();
            this.loadJqueryPlugins();
            this.createEvents();

        },

        toggle_required_documents : function ()
        {
            var uiItinerary     = $("div.itinerary-box");
            var uiStartDate     = uiItinerary.find(".start_date");
            var uiPersonalInfo  = $("[data-property=personal_info]");
            var uiDocuments     = $("[data-property=documents]");
            
            if ( uiStartDate.length > 0 && uiPersonalInfo.find(".birth_date").val().length > 0 ) {
                var iAge = moment(new Date(uiStartDate.val())).diff(new Date(uiPersonalInfo.find(".birth_date").val()),'years');

                if ( iAge >= 60 ) {
                    uiDocuments.removeClass("hidden");
                    uiDocuments.find(".main_member").removeClass("hidden");
                } else {
                     uiDocuments.find(".main_member").addClass("hidden");
                }
            } else {
                if ( uiDocuments.hasClass("hidden") == false ) {
                    uiDocuments.addClass("hidden");
                }   
            }
        },

        loadDestinations: function() {
            var destinationSelect;

            $.each($('select.destination'), function() {
                destinationSelect = $(this);

                if(!$.trim(destinationSelect.html())) {
                    destinationSelect.html(QuoteRequest.templates.countryOptions);
                    destinationSelect.chosen({width:"100%", disable_search_threshold: 10, search_contains: true});
                }
            });
        },

        loadCountries: function() {
            var country;
            QuoteRequest.templates.countryOptions = '<option></option>';

            $.each($('#countries-list span'), function() {
                country = $(this);
                QuoteRequest.templates.countryOptions += '<option data-country-type="$type" value="$pk">$label</option>'
                                                         .replace('$pk',    country.attr('data-pk'))
                                                         .replace('$label', country.text())
                                                         .replace('$type',  country.attr('data-country-type-pk'));
            });
        },

        loadJqueryPlugins: function() {

            this.elements.get('chosenSelect').chosen({width:"100%", disable_search_threshold: 10, search_contains: true});

            this.elements.get('date').datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: '+1d',
                changeMonth: true,
                changeYear: true
            });

            this.elements.get('birthDate').datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: '-18y',
                minDate: '-80y',
                yearRange: '-80y:-18y',
                changeMonth: true,
                changeYear: true,
                onSelect : function(date,obj) {
                    
                   /* var iAge = moment().diff(new Date(date),'years');

                    if ( iAge >= 60 ) {
                        $("[data-property=valid_id]").removeClass("hidden");
                    } else {
                        $("[data-property=valid_id]").addClass("hidden");
                    }*/
                    //QuoteRequest.toggle_required_documents();
                }
            });

            this.elements.get('memberBdate').datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: '-1d',
                minDate: '-80y',
                yearRange: '-80y:+1y',
                changeMonth: true,
                changeYear: true,
                onSelect : function(date,obj) {
                    var uiItinerary     = $("div.itinerary-box");
                    var uiStartDate     = uiItinerary.find(".start_date");
                    var iAge = moment(new Date(uiStartDate.val())).diff(date,'years');

                    if ( iAge >= 60 ) {
                        if ($("[data-property=member_valid_id]").length == 0) {
                            var uiParent    = $(this).parents(".form_line");
                            var uiWrapper   = $(this).parents("[data-property=members_box]");
                            var uiMembers   = uiWrapper.find("[data-property=members_list]");
                            //template = QuoteRequest.templates['memberValidID'];
                            //template = template.replace(new RegExp('\\$index', 'g') , uiMembers.find("div.gray-box").length);
                            //$(template).insertAfter(uiParent);
                            /*$("[data-property=member_valid_id] input[type=file]").nicefileinput({
                                label : ' '
                            }); */  
                        }
                    } else {
                        if ($("[data-property=member_valid_id]").length > 0) {
                            $("[data-property=member_valid_id]").remove();
                        }
                    }
                }
            });
            //file upload
           /* this.elements.get('file').nicefileinput({
                label : ' '
            });*/
        },

        createEvents: function() {
            $('#personal_information_first_name, #personal_information_last_name').change(function() {
                var id    = $(this).attr('id');
                var value = $(this).val();

                $('[data-dependency="'+id+'"]').val(value).text(value);
            });

        /*
         |--------------------------------------------------------------------------
         | TRAVEL INFORMATION
         |--------------------------------------------------------------------------
         */
            /**
             * Toggles the Purpose of Travel Others
             */
            this.elements.get('form').find('[name="travel_information[purpose_of_travel]"]').change(function() {
                if($(this).val() == 'Others') {
                    QuoteRequest.elements.get('purposeOfTravelSpecify').show();
                }
                else {
                    QuoteRequest.elements.get('purposeOfTravelSpecify').hide();
                }

                if($(this).val() == 'Training / Seminar') {
                    $('#work-warning-modal').modal('toggle');
                }
            });

        /*
         |--------------------------------------------------------------------------
         | COVERAGE TYPE EVENTS
         |--------------------------------------------------------------------------
         */
            /**
             * Toggles the FIELDS for FAMILY, GROUP, INDIVIDUAL
             */
            this.elements.get('form').find('[name="travel_information[coverage_type]"]').change(function() {
                // Hide All
                QuoteRequest.elements.get('groupFields').hide();
                QuoteRequest.elements.get('groupName').hide();
                QuoteRequest.elements.get('familyFields').hide();
                QuoteRequest.elements.get('individualFields').hide();

                /**
                 * Show depending on Coverage Group
                 */
                if($(this).val() == 'Group') {
                    QuoteRequest.elements.get('groupName').show();
                    QuoteRequest.elements.get('groupFields').show();
                }
                else if($(this).val() == 'Family') {
                    QuoteRequest.elements.get('familyFields').show();
                }
                else if($(this).val() == 'Individual') {
                    QuoteRequest.elements.get('individualFields').show();
                }
            });

        /*
         |--------------------------------------------------------------------------
         | GROUP EVENTS
         |--------------------------------------------------------------------------
         */
            /**
             * Group Itinerary Toggle
             */
            this.elements.get('form').find('[name="group_itinerary[is_same]"]').click(function() {
                QuoteRequest.elements.get('groupSameItinerary').hide();
                QuoteRequest.elements.get('groupDifferentItinerary').hide();

                if($(this).val() == 'yes') {
                    QuoteRequest.elements.get('groupSameItinerary').show();
                }
                else {
                    QuoteRequest.elements.get('groupDifferentItinerary').show();
                }
            });

            /**
             * Group Package Toggle
             */
            this.elements.get('form').find('[name="group_package[is_same]"]').click(function() {
                QuoteRequest.elements.get('groupSamePackage').hide();
                QuoteRequest.elements.get('groupDifferentPackage').hide();

                if($(this).val() == 'yes') {
                    QuoteRequest.elements.get('groupSamePackage').show();
                }
                else {
                    QuoteRequest.elements.get('groupDifferentPackage').show();
                }
            });

            /**
             * Adds Group Member
             */
            this.elements.get('groupMemberAddBtn').click(function() {
                var uiItinerary     = $("div.itinerary-box");
                if (uiItinerary.find(".gray-box").length > 0) {
                    var fields = QuoteRequest.addGrayBox($('#group-members'), $('#group-members-list'), 'member', ['first_name', 'last_name', 'birth_date', 'relationship','civil_status'], 'group');
                    if(fields) {
                        QuoteRequest.addGrayBoxWithFields(fields.first_name, fields.last_name, $('#group-different-itinerary'), 'differentItinerary', fields.dependencyControl, 'group');
                        QuoteRequest.addGrayBoxWithFields(fields.first_name, fields.last_name, $('#group-different-package'), 'differentPackage', fields.dependencyControl, 'group');
                    }
                } else {
                    alert('Please add itinerary first');
                }
            });

            /**
             * Adds Group Same Itinerary
             */
            this.elements.get('groupSameItineraryAddBtn').click(function() {
                QuoteRequest.addGrayBox($('#group-same-itinerary'), $('#group-same-itinerary-list'), 'sameItinerary', ['destination', 'start_date', 'end_date'], 'group');
                //QuoteRequest.toggle_required_documents();
            });

        /*
         |--------------------------------------------------------------------------
         | FAMILY EVENTS
         |--------------------------------------------------------------------------
         */
            /**
             * Family Itinerary Toggle
             */
            this.elements.get('form').find('[name="family_itinerary[is_same]"]').click(function() {
                QuoteRequest.elements.get('familySameItinerary').hide();
                QuoteRequest.elements.get('familyDifferentItinerary').hide();

                if($(this).val() == 'yes') {
                    QuoteRequest.elements.get('familySameItinerary').show();
                }
                else {
                    QuoteRequest.elements.get('familyDifferentItinerary').show();
                }
            });

            /**
             * Family Package Toggle
             */
            this.elements.get('form').find('[name="family_package[is_same]"]').click(function() {
                QuoteRequest.elements.get('familySamePackage').hide();
                QuoteRequest.elements.get('familyDifferentPackage').hide();

                if($(this).val() == 'yes') {
                    QuoteRequest.elements.get('familySamePackage').show();
                }
                else {
                    QuoteRequest.elements.get('familyDifferentPackage').show();
                }
            });

            /**
             * Adds Family Member
             */
            this.elements.get('familyMemberAddBtn').click(function() {
                var uiItinerary     = $("div.itinerary-box");
                if (uiItinerary.find(".gray-box").length > 0) {
                    var total_members = $("#family-members-list").find("div.gray-box").length;
                    if ( total_members >= 5 ) {
                        alert("Maximum of 6 persons only");
                    }
                    else {
                        var fields = QuoteRequest.addGrayBox($('#family-members'), $('#family-members-list'), 'member', ['first_name', 'last_name', 'birth_date', 'relationship','civil_status'], 'family');
                        if(fields) {
                            QuoteRequest.addGrayBoxWithFields(fields.first_name, fields.last_name, $('#family-different-itinerary'), 'differentItinerary', fields.dependencyControl, 'family');
                            QuoteRequest.addGrayBoxWithFields(fields.first_name, fields.last_name, $('#family-different-package'), 'differentPackage', fields.dependencyControl, 'family');
                        }
                    }
                } else {
                    alert("Please add itinerary first");
                }
            });

            /**
             * Adds Family Same Itinerary
             */
            this.elements.get('familySameItineraryAddBtn').click(function() {
                QuoteRequest.addGrayBox($('#family-same-itinerary'), $('#family-same-itinerary-list'), 'sameItinerary', ['destination', 'start_date', 'end_date'], 'family');
                //QuoteRequest.toggle_required_documents();
            });

        /*
         |--------------------------------------------------------------------------
         | INDIVIDUAL EVENTS
         |--------------------------------------------------------------------------
         */
            this.elements.get('individualItineraryAddBtn').click(function() {
                QuoteRequest.addGrayBox($('#individual-itinerary'), $('#individual-itinerary-list'), 'sameItinerary', ['destination', 'start_date', 'end_date'], 'individual');
                //QuoteRequest.toggle_required_documents();
            });

        /*
         |--------------------------------------------------------------------------
         | FAMILY, GROUP, INDIVIDUAL EVENTS
         |--------------------------------------------------------------------------
         */
            /**
             * Remove Family/Group Member.
             */
            $(document).on('click', this.elements.memberRemoveBtn, function() {
                var wrapper = $(this).parents(QuoteRequest.elements.grayBox);

                $('[data-dependency="'+wrapper.attr('data-dependency-control')+'"]').remove();
                wrapper.remove();
            });

            /**
             * Remove Family/Group Same Itinerary.
             */
            $(document).on('click', this.elements.sameItineraryRemoveBtn, function() {
                var parent          = $(this).parents(QuoteRequest.elements.grayBoxField);
                var country_type    = parent.attr("data-country-type");
                $(this).parents(QuoteRequest.elements.grayBox).remove();         
                QuoteRequest.toggle_package_disable(parent,country_type);
                var startDate = parent.find('input[name=start_date]');
                if(parent.find(QuoteRequest.elements.grayBox).length > 0) {
                    var end_date = parent.find(QuoteRequest.elements.grayBox + ':last-child .end_date').val();
                    startDate.datepicker("option","minDate", end_date);
                } else {
                    startDate.datepicker("option","minDate", '+1d');
                }
            });

            /**
             * Remove Family/Group Different Itinerary.
             */
            $(document).on('click', this.elements.differentItineraryRemoveBtn, function() {
                var wrapper      = $(this).parents(QuoteRequest.elements.grayBoxField);
                var dependencyId = wrapper.attr('data-dependency');
                var parent          = $(this).parents(QuoteRequest.elements.grayBoxField);
                var country_type    = parent.attr("data-country-type");

                // Removes the Different Itinerary if Member's Itinerary > 0
                if(wrapper.siblings('[data-dependency="'+dependencyId+'"]').length > 0) {
                    wrapper.remove();
                }
                else {
                    common.viewDialog('Member must have at least 1 itinerary');
                }

                QuoteRequest.toggle_package_disable(parent,country_type);
            });

            /* Select Insurance Cover */
            $("body").on('change', this.elements.insurance_cover, function(){
                var sValue = $(this).val();
                if (sValue == 'company') {
                    $('[data-property=personal_info]').addClass("hidden");
                    $('[data-property=company_info]').removeClass("hidden");
                } else {
                    $('[data-property=personal_info]').removeClass("hidden");
                    $('[data-property=company_info]').addClass("hidden");
                }
            });

            /**
             * Add Family/Group Different Itinerary
             */
            $(document).on('click', this.elements.differentItineraryAddBtn, function() {
                var wrapper              = $(this).parents(QuoteRequest.elements.grayBoxField);
                var type                 = wrapper.attr('data-type'); // Family or Group
                var dependencyId         = wrapper.attr('data-dependency');
                var firstName            = wrapper.find('.first_name').val();
                var lastName             = wrapper.find('.last_name').val();
                var bindedInPersonalInfo = wrapper.find('[data-dependency=personal_information_first_name]')[0];

                QuoteRequest.addGrayBoxWithFields(firstName, lastName, $('#family-different-itinerary'), 'differentItinerary', dependencyId, type, bindedInPersonalInfo, wrapper);
            });

        /*
         |--------------------------------------------------------------------------
         | Gray Box Field Events
         |--------------------------------------------------------------------------
         */
            $(document).on('keyup change', this.elements.grayBoxField+' input, '+this.elements.grayBoxField+' select', function() {
                QuoteRequest.checkboxCloneValue($(this));
            });


            /**
             * Updates the Start and End Date of Datepicker Ranges
             */
            $(document).on('change', QuoteRequest.elements.grayBoxField+' .start_date', function() {
                var wrapper = $(this).parents(QuoteRequest.elements.grayBoxField);
                var endDate = wrapper.find('.end_date');
                endDate.datepicker("option","minDate",$(this).val());
            });
            $(document).on('change', QuoteRequest.elements.grayBoxField+' .end_date', function() {
                var wrapper   = $(this).parents(QuoteRequest.elements.grayBoxField);
                var startDate = wrapper.find('.start_date');

                //startDate.data('datepicker').setEndDate($(this).val());
            });

            $("input.number").numeric({
                allowPlus           : true, // Allow the + sign
                allowMinus          : true,  // Allow the - sign
                allowSpace          : true
            });
        },

        /**
         * Add Gray Box
         *
         * @param formWrapper
         * @param appendTo
         * @param template
         * @param fields
         * @param type
         * return (Array)
         */
        addGrayBox: function(formWrapper, appendTo, template, fields, type) {
            template = QuoteRequest.templates[template];
            var append = true;
            var fieldValue;
            // This one is use to control addGrayBoxWithFields dependency.
            var dependencyControl = 'dependency-control-' + (Math.ceil(Math.random() * (99999 - 10000) + 10000));
            // Array of fields to be returned
            var fieldValueArray = { dependencyControl: dependencyControl };

            /**
             * Update the fields in the template.
             */
            
            for(var i in fields) {
                fieldValue = formWrapper.find('[name='+fields[i]+']').val();
                fieldValueArray[fields[i]] = fieldValue;

                if(fieldValue) {
                    template = template.replace(new RegExp('\\$'+fields[i], 'g') , fieldValue);
                    if (fields[i] == 'destination') {
                        destinationTitle = formWrapper.find('[name='+fields[i]+']').find("option:selected").text();
                        var country_type = {};
                        formWrapper.find('[name='+fields[i]+']').find("option:selected").each(function(index){
                            country_type[index] = $(this).attr("data-country-type");
                        });
                        template = template.replace(new RegExp('\\$destiname', 'g') , destinationTitle);
                        template = template.replace(new RegExp('\\$country_type', 'g') , JSON.stringify(country_type));
                    }
                }
                else {
                    append = false;
                    break;
                }                   
            }
            
            //var uiMemberValidIdWrapper = $("[data-property=member_valid_id]");
            var iIndex = Math.floor((Math.random() * 100000) + 1);
           /* if ( uiMemberValidIdWrapper.length > 0 ) {
                if (uiMemberValidIdWrapper.find("input[type=file]").val().length > 0) {
                    var uiMemberValidId = uiMemberValidIdWrapper.find("input[type=file]").clone();
                        uiMemberValidId.attr("name","member_valid_id["+iIndex+"]");
                } else {
                    append = false;
                }
            }*/      

            // Replace the $type(family, group, or individual
            template = template.replace(/\$type/g, type)
                               .replace(/\$index/g, iIndex);

            if(append) {
                 /* Set the valid start date */
                uiStartDate = formWrapper.find('[name=start_date]');
                uiEndDate   = formWrapper.find('[name=end_date]');

                if (uiStartDate.length > 0 && uiEndDate.length > 0) {
                    //uiStartDate.data('datepicker').setStartDate(uiEndDate.val());
                    //uiEndDate.data('datepicker').setStartDate(uiEndDate.val());
                    uiStartDate.datepicker("option","minDate",uiEndDate.val());
                    uiEndDate.datepicker("option","minDate",uiEndDate.val());
                }

                appendTo.append(template.replace('$dependencyControl', dependencyControl));

                /*if (uiMemberValidId) {
                    appendTo.find("div.item_choice:last-child").prepend(uiMemberValidId);
                    uiMemberValidIdWrapper.remove();
                }*/

                formWrapper.find('input[type=text], select').val('');
                this.elements.get('chosenSelect').trigger('chosen:updated');

                
            }
            else {
                fieldValueArray = false;
            }

            QuoteRequest.toggle_package_disable(formWrapper,type);

            return fieldValueArray;
        },

        /**
         * Add Gray Box with Fields
         *
         * @param firstName
         * @param lastName
         * @param appendTo
         * @param template
         * @param dependency
         * @param type (group or family)
         * @param me (true or false, to bind it in personal information)
         * @param next (append next to)
         */
        addGrayBoxWithFields: function(firstName, lastName, appendTo, template, dependency, type, me, next) {
            template = QuoteRequest.templates[template];
            template = template.replace(/\$first_name/g, firstName)
                               .replace(/\$last_name/g, lastName)
                               .replace('$dependency', dependency)
                               .replace(/\$type/g, type)
                               .replace(/\$index/g, Math.floor((Math.random() * 100000) + 1));

            /**
             * To Bind it From Personal Information
             */
            if(me) {
                template = template.replace(/\$dependencyFirstName/g, 'personal_information_first_name')
                                   .replace(/\$dependencyLastName/g, 'personal_information_last_name')
                                   .replace(/\$isMe/g, 'true');
            }
            else {
                template = template.replace(/\$isMe/g, '');
            }

            if(!next) {
                appendTo.append(template);
            }
            else {
                $(template).insertAfter(next);
            }

            QuoteRequest.loadDestinations();
            QuoteRequest.loadJqueryPlugins();
        },

        /**
         * Clone the value when CHECKBOX is TICKED
         * @param el
         */
        checkboxCloneValue: function(el) {
            var wrapper   = el.parents(QuoteRequest.elements.grayBoxField);
            var isChecked = wrapper.find('input[type=checkbox]').prop('checked');
            var type      = el.attr('data-type');
            var value     = el.val();

            // If current field's checkbox is ticked
            if(isChecked) {
                var sibling, input;

                $.each(wrapper.siblings(), function() {
                    sibling = $(this);
                    isChecked = sibling.find('input[type=checkbox]').prop('checked');

                    // If siblings field's checkbox is ticked
                    if(isChecked) {
                        input = sibling.find('[data-type="'+type+'"]');
                        input.val(value);

                        if(input.data('datepicker')) {
                            input.data({ date: value });
                            input.datepicker('update');
                        }
                    }
                });

                this.elements.get('chosenSelect').trigger('chosen:updated');
            }
        },

        toggle_package_disable : function (parent,type)
        {
            var disable = false;
            parent.find("span.value_country_type").each(function(){
                var country_type = $.parseJSON($(this).text());
                for (var x in country_type) {
                    if (country_type[x] == 3) {
                        disable = true;
                        return false;
                    }
                }
            });

            if ( disable == true ) {
                $("select#" + type + "_package").val("Platinum");
                $("select#" + type + "_package").find("option[value=Gold]").attr("disabled","disabled");
                $("select#" + type + "_package").find("option[value=Silver]").attr("disabled","disabled");
                $("select#" + type + "_package").trigger("chosen:updated");
            } else {
                $("select#" + type + "_package").find("option[disabled]").removeAttr("disabled");
                $("select#" + type + "_package").trigger("chosen:updated");
            }
        }
    };

    QuoteRequest.initialize();

});