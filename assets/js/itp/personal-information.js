$(document).ready(function() {

    var QuoteRequest = {

        /**
         * List of elements to be used.
         */
        elements: {
            chosenSelect:               '.chosen-select',
            birthDate:                  '.birth_date',
            date:                       '.date',

            /**
             * This is to return the jQuery element.
             * Because some of the elements we only need their classes like the one we use on .on() events.
             */
            get: function(selector) {
                return $(QuoteRequest.elements[selector]);
            }
        },

        templates: {

        },

        initialize: function() {

            this.loadJqueryPlugins();
            this.createEvents();

        },

        loadJqueryPlugins: function() {

            this.elements.get('chosenSelect').chosen({width:"100%", disable_search_threshold: 10, search_contains: true});

            this.elements.get('date').datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate : '-1d',
                changeMonth: true,
                changeYear: true
            });

            this.elements.get('birthDate').datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: '-18y',
                changeMonth: true,
                changeYear: true
            });
        },

        createEvents: function() {
            $("body").on("click",function(e){
                var uiTarget = $(e.target);

                if (uiTarget.closest("[data-button='add_emergency_contact']").length > 0)
                {
                    e.preventDefault();

                    var uiThis      = uiTarget.closest("[data-button='add_emergency_contact']"),
                        uiParent    = uiThis.parents("[data-model='emergency_contact']"),
                        iMemberId   = uiParent.attr("data-member-id"),
                        iCount      = uiParent.parent().find("[data-model='emergency_contact'][data-member-id='"+iMemberId+"']").length,
                        sTemplate   = uiThis.attr("data-clone-template"),
                        uiMember    = $("[data-model='emergency_contact'][data-member-id='"+iMemberId+"']");;
                    
                        if (uiMember.length <= 2) {
                            $("[data-template='"+sTemplate+"']").clone()
                                                            .html(function(i, html) {
                                                                return html.replace(/\{counter}/g, iCount);
                                                            })
                                                            .removeAttr("data-template")
                                                            .insertAfter(uiParent)
                                                            .find("[data-chosen-select]").addClass('chosen-select').chosen({width:"100%", disable_search_threshold: 10, search_contains: true});
                        } else {
                             alert("Only 2 emergency contact per member only");
                        }
                       
                } else if (uiTarget.closest("[data-button='remove_emergency_contact']").length > 0) {
                    e.preventDefault();

                    var uiThis      = uiTarget.closest("[data-button='remove_emergency_contact']"),
                        uiParent    = uiThis.parents("[data-model='emergency_contact']");

                        uiParent.remove();
                } else if (uiTarget.closest("[data-button='add_beneficiary']").length > 0) {
                    e.preventDefault();

                    var uiThis      = uiTarget.closest("[data-button='add_beneficiary']"),
                        uiParent    = uiThis.parents("[data-model='beneficiary']"),
                        iMemberId   = uiParent.attr("data-member-id"),
                        iCount      = uiParent.parent().find("[data-model='beneficiary'][data-member-id='"+iMemberId+"']").length,
                        sTemplate   = uiThis.attr("data-clone-template")
                        uiMember    = $("[data-model='beneficiary'][data-member-id='"+iMemberId+"']");
                        
                        if (uiMember.length <= 4) {
                            $("[data-template='"+sTemplate+"']").clone()
                                .html(function(i, html) {
                                    return html.replace(/\{counter}/g, iCount);
                                })
                                .removeAttr("data-template")
                                .insertAfter(uiParent)
                                .find("[data-chosen-select]").addClass('chosen-select').chosen({width:"100%", disable_search_threshold: 10, search_contains: true});
                        } else {
                            alert("Only 4 beneficiaries per member only");
                        }

                } else if (uiTarget.closest("[data-button='remove_beneficiary']").length > 0) {
                    e.preventDefault();

                    var uiThis      = uiTarget.closest("[data-button='remove_beneficiary']"),
                        uiParent    = uiThis.parents("[data-model='beneficiary']");

                        uiParent.remove();
                }
            });
        }
    };

    QuoteRequest.initialize();

});